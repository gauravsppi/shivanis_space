import { Injectable, Input } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { environment } from '../../environments/environment';
import { candidateLogin_model } from '../models/candidateLogin_model';
import { admin_model } from '../models/admin_model';
import { employer_model } from '../models/employer_model';
@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private CandidateSignup = environment.apiBaseUrl + "Candidate/candidateSignup";
  private CandidateLogin = environment.apiBaseUrl + "Candidate/candidateLogin";
  private GetStatus = environment.apiBaseUrl + "Candidate/statusGet";
  //private LetsGo = environment.apiBaseUrl + "Candidate/tabLetsgo";
  private YouTab = environment.apiBaseUrl + "Candidate/youTab";
  //private TabDegree = environment.apiBaseUrl + "Candidate/degreetab";
  private QualificationTab = environment.apiBaseUrl + "Candidate/qualification";
 // private TabCurrent = environment.apiBaseUrl + "Candidate/currentTab";
 private TabIdealJob = environment.apiBaseUrl + "Candidate/jobIdealTab";
  private TabInterest = environment.apiBaseUrl + "Candidate/interestsTab";
  private Tabupload = environment.apiBaseUrl + "Candidate/uploadTab";
  private uploadCVFilesUrl = environment.apiBaseUrl + "Candidate/uploadCVFiles";
  private uploadSkillFilesUrl = environment.apiBaseUrl + "Candidate/uploadSkillFiles";
  private uploadReferenceFilesUrl = environment.apiBaseUrl + "Candidate/uploadReferenceFiles";
  private uploadVideoFilesUrl = environment.apiBaseUrl + "Candidate/uploadVideoFiles";
  private getCandidateWithIDUrl = environment.apiBaseUrl + "Candidate/getByIdCandidate";
  private CandidateInterestupdate = environment.apiBaseUrl + "Candidate/updateCandidate";
  private CandidateIdealjobupdate = environment.apiBaseUrl + "Candidate/updateCandidateIdealJob"
  private CandidateFirstTabupdate = environment.apiBaseUrl + "Candidate/updateCandidateLetsgo"
  private CandidateQualificationupdate = environment.apiBaseUrl + "Candidate/updateCandidateQualification"
  private accountStatusUpdate = environment.apiBaseUrl + "Candidate/updateAccountStatus";

  // --------- For Admin Side API's ---------------
  private LoginAdmin = environment.apiBaseUrl + "Admin/signinAdmin";
  private Candidatesdetail = environment.apiBaseUrl + "Admin/getCandidates";
  private UpdateStatus = environment.apiBaseUrl + "Candidate/statusUpdate";
  private TrashStatus = environment.apiBaseUrl + "Candidate/candidateTrash";
  private RejectStatus = environment.apiBaseUrl + "Candidate/candidateReject";
  private Employersdetail = environment.apiBaseUrl + "Admin/getEmployers";
  private PackageDetails = environment.apiBaseUrl + "Admin/packagesAdd";
  private Subscriptionplansget = environment.apiBaseUrl + "Admin/getsubscriptionPlans";
  private PackageById = environment.apiBaseUrl + "Admin/getByIdPackage";
  private Packageupdate = environment.apiBaseUrl + "Admin/updatePackage";
  private candidateDetailsWithIDUrl = environment.apiBaseUrl + "Admin/candidateDetailsWithID";
  private getEmployerdetailsWithIDUrl = environment.apiBaseUrl + "Admin/getEmployerdetailsWithID";
  //private getEmployerPackagedetails = environment.apiBaseUrl + "Admin/getEmployerPackagedetails";
  private getEmployerPackagedetailsWithIDUrl = environment.apiBaseUrl + "Admin/getEmployerPackagedetailsWithID";
  private Packageactivate = environment.apiBaseUrl + "Admin/activatePackagebyid";
  private getEmployerJobPostWithIDUrl = environment.apiBaseUrl + "Admin/getEmployerJobPostWithID";
  private getMatchedCandidateWithUrl = environment.apiBaseUrl + "Admin/getMatchedCandidate";
  private mailsendForVerifies = environment.apiBaseUrl + "Admin/mailsendForVerify";
  private sendMail = environment.apiBaseUrl + "Admin/sendPlansMail";

  // -------- For Employer Side API's ---------------
  private Packageadd = environment.apiBaseUrl + "Employer/packageAdd";
  private CampaignDetails = environment.apiBaseUrl + "Employer/tabcampaigndetails";
  private SkillsNeed = environment.apiBaseUrl + "Employer/tabskillsneed";
  private CompanyRoleDesc = environment.apiBaseUrl + "Employer/tabcompanyroledesc";
  private CampaignQues = environment.apiBaseUrl + "Employer/tabcampaignques";
  private FinishTabupload = environment.apiBaseUrl + "Candidate/tabfinishupload";
  private employerWithIDUrl = environment.apiBaseUrl + "Employer/getByIdEmployer";
  private getEmployerPostWithIDUrl = environment.apiBaseUrl + "Employer/getEmployerJobPostWithID";
  private updateCompanyDetails = environment.apiBaseUrl + "Employer/companyDetailsUpdate";

  
  constructor(private baseHttpService: BaseHttpService) { }

  SignupCandidate(candidatelogin: candidateLogin_model): Promise<any> {
    return this.baseHttpService.Post(this.CandidateSignup, candidatelogin)
      .then(function (response) {
        return response.json();
      });
  }
  LoginCandidate(candidatelogin: candidateLogin_model): Promise<any> {
    return this.baseHttpService.Post(this.CandidateLogin, candidatelogin)
      .then(function (response) {
        return response.json();
      });
  }
  StatusGet(): Promise<any> {
    return this.baseHttpService.Get(this.GetStatus)
      .then(function (response) {
        return response.json();
      });
  }
  FirstTab(candidatelogin: candidateLogin_model): Promise<any> {
    return this.baseHttpService.Post(this.YouTab, candidatelogin)
      .then(function (response) {
        return response.json();
      });
  }
  SecondTab(candidatelogin: candidateLogin_model): Promise<any> {
    return this.baseHttpService.Post(this.QualificationTab, candidatelogin)
      .then(function (response) {
        return response.json();
      });
  }
  ThirdTab(candidatelogin: candidateLogin_model): Promise<any> {
    return this.baseHttpService.Post(this.TabIdealJob, candidatelogin)
      .then(function (response) {
        return response.json();
      });
  }
  FourthTab(candidatelogin: candidateLogin_model): Promise<any> {
    return this.baseHttpService.Post(this.TabInterest, candidatelogin)
      .then(function (response) {
        return response.json();
      });
  }
  FifthTab(candidatelogin: candidateLogin_model): Promise<any> {
    return this.baseHttpService.Post(this.Tabupload, candidatelogin)
      .then(function (response) {
        return response.json();
      });
  }
  uploadCVFiles(candidatedataforcv): Promise<any> {
    return this.baseHttpService.Post(this.uploadCVFilesUrl, {candidatedataforcv})
      .then(function (response) {
        return response.json();
      });
  }
  uploadSkillFiles(candidatedataforskill): Promise<any> {
    return this.baseHttpService.Post(this.uploadSkillFilesUrl, {candidatedataforskill})
      .then(function (response) {
        return response.json();
      });
  }
  uploadReferenceFiles(candidatedataforreference): Promise<any> {
    return this.baseHttpService.Post(this.uploadReferenceFilesUrl, {candidatedataforreference})
      .then(function (response) {
        return response.json();
      });
  }

  uploadVideoFiles(candidatedataforvideo): Promise<any> {
    return this.baseHttpService.Post(this.uploadVideoFilesUrl, {candidatedataforvideo})
      .then(function (response) {
        return response.json();
      });
  }

  getCandidateWithID(candidatelogin: candidateLogin_model): Promise<any> {
    return this.baseHttpService.Post(this.getCandidateWithIDUrl, candidatelogin)
      .then(function (response) {
        return response.json();
      });
  }

  updateCandidateInterest(candidatelogin: candidateLogin_model): Promise<any> {
    return this.baseHttpService.Post(this.CandidateInterestupdate, candidatelogin)
      .then(function (response) {
        return response.json();
      });
  }

  updateCandidateIdealjob(candidatelogin: candidateLogin_model): Promise<any> {
    return this.baseHttpService.Post(this.CandidateIdealjobupdate, candidatelogin)
      .then(function (response) {
        return response.json();
      });
  }

  updateCandidateFirstTab(candidatelogin: candidateLogin_model): Promise<any> {
    return this.baseHttpService.Post(this.CandidateFirstTabupdate, candidatelogin)
      .then(function (response) {
        return response.json();
      });
  }

  updateQualificationTab(candidatelogin: candidateLogin_model): Promise<any> {
    return this.baseHttpService.Post(this.CandidateQualificationupdate, candidatelogin)
      .then(function (response) {
        return response.json();
      });
  }
  updatedAccountStatus(candidatelogin: candidateLogin_model): Promise<any> {
    return this.baseHttpService.Post(this.accountStatusUpdate, candidatelogin)
      .then(function (response) {
        return response.json();
      });
  }

  // ---------- ADMIN SIDE API's -----------
  AdminLogin(adminlogin: admin_model): Promise<any> {
    return this.baseHttpService.Post(this.LoginAdmin, adminlogin)
      .then(function (response) {
        return response.json();
      });
  }
  Candidatesget(adminlogin: admin_model): Promise<any> {
    return this.baseHttpService.Post(this.Candidatesdetail,adminlogin)
      .then(function (response) {
        return response.json();
      });
    }
    CandidateStatus(adminlogin: admin_model): Promise<any> {
      return this.baseHttpService.Post(this.UpdateStatus, adminlogin)
        .then(function (response) {
          return response.json();
        });
    }
    Statustrash(adminlogin: admin_model): Promise<any> {
      return this.baseHttpService.Post(this.TrashStatus, adminlogin)
        .then(function (response) {
          return response.json();
        });
    }
    Statusreject(adminlogin: admin_model): Promise<any> {
      return this.baseHttpService.Post(this.RejectStatus, adminlogin)
        .then(function (response) {
          return response.json();
        });
    }
    Employerget(adminlogin: admin_model): Promise<any> {
      return this.baseHttpService.Post(this.Employersdetail, adminlogin)
        .then(function (response) {
          return response.json();
        });
      }
      Plansdetailadd(adminlogin: admin_model): Promise<any> {
        return this.baseHttpService.Post(this.PackageDetails, adminlogin)
          .then(function (response) {
            return response.json();
          });
      }
      getPlans(): Promise<any> {
        return this.baseHttpService.Get(this.Subscriptionplansget)
          .then(function (response) {
            return response.json();
          });
        }

        /*getEmployerPackageDetails(): Promise<any> {
          return this.baseHttpService.Get(this.getEmployerPackagedetails)
            .then(function (response) {
              return response.json();
            });
          }*/
        singlePackage(adminlogin: admin_model): Promise<any> {
          return this.baseHttpService.Post(this.PackageById, adminlogin)
            .then(function (response) {
              return response.json();
            });
        }
        updatePlan(adminlogin: admin_model): Promise<any> {
          return this.baseHttpService.Post(this.Packageupdate, adminlogin)
            .then(function (response) {
              return response.json();
            });
        }

        activatePlan(adminlogin: admin_model): Promise<any> {
          return this.baseHttpService.Post(this.Packageactivate, adminlogin)
            .then(function (response) {
              return response.json();
            });
        }

        candidateDetailsWithID(adminlogin: admin_model): Promise<any> {
          return this.baseHttpService.Post(this.candidateDetailsWithIDUrl, adminlogin)
            .then(function (response) {
              return response.json();
            });
        }
        getEmployerdetailsWithID(adminlogin: admin_model): Promise<any> {
          return this.baseHttpService.Post(this.getEmployerdetailsWithIDUrl, adminlogin)
            .then(function (response) {
              return response.json();
            });
        }
    
        getEmployerPackagedetailsWithID(adminlogin: admin_model): Promise<any> {
          return this.baseHttpService.Post(this.getEmployerPackagedetailsWithIDUrl, adminlogin)
            .then(function (response) {
              return response.json();
            });
        }  

        getEmployerJobPostWithID(adminlogin: admin_model): Promise<any> {
          return this.baseHttpService.Post(this.getEmployerJobPostWithIDUrl, adminlogin)
            .then(function (response) {
              return response.json();
            });
        }

        getCandidateMatched(adminlogin: employer_model): Promise<any> {
          return this.baseHttpService.Post(this.getMatchedCandidateWithUrl, adminlogin)
            .then(function (response) {
              return response.json();
            });
        }
        mailsendForVerify(adminlogin: admin_model): Promise<any> {
          return this.baseHttpService.Post(this.mailsendForVerifies, adminlogin)
            .then(function (response) {
              return response.json();
            });
        }
        
    // -------- Employer Side API's --------------
    getEmployerPostWithID(employerlogin: employer_model): Promise<any> {
      return this.baseHttpService.Post(this.getEmployerPostWithIDUrl, employerlogin)
        .then(function (response) {
          return response.json();
        });
    }
    PlanAdd(employerModel: employer_model): Promise<any> {
      return this.baseHttpService.Post(this.Packageadd, employerModel)
        .then(function (response) {
          return response.json();
        });
    }
   
    EmployerFirstTab(employerModel: employer_model): Promise<any> {
      return this.baseHttpService.Post(this.CampaignDetails, employerModel)
        .then(function (response) {
          return response.json();
        });
    }

    EmployerSecondTab(employerModel: employer_model): Promise<any> {
      return this.baseHttpService.Post(this.SkillsNeed, employerModel)
        .then(function (response) {
          return response.json();
        });
    }
    EmployerThirdTab(employerModel: employer_model): Promise<any> {
      return this.baseHttpService.Post(this.CompanyRoleDesc, employerModel)
        .then(function (response) {
          return response.json();
        });
    }
    EmployerFourthTab(employerModel: employer_model): Promise<any> {
      return this.baseHttpService.Post(this.CampaignQues, employerModel)
        .then(function (response) {
          return response.json();
        });
    }
    EmployerFifthTab(employerModel: employer_model): Promise<any> {
      return this.baseHttpService.Post(this.FinishTabupload, employerModel)
        .then(function (response) {
          return response.json();
        });
      }    
      getEmployerWithID(employerModel: employer_model): Promise<any> {
        return this.baseHttpService.Post(this.employerWithIDUrl, employerModel)
          .then(function (response) {
            return response.json();
          });
      }
      planSendMail(adminlogin: admin_model): Promise<any> {
        return this.baseHttpService.Post(this.sendMail, adminlogin)
          .then(function (response) {
            return response.json();
          });
      }
      companyDetailsEdit(formData): Promise<any> {
        return this.baseHttpService.FormPost(this.updateCompanyDetails, formData)
          .then(function (response) {
            return response.json();
          });
      }
}
