import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import  * as $ from 'jquery';

import { AccountService } from '../../services/account.service';
import { Headers, Http } from '@angular/http';
import { Login_model } from '../../models/login_model';

@Component({
  selector: 'app-list-content',
  templateUrl: './list-content.component.html',
  styleUrls: ['./list-content.component.scss']
})
export class ListContentComponent implements OnInit {

  public loginModel: Login_model;
  public addValidationError: Login_model;

  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http) { }
  sucMsg: boolean = false;
  errMsg: boolean = false;
  successMessage: string;   
  errorMessage: string; 
  content: [];  
  noRecordsShow: boolean = false;
  loaderShow: boolean = false;
  total : number= 0;
  totalPages : number= 0;
  currentPage:number=1;
  page : number =1;
  key: string = 'id'; //set default
  reverse: boolean = false;

  ngOnInit(): void {
    this.loginModel = <Login_model>{
      admin_id: '',
      school_id: '',
      searchKey: '',
      sortKey: 'title',
      sortBy: 'desc'    
    }
    this.getAllContent();
  }
  getAllContent(){
    this.loaderShow = true;
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    this.loginModel.page = this.currentPage;
    this.accountService.getAllContent(this.loginModel).then((response) => {
      console.log('content=> ', response);
      if (response.code == 200 && response.status == 'success') {
        this.content = response.result; 
        this.total = response.result.length;
        this.totalPages=Math.ceil(response.result.length/10);
        this.noRecordsShow = false;
      }
      if (response.code == 200 && response.status == 'notFound') {
        this.noRecordsShow = true;
        this.content = []; 
        this.total = 0;
        this.totalPages= 0;
      }
      this.loaderShow = false;
    })
  }
  pageChange(currentPage){
    this.currentPage=currentPage;
    if(this.loginModel.searchKey==""){
      this.getAllContent()
    }else{
      //this.searchFunction(this.currentPage);
    } 
  }

  counter(i: number) {
    return new Array(i);
  }

  sortFunction(sortKey){
    this.key = sortKey;
    this.loginModel.sortKey = sortKey;
    if(this.reverse === true){
      this.loginModel.sortBy = 'asc';
    }else{
      this.loginModel.sortBy = 'desc';
    }
    this.reverse = !this.reverse;   
    if(this.loginModel.searchKey== ""){
      this.currentPage=1;
      this.getAllContent();
    }else{
      //this.searchFunction(this.currentPage);
    }
  }
  searchFunction(currentPage){
    if(this.loginModel.searchKey== ""){
      this.currentPage=1;
      this.getAllContent();
    }else{
      this.loaderShow = true;
      this.currentPage=currentPage;
      this.loginModel.page = this.currentPage;
      this.loginModel.school_id = this.route.snapshot.params.id;
      this.accountService.getAllContent(this.loginModel).then((response) => {
        if (response.code == 200 && response.status == 'success') {
          this.total = response.count;
          this.totalPages=Math.ceil(response.count/10);
          this.content = response.result;
          this.noRecordsShow = false;                    
        }else{
          this.noRecordsShow = true;
          this.total = 0;
          this.totalPages=0;
          this.content = [];  
        }
        this.loaderShow = false;
      })
    }
  }
}
