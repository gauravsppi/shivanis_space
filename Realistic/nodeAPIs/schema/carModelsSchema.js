const mongoose = require('mongoose');
var carModelsSchema = new mongoose.Schema({
    model_name: {
        type: String,       
    },
    company_id: {
        type: String,       
    },
    status: {
        type: String
    },
    created_at: {
        type: Date,
        //default: Date.now
    },
});

carModelsSchema.virtual('company_details',{
    ref: 'car_company',
    localField: 'company_id',
    foreignField: '_id',
    justOne: true
});

carModelsSchema.set('toObject', { virtuals: true });
carModelsSchema.set('toJSON', { virtuals: true });

module.exports = carModelsSchema;
