import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListObservationSubCategoryComponent } from './list-observation-sub-category.component';

describe('ListObservationSubCategoryComponent', () => {
  let component: ListObservationSubCategoryComponent;
  let fixture: ComponentFixture<ListObservationSubCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListObservationSubCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListObservationSubCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
