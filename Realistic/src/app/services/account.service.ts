import { Injectable, Input } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { environment } from '../../environments/environment';
import { Login_model } from '../models/login_model';
import { HttpParams } from "@angular/common/http";



@Injectable({
  providedIn: 'root'
})
export class AccountService {

  //Admin Details
  private adminLoginUrl = environment.apiBaseUrl + "adminlogin";
  private adminProfileUrl = environment.apiBaseUrl + "adminProfile";
  private adminProfileUpdate = environment.apiBaseUrl + "adminProfileUpdate";
  private adminPswdUpdate = environment.apiBaseUrl + "adminChangePassword";

  //User Module
  private usersUrl = environment.apiBaseUrl + "getAllUsers";
  private usersDetailsUrl = environment.apiBaseUrl + "getUserInfo";
  private get_user_carsUrl = environment.apiBaseUrl + "getUserCar";

  //Car Company
  private add_company_Url = environment.apiBaseUrl + "addCarCompanyName";
  private getCompanyNameUrl = environment.apiBaseUrl + "getCarCompanyName";
  private getCompanyName = environment.apiBaseUrl + "viewCarCompanyName";
  private deleteCarCompanyName = environment.apiBaseUrl + "deleteCarCompanyName";
  private updateCarCompanyName = environment.apiBaseUrl + "updateCarCompanyName";

  //Car Model
  private add_model_Url = environment.apiBaseUrl + "addCarModel";
  private deleteCarModelName = environment.apiBaseUrl + "deleteCarModels";
  private updateModelName = environment.apiBaseUrl + "updateCarModel";
  private getModelName = environment.apiBaseUrl + "viewCarModels";
  private getCarModelUrl = environment.apiBaseUrl + "getCarModel";

  //Content Manage
  private getContentUrl = environment.apiBaseUrl + "getContent";
  private UpdateContentUrl = environment.apiBaseUrl + "updateContent";

  //Enquiry Form
  private getContactDataUrl = environment.apiBaseUrl + "getContactData";
  

  //User Counts Dashboard
  private user_countUrl = environment.apiBaseUrl + "countAllUsers";

  //Get parts
  private getPartsUrl = environment.apiBaseUrl + "getParts";
  private addPartsUrl = environment.apiBaseUrl + "addParts";
  private deletePartsUrl = environment.apiBaseUrl + "deleteParts";
  private viewPartsUrl = environment.apiBaseUrl + "viewParts";
  private updatePartsUrl = environment.apiBaseUrl + "updateParts";

  //Get company names
  private getCarCompanyNamesUrl = environment.apiBaseUrl + "getCarCompanyNames";
  private getCarModelsUrl = environment.apiBaseUrl + "getCarModels";

  //Save Car Models
  private save_multiCarModel = environment.apiBaseUrl + "save_multiCarModel";
  private getmultiCarModel = environment.apiBaseUrl + "getmultiCarModel";
  private deleteMultiPart = environment.apiBaseUrl + "deleteMultiParts";
  private DeleteAllmultiCarModelsURL = environment.apiBaseUrl + "deleteAllmultiCarModels";
  
//Update Multi Car Model
  private get_edit_multiCarModel = environment.apiBaseUrl + "get_edit_multiCarModel";
  private save_multiCarModelwithID = environment.apiBaseUrl + "save_multiCarModelwithID";
  

  constructor(private baseHttpService: BaseHttpService) { }



  //Admin Details Start
  adminLogin(loginModel: Login_model): Promise<any> {
    return this.baseHttpService.Post(this.adminLoginUrl, loginModel)
      .then(function (response) {
        return response.json();
      });
  }
  get_admin_details(): Promise<any> {
    let admin_id = localStorage.getItem('admin_id');
    return this.baseHttpService.Post(this.adminProfileUrl, { admin_id })
      .then(function (response) {
        return response.json();
      });
  }

  adminProfileAllUpdate(loginModel: Login_model): Promise<any> {
    let admin_id = localStorage.getItem('admin_id');
    return this.baseHttpService.Post(this.adminProfileUpdate, loginModel)
      .then(function (response) {
        return response.json();
      });
  }
  update_admin_password(loginModel: Login_model): Promise<any> {
    return this.baseHttpService.Post(this.adminPswdUpdate, loginModel)
      .then(function (response) {
        return response.json();
      });
  }
  // Admin Details Close

  // Dashboard Counts
  user_counts(): Promise<any> {
    let admin_id = localStorage.getItem('admin_id');
    return this.baseHttpService.Post(this.user_countUrl, { admin_id })
      .then(function (response) {
        return response.json();
      });
  }
  // Dashboard Count close




  get_users(loginModel: Login_model): Promise<any> {
    return this.baseHttpService.Post(this.usersUrl, loginModel)
      .then(function (response) {
        return response.json();
      });
  }


  get_user_detail(user_id, admin_id): Promise<any> {
    return this.baseHttpService.Post(this.usersDetailsUrl, { user_id, admin_id })
      .then(function (response) {
        return response.json();
      });
  }
  get_user_car(user_id): Promise<any> {
    return this.baseHttpService.Post(this.get_user_carsUrl, { user_id })
      .then(function (response) {
        return response.json();
      });
  }


  //Car Modules Calls
  add_company_name(loginModel: Login_model): Promise<any> {
    return this.baseHttpService.Post(this.add_company_Url, loginModel)
      .then(function (response) {
        return response.json();
      });
  }
  add_model_name(loginModel: Login_model): Promise<any> {
    return this.baseHttpService.Post(this.add_model_Url, loginModel)
      .then(function (response) {
        return response.json();
      });
  }


  getCompany(loginModel: Login_model): Promise<any> {
    return this.baseHttpService.Post(this.getCompanyNameUrl, loginModel)
      .then(function (response) {
        return response.json();
      });
  }

  delete_model(model_id): Promise<any> {
    return this.baseHttpService.Post(this.deleteCarModelName, { model_id })
      .then(function (response) {
        return response.json();
      });
  }

  delete_company(company_id): Promise<any> {
    return this.baseHttpService.Post(this.deleteCarCompanyName, { company_id })
      .then(function (response) {
        return response.json();
      });
  }

  updateCarCompanyNames(loginModel: Login_model): Promise<any> {
    return this.baseHttpService.Post(this.updateCarCompanyName, loginModel)
      .then(function (response) {
        return response.json();
      });
  }
  updateCarModelNames(loginModel: Login_model): Promise<any> {
    return this.baseHttpService.Post(this.updateModelName, loginModel)
      .then(function (response) {
        return response.json();
      });
  }

  getCompanyNames(loginModel: Login_model): Promise<any> {
    return this.baseHttpService.Post(this.getCompanyName, loginModel)
      .then(function (response) {
        return response.json();
      });
  }
  getModelNames(loginModel: Login_model): Promise<any> {
    return this.baseHttpService.Post(this.getModelName, loginModel)
      .then(function (response) {
        return response.json();
      });
  }

  getCarModel(loginModel: Login_model): Promise<any> {
    return this.baseHttpService.Post(this.getCarModelUrl, loginModel )
      .then(function (response) {
        return response.json();
      });
  }
  getContentdetails(loginModel: Login_model): Promise<any> {
    return this.baseHttpService.Post(this.getContentUrl, loginModel )
      .then(function (response) {
        return response.json();
      });
  }
  update_contents(loginModel: Login_model): Promise<any> {
    return this.baseHttpService.Post(this.UpdateContentUrl, loginModel )
      .then(function (response) {
        return response.json();
      });
  }
  getContactDatas(loginModel: Login_model): Promise<any> {
    return this.baseHttpService.Post(this.getContactDataUrl, loginModel )
      .then(function (response) {
        return response.json();
      });
  }
  
  getParts(loginModel: Login_model): Promise<any> {
    return this.baseHttpService.Post(this.getPartsUrl, loginModel )
      .then(function (response) {
        return response.json();
      });
  }
    
  addParts(formdata): Promise<any> {
    return this.baseHttpService.FormPost(this.addPartsUrl, formdata )
      .then(function (response) {
        return response.json();
      });
  } 
  deleteParts(loginModel: Login_model): Promise<any> {
    return this.baseHttpService.Post(this.deletePartsUrl, loginModel )
      .then(function (response) {
        return response.json();
      });
  } 
viewParts(loginModel: Login_model): Promise<any> {
    return this.baseHttpService.Post(this.viewPartsUrl, loginModel )
      .then(function (response) {
        return response.json();
      });
  } 
  updateParts(formdata): Promise<any> {
    return this.baseHttpService.FormPost(this.updatePartsUrl, formdata )
      .then(function (response) {
        return response.json();
      });
  } 
  getCarCompanyNames(loginModel: Login_model): Promise<any> {
    return this.baseHttpService.Post(this.getCarCompanyNamesUrl, loginModel )
      .then(function (response) {
        return response.json();
      });
  } 

  getModels(loginModel: Login_model): Promise<any> {
    return this.baseHttpService.Post(this.getCarModelsUrl, loginModel )
      .then(function (response) {
        return response.json();
      });
  } 
  multiCarModels(loginModel: Login_model): Promise<any> {
    return this.baseHttpService.Post(this.save_multiCarModel, loginModel )
      .then(function (response) {
        return response.json();
      });
  } 
  getmultiCarModels(): Promise<any> {
    let admin_id = localStorage.getItem('admin_id');
    return this.baseHttpService.Post(this.getmultiCarModel, { admin_id })
      .then(function (response) {
        return response.json();
      });
  }
  deleteMultiParts(loginModel: Login_model): Promise<any> {
    return this.baseHttpService.Post(this.deleteMultiPart, loginModel )
      .then(function (response) {
        return response.json();
      });
  } 
  deleteAllmultiCarModels(): Promise<any> {
    let admin_id = localStorage.getItem('admin_id');
    return this.baseHttpService.Post(this.DeleteAllmultiCarModelsURL, { admin_id })
      .then(function (response) {
        return response.json();
      });
  }

  get_edit_multiCarModels(loginModel: Login_model): Promise<any> {
    return this.baseHttpService.Post(this.get_edit_multiCarModel, loginModel )
      .then(function (response) {
        return response.json();
      });
  } 

  edit_multiCarModels(loginModel: Login_model): Promise<any> {
    return this.baseHttpService.Post(this.save_multiCarModelwithID, loginModel )
      .then(function (response) {
        return response.json();
      });
  } 
  
  

}
