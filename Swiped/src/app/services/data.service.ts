import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
  })
export class DataService {

  private messageSource = new BehaviorSubject(false);
  currentMessage = this.messageSource.asObservable();
  private headerSource = new BehaviorSubject(false);
  headerMessage = this.headerSource.asObservable();
  constructor() { }

  changeMessage(message: boolean) {
    this.messageSource.next(message);
  }
  changeStatus(messages: boolean) {
    this.headerSource.next(messages);
  }

}