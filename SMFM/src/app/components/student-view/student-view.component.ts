import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Headers, Http } from '@angular/http';
import { DatePipe } from '@angular/common';

import { environment } from '../../../environments/environment';
import { AccountService } from '../../services/account.service';
import { student_model } from '../../models/student_model';
import { school_model } from '../../models/school_model';
import { RoutingService } from '../../services/routing.service';


@Component({
  selector: 'app-student-view',
  templateUrl: './student-view.component.html',
  styleUrls: ['./student-view.component.scss']
})
export class StudentViewComponent implements OnInit {

  public studentModel: student_model;
  public schoolModel: school_model;

  public classes = [];
  public IEPs = [];
  public observations = [];
  public observationNotes = [];
  public observationStaff = [];
  public feeling = [];
  public goals = [];
  public academic = [];
  public attendence = [];
  public members = [];
  public infos = [];

  classes_count = 0;
  classActive: string = 'active';
  noRecords: boolean = true;
  noRecordsClasses: boolean = false;
  IEPShow: boolean = false;
  IEPShowLoader: boolean = false;
  observationShow: boolean = false;
  observationShowLoader: boolean = false;
  feelingShow: boolean = false;
  feelingShowLoader: boolean = false;
  academicShow: boolean = false;
  academicShowLoader: boolean = false;
  attendenceShow: boolean = false;
  attendenceShowLoader: boolean = false;
  goalsShow: boolean = false;
  goalsShowLoader: boolean = false;
  observationNotesShowLoader: boolean = false;
  NoObservationNotes: boolean = false;
  infoLoader: boolean = false;
  noInfoShow: boolean = false;

  student_id = this.route.snapshot.params.id;

  total : number= 0;
  totalPages : number= 0;
  currentPage:number=1;
  page : number =1;

  sucMsg: boolean = false;
  errMsg: boolean = false;
  successMessage: string;   
  errorMessage: string; 
  loaderShow: boolean = false;

  classValidationError: boolean = false;
  monthValidationError: boolean = false;
  menushow: boolean = false;

  previousUrl: string = '';
  currentUrl: string = '';
  arr = [];
  breadcrumb_student : boolean = false;
  breadcrumb_class : boolean = false;
  class_id : string = '';

  constructor(public datepipe: DatePipe, private router: Router, private accountService: AccountService, private route: ActivatedRoute, private http: Http, private routingService : RoutingService) {  }
 
 
  admin_id = localStorage.getItem('admin_id');

  menuCheck(admin_id){
    if(admin_id == 1){
      this.menushow =  true;
    }
  }

  ngOnInit(): void {
    this.studentModel = <student_model>{      
      student_id : '',
      school_id : '',
      name:'',
      email:'',
      phone:'',
      image:'',
      class_id:'',
      month:'',
      student_sourcedId:'',
      user_type:'',
      user_id:'',
    }

    this.schoolModel = <school_model>{   
      admin_id: '',
      school_id: '',
      searchKey: '',
      sortKey: 'name',
      sortBy: 'asc'  
    }

    this.previousUrl = this.routingService.getPreviousUrl(); //get previous url
    this.currentUrl = this.routingService.getCurrentUrl(); //get current url
    
    console.log('get previuo URL :: ', this.previousUrl);
    console.log('get current URL :: ', this.currentUrl);
    if(this.previousUrl != undefined){ 
      this.arr = this.previousUrl.split('/');
      this.previousUrl = this.arr[1];
      this.class_id = this.arr[2];
      if(this.previousUrl == 'class-view'){
        this.breadcrumb_student = false;
        this.breadcrumb_class = true;
        
      }else{          
        this.breadcrumb_student = true;
      }   
    }else{
      this.breadcrumb_student = true;
    }  
    
    console.log('breadcrumb student ::', this.breadcrumb_student)
    this.menuCheck(this.admin_id);
    this.getStudentInfo(this.student_id);
    this.getStudentParentInfo(this.student_id);
    this.getIEPByStudentID(this.student_id); 
    this.getObservationsByStudentID(this.student_id);
    this.getStudentFeeling(this.student_id);
    this.getAcademicBehaviourByStudentID(this.student_id);
    if(!this.menushow){ 
      this.getStudentAdditionalInfo(this.student_id);
    }
    
    this.getStaff();
  }

  assignStaffMember(staff_id) {
    this.schoolModel.admin_id = localStorage.getItem('admin_id');
    this.schoolModel.school_id = localStorage.getItem('school_id');
    this.schoolModel.staff_id = staff_id
    this.schoolModel.user_type = 'admin'
    this.schoolModel.student_id = this.student_id
    this.accountService.assignStaffToStudent(this.schoolModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
       // this.sucMsg= true;  
       // this.successMessage = response.message;
        this.getStaff();
        setTimeout (() => { this.sucMsg = false; }, 2000); 
      }else{
        this.errMsg= false;  
        this.errorMessage = response.message; 
        setTimeout (() => { this.errMsg = false; }, 2000);     
      }
    })    
  }

  getStaff() {
    this.loaderShow = true;
    this.schoolModel.admin_id = localStorage.getItem('admin_id');
    this.schoolModel.school_id = localStorage.getItem('school_id');
    this.schoolModel.student_id = this.student_id;
    this.schoolModel.user_type = 'admin'
    this.accountService.getAssignStaffToStudent(this.schoolModel).then((response) => {
        if (response.code == 200 && response.status == 'success') {
          this.members = response.result;  
        }else{
          this.members = []; 
        }
        this.loaderShow = false;
    })
  }

  getStudentInfo(student_id){

    this.studentModel.student_sourcedId = student_id;
    this.accountService.getStudentInfo(this.studentModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
         this.studentModel.name = response.result[0].name;     
         if(response.result[0].image != null){
          this.studentModel.image = environment.teacherImagePath + response.result[0].image;                    
         }else{
          this.studentModel.image = environment.teacherImagePath + 'student.png';
         }  
         this.studentModel.email = response.result[0].email;                    
         this.studentModel.phone = (response.result[0].phone == '' || response.result[0].phone == undefined || response.result[0].phone == 'undefined')? '--' : response.result[0].phone ;                    
         this.studentModel.sex = response.result[0].sex;                    
         this.studentModel.identifier = response.result[0].identifier;                    
         this.studentModel.birthDate = response.result[0].birthDate;                    
         this.studentModel.cityOfBirth = response.result[0].cityOfBirth;                    
         this.studentModel.countryOfBirthCode = response.result[0].countryOfBirthCode;                    
         this.studentModel.stateOfBirthAbbreviation = response.result[0].stateOfBirthAbbreviation;                    
         this.studentModel.username = response.result[0].username;                    
         this.studentModel.student_sourcedId = response.result[0].sourcedId;
         this.studentModel.school_id = response.result[0].school_id;

         this.studentModel.hispanicOrLatinoEthnicity = response.result[0].hispanicOrLatinoEthnicity;
         this.studentModel.nativeHawaiianOrOtherPacificIslander = response.result[0].nativeHawaiianOrOtherPacificIslander;
         this.studentModel.americanIndianOrAlaskaNative = response.result[0].americanIndianOrAlaskaNative;
         this.studentModel.blackOrAfricanAmerican = response.result[0].blackOrAfricanAmerican;
         this.studentModel.asian = response.result[0].asian;
         this.studentModel.white = response.result[0].white;

         this.studentModel.home_phone = response.result[0].home_phone;
         this.studentModel.work_phone = response.result[0].work_phone;
         this.studentModel.sms = response.result[0].sms;
         this.studentModel.preferred_contact_method = response.result[0].preferred_contact_method;
         this.studentModel.preferred_language = response.result[0].preferred_language;
         this.studentModel.primaryGrade  = response.result[0].primaryGrade ;
         
         if(this.studentModel.student_sourcedId != ''){
          this.accountService.getClassesByStudentID(this.studentModel).then((response) => {
              if(response.code == 200 && response.status == 'success'){ 
                this.classes = response.classes.classes;
                this.classes_count = response.classes.classes.length;
                this.noRecords = false;
              }else{
                this.noRecordsClasses = true;
              }
              this.noRecords = false;
          })
        } else{
          this.noRecords = false;
          this.noRecordsClasses = true;
        }   
      }else{
          alert('Student details not found.');
          this.noRecords = false;
          this.noRecordsClasses = true;
      }
    })     
  }

  getObservationNotesByID(observation_id){
    this.observationNotesShowLoader = true;
    this.studentModel.observation_id = observation_id;
    this.studentModel.admin_id = localStorage.getItem('admin_id') ;  
    this.accountService.getObservationNotesByID(this.studentModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
        this.observationNotes = response.result;
        this.NoObservationNotes = false;
      }else{
        this.observationNotes = [];
        this.NoObservationNotes = true;
      }
      this.observationNotesShowLoader = false;
    })
  } 
  getStudentParentInfo(student_id){
    this.studentModel.student_id = student_id;
    this.studentModel.admin_id = localStorage.getItem('admin_id') ;  
    this.accountService.getStudentParent(this.studentModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
          this.studentModel.parentName = response.result[0].name;
      }else{
        this.studentModel.parentName = "--";
      }
    })
  }      
  getIEPByStudentID(student_id){
    this.IEPShowLoader = true;
    this.studentModel.student_id = student_id;
    this.studentModel.admin_id = localStorage.getItem('admin_id') ;  
    this.accountService.getIEPByStudentID(this.studentModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
          this.IEPs = response.result;
          this.IEPShow = false;
      }else{       
        this.IEPShow = true;
      }
      this.IEPShowLoader = false;
    })
  }   
  
  getObservationsByStudentID(student_id){
    this.observationShowLoader = true;
    this.studentModel.student_id = student_id;
    this.studentModel.admin_id = localStorage.getItem('admin_id') ;  
    this.accountService.getObservationsByStudentID(this.studentModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
          this.observations = response.result[0];
          this.observationStaff = response.result[1];
          if(this.observations.length ==0 && this.observationStaff.length == 0){
            this.observationShow = true;
          }else{ 
            this.observationShow = false;          
          }  
      }else{       
        this.observationShow = true;
      }
      this.observationShowLoader = false;
    })
  } 
   
  getStudentFeeling(student_id){
    this.feelingShowLoader = true;
    this.studentModel.student_id = student_id;
    this.studentModel.admin_id = localStorage.getItem('admin_id') ;  
    this.accountService.getStudentFeeling(this.studentModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
          this.feeling = response.result;   
          this.feelingShow = false;
      }else{       
        this.feelingShow = true;
      }
      this.feelingShowLoader = false;
    })
  } 
   
  getAcademicBehaviourByStudentID(student_id){
    this.academicShowLoader = true;
    this.studentModel.student_id = student_id;
    this.studentModel.admin_id = localStorage.getItem('admin_id') ;  
    this.accountService.getAcademicBehaviourByStudentID(this.studentModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
          this.academic = response.result;
          this.academicShow = true;
      }else{       
        this.academicShow = false;
      }
      this.academicShowLoader = false;
    })
  } 

  getStudentAdditionalInfo(student_id){
    this.infoLoader = true;
    this.studentModel.student_id = student_id;
    this.studentModel.school_id = localStorage.getItem('school_id');
    this.studentModel.user_id = student_id;
    this.studentModel.user_type = 'student';
    this.studentModel.admin_id = localStorage.getItem('admin_id'); 
    this.accountService.getStudentAdditionalInfo(this.studentModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
          this.infos = response.result;
          var new_arr= [];
          this.infos.forEach(element => {
              if(element.selected_type == 1){ 
                new_arr.push(element);
              } 
          });
          this.infos= [];
          this.infos= new_arr;
          this.noInfoShow = true;
      }else{ 
        this.infos = [];      
        this.noInfoShow = false;
      }
      this.infoLoader = false;
    })
  } 

  getStudentAttendenceByMonth(){
    var valid = true;
    this.classValidationError = false;
    this.monthValidationError = false;
    if(this.studentModel.class_id == ''){
      valid = false;
      this.classValidationError = true;
    }
    if(this.studentModel.month == ''){
      valid = false;
      this.monthValidationError = true;
    }
    if(valid){ 
      this.attendenceShowLoader = true;
      this.studentModel.student_id = this.student_id;
      this.studentModel.admin_id = localStorage.getItem('admin_id') ; 
      console.log('attendence params: ', this.studentModel);
      this.accountService.getStudentAttendenceByMonth(this.studentModel).then((response) => {
        if (response.code == 200 && response.status == 'success') {
            this.attendence = response.result;
            this.attendenceShow = false;
            this.classValidationError = false;
            this.monthValidationError = false;
        }else{     
          this.attendence = [];  
          this.attendenceShow = true;
        }
        this.attendenceShowLoader = false;
      })
    } 
  } 

  getStudentGoalsByStudentID(student_id){
    this.observationShowLoader = true;
    this.studentModel.student_id = student_id;
    this.studentModel.admin_id = localStorage.getItem('admin_id') ;  
    this.accountService.getStudentGoalsByStudentID(this.studentModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
          this.observations = response.result;
          this.observationShow = true;
      }else{       
        this.observationShow = false;
      }
      this.observationShowLoader = false;
    })
  } 
}
