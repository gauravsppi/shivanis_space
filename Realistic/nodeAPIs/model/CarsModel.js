var dateFormat = require('dateformat'); //for using date format
const current_datetime = dateFormat(new Date().toLocaleString('en-US'),"yyyy-mm-dd hh:MM:ss"); //get current date and time
const md5 = require('md5'); //for encrypt passwords

const mongoose = require('mongoose');
var carCompanySchema = require('../schema/carCompanySchema');
var carsCompanyDB = mongoose.model('car_company', carCompanySchema);

var carModelsSchema = require('../schema/carModelsSchema');
var carModelsDB = mongoose.model('car_models', carModelsSchema);

var userCarsSchema = require('../schema/userCarsSchema');
var userCarsDB = mongoose.model('user_cars', userCarsSchema);

var tryPartsSchema = require('../schema/tryPartsSchema');
var tryPartsDB = mongoose.model('try_parts', tryPartsSchema);

var tryPartsUseSchema = require('../schema/tryPartsUseSchema');
var tryPartsUseDB = mongoose.model('try_parts_use', tryPartsUseSchema);

var multiCarCompanyModelSchema = require('../schema/multiCarCompanyModelSchema');
var multiCarCompanyModelDB = mongoose.model('multiCarCompanyModel', multiCarCompanyModelSchema);

module.exports = {
    addCarCompanyName: function(con, data, callback) { // add car company name into db
        var CarsCompany = new carsCompanyDB();
        CarsCompany.name = data.company_name;
        CarsCompany.status =  '1';   
       // CarsCompany.created_at = current_datetime;    
        console.log('data name: ', current_datetime);
        CarsCompany.save(callback);
    },
    updateCarCompanyName:function(con, data, callback) { //Update car company name
        var myquery = {  _id: data.company_id  };
        var newvalues = { $set: 
            { 
                name: data.company_name, 
            } 
        };
        carsCompanyDB.updateOne(myquery, newvalues, callback)
    },
    deleteCarCompanyName:function(con, data, callback) { //Delete car company names
       // carsCompanyDB.findByIdAndRemove({'_id': data.company_id}, callback)
            var myquery = {  _id: data.company_id  };
        var newvalues = { $set: 
            { 
                status: '0', 
            } 
        };
        carsCompanyDB.updateOne(myquery, newvalues, callback)

    },
    viewCarCompanyName:function(con, data, callback) { //view car company names
        carsCompanyDB.find({'_id': data.company_id}, callback)
    },
    getCarCompanyNames:function(con, data, callback) { //Get car company names

        carsCompanyDB.find({'status': '1'}, callback)
    },    
    addCarModel: function(con, data, callback) { // add car company name into db
        var CarModel = new carModelsDB();
        CarModel.model_name = data.model_name;
        CarModel.company_id = data.company_id;
        CarModel.status =  '1';   
        CarModel.created_at = current_datetime;    
        CarModel.save(callback);
    },
    updateCarModel:function(con, data, callback) { //Update car company name
        var myquery = {  _id: data.model_id  };
        var newvalues = { $set: 
            { 
                model_name: data.model_name, 
                company_id: data.company_id, 
            } 
        };
        carModelsDB.updateOne(myquery, newvalues, callback)
    },
    deleteCarModels:function(con, data, callback) { //delete car models by company id
        //carModelsDB.findByIdAndRemove({ _id: data.model_id}, callback)
         var myquery = {  _id: data.model_id  };
        var newvalues = { $set: 
            { 
                status: '0', 
            } 
        };
        carModelsDB.updateOne(myquery, newvalues, callback)
    },
    getCarCompanyName:function(con, data, callback) { //Admin: Get car company names
        var start = (data.page-1)*10;
        var sortKey = data.sortKey;
        var sortBy = data.sortBy;
        if(sortBy == 'asc'){ sortBy = 1; }else{ sortBy = -1; }
        if(data.searchKey == ''){ 
            carsCompanyDB.find({ status:'1'}).sort({ [sortKey]: sortBy  }).skip(start).limit(10).exec(callback)
        }else{
            carsCompanyDB.find({ status:'1', $or: [{'name': { $regex : '.*' +data.searchKey, $options: 'i' } }] }).sort({ [sortKey]: sortBy }).skip(start).limit(10).exec(callback)
        }   
    }, 
    countCarCompanyName:function(con, data, callback) { //Admin: Get car company names
        if(data.searchKey == ''){ 
            carsCompanyDB.find({status:'1'}).exec(callback)
        }else{
            carsCompanyDB.find({ status:'1', $or: [{'name': { $regex : '.*' +data.searchKey, $options: 'i' } }] }).exec(callback)
        }   
    },  
    getCarModels:function(con, data, callback) { //Get car models by company id
        carModelsDB.find({company_id: data.company_id, 'status':'1'}).populate('company_details').exec(callback)
    },
    getCarModel:function(con, data, callback) { //Get car models by company id
        console.log(data);
        var start = (data.page-1)*10;
        var sortKey = data.sortKey;
        var sortBy = data.sortBy;
        if(sortBy == 'asc'){ sortBy = 1; }else{ sortBy = -1; }
        if(data.searchKey == ''){ 
            carModelsDB.find({ company_id: data.company_id, status:'1' }).populate('company_details').sort({ [sortKey]: sortBy  }).skip(start).limit(10).exec(callback)
        }else{
            carModelsDB.find({$and: [{ company_id: data.company_id,status:'1' },{'model_name': { $regex : '.*' +data.searchKey, $options: 'i' } }] }).sort({ [sortKey]: sortBy }).populate('company_details').skip(start).limit(10).exec(callback)
        }   
    },
    countCarModel:function(con, data, callback) { //Get car models by company id

        if(data.searchKey == ''){ 
            carModelsDB.find({ company_id: data.company_id }).populate('company_details').exec(callback)
        }else{
            carModelsDB.find({$and: [{ 'company_id' : data.company_id },{'model_name': { $regex : '.*' +data.searchKey, $options: 'i' } }] }).populate('company_details').exec(callback)
        }   
    },
    viewCarModels:function(con, data, callback) { //View car models by company id
        carModelsDB.find({_id: data.model_id}, callback)
    },
    addTryParts: function(con, data, callback) { // add try parts
        var TryParts = new tryPartsDB();
        TryParts.image_name = data.image;
        TryParts.created_at = current_datetime;    
        TryParts.save(callback);
    }, 
    getTryParts:function(con, data, callback) { //Get try parts
        tryPartsDB.find({ }, callback)
    }, 
    getmultiCarModels:function(con, data, callback) { //Get try parts
        multiCarCompanyModelDB.find({ status:'0' }, callback)
    }, 
    get_edit_multiCarModels:function(con, data, callback) { //Get try parts
        multiCarCompanyModelDB.find({ add_part_id:data.id }, callback)
    }, 
    saveUsedTryParts: function(con, data, callback) { // add try parts
        var TryPartsUse = new tryPartsUseDB();
        TryPartsUse.user_id = data.user_id;
        TryPartsUse.car_id = data.car_id;
        TryPartsUse.part_id = data.part_id;
        TryPartsUse.image = '';
        TryPartsUse.created_at = current_datetime;    
        TryPartsUse.save(callback);
    },  
    saveTryPartsOfImage: function(con, data, callback) { // add try parts with image
        var TryPartsUse = new tryPartsUseDB();
        TryPartsUse.user_id = data.user_id;
        TryPartsUse.car_id = data.car_id;
        TryPartsUse.part_id = data.part_id;
        TryPartsUse.image = data.image;
        TryPartsUse.created_at = current_datetime;    
        TryPartsUse.save(callback);
    },
    savemultiCarCompanyModel: function(con, data, callback) { // add try parts with image
        var multiCarCompanyModel = new multiCarCompanyModelDB();
        multiCarCompanyModel.company_name = data.companyNames;
        multiCarCompanyModel.company_id = data.company_id;
        multiCarCompanyModel.model_id = data.model_id;
        multiCarCompanyModel.model_name = data.models_name;
        multiCarCompanyModel.created_at = current_datetime;    
        multiCarCompanyModel.save(callback);
    },
    deleteMultiPart:function(con, data, callback) { 
    multiCarCompanyModelDB.findByIdAndRemove({ '_id': data.id }).exec(callback)  
    },
    deleteAllMultiPart:function(con, data, callback) { 
    multiCarCompanyModelDB.remove({ 'status': "0" }).exec(callback)  
    },
    getUserTryPartsImage:function(con, data, callback) { // Get user try parts car images
    tryPartsUseDB.find({ 'user_id': data.user_id, image : { $ne: '' } }).select('_id image user_id').exec(callback)   
    },
    deleteUserTryPartsImage:function(con, data, callback) { // Delete user try parts car images
    tryPartsUseDB.findByIdAndRemove({ '_id': data.partImg_id }).exec(callback)   
    },

    //Multipart Update Cases
    save_edit_multiCarCompanyModel: function(con, data, callback) { // add try parts with image
        var multiCarCompanyModel = new multiCarCompanyModelDB();
        multiCarCompanyModel.company_name = data.companyNames;
        multiCarCompanyModel.company_id = data.company_id;
        multiCarCompanyModel.add_part_id = data.id;
        multiCarCompanyModel.model_id = data.model_id;
        multiCarCompanyModel.model_name = data.models_name;
        multiCarCompanyModel.created_at = current_datetime;    
        multiCarCompanyModel.save(callback);
    },
}
