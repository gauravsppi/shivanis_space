import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Cookie } from 'ng2-cookies';

import { AccountService } from '../../services/account.service';
import { Headers, Http } from '@angular/http';
import { Login_model } from '../../models/login_model';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    public loginModel: Login_model;
    public addValidationError: Login_model;

    private headers = new Headers();

    constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http) { }

    sucMsg: boolean = false;
    errMsg: boolean = false;
    successMessage: string;   
    errorMessage: string;   

    ngOnInit(): void {
        this.loginModel = <Login_model>{
            email: '',
            password: '',
            remember: '',
        }

        this.addValidationError = <Login_model>{
            email: '',
            password: '',
            remember: '',
        }
        this.isLoggedIn();
       
    }

    isLoggedIn() {
        this.loginModel.admin_id = localStorage.getItem('admin_id');
        if(this.loginModel.admin_id == null){
            this.router.navigate(['/']);
        }else{ 
            this.router.navigate(['/dashboard']);
        }    
    } 

    onSubmit() {
       
        var valid = true;
        this.addValidationError.email = '';
        this.addValidationError.password = '';
        if (this.loginModel.email == "") {
            this.addValidationError.email = 'Please enter email';
            valid = false;
        }
        if (this.loginModel.password == "") {
            this.addValidationError.password = 'Please enter password';
            valid = false;
        }
        if (valid) {
            this.accountService.adminLogin(this.loginModel).then((response) => {
                if (response.code == 200 && response.status == 'success') {
                    if(this.loginModel.remember != ''){
                        Cookie.set('add', 'erwwerwer', 1); 
                        Cookie.set('adda', '123')                       
                    }
                    localStorage.setItem('admin_id', response.result.id);                 
                    localStorage.setItem('school_id', response.result.school_id);                 
                    localStorage.setItem('school_sourcedId', response.result.school_sourcedId);                 
                    localStorage.setItem('sourcedId', response.result.sourcedId);                 
                    localStorage.setItem('role', response.result.role);                 
                    this.sucMsg = true;
                    this.successMessage = response.message;
                    setTimeout (() => { this.sucMsg = false;  }, 3000); 
                    this.router.navigate(['/dashboard']);
                }else if(response.code == 400 && response.status == 'notMatch'){
                    this.errMsg = true;
                    this.errorMessage = response.message;
                    setTimeout (() => { this.errMsg = false;  }, 3000);
                } else {
                    this.errMsg = true;
                    this.errorMessage = response.message;
                    setTimeout (() => { this.errMsg = false;  }, 3000); 
                }
            })
        }
    }

}
