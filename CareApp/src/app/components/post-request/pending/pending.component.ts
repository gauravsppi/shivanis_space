import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "../../../shared/services/spinner.service";
import { PostRequestService } from '../../../shared/services/post-request/post-request.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-pending',
  templateUrl: './pending.component.html',
  styleUrls: ['./pending.component.scss']
})
export class PendingComponent implements OnInit {
  pendingArray: any = [];
  page = 1;
  count : number;
  pageSize = 10;
  postDetail: any;
  selectedIndex: any;
  constructor(
    private PostRequestService: PostRequestService,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.spinner.show();
    this.fetchRecords().toPromise().then((res) => {
      console.log('res++++++++++++',res);
      this.spinner.hide();
      if(res.status == 1){
        this.pendingArray = res.data
        this.count = res.numOfPendingRequest
      }else{
        Swal.fire({ icon: 'error', title: 'Oops...', text: res.message })
      }
    })
    .catch(err=> { 
      this.spinner.hide();
      console.log(err) 
    });
  }

  fetchRecords () {
    var Params = {
      page:  this.page, 
      pageSize: this.pageSize,
    }
    return this.PostRequestService.fetch_pending_requests(Params);
  }


  onTableDataChange(event){
    this.page = event;
    this.ngOnInit();
  }  

  pickCancel(post, index): void{
    console.log('post++++++', post)
    this.postDetail = post;
    this.selectedIndex = index
  }


  cancelPost(status): void {
    this.spinner.show();
    this.cancelPostRequest(status).toPromise().then((res) => {
      console.log('res++++++++++++',res);
      this.spinner.hide();
      if(res.status == 1){
        this.ngOnInit();
        Swal.fire( 'Canceled!', 'Post has been Canceled.', 'success');
        this.pendingArray.splice(this.selectedIndex , 1);

      }else{
        Swal.fire({ icon: 'error', title: 'Oops...', text: res.message })
      }
    })
    .catch(err=> { 
      this.spinner.hide();
      console.log(err) 
    });
  }

  cancelPostRequest (status) {
    var Params = {
      postId:  this.postDetail.postId, 
      receiverId:  this.postDetail.receiverId, 
      workerId:  this.postDetail.workerId, 
      status: status,
    }
    return this.PostRequestService.cancel_post_requests(Params);
  }
}
