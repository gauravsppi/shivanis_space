const mongoose = require('mongoose');
// Get the Schema constructor
var Schema = mongoose.Schema;
  
var categorySchema = new mongoose.Schema({
    name: {
        type: String,       
    },  
    image: {
        type: String,       
    },  
    status: {
        type: String,       
    },  
    created_at: {
        type : Date, 
        default: Date.now
    },    
});
module.exports = categorySchema;
