import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter, pairwise } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class RoutingService {

  private previousUrl1: [];
  private previousUrl: string;
  private currentUrl: string;

  constructor(private router: Router) {
    this.currentUrl = this.router.url;
    router.events
    .pipe(filter((evt: any) => evt instanceof NavigationEnd), pairwise())
    .subscribe((events: NavigationEnd[]) => {
        this.previousUrl = events[0].urlAfterRedirects;
        this.currentUrl = events[1].urlAfterRedirects;     
    });
  }

  public getPreviousUrl() {
    return this.previousUrl;
  }  
  
  public getCurrentUrl() {
    return this.currentUrl;
  }  
}
