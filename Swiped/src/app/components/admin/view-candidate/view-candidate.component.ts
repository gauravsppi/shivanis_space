import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from '../../../services/account.service';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import { admin_model } from '../../../models/admin_model';
import { NgxSpinnerService } from "ngx-spinner";




@Component({
  selector: 'app-view-candidate',
  templateUrl: './view-candidate.component.html',
  styleUrls: ['./view-candidate.component.scss']
})
export class ViewCandidateComponent implements OnInit {
  public adminModel: admin_model;
  public addValidationError: admin_model;
  public headers = new Headers();
  errMsg:boolean=false;
  message:string;
  candidatelist:any;
  getId:any;
  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService, private spinner:NgxSpinnerService) { }
  //constructor() { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.getId = params.get('id');
    });
    this.adminModel = <admin_model>{
      id: ''
    }
    this.addValidationError = <admin_model>{
      id: '',
      admin_password: ''
    }
    this.getCandidatedetails();
  }
  getCandidatedetails(){
    this.adminModel.id = this.getId;
    this.spinner.show();
    this.accountService.candidateDetailsWithID(this.adminModel).then((response) => {
      debugger;
      if (response.code == 200) {
        this.spinner.hide();
        this.candidatelist=response.result;
      } else {
        this.errMsg = true;
        this.message = "No Data Found!!!";
      }
    })
  }

}
