var dateFormat = require('dateformat'); //for using date format
const current_datetime = dateFormat(new Date().toLocaleString('en-US'),"yyyy-mm-dd hh:MM:ss"); //get current date and time
const md5 = require('md5'); //for encrypt passwords

const mongoose = require('mongoose');
var pagesSchema = require('../schema/pagesSchema');
var pagesDB = mongoose.model('pages', pagesSchema);


module.exports = {
    checkIDExits:function(con, data, callback) { // for check email exists or not
        usersDB.find({ '_id': data.user_id }, callback)
    },
    contentAdd: function(con, data, callback) { //for user signup
        var Page = new pagesDB();
        Page.title = data.title;         
        Page.content = data.content;         
        Page.type = data.type;         
        Page.created_at = current_datetime;    
        Page.save(callback);
    },
    getContent:function(con, data, callback) { //User selected category added 
        pagesDB.find({'type': data.type }).exec(callback)
    },
    userLogin: function(con, data, callback) { //for user login 
        var password = md5(data.password);
        usersDB.find({ 'email': data.email, 'password': password }, callback)
    },
    contentUpdate:function(con, data, callback) { //Update token after login
        var myquery = {  type: data.type  };
        var newvalues = { $set: 
            { 
                title: data.title, 
                content: data.content
            } 
        };
        pagesDB.updateOne(myquery, newvalues, callback)
    },
    updateUserPin:function(con, data, callback) { //Update User Pin
        var myquery = {  _id: data.user_id  };
        var newvalues = { $set: 
            { 
                pin: data.pin,                 
                status: 2
            } 
        };
        usersDB.updateOne(myquery, newvalues, callback)
    },
    updateUserInfo:function(con, data, callback) { //Update User info
        var myquery = {  _id: data.user_id  };
        var newvalues = { $set: 
            { 
                name: data.name, 
                image: data.image, 
                gender: data.gender, 
                dob: data.dob, 
                email: data.email
            } 
        };
        usersDB.updateOne(myquery, newvalues, callback)
    },   
    userAddCategory:function(con, data, callback) { //User selected category added 
        userCategoryDB.find({ 'user_id': data.user_id, 'category_id': data.category_id }, function(err, res){
            if(err){
                callback(err,'');
            }else{
                if(res.length > 0){
                    var t = {'_id':  '500'}; 
                    callback('', t);
                }else{
                    var UserCategory = new userCategoryDB();
                    UserCategory.user_id = data.user_id;
                    UserCategory.users = data.user_id;
                    UserCategory.category_id = data.category_id;
                    UserCategory.target = data.target;
                    UserCategory.weekly_target = data.weekly_target;
                    UserCategory.save(callback);
                }
            }
        })
    },
    
    forgotPassword:function(con, otp, user_id, callback){
        var myquery = {  _id: user_id  };
        var newvalues = { $set: 
            { 
                otp: otp
            } 
        };
        usersDB.updateOne(myquery, newvalues, callback)
    },
    otpVerify: function(con, data, callback) { 
        usersDB.find({ 'otp': data.otp, 'email': data.email }, callback)
    },
    resetPassword:function(con, data, user_id, callback){
        var password = md5(data.password);
        var myquery = {  _id: user_id  };
        var newvalues = { $set: 
            { 
                password: password
            } 
        };
        usersDB.updateOne(myquery, newvalues, callback)
    },   
    changePassword:function(con, data, user_id, callback){
        var password = md5(data.password);
        var myquery = {  _id: user_id  };
        var newvalues = { $set: 
            { 
                password: password
            } 
        };
        usersDB.updateOne(myquery, newvalues, callback)
    },
}
