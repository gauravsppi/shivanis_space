import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditModelNameComponent } from './edit-model-name.component';

describe('EditModelNameComponent', () => {
  let component: EditModelNameComponent;
  let fixture: ComponentFixture<EditModelNameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditModelNameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditModelNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
