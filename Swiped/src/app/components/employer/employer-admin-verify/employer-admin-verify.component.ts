import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployerSignupLoginService } from '../../../services/employer-signup-login/employer-signup-login.service';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import { employer_model } from '../../../models/employer_model';

@Component({
  selector: 'app-employer-admin-verify',
  templateUrl: './employer-admin-verify.component.html',
  styleUrls: ['./employer-admin-verify.component.scss']
})
export class EmployerAdminVerifyComponent implements OnInit {
  public employerModel: employer_model;
  public addValidationError: employer_model;
  sucMsg: boolean = false;
  errMsg: boolean = false;
  idGet:any;
  constructor(private accountService: EmployerSignupLoginService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService) { }

  ngOnInit(): void {
    this.employerModel = <employer_model>{
      id:'',
    }
    this.route.paramMap.subscribe(params => {
      this.idGet = params.get('id');
      console.log('here is id>>>',this.idGet);
      if(this.idGet=='' || this.idGet==null){

      }else{
        this.employerModel.id = this.idGet;
        this.accountService.statusUpdateVerify(this.employerModel).then((response) => {
          if (response.code == 200) {
            console.log('incoming data from api',response);
          }else{
            this.router.navigate(['/employer-login']);
          }
        })
      }
    });
  }
  signin(){
    localStorage.clear();
    this.router.navigate(['/employer-login']);
  }
}
