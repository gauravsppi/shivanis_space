const mongoose = require('mongoose');
var carsCompanySchema = new mongoose.Schema({
    name: {
        type: String,       
    },
    status: {
        type: String
    },
    created_at: {
        type: Date,
        default: Date.now
    },
});
module.exports = carsCompanySchema;
