import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from '../../../services/account.service';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import { admin_model } from '../../../models/admin_model';
import { employer_model } from '../../../models/employer_model';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2'

@Component({
  selector: 'app-view-job',
  templateUrl: './view-job.component.html',
  styleUrls: ['./view-job.component.scss']
})
export class ViewJobComponent implements OnInit {
  public employerModel: employer_model;
  employerlist:any ;
  getId:any;

  
  campaigndetailstabdata:any  ;
  skillsneedtabdata:any  ;
  companyroledesctabdata:any  ;
  campaignquestabdata:any  ;

  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService, private spinner:NgxSpinnerService) { }


  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.getId = params.get('id');
      
    });
    this.employerModel = <employer_model>{
      id:'',
      //name:'',
      //post:'',
      //job_type:'',
      //e_specialism:[],
      
    }
    this.getEmployerPostWithID();
  }

   //----------------------------get post details------------------

   getEmployerPostWithID(){
    console.log(this.employerModel);
    this.employerModel.id = this.getId;
    this.spinner.show();
    debugger;
    this.accountService.getEmployerPostWithID(this.employerModel).then((response) => {
      debugger;
      console.log(response);
    if (response.code == 200) {
      this.spinner.hide();
      this.campaigndetailstabdata=response.result;
      //this.campaigndetailstabdata=response.campaigndetailstabdata;
      this.skillsneedtabdata=response.skillsneedtabdata;
      this.companyroledesctabdata=response.companyroledesctabdata;
      this.campaignquestabdata=response.campaignquestabdata;
    
    } else {
      //this.errMsg = true;
      //this.message = response.message; 
      }
    })
  }

}
