import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from '../../components/candidate/login/login.component';
import { SignUpComponent } from '../../components/candidate/sign-up/sign-up.component';
import { EmailVerifyComponent } from '../../components/candidate/email-verify/email-verify.component';
import { AccountCreatedComponent } from '../../components/candidate/account-created/account-created.component';
import { CandidateDetailsComponent } from '../../components/candidate/candidate-details/candidate-details.component';
import { CandidateDashboardComponent } from '../../components/candidate/candidate-dashboard/candidate-dashboard.component';
import { CandidateEmailVerifyComponent } from '../../components/candidate/candidate-email-verify/candidate-email-verify.component';
import { AuthGuard } from '../../services/auth.guard';

const routes: Routes = [
  {
    path:'', 
    redirectTo: '/candidate', 
    pathMatch:'full'
  },
  {
    path: 'sign-in',
    component: LoginComponent
  },
  {
    path: 'candidate',
    component: SignUpComponent
  },
  {
    path: 'email_verify',
    component: EmailVerifyComponent
  },
  {
    path: 'account_created/:id',
    component:AccountCreatedComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'candidate_details',
    component:CandidateDetailsComponent
  },
  {
    path: 'candidate_details/:id',
    component:CandidateDetailsComponent
  },
  {
    path: 'candidate_dashboard/:id',
    component:CandidateDashboardComponent
    //canActivate:[AuthGuard]
  },
  {
    path: 'verify_mail',
    component:CandidateEmailVerifyComponent
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CandidateRoutingModule { }
