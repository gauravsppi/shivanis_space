import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Headers, Http } from '@angular/http';


import { environment } from '../../../environments/environment';
import { AccountService } from '../../services/account.service';

import { school_model } from '../../models/school_model';
import { Login_model } from '../../models/login_model';

@Component({
  selector: 'app-school-view',
  templateUrl: './school-view.component.html',
  styleUrls: ['./school-view.component.scss']
})
export class SchoolViewComponent implements OnInit {

  public schoolModel: school_model;
  constructor(private router: Router, private accountService: AccountService, private route: ActivatedRoute, private http: Http) { }
  school_id = this.route.snapshot.params.id;

  ngOnInit(): void {
    this.schoolModel = <school_model>{
      name: '',
      identifier: '',
      phoneNumber: '',
      schoolNumber: '',
      admin_id: '',
      school_id: '',
      searchKey: ''
    }
    this.getSchoolId(this.school_id);
  }

  getSchoolId(school_id){
    this.schoolModel.school_id = school_id;
    this.accountService.getSchoolsInfo(this.schoolModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
         this.schoolModel.name = response.result[0].name;                    
         this.schoolModel.schoolNumber = response.result[0].metadataSchoolNumber;                    
         this.schoolModel.identifier = response.result[0].identifier;                    
         this.schoolModel.phoneNumber = response.result[0].phone;                    
         this.schoolModel.school_sourcedId = response.result[0].sourcedId;                    
      }
    })
  }

}
