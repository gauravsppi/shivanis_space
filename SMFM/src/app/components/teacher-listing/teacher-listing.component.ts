import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd, RoutesRecognized } from '@angular/router';
import { Headers, Http } from '@angular/http';
import { Location } from '@angular/common';
import { filter, pairwise } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { AccountService } from '../../services/account.service';
import { teacher_model } from '../../models/teacher_model';

@Component({
  selector: 'app-teacher-listing',
  templateUrl: './teacher-listing.component.html',
  styleUrls: ['./teacher-listing.component.scss']
})
export class TeacherListingComponent implements OnInit {
  public teacherModel: teacher_model;
  teachers: [];
  school_id: '';
  school_name: '';
  school_number: '';
  school_phone: '';
  sucMsg: boolean = false;
  errMsg: boolean = false;
  successMessage: string;   
  errorMessage: string; 
  total : number= 0;
  totalPages : number= 0;
  currentPage:number=1;
  page : number =1;
  key: string = 'name'; //set default
  reverse: boolean = false;
  noRecords: boolean = false;
  loaderShow: boolean = false;
  j=0;

  constructor(private router: Router, private accountService: AccountService, private route: ActivatedRoute, private http: Http, private location: Location) {  }

  ngOnInit(): void {
    this.teacherModel = <teacher_model>{      
      teacher_id : '',
      school_id : '',
      name:'',
      email:'',
      phone:'',
      image:'',
      searchKey: '',
      sortKey: 'name',
      sortBy: 'asc'
    }
    this.getTeachers();

    this.router.events
    .pipe(filter((evt: any) => evt instanceof RoutesRecognized), pairwise())
    .subscribe((events: RoutesRecognized[]) => {
      console.log('-> ', events);
      console.log('previous url', events[0].urlAfterRedirects);
      console.log('current url', events[1].urlAfterRedirects);
    });
  }
  getTeachers() {
    this.loaderShow = true;
    this.teacherModel.school_id = this.route.snapshot.params.id;   
    this.teacherModel.page = this.currentPage;
    this.accountService.getTeachersBySchoolID(this.teacherModel).then((response) => {
        if (response.code == 200 && response.status == 'success') {        
          this.total = response.count;
          this.totalPages=Math.ceil( response.count/10);
          this.teachers = response.result;                    
          this.school_id = response.school_id;                         
          this.school_name = response.school_name;                    
          this.school_number = response.school_number;                    
          this.school_phone = (response.school_phone == '' || response.school_phone == undefined)? '--' :  response.school_phone ; 
          this.noRecords = false;
        }else{ 
          this.noRecords = true;        
          this.total = 0
          this.totalPages=0
          this.teachers = []; 
        }
        this.loaderShow = false;
    })
  }

  pageChange(currentPage){
    this.currentPage=currentPage;
    if(this.teacherModel.searchKey==""){
      this.getTeachers();
    }else{
      this.searchFunction(this.currentPage);
    } 
  }

  counter(i: number) {
    return new Array(i);
  }

  searchFunction(currentPage){
    if(this.teacherModel.searchKey== ""){
      this.currentPage=1;
      this.getTeachers();
    }else{
      this.loaderShow = true;
      this.currentPage=currentPage; 
      this.teacherModel.page = this.currentPage;
      this.teacherModel.school_id = this.route.snapshot.params.id;
      this.accountService.getTeachersBySchoolID(this.teacherModel).then((response) => {
        if (response.code == 200 && response.status == 'success') {
          this.total = response.count;
          this.totalPages=Math.ceil(response.count/10);
          this.teachers = response.result;
          this.noRecords = false;
        }else{
          this.noRecords = true;
          this.total = 0;
          this.totalPages=0;
          this.teachers = [];
        } 
        this.loaderShow = false;     
      })
    }
  }

  sortFunction(sortKey){
    this.key = sortKey;
    this.teacherModel.sortKey = sortKey;
    if(this.reverse === true){
      this.teacherModel.sortBy = 'asc';
    }else{
      this.teacherModel.sortBy = 'desc';
    }
    this.reverse = !this.reverse;   
    if(this.teacherModel.searchKey== ""){
      this.currentPage=1;
      this.getTeachers();
    }else{
      this.searchFunction(this.currentPage);
    }
  }


  blockTeacher(teacher_id){
    this.teacherModel.admin_id = localStorage.getItem('admin_id');
    this.teacherModel.teacher_id = teacher_id;
    this.accountService.blockTeacherByID(this.teacherModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
          this.sucMsg= true;  
          this.successMessage = response.message;
          setTimeout (() => { this.sucMsg = false; }, 2000); 
          this.getTeachers();       
      }else{
          this.errMsg= false;  
          this.errorMessage = response.message; 
          setTimeout (() => { this.errMsg = false; }, 2000);       
      }
    })
  }
}
