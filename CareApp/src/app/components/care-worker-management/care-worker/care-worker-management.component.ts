import { Component, OnInit, ViewChild } from '@angular/core';
import { NgxSpinnerService } from "../../../shared/services/spinner.service";
import { WorkerService } from '../../../shared/services/worker-service/worker.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-care-worker-management',
  templateUrl: './care-worker-management.component.html',
  styleUrls: ['./care-worker-management.component.scss']
})
export class CareWorkerManagementComponent implements OnInit {
  @ViewChild('closebutton') closebutton;
  submitted = false;
  editCareWorkerForm: FormGroup;
  workerArray: any = [];
  workerLanguage: any = [];
  page = 1;
  count : number;
  pageSize = 10;
  selectedIndex: number = 0;
  defaultImage = 'assets/images/loader.gif';
  workerDetail = {
    name: '',
    email: '',
    mobileno: '',
    profileimage:'',
    gender:'',
    religions: {},
    hairandgroomings:{},
    ethnicorigins: {},
    sexualorientations: {},
    ageanddisabilitie:{},
    languageandculturalcuisines: {
      language: []
    }


  };
  constructor(
    private WorkerService: WorkerService,
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder, 

  ) {
    this.editCareWorkerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      name: ['',[Validators.required]],
      phone: ['',[Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      _id: [''],
      profileimage:['']

    });
   }

  ngOnInit(): void {
    this.spinner.show();
    this.fetchRecords().toPromise().then((res) => {
      console.log('res++++++++++++',res);
      this.spinner.hide();
      if(res.code == 200){
        this.workerArray = res.result
        this.count = res.numOfWorkers
      }else{
        Swal.fire({ icon: 'error', title: 'Oops...', text: res.message })
      }
    })
    .catch(err=> { 
      this.spinner.hide();
      console.log(err) 
    });
  }

  get f() { return this.editCareWorkerForm.controls; }


  fetchRecords () {
    var Params = {
      page:  this.page, 
      pageSize: this.pageSize,
    }
    return this.WorkerService.fetch_care_worker(Params);
  }

  onTableDataChange(event){
    this.page = event;
    this.ngOnInit();
  } 

  //******** Edit care worker function **************
  onSubmit(): void {
    console.log(this.editCareWorkerForm.value)
    this.submitted = true;

    // stop here if form is invalid
    if (this.editCareWorkerForm.invalid) {
        return;
    }


    this.spinner.show();

    this.WorkerService.edit_care_worker(this.editCareWorkerForm.value).subscribe((data: any) => {
      console.log('data++++++++',data)
      this.spinner.hide();

      if(data.status == 0){
        Swal.fire({ icon: 'error',  title: 'Oops...',  text: data.message })

      }else{
        this.closebutton.nativeElement.click();
        this.workerArray[this.selectedIndex].name =  data.data.name;
        this.workerArray[this.selectedIndex].email =  data.data.email;
        this.workerArray[this.selectedIndex].mobileno =  data.data.phone;
        Swal.fire('Worker!',  'Worker has been updated.', 'success')

        // this.router.navigate(['dashboard'])
      }
      
    }, error => {
      this.spinner.hide();
      console.log('login data ----------',error)
    
    })
  }


  editCareWorker(worker, index) : void{
    this.selectedIndex =  index;
    console.log(worker)
    this.editCareWorkerForm.patchValue({name: worker.name, email: worker.email, phone: worker.mobileno, _id : worker._id, profileimage: worker.profileimage})
  }


  viewWorker(worker): void {
    console.log('worker+++++++', worker)
    this.workerDetail = worker;
    this.workerLanguage = worker.languageandculturalcuisines ? worker.languageandculturalcuisines.language : [];
  }


    //******** Detele Worker function **************
    deleteReceiver(id, status): void {
      // this.page = 1;
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {

          var foundIndex = this.workerArray.findIndex(x => x._id == id);
           this.workerArray.splice(foundIndex , 1)
  
          this.spinner.show();
          this.WorkerService.delete_worker(id, status).subscribe((data: any) => {
            this.spinner.hide();
            console.log(data);
            if(data.status == 1){
              this.ngOnInit()
              var foundIndex = this.workerArray.findIndex(x => x._id == id);
              this.workerArray.splice(foundIndex , 1)
              Swal.fire( 'Deleted!', 'Worker has been deleted.', 'success')
            }else{
              Swal.fire({  icon: 'error', title: 'Oops...', text: data.message })
            }
          }, error => {
            this.spinner.hide();
            console.log(error)
          })
        }
      })
    }


    //******** Active/Deactivate Worker function **************
    onChangeEvent(worker){
      console.log('reveiver+++++++', worker);
      var reqParams = {
        _id : worker._id,
        status: worker.status == false ? 1 : 0
      }
  
      console.log(reqParams);
  
      this.spinner.show();
          this.WorkerService.delete_worker(reqParams._id, reqParams.status).subscribe((data: any) => {
            this.spinner.hide();
            console.log(data);
            if(reqParams.status == 1){
              Swal.fire( 'Receiver activated!', 'Worker has been activated.', 'success')
            }else if(reqParams.status == 0){
              Swal.fire( 'Receiver dactivated!', 'Worker has been dactivated.', 'success')
  
            }else{
              Swal.fire({  icon: 'error', title: 'Oops...', text: data.message })
  
            }
          }, error => {
            this.spinner.hide();
            console.log(error)
          })
  
    }

}
