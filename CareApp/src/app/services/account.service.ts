import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { environment } from '../../environments/environment';
import { Admin } from '../models/admin.model';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private AdminLogin = environment.apiBaseUrl + "loginAdmin";
  private Addsubadmin = environment.apiBaseUrl + "subadminAdd";
  private Getsubadmin = environment.apiBaseUrl + "subadminGet";
  private Updatesubadmin = environment.apiBaseUrl + "subadminUpdate";
  private Deletesubadmin = environment.apiBaseUrl + "subadminDelete";
  private Getreligions = environment.apiBaseUrl + "religionsGet";
  private AddReligions = environment.apiBaseUrl + "religionAdd";
  private OneReligion = environment.apiBaseUrl + "getOnereligion";

  constructor(private baseHttpService: BaseHttpService) { }

  LoginAdmin(candidatelogin: Admin): Promise<any> {
    return this.baseHttpService.Post(this.AdminLogin, candidatelogin)
      .then(function (response) {
        return response.json();
      });
  }
  Subadmin(candidatelogin: Admin): Promise<any> {
    return this.baseHttpService.Post(this.Addsubadmin, candidatelogin)
      .then(function (response) {
        return response.json();
      });
  }
  //  subAdminGet(): Promise<any> {
  //   return this.baseHttpService.Get(this.Getsubadmin)
  //     .then(function (response) {
  //       return response.json();
  //     });
  //   }
    subAdminGet(page): Promise<any> {
      return this.baseHttpService.Get(this.Getsubadmin+"/"+page)
        .then(function (response) {
          return response.json();
        });
    }
    SubadminUpdate(candidatelogin: Admin): Promise<any> {
      return this.baseHttpService.Post(this.Updatesubadmin, candidatelogin)
        .then(function (response) {
          return response.json();
        });
    }
    SubadminDelete(candidatelogin: Admin): Promise<any> {
      return this.baseHttpService.Post(this.Deletesubadmin, candidatelogin)
        .then(function (response) {
          return response.json();
        });
    }
    ReligionGet(): Promise<any> {
        return this.baseHttpService.Get(this.Getreligions)
          .then(function (response) {
            return response.json();
          });
        }
    ReligionsADD(candidatelogin: Admin): Promise<any> {
      return this.baseHttpService.Post(this.AddReligions, candidatelogin)
        .then(function (response) {
          return response.json();
        });
    }
    OneReligionGet(candidatelogin: Admin): Promise<any> {
      return this.baseHttpService.Post(this.OneReligion, candidatelogin)
        .then(function (response) {
          return response.json();
        });
    }
}
