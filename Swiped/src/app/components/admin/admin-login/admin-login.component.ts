import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from '../../../services/account.service';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import { admin_model } from '../../../models/admin_model';


@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.scss']
})
export class AdminLoginComponent implements OnInit {
  public adminModel: admin_model;
  public addValidationError: admin_model;
  public headers = new Headers();
  emailvalidation:boolean=false;
  passwordvalidation:boolean=false;
  marked: boolean = false;
  ischecked: boolean = false;
  cookiedata: any[];
  cookiecheck:any;
  errMsg: boolean = false;
  message: string;
  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService) { }
  ngOnInit(): void {
    this.adminModel = <admin_model>{
      admin_email: '',
      admin_password: ''
    }
    this.addValidationError = <admin_model>{
      admin_email: '',
      admin_password: ''
    }
    localStorage.clear();
    this.checkcookies();
  }
  checkcookies(){
    this.cookiecheck=this.cookieService.getAll();
    console.log(this.cookiecheck);
    if(this.cookiecheck==null){
      
    }else{
      this.adminModel.admin_email=this.cookiecheck.adminemail;
      this.adminModel.admin_password=this.cookiecheck.adminpassword;
    }
  }
  validationcheck(){
    var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    if (!EMAIL_REGEXP.test(this.adminModel.admin_email) || this.adminModel.admin_email == undefined) {
      this.emailvalidation = true;
    } else {
      this.emailvalidation = false;
    }
    if (this.adminModel.admin_password == "" || this.adminModel.admin_password == undefined) {
      this.passwordvalidation = true;
    } else {
      this.passwordvalidation = false;
    }
  }
  toggleVisibility(e) {
    this.marked = e.target.checked;
    console.log(e.target.checked);
  }
  loginadmin(){
    console.log(this.adminModel);
    var valid = true;
    this.addValidationError.admin_email = '';
    this.addValidationError.admin_password = '';
    if (this.adminModel.admin_email == "" || this.adminModel.admin_email == undefined) {
      this.emailvalidation = true;
      valid = false;
    }
    if (this.adminModel.admin_password == "" || this.adminModel.admin_password == undefined) {
      this.passwordvalidation = true;
      valid = false;
    }
    if (valid) {
      if (this.marked == true) {
        this.accountService.AdminLogin(this.adminModel).then((response) => {
          if (response.code == 200) {
            localStorage.setItem('token', response.result[0].jwtToken);
            this.cookieService.set("adminemail",response.result[0].admin_email);
            this.cookieService.set("adminpassword",response.result[0].admin_password);
            // -------- For Check Login Type Status ----------
              this.router.navigate(['/admin/dashboard']);
          } else {
            this.errMsg = true;
            this.message = response.message;
          }
        })
      }else{
        this.accountService.AdminLogin(this.adminModel).then((response) => {
          if (response.code == 200) {
            localStorage.setItem('token', response.result[0].jwtToken);
            this.cookiecheck=this.cookieService.getAll();
            if(this.cookiecheck==null){
              // -------- For Check Login Type Status ----------
                this.router.navigate(['/admin/dashboard']);
            }else{
              this.cookieService.deleteAll();
              // -------- For Check Login Type Status ----------
              this.router.navigate(['/admin/dashboard']);
            }
          } else {
            this.errMsg = true;
            this.message = response.message;
          }
        })
      }
    }
  }
}
