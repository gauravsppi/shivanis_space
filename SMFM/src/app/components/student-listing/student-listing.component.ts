import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Headers, Http } from '@angular/http';

import { environment } from '../../../environments/environment';
import { AccountService } from '../../services/account.service';
import { student_model } from '../../models/student_model'

@Component({
  selector: 'app-student-listing',
  templateUrl: './student-listing.component.html',
  styleUrls: ['./student-listing.component.scss']
})
export class StudentListingComponent implements OnInit {
  public studentModel: student_model;
  students: [];
  teacher_id: '';
  teacher_name: '';
  teacher_email: '';
  teacher_phone: '';
  total : number= 0;
  totalPages : number= 0;
  currentPage:number=1;
  page : number =1;
  key: string = 'name'; //set default
  reverse: boolean = false;
  noRecords: boolean = false;
  loaderShow: boolean = false;
  sucMsg: boolean = false;
  errMsg: boolean = false;
  successMessage: string;   
  errorMessage: string; 

  constructor(private router: Router, private accountService: AccountService, private route: ActivatedRoute, private http: Http) {  }

  ngOnInit(): void {
    this.studentModel = <student_model>{      
      admin_id: '',
      school_id: '',
      name:'',
      email:'',
      phone:'',
      image:'',
      searchKey: '',
      sortKey: 'name',
      sortBy: 'asc'
    }
    this.getStudents();
  }
  getStudents() {
    this.loaderShow = true;
    this.studentModel.page = this.currentPage;
    this.studentModel.admin_id = localStorage.getItem('admin_id') ;
    this.studentModel.school_id = localStorage.getItem('school_id');
    this.accountService.getStudents(this.studentModel).then((response) => {
        if (response.code == 200 && response.status == 'success') {
          this.total = response.count;
          this.totalPages=Math.ceil( response.count/10);
          this.students = response.result;  
          this.noRecords = false;        
        }else{ 
          this.noRecords = true;        
          this.total = 0
          this.totalPages=0
          this.students = []; 
        }
        this.loaderShow = false;
    })
  }

  pageChange(currentPage){
    this.currentPage=currentPage;
    if(this.studentModel.searchKey==""){
      this.getStudents();
    }else{
      this.searchFunction(this.currentPage);
    } 
  }

  counter(i: number) {
    return new Array(i);
  }

  searchFunction(currentPage){
    if(this.studentModel.searchKey== ""){
      this.currentPage=1;
      this.getStudents();
    }else{
      this.loaderShow = true;
      this.currentPage=currentPage;
      this.studentModel.page = this.currentPage;
      this.studentModel.admin_id = localStorage.getItem('admin_id') ;
      this.studentModel.school_id = localStorage.getItem('school_id');
      this.accountService.getStudents(this.studentModel).then((response) => {
        if (response.code == 200 && response.status == 'success') {
          this.total = response.count;
          this.totalPages=Math.ceil(response.count/10);
          this.students = response.result;
          this.noRecords = false;
        }else{
          this.noRecords = true;
          this.total = 0;
          this.totalPages=0;
          this.students = [];
        } 
        this.loaderShow = false;     
      })
    }
  }

  sortFunction(sortKey){
    this.key = sortKey;
    this.studentModel.sortKey = sortKey;
    if(this.reverse === true){
      this.studentModel.sortBy = 'asc';
    }else{
      this.studentModel.sortBy = 'desc';
    }
    this.reverse = !this.reverse;   
    if(this.studentModel.searchKey== ""){
      this.currentPage=1;
      this.getStudents();
    }else{
      this.searchFunction(this.currentPage);
    }
  }

  getStudentId(student_id){
    this.studentModel.student_id = student_id;
    this.accountService.getStudentInfo(this.studentModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
         this.studentModel.name = response.result[0].name;                    
         this.studentModel.image = environment.studentImagePath + response.result[0].image;                    
         this.studentModel.email = response.result[0].email;                    
         this.studentModel.phone = response.result[0].phone;                    
      }
    })
  }

  blockStudent(student_id){
    this.studentModel.admin_id = localStorage.getItem('admin_id');
    this.studentModel.student_id = student_id;
    this.accountService.blockStudentByID(this.studentModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
          this.sucMsg= true;  
          this.successMessage = response.message;
          setTimeout (() => { this.sucMsg = false; }, 2000); 
          this.getStudents();       
      }else{
          this.errMsg= false;  
          this.errorMessage = response.message; 
          setTimeout (() => { this.errMsg = false; }, 2000);       
      }
    })
  }

}
