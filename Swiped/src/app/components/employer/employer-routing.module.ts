import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CampaignDetailComponent } from '../../components/employer/campaign-detail/campaign-detail.component';
import { ChoosePlanComponent } from '../../components/employer/choose-plan/choose-plan.component';
import { EmailVerifyComponent } from '../../components/employer/email-verify/email-verify.component';
import { CandidateListComponent } from '../../components/employer/candidate-list/candidate-list.component';
import { EmployerDashboardComponent } from './employer-dashboard/employer-dashboard.component';
import { JobManageComponent } from './job-manage/job-manage.component';
import { ViewJobComponent } from './view-job/view-job.component';
import { AuthGuard } from '../../services/auth.guard';
import { EmployerSignupComponent } from './employer-signup/employer-signup.component';
import { EmployerLoginComponent } from './employer-login/employer-login.component';
import { EmployerAdminVerifyComponent } from './employer-admin-verify/employer-admin-verify.component';
import { CompanyDetailsComponent } from './company-details/company-details.component';
import { EmployerWelcomeComponent } from './employer-welcome/employer-welcome.component';

const routes: Routes = [
  {
    path: 'verify_email',
    component: EmailVerifyComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'campaign_details',
    component: CampaignDetailComponent
  },
  {
    path: 'choose_plan',
    component: ChoosePlanComponent
  },
  {
    path: 'candidate_list',
    component: CandidateListComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'employer-dashboard',
    component: EmployerDashboardComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'job-manage',
    component: JobManageComponent
  },
  {
    path: 'view-job/:id',
    component: ViewJobComponent,
     canActivate:[AuthGuard]
  },
  {
    path: 'employer',
    component: EmployerSignupComponent
  },
  {
    path: 'employer-login',
    component: EmployerLoginComponent
  },
  {
    path: 'verify',
    component: EmployerAdminVerifyComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'verify/:id',
    component: EmployerAdminVerifyComponent
  },
  {
    path: 'employer-profile/:id',
    component: CompanyDetailsComponent
  },
  {
    path: 'welcome/:id',
    component: EmployerWelcomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployerRoutingModule { }
