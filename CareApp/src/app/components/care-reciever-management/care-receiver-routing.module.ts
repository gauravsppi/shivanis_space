import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CareRecieverManagementComponent } from './care-receiver/care-reciever-management.component';
import { PostListComponent } from './post-list/post-list.component';
import { PostDetailsComponent } from './post-details/post-details.component';
import { PostDetailsEditComponent } from './post-details-edit/post-details-edit.component';
import { AddPostComponent } from './add-post/add-post.component';
import { AddRecieverComponent } from './add-reciever/add-reciever.component';


const routes: Routes = [
  {
    path: '',
    component: CareRecieverManagementComponent
  },
  {
    path: 'post-list/:id',
    component: PostListComponent
  },
  {
    path: 'post-details/:postId/:receiverId',
    component: PostDetailsComponent
  },
  {
    path: 'edit-post-details',
    component: PostDetailsEditComponent
  },
  {
    path: 'add-post',
    component: AddPostComponent
  },

  {
    path: 'add-reciever',
    component: AddRecieverComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CareRecevierRoutingModule { }