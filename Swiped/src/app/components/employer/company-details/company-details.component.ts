import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from '../../../services/account.service';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from "../../../services/data.service";
import { Headers, Http } from '@angular/http';
//import { candidateLogin_model } from '../../../models/candidateLogin_model';
import { employer_model } from '../../../models/employer_model';

@Component({
  selector: 'app-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.scss']
})
export class CompanyDetailsComponent implements OnInit {
  public loginModel: employer_model;
  public addValidationError: employer_model;
  profileEditForm: FormGroup;
  submitted = false;
  getId:any;
  file: number=0;
  filename:string;
  public sectorlists = [
    { id: "DATA", name: "DATA" },
    { id: "TECHNOLOGY", name: "TECHNOLOGY" },
    { id: "MARKETING", name: "MARKETING" },
    { id: "CREATIVE", name: "CREATIVE" }
  ];
  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService,private formBuilder: FormBuilder, private data:DataService) { 
    this.profileEditForm = this.formBuilder.group({
      company_name: [''],
      contact_name: [''],
      email: [''],
      contact_number: [''],
      website: [''],
      invoice_email: [''],
      industry_sector: [null],
      headcount: [''],
      company_linkdin: [''],
      office_address: [''],
    });
  }

  ngOnInit(): void {
    this.loginModel = < employer_model>{
      id:'',
      company_name:'',
      contact_name:'',
      email:'',
      contact_number:'',
      invoice_email:'',
      industry_sector:'',
      headcount:'',
      company_linkdin:'',
      company_brochure:'',
      office_address:'',
      website:''
    }
    this.route.paramMap.subscribe(params => {
      this.getId = params.get('id');
    });
    this.getEmployerbyId();
  }
  //get f() { return this.profileEditForm.controls; }
  onSubmit() {
    this.submitted = true;

    console.log(this.profileEditForm.value)
    // stop here if form is invalid
    // if (this.profileEditForm.invalid) {
    //     return;
    // }
    const formData = new FormData();
    formData.append('id', this.getId);
    formData.append('company_name', this.profileEditForm.get('company_name').value);
    formData.append('contact_name', this.profileEditForm.get('contact_name').value);
    formData.append('email', this.profileEditForm.get('email').value);
    formData.append('contact_number', this.profileEditForm.get('contact_number').value);
    formData.append('invoice_email', this.profileEditForm.get('invoice_email').value);
    formData.append('headcount', this.profileEditForm.get('headcount').value);
    formData.append('company_linkdin', this.profileEditForm.get('company_linkdin').value);
    formData.append('office_address', this.profileEditForm.get('office_address').value);
    formData.append('industry_sector', this.profileEditForm.get('industry_sector').value);
    formData.append('website', this.profileEditForm.get('website').value);
    this.accountService.companyDetailsEdit(formData).then((response) => {
      console.log('response from data>>>>',response);
      if(response.code==200){
        this.router.navigate(['/employer-dashboard']); 
      }else{

      }
    })
  }
  //-----------------------get employer details API------------------------
  getEmployerbyId(){
    this.loginModel.id = this.getId;
    this.accountService.getEmployerWithID(this.loginModel).then((response) => {
      if (response.code == 200 || response.code == 400) {
        console.log('enter here with data>>>', response.result);
        if(response.result.screen_status==3){
          this.data.changeStatus(true);
        }else{
        this.data.changeStatus(false);  
      }
        this.profileEditForm.patchValue({email:response.result.email, contact_number:response.result.phone, website:response.result.company_website, company_name:response.result.company_name, contact_name:response.result.contact_name,invoice_email:response.result.invoice_email,industry_sector: response.result && response.result.industry_sector != '' ? response.result.industry_sector : null, headcount:response.result.headcount, company_linkdin:response.result.company_linkdin,office_address:response.result.office_address})
        
      }
    })
  }
  // ----------- Upload optional video Image -----------
  selectFile(event){
    console.log(event.target.files[0]);
    this.file=event.target.files.length;
    this.filename=event.target.files[0].name;
      const formData = new FormData();
      formData.append('video_upload', <File>event.target.files[0]);
  }
  removeFile(event){
    this.file=0;
    event.value="";
  }
}
