import { Component, OnInit, ViewChild } from '@angular/core';

import { NgxSpinnerService } from "../../../shared/services/spinner.service";
import { ReceiverService } from '../../../shared/services/receiver-service/receiver.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { AgencyService } from 'src/app/shared/services/agency-service/agency.service';


@Component({
  selector: 'app-manage-agency',
  templateUrl: './manage-agency.component.html',
  styleUrls: ['./manage-agency.component.scss']
})
export class ManageAgencyComponent implements OnInit {
  @ViewChild('closebutton') closebutton;
  submitted = false;
  enable: false
  agencyArray: any = [];
  page = 1;
  count : number;
  pageSize = 2;
  selectedIndex: number = 0;
  defaultImage = 'assets/images/loader.gif';
  receiverLanguage: any = [];

  agencyDetail = {
    agency_name: '',
    email: '',
    agency_url:'',
    phone: '',
    image:'',
    agency_address:''
  };

  constructor(
    private AgencyService: AgencyService,
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder, 


  ) {  }

  ngOnInit(): void {
    this.spinner.show();
    this.fetchRecords().toPromise().then((res) => {
      console.log(res)
      this.spinner.hide();
      if(res.status == 1){
        this.agencyArray = res.result
        this.count = res.numOfAgencies
      }else{
        // Swal.fire({ icon: 'error', title: 'Oops...', text: res.message })
      }
    })
    .catch(err=> { 
      this.spinner.hide();
      console.log(err) 
    });
  }

  fetchRecords (){
    var Params = {
      page:  this.page, 
      pageSize: this.pageSize,
    }
    return this.AgencyService.fetch_agency(Params);
  }

  onTableDataChange(event){
    this.page = event;
    this.ngOnInit();
  }
  
  //******** Detele receiver function **************
  deleteAgency(id, status): void {
    // this.page = 1;
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {

        this.spinner.show();
        this.AgencyService.delete_agency(id, status).subscribe((data: any) => {
          this.spinner.hide();
          console.log(data);
          if(data.status == 1){
            this.ngOnInit()
            var foundIndex = this.agencyArray.findIndex(x => x._id == id);
            this.agencyArray.splice(foundIndex , 1)
            Swal.fire( 'Deleted!', 'Agency has been deleted.', 'success')
          }else{
            Swal.fire({  icon: 'error', title: 'Oops...', text: data.message })
          }
        }, error => {
          this.spinner.hide();
          console.log(error)
        })
      }
    })
  }


  viewAgency(agency): void {
    console.log('receiver++++++', agency)
    this.agencyDetail = agency;
  }

  onChangeEvent(agency){
    console.log('reveiver+++++++', agency);
    var reqParams = {
      _id : agency._id,
      status: agency.status == false ? 1 : 0
    }

    console.log(reqParams);

    this.spinner.show();
        this.AgencyService.delete_agency(reqParams._id, reqParams.status).subscribe((data: any) => {
          this.spinner.hide();
          console.log(data);
          if(reqParams.status == 1){
            Swal.fire( 'Agency activated!', 'Agency has been activated.', 'success')
          }else if(reqParams.status == 0){
            Swal.fire( 'Agency dactivated!', 'Agency has been dactivated.', 'success')

          }else{
            Swal.fire({  icon: 'error', title: 'Oops...', text: data.message })

          }
        }, error => {
          this.spinner.hide();
          console.log(error)
        })

  }

}
