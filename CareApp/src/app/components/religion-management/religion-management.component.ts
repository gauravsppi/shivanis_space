import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import {Admin} from '../../models/admin.model';
import { AccountService } from '../../services/account.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-religion-management',
  templateUrl: './religion-management.component.html',
  styleUrls: ['./religion-management.component.scss']
})
export class ReligionManagementComponent implements OnInit {
  public adminModel: Admin;
  public addValidationError:Admin;
  religionList:any=[];
  constructor(private accountService: AccountService,private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService) { }
  ngOnInit(): void {
    this.adminModel=<Admin>{
      id:'',
      religion_name:'',
      religion_description:''
    }
    this.addValidationError=<Admin>{
      id:'',
      religion_name:'',
      religion_description:''
    }
    this.getReligion();
  }
  ngAfterViewInit() {
    $('#choosereligionmodal .choosebtn a').click(function (){
      $('.modal-backdrop').addClass('d-none');
    });
  }
  getReligion(){
    debugger;
    this.accountService.ReligionGet().then((response) => {
      debugger;
      if(response.code==200){
        this.religionList=response.result;
      }else{
        
      }
    })
  }
  addReligion(){
    console.log(this.adminModel);
    this.accountService.ReligionsADD(this.adminModel).then((response) => {
      if(response.code==200){
        console.log(response);
      }else{
        console.log(response);
      }
    })
  }
  editReligion(id){
    console.log(this.adminModel.id=id);
  }
}
