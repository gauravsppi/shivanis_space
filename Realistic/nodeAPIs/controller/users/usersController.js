const UsersModel = require("../../model/UsersModel"); //for define users model
const CarsModel = require("../../model/CarsModel"); //for define cars model
var randtoken = require('rand-token').generator();//for genrate random passwords
const path = require('path'); //for define path
const fs = require('fs'); //for file system 
const md5 = require('md5'); //for encrypt passwords
var nodemailer = require('nodemailer'); 
var formidable = require('formidable'); 

module.exports = {	
	/**** Add User *****/
	addUser: function(req, res) { //User signup api
		var data=req.body;	 
		var form = new formidable.IncomingForm();
		form.parse(req, function(err, fields, files){
			var fileName = '';
			var data = fields;
	 		if(Object.keys(data).length > 0){ //Check if parameter passes or not
	 			if(data.email != ''){ 
				  	UsersModel.checkEmailAlradyExit(req.con, data, function(err, result) { //check email exists or not
				  		if (err) throw err;
				  		if(result.length >0 ){
				  			res.send({
				  				"code": 400,
				  				"status": 'alreadyExits',
				  				"message":"Email already exists"
				  			});
				  		}else{  
				  			/***** Save user into DB ******/		                
			                UsersModel.userAdd(req.con, data, function(err, added) { //register User data
			                	if (err) throw err;	
			                	if(added._id != ''){															
			                		res.send({
			                			"code": 200,									
			                			"status": 'success',
			                			"user_id": added._id,
			                			"user_email": data.email,
			                			"user_name": data.name,
			                			"signup_status":'1',
			                			"message":"Sign Up Successfully"
			                		});
			                	}else{
			                		res.send({
			                			"code": 400,
			                			"status": 'notInserted',
			                			"message":"Record Not Insert"
			                		});
			                	}
			                })									
			            }
					})//end user exits function
				  }else{
				  	res.send({
				  		"code": 400,
				  		"status": 'notEmpty',
				  		"message":"Please fill all the required fileds"
				  	});
				  }
				}else{
					res.send({
						"code": 400,
						"status": 'noParameter',
						"message":"Something went wrong"
					});
				}
			})		
	},
	/*** Login user ***/
	userLogin: function(req, res) { 
		var data=req.body;	 
	 	if(Object.keys(data).length > 0){ //Check if parameter passes or not
	 		if(data.email != '' && data.password != '' && data.device_token != '' && data.device_id != ''){ 
	 			UsersModel.userLogin(req.con, data, function(err, result) { //login user
	 				if (err) throw err;						
	 				if(result.length > 0){						
	 					UsersModel.updateDeviceToken(req.con, data, result, function(err, updated) { 
	 						if(err){
	 							res.send({
	 								"code": 400,
	 								"error": err,
	 								"status": 'sqlError',							
	 								"message":"Something went wrong"
	 							});
	 						}else{
	 							if(updated.ok > 0 || updated.nModified > 0 || updated.n > 0){ 								
	 								res.send({
	 									"code": 200,
	 									"result": result[0],
	 									"status": 'success',
	 									"message":"Login successfully"
	 								});
	 							}else{
	 								res.send({
	 									"code": 400,										
	 									"status": 'error',							
	 									"err": err,							
	 									"message":"Something went wrong"
	 								});
	 							}	
	 						}	
	 					});
	 				}else{
	 					res.send({"code": 400,"status": 'notMatch',	"message":"Email and password incorrect" });
	 				}
	 			})			
	 		}else{
	 			res.send({"code": 400,"status": 'notEmpty',"message":"Please fill all the required fileds" });
	 		}
	 	}else{
	 		res.send({"code": 400,"status": 'noParameter',"message":"Something went wrong"});
	 	}
	 },
	updateUserInfo: function(req, res){ //update User info
		var data=req.body;	 
		var form = new formidable.IncomingForm();
		form.parse(req, function(err, fields, files){
			var fileName = '';
			var data = fields;
		 	if(Object.keys(data).length > 0){ //Check if parameter passes or not
		 		if(data.user_id != ''){ 
		 			UsersModel.checkIDExits(req.con, data, function(err, result) { //check email exists or not
		 				if (err) throw err;
		 				if(result.length > 0 ){
		 					/**** Upload User image ********/					  							  		
		 					if(Object.keys(files).length > 0){ 
		 						fileName = Date.now()+path.extname(files.image.name); 	           
		 						var rawData = fs.readFileSync(files.image.path) 
		 						fs.writeFile("public/uploads/users/"+fileName, rawData, function(err) {
		 							if(err){ 
		 								res_data = {'code': 400,'error': err,'message': 'File not uploaded', status: 'error'}
		 								res.send(res_data);
		 							}else{  
		 								console.log('file uploaded successfully');
		 								data.image = fileName;
		 								/** update user info **/
		 								UsersModel.updateUserInfo(req.con, data, function(err, updated){ 
		 									if(err){
		 										res.send({
		 											"code": 400,
		 											"error": err,
		 											"status": 'sqlError',							
		 											"message":"Something went wrong"
		 										});
		 									}else{
		 										if(updated.ok > 0 && updated.nModified > 0 && updated.n > 0){ 
		 											res.send({
		 												"code": 200,
		 												"result": data,
		 												"status": 'success',
		 												"message":"Info updated successfully"
		 											});
		 										}else{
		 											res.send({
		 												"code": 400,										
		 												"status": 'notUpdated',							
		 												"message":"Something went wrong"
		 											});
		 										}	
		 									}
		 								})	
		 							}
							  	})//end fs function
		 					}else{
		 						/** update user info **/
		 						UsersModel.updateUserInfo(req.con, data, function(err, updated){ 
		 							if(err){
		 								res.send({
		 									"code": 400,
		 									"error": err,
		 									"status": 'sqlError',							
		 									"message":"Something went wrong"
		 								});
		 							}else{
		 								if(updated.ok > 0 && updated.nModified > 0 && updated.n > 0){ 
		 									res.send({
		 										"code": 200,
		 										"result": data,
		 										"status": 'success',
		 										"message":"Info updated successfully"
		 									});
		 								}else{
		 									res.send({
		 										"code": 400,										
		 										"status": 'notUpdated',							
		 										"message":"Something went wrong"
		 									});
		 								}	
		 							}
		 						})	
		 					}  			
		 				}else{
		 					res.send({"code": 400, "status": 'notExits', "message":"User ID not exits"});
		 				}
		 			})	
		 		}else{
		 			res.send({"code": 400,"status": 'notEmpty',"message":"Please fill all the required fileds" });
		 		}
		 	}else{
		 		res.send({"code": 400,"status": 'noParameter',"message":"Something went wrong"});
		 	}
		})//end form parsing 		
	},
  	forgotPassword: function(req, res) { //forgot user password
  		var data=req.body;	
	 	if(Object.keys(data).length > 0){ //Check if parameter passes or not
	 		if(data.email != ''){ 
	 			UsersModel.checkEmailAlradyExit( req.con,req.body, function(err,result) {
	 				if (err) throw err;
	 				if(result.length > 0){		
	 					var user_id = result[0]._id
	 					var user_email = result[0].email
	 					var user_name = result[0].name
	 					var otp = randtoken.generate(4, "1234567890")  
	 					var msg = ''				
	 					UsersModel.forgotPassword(req.con, otp, user_id, function(err,updated) {
	 						if (err) throw err;
	 						if(updated.ok > 0 && updated.nModified > 0 && updated.n > 0){ 
	 							var transporter = nodemailer.createTransport({						     
	 								host: 'smtp.sendgrid.net',
	 								port: 587,
								    secure: false, // true for 465, false for other ports
								    auth: {
								        user: 'KTCL3C', // generated ethereal user
								        pass: 'MakingChangeWithChange2020' // generated ethereal password
								    }  
								});
	 							msg = "Hello "+user_name+"<br><br>Forgot Password OTP is :"+otp+"<br><br>Best Regards"									
	 							var mailOptions = {
	 								from: 'Realstic Cars <info@realsticcars.com>', 
	 								to: 'saini.stleath@gmail.com',
									//to: 'reshma.stealth@gmail.com',
									//to: user_email,
									subject: 'Realstic Cars || Reset Password',
									text: 'Testing mail',
									html: msg
								};

								transporter.sendMail(mailOptions, function(error, info){
									if (error) {
										console.log(error);
										res.send({ "code": 400, "error": error,"status":"error", "message":"Something Went Wrong" });
									} else {
										console.log('Email sent: ' + info.response);
										res.send({ "code": 200, "result": user_email,"status":"success", "message":"Email sent successfully" });
									}
								}); 								
							}else{
								res.send({ 
									"code": 400,
									"status":"error",
									"message":"Something Went Wrong"	
								});
							}				
						});
	 				}else{
	 					res.send({
	 						"code": 400,
	 						"status":"notFound", "message":"Record Not Found"
	 					});
	 				}	
	 			});	
	 		}else{
	 			res.send({"code": 400,"status": 'notEmpty',"message":"Please fill all the required fileds" });
	 		}
	 	}else{
	 		res.send({"code": 400,"status": 'noParameter',"message":"Something went wrong"});
	 	}	
	 },	
  	otpVerify: function(req, res) { // reset password
  		var data=req.body;	
	 	if(Object.keys(data).length > 0){ //Check if parameter passes or not
	 		if(data.email != ''){ 	 			
	 			UsersModel.otpVerify(req.con,req.body, function(err,result) {
	 				if (err) throw err;
	 				if(result.length > 0){
	 					res.send({
	 						"code": 200,
	 						"status": 'success',
	 						"message":"OTP verify successfully"
	 					});
	 				}else{
	 					res.send({
	 						"code": 400,
	 						"status": 'notMatch',
	 						"message":"OTP does not match"
	 					});
	 				}
	 			});
	 		}else{
	 			res.send({"code": 400,"status": 'notEmpty',"message":"Please fill all the required fileds" });
	 		}
	 	}else{
	 		res.send({"code": 400,"status": 'noParameter',"message":"Something went wrong"});
	 	}	
	 },
  	resetPassword: function(req, res) { //Reset password
  		var data=req.body;	
	 	if(Object.keys(data).length > 0){ //Check if parameter passes or not
	 		if(data.email != ''){ 
	 			UsersModel.checkEmailAlradyExit(req.con, data, function(err,result) {
	 				if (err) throw err;
	 				if(result.length > 0){	
	 					var user_id = result[0]._id;
	 					UsersModel.resetPassword(req.con, data, user_id, function(err, updated) {
	 						if(err){
	 							res.send({
	 								"code": 400,
	 								"error": err,
	 								"status": 'sqlError',							
	 								"message":"Something went wrong"
	 							});
	 						}else{
	 							if(updated.ok > 0 || updated.nModified > 0 || updated.n > 0){ 
	 								res.send({
	 									"code": 200,
	 									"status": "success",
	 									"message":"Password updated successfully"
	 								});
	 							}else{
	 								res.send({
	 									"code": 400,
	 									"status": 'error',
	 									"message":"Something Went Wrong"
	 								});
	 							}
	 						}	
	 					});
	 				}else{
	 					res.send({
	 						"code": 400,
	 						"status": 'notExits',
	 						"message":"Email does not exits"
	 					});
	 				}   
	 			});  
	 		}else{
	 			res.send({"code": 400,"status": 'notEmpty',"message":"Please fill all the required fileds" });
	 		}
	 	}else{
	 		res.send({"code": 400,"status": 'noParameter',"message":"Something went wrong"});
	 	}		 
	 },  
  	changePassword: function(req, res) { //change password
  		var data=req.body;	
	 	if(Object.keys(data).length > 0){ //Check if parameter passes or not
	 		if(data.email != ''){ 
	 			UsersModel.checkEmailAlradyExit(req.con, data, function(err,result) {
	 				if (err) throw err;
	 				if(result.length > 0){
	 					var user_id = result[0]._id;
	 					if(result[0].password == md5(data.old_password)){ 
	 						UsersModel.changePassword(req.con, data, user_id, function(err, updated) {
	 							if(err){
	 								res.send({
	 									"code": 400,
	 									"error": err,
	 									"status": 'sqlError',							
	 									"message":"Something went wrong"
	 								});
	 							}else{	
	 								if(updated.ok > 0 && updated.nModified > 0 && updated.n > 0){ 
	 									res.send({
	 										"code": 200,
	 										"status": 'success',
	 										"message":"Password Updated Successfully"
	 									});
	 								}else{
	 									res.send({
	 										"code": 400,
	 										"status": 'success',
	 										"message":"Something Went Wrong"
	 									});
	 								}
	 							}	
	 						});
	 					}else{
	 						res.send({
	 							"code": 400,
	 							"status": 'success',
	 							"message":"Old Password Incorrect"
	 						});
	 					}			      
	 				}else{
	 					res.send({
	 						"code": 400,
	 						"status": 'notExits',
	 						"message":"Email not exits"
	 					});
	 				}   
	 			}); 
	 		}else{
	 			res.send({"code": 400,"status": 'notEmpty',"message":"Please fill all the required fileds" });
	 		}
	 	}else{
	 		res.send({"code": 400,"status": 'noParameter',"message":"Something went wrong"});
	 	}	 	 
	 },
	 /**** Add User car details *****/
	addUserCar: function(req, res) { //User signup api
		var data=req.body;	 
		var form = new formidable.IncomingForm();
		form.multiples = true;
		form.parse(req, function(err, fields, files){
			var fileName = '';
			var data = fields;
			console.log('Add User CAr data:;', data);
			console.log('Add User CAr files:;', files);
	 		if(Object.keys(data).length > 0){ //Check if parameter passes or not
	 			if(data.user_id != '' && data.company_id != 0 && data.model_id != 0 && data.company_id != '' && data.model_id != ''){ 
				  	UsersModel.checkIDExits(req.con, data, function(err, result) { //check email exists or not
				  		if (err) throw err;
				  		if(result.length == 0 ){
				  			res.send({
				  				"code": 400,
				  				"status": 'notExits',
				  				"message":"User not exits"
				  			});
				  		}else{  
				  			/***** Upload car images *******/
				  			var image_arr= [];
				  			if(Object.keys(files).length > 0){ 
				  				console.log('array', Array.isArray(files.image));
				  				if(Array.isArray(files.image)){ 
				  					var files_count = files.image.length;
				  					var j= 1;
				  					for(i=0; i<files_count; i++){ 
				  						fileName = Date.now()+i+path.extname(files.image[i].name);
				  						var rawData = fs.readFileSync(files.image[i].path) 
				  						image_arr.push(fileName);
				  						fs.writeFile("public/uploads/cars/"+fileName, rawData, function(err) {
				  							console.log('Images name:: ', fileName); 
				  							if(err){ 
				  								res_data = {'code': 400,'error': err,'message': 'File not uploaded', status: 'error'}
				  								res.send(res_data);
				  							}else{  
				  								if(files_count == j){
				  									console.log('response sent to client....');
				  									data.images = image_arr;							                		
				  									/***** Save user into DB ******/		                
									                UsersModel.addUserCar(req.con, data, function(err, added) { //register User data
									                	if (err) throw err;	
									                	if(added._id != ''){	
									                		res.send({
									                			"code": 200,									
									                			"status": 'success',
									                			"message":"Car added Successfully"
									                		});
									                	}else{
									                		res.send({
									                			"code": 400,
									                			"status": 'notInserted',
									                			"message":"Record Not Insert"
									                		});
									                	}
									                })					                		
									            }
									            j++;
									        }
									    })							        
				  					}  
				  				}else{
				  					fileName = Date.now()+path.extname(files.image.name);
				  					var rawData = fs.readFileSync(files.image.path) 
				  					fs.writeFile("public/uploads/cars/"+fileName, rawData, function(err) {
				  						if(err){ 
				  							res_data = {'code': 400,'error': err,'message': 'File not uploaded', status: 'error'}
				  							res.send(res_data);
				  						}else{  
				  							image_arr.push(fileName);
				  							data.images = image_arr;							                		
				  							/***** Save user into DB ******/		                
							                UsersModel.addUserCar(req.con, data, function(err, added) { //register User data
							                	if (err) throw err;	
							                	if(added._id != ''){	
							                		res.send({
							                			"code": 200,									
							                			"status": 'success',
							                			"message":"Car added Successfully"
							                		});
							                	}else{
							                		res.send({
							                			"code": 400,
							                			"status": 'notInserted',
							                			"message":"Record Not Insert"
							                		});
							                	}
							                })	
							            }
							        })
				  				}     
				  			}else{
				  				data.images = [];
				  				/***** Save user into DB ******/		                
				                UsersModel.addUserCar(req.con, data, function(err, added) { //register User data
				                	if (err) throw err;	
				                	if(added._id != ''){															
				                		res.send({
				                			"code": 200,									
				                			"status": 'success',
				                			"message":"Car added Successfully"
				                		});
				                	}else{
				                		res.send({
				                			"code": 400,
				                			"status": 'notInserted',
				                			"message":"Record Not Insert"
				                		});
				                	}
				                })	
				            }  							
				        }
					})//end user exits function
}else{
	res.send({
		"code": 400,
		"status": 'notEmpty',
		"message":"Please fill all the required fileds"
	});
}
}else{
	res.send({
		"code": 400,
		"status": 'noParameter',
		"message":"Something went wrong"
	});
}
})		
},
/**** Get User Car details *****/
getUserCar: function(req, res) { 
	var data=req.body;	
		if(Object.keys(data).length > 0){ //Check if parameter passes or not
			if(data.user_id != ''){ 
				UsersModel.checkIDExits(req.con, data, function(err, exits) { //check email exists or not
					if (err) {
						res.send({"code": 400,"status": 'sqlError',"error": err, "message":"Something went wrong"	});
					}else{ 
						if(exits._id != '' ){
				  			UsersModel.getUserCars(req.con, data, function(err, result) { //check email exists or not
				  				console.log('data: ', result);
				  				if (err) throw err;
				  				if(result.length > 0 ){
				  					res.send({"code": 200,"result": result , "status": 'success', "message":"Records fetch Successfully" });
				  				}else{
				  					res.send({"code": 200,"status": 'notFound',"message":"Record not found"	});
				  				}
				  			})
				  		}else{
				  			res.send({ "code": 400,"status": 'notExits',"message":"User ID not exists" });
				  		}
				  	}	
				  })
			}else{
				res.send({ "code": 400,"status": 'notEmpty',"message":"Please fill all the required fileds" });
			}	
		}else{
			res.send({ "code": 400,"status": 'noParameter',"message":"Something went wrong" });
		}
	}, 
	/**** Update User Car details *****/
	
	/**** Update User Car details *****/
	updateUserCar: function(req, res) { //User signup api
		var data=req.body;	 
		var form = new formidable.IncomingForm({ multiples: true });
		form.parse(req, function(err, fields, files){
			var fileName = '';
			var data = fields;
			console.log('Update image data: ', data);
	 		if(Object.keys(data).length > 0){ //Check if parameter passes or not
	 			if(data.user_id != ''){ 
				  	UsersModel.checkIDExits(req.con, data, function(err, result) { //check email exists or not
				  		if (err) throw err;
				  		if(result.length == 0 ){
				  			res.send({
				  				"code": 400,
				  				"status": 'notExits',
				  				"message":"User not exits"
				  			});
				  		}else{  
				  			/***** Upload car images *******/
				  			var image_arr= [];
				  			if(Object.keys(files).length > 0){ 
				  				if(Array.isArray(files.image)){ 
				  					var files_count = files.image.length;
				  					var j= 1;
				  					for(i=0; i<files_count; i++){ 
				  						fileName = Date.now()+i+path.extname(files.image[i].name);
				  						var rawData = fs.readFileSync(files.image[i].path) 
				  						image_arr.push(fileName);
				  						fs.writeFile("public/uploads/cars/"+fileName, rawData, function(err) {
				  							console.log('Images name:: ', fileName); 
				  							if(err){ 
				  								res_data = {'code': 400,'error': err,'message': 'File not uploaded', status: 'error'}
				  								res.send(res_data);
				  							}else{  
				  								if(files_count == j){						                	
				  									data.images = image_arr;							                		
				  									/***** Save user into DB ******/		                
									                UsersModel.updateUserCar(req.con, data, function(err, updated) { //register User data
									                	if (err) throw err;	
									                	if(updated.ok > 0 && updated.nModified > 0 && updated.n > 0){ 
									                		data.old_images = JSON.parse(data.old_images);
									                		if(data.old_images.length > 0){ 
									                			/** Unlink images ***/
									                			var old_files_count = data.old_images.length;
									                			var K= 1;
									                			for(i=0;  i < old_files_count; i++){ 
									                				var path_img = 'public/uploads/cars/'+data.old_images[i]
									                				try {
									                					fs.unlinkSync(path_img)
																	   //file removed
																	} catch(err) {
																	    //console.log(err)
																	}
																	if(K == old_files_count){ 
																		res.send({
																			"code": 200,									
																			"status": 'success',
																			"message":"Car details updated Successfully"
																		});
																	}
																	K++;	
																}	
															}else{
																res.send({
																	"code": 200,									
																	"status": 'success',
																	"message":"Car details updated Successfully"
																});
															}	
														}else{
															res.send({
																"code": 400,
																"status": 'notUpdated',
																"message":"Something went wrong"
															});
														}
													})					                		
									            }
									            j++;
									        }
									    })							        
				  					}   
				  				}else{
				  					fileName = Date.now()+path.extname(files.image.name);
				  					var rawData = fs.readFileSync(files.image.path) 
				  					image_arr.push(fileName);
				  					fs.writeFile("public/uploads/cars/"+fileName, rawData, function(err) {
				  						if(err){ 
				  							res_data = {'code': 400,'error': err,'message': 'File not uploaded', status: 'error'}
				  							res.send(res_data);
				  						}else{ 

				  							data.images = image_arr;							                		
				  							/***** Save user into DB ******/		                
							                UsersModel.updateUserCar(req.con, data, function(err, updated) { //register User data
							                	if (err) throw err;	
							                	if(updated.ok > 0 && updated.nModified > 0 && updated.n > 0){ 
							                		data.old_images = JSON.parse(data.old_images);
							                		if(data.old_images.length > 0){ 
							                			/** Unlink images ***/
							                			var old_files_count = data.old_images.length;
							                			var K= 1;
							                			for(i=0;  i < old_files_count; i++){ 
							                				var path_img = 'public/uploads/cars/'+data.old_images[i]
							                				try {
							                					fs.unlinkSync(path_img)
															   //file removed
															} catch(err) {
															    //console.log(err)
															}
															if(K == old_files_count){ 
																res.send({
																	"code": 200,									
																	"status": 'success',
																	"message":"Car details updated Successfully"
																});
															}
															K++;	
														}	
													}else{
														res.send({
															"code": 200,									
															"status": 'success',
															"message":"Car details updated Successfully"
														});
													}	
												}else{
													res.send({
														"code": 400,
														"status": 'notUpdated',
														"message":"Something went wrong"
													});
												}
											})
							            }
							        })	
				  				}    
				  			}else{
				  				data.images = data.old_images;
				  				/***** Save user into DB ******/		                
				                UsersModel.updateUserCar(req.con, data, function(err, added) { //register User data
				                	if (err) throw err;	
				                	if(added._id != ''){															
				                		res.send({
				                			"code": 200,									
				                			"status": 'success',
				                			"message":"Car details updated Successfully"
				                		});
				                	}else{
				                		res.send({
				                			"code": 400,
				                			"status": 'notUpdated',
				                			"message":"Something went wrong"
				                		});
				                	}
				                })	
				            }  							
				        }
					})//end user exits function
}else{
	res.send({
		"code": 400,
		"status": 'notEmpty',
		"message":"Please fill all the required fileds"
	});
}
}else{
	res.send({
		"code": 400,
		"status": 'noParameter',
		"message":"Something went wrong"
	});
}
})		
},
deleteUserCar: function(req, res, callback){
	var data=req.body;	
		if(Object.keys(data).length > 0){ //Check if parameter passes or not
			if(data.user_id != ''){ 
				UsersModel.checkIDExits(req.con, data, function(err, exits) { //check email exists or not
					if (err) {
						res.send({"code": 400,"status": 'sqlError',"error": err, "message":"Something went wrong"	});
					}else{ 
						if(exits._id != '' ){
				  			UsersModel.deleteUserCar(req.con, data, function(err, deleted) { //check email exists or not
				  				if (err) throw err;
				  				if(deleted._id != '' ){
				  					res.send({"code": 200,"status": 'success', "message":"Car deleted Successfully" });
				  				}else{
				  					res.send({"code": 400,"status": 'error',"message":"Something went wrong" });
				  				}
				  			})
				  		}else{
				  			res.send({ "code": 400,"status": 'notExits',"message":"User ID not exists" });
				  		}
				  	}	
				  })
			}else{
				res.send({ "code": 400,"status": 'notEmpty',"message":"Please fill all the required fileds" });
			}	
		}else{
			res.send({ "code": 400,"status": 'noParameter',"message":"Something went wrong" });
		}
	},
	saveContactForm: function(req, res, callback){
		var data=req.body;	
		if(Object.keys(data).length > 0){ //Check if parameter passes or not
			if(data.user_id != ''){ 
				UsersModel.checkIDExits(req.con, data, function(err, exits) { //check email exists or not
					if (err) {
						res.send({"code": 400,"status": 'sqlError',"error": err, "message":"Something went wrong"	});
					}else{ 
						if(exits._id != '' ){
				  			UsersModel.saveContactForm(req.con, data, function(err, saved) { //check email exists or not
				  				if (err) throw err;
				  				if(saved._id != '' ){
				  					res.send({"code": 200,"status": 'success', "message":"Contact info saved successfully" });
				  				}else{
				  					res.send({"code": 400,"status": 'error',"message":"Something went wrong" });
				  				}
				  			})
				  		}else{
				  			res.send({ "code": 400,"status": 'notExits',"message":"User ID not exists" });
				  		}
				  	}	
				  })
			}else{
				res.send({ "code": 400,"status": 'notEmpty',"message":"Please fill all the required fileds" });
			}	
		}else{
			res.send({ "code": 400,"status": 'noParameter',"message":"Something went wrong" });
		}
	},
	/**** Get User try parts images *****/
	getUserTryPartsImage: function(req, res) { 
		var data=req.body;	
		if(Object.keys(data).length > 0){ //Check if parameter passes or not
			if(data.user_id != ''){ 
				UsersModel.checkIDExits(req.con, data, function(err, exits) { //check email exists or not
					if (err) {
						res.send({"code": 400,"status": 'sqlError',"error": err, "message":"Something went wrong"	});
					}else{ 
						if(exits._id != '' ){
				  			CarsModel.getUserTryPartsImage(req.con, data, function(err, result) { //check email exists or not
				  				console.log('data: ', result);
				  				if (err) throw err;
				  				if(result.length > 0 ){
				  					res.send({"code": 200,"result": result , "status": 'success', "message":"Records fetch successfully" });
				  				}else{
				  					res.send({"code": 200,"status": 'notFound',"message":"Record not found"	});
				  				}
				  			})
				  		}else{
				  			res.send({ "code": 400,"status": 'notExits',"message":"User ID not exists" });
				  		}
				  	}	
				  })
			}else{
				res.send({ "code": 400,"status": 'notEmpty',"message":"Please fill all the required fileds" });
			}	
		}else{
			res.send({ "code": 400,"status": 'noParameter',"message":"Something went wrong" });
		}
	}, 
	/**** Delete User try parts images *****/
	deleteUserTryPartsImage: function(req, res) { 
		var data=req.body;	
		if(Object.keys(data).length > 0){ //Check if parameter passes or not
			if(data.user_id != ''){ 
				UsersModel.checkIDExits(req.con, data, function(err, exits) { //check email exists or not
					if (err) {
						res.send({"code": 400,"status": 'sqlError',"error": err, "message":"Something went wrong"	});
					}else{ 
						if(exits._id != '' ){
							CarsModel.deleteUserTryPartsImage(req.con, data, function(err, deleted) {
								if (err) throw err;
								if(deleted._id != '' ){
									res.send({"code": 200, "status": 'success', "message":"Image deleted successfully" });
								}else{
									res.send({"code": 200,"status": 'notDeleted',"message":"Something went wrong"	});
								}
							})
						}else{
							res.send({ "code": 400,"status": 'notExits',"message":"User ID not exists" });
						}
					}	
				})
			}else{
				res.send({ "code": 400,"status": 'notEmpty',"message":"Please fill all the required fileds" });
			}	
		}else{
			res.send({ "code": 400,"status": 'noParameter',"message":"Something went wrong" });
		}
	}, 

	AddPart_to_cart: function(req, res) { //Add part to cart API
       var data=req.body;	
       console.log('CUrrent Scene',data);
		if(Object.keys(data).length > 0){ //Check if parameter passes or not
			if(data.user_id != ''){ 
				UsersModel.checkIDExits(req.con, data, function(err, exits) { //check email exists or not
					if (err) {
						res.send({"code": 400,"status": 'sqlError',"error": err, "message":"User Not Exist"	});
					}else{ 
						if(exits._id != '' ){
                         /***** Save user into DB ******/		                
			                UsersModel.addtoCart(req.con, data, function(err, added) { //register User data
			                	if (err) throw err;	
			                	if(added._id != ''){															
			                		res.send({
			                			"code": 200,									
			                			"status": 'success',
			                			"id": added._id,
			                			"message":"Part Added to Cart Successfully"
			                		});
			                	}else{
			                		res.send({
			                			"code": 400,
			                			"status": 'notInserted',
			                			"message":"Record Not Insert"
			                		});
			                	}
			                })									
			            }
					}//end user exits function
              })}else{
                   res.send({
                    "code": 400,
                    "status": 'notEmpty',
                    "message":"Please fill all the required fileds"
                });
               }
           }else{
             res.send({
              "code": 400,
              "status": 'noParameter',
              "message":"Something went wrong"
          });
         }		
     },
   getAddedCartParts: function(req, res) { //User Get Cart Details API
       var data=req.body; 
        if(Object.keys(data).length > 0){ //Check if parameter passes or not
            if(data.user_id != ''){ 
                UsersModel.checkIDExits(req.con, data, function(err, exits) { //check email exists or not
                    if (err) {
                        res.send({"code": 400,"status": 'sqlError',"error": err, "message":"User Not Exist" });
                    }else{ 
                        if(exits._id != '' ){
                         /***** Get user into DB ******/                     
                            UsersModel.getUserCartDetails(req.con, data, function(err, result) { 
                            if (err) throw err; 
                                if(result.length > 0 ){                                                         
                                    res.send({
                                        "code": 200,                                    
                                        "status": 'success',
                                        "result": result,
                                        "message":"Added Part Show Successfully"
                                    });
                                }else{
                                    res.send({
                                        "code": 200,
                                        "status": 'success',
                                        "message":"Your Cart is Empty!"
                                    });
                                }
                            })                                  
                        }
                    }//end user exits function
                })}else{
                   res.send({
                    "code": 400,
                    "status": 'notEmpty',
                    "message":"Please fill all the required fileds"
                });
               }
           }else{
             res.send({
              "code": 400,
              "status": 'noParameter',
              "message":"Something went wrong"
          });
         }        
     },
    deleteCartPart: function(req, res) { //User Get Cart Details API
        var data=req.body; 
        if(Object.keys(data).length > 0){ //Check if parameter passes or not
            if(data.user_id != ''){ 
                UsersModel.checkIDExits(req.con, data, function(err, exits) { //check email exists or not
                    if (err) {
                        res.send({"code": 400,"status": 'sqlError',"error": err, "message":"User Not Exist" });
                    }else{ 
                        if(exits._id != '' ){
                         /***** Delete user into DB ******/                     
                            UsersModel.deleteCartAddedParts(req.con, data, function(err, deleted) { 
                                console.log('here is the response',deleted);
                                if (err) throw err; 
                                if(deleted._id != '' ){
                                    res.send({"code": 200,"status": 'success', "message":"Car part deleted Successfully" });
                                }else{
                                    res.send({"code": 400,"status": 'error',"message":"Something went wrong" });
                                }
                            })                                  
                        }
                    }//end user exits function
                })}else{
                   res.send({
                    "code": 400,
                    "status": 'notEmpty',
                    "message":"Please fill all the required fileds"
                });
               }
           }else{
             res.send({
              "code": 400,
              "status": 'noParameter',
              "message":"Something went wrong"
          });
         }        
     },


	// Get Data Filteration Based on User Car Selection
	getAllParts_UserCarBased: function(req, res){
		var data=req.body;	
		if(Object.keys(data).length > 0){ //Check if parameter passes or not
			if(data.user_id != '' && data.car_id != ''){ 
				UsersModel.checkIDExits(req.con, data, function(err, exits) { //check email exists or not
					if (err) {
						res.send({"code": 400,"status": 'sqlError',"error": err, "message":"Something went wrong"	});
					}else{ 
						if(exits._id != '' ){
				  			UsersModel.getCompanyAndModelDetails(req.con, data, function(err, result) { //check email exists or not
				  				if (err) throw err;
				  				if(result.length > 0 ){
				  					UsersModel.getRecordsCompanyModelBased(req.con, result, function(err, results){
				  						/*start*/
				  						if(results.length > 0 ){
				  							UsersModel.getFilteredRecords(req.con, results, function(err, resultss){
				  								if(resultss.length>0){
				  									res.send({"code": 200,"result": resultss , "status": 'success', "message":"Records fetch successfully" });
				  								}else{
				  									/*If Record Not Found then Send All the Records of Parts*/
				  									UsersModel.getAllPartsRecords(req.con, data, function(err, resultts){
				  										if(resultts.length>0){
				  											res.send({"code": 200,"result": resultts , "status": 'success', "message":"Records fetch successfully" });
				  										}else{
				  											res.send({"code": 401,"status": 'Record not found00',"message":"Record not found"	});	
				  										}
				  									})	
				  								}		
				  							})		
                                      }
                                      else{
                                       /*If Record Not Found then Send All the Records of Parts*/
                                       UsersModel.getAllPartsRecords(req.con, data, function(err, resultts){
                                        if(resultts.length>0){
                                         res.send({"code": 200,"result": resultts , "status": 'success', "message":"Records fetch successfully" });
                                     }else{
                                         res.send({"code": 401,"status": 'Record not found00',"message":"Record not found"	});	
                                     }
                                 })
                                   }
                                   /*close*/
                               })		
                                }else{
                                 res.send({"code": 401,"status": 'Record not found',"message":"Record not found"	});
                             }
                         })
                      }else{
                       res.send({ "code": 400,"status": 'notExits',"message":"User ID not exists" });
                   }
               }	
           })
            }else{
             res.send({ "code": 400,"status": 'notEmpty',"message":"Please fill all the required fileds" });
         }	
     }else{
        res.send({ "code": 400,"status": 'noParameter',"message":"Something went wrong" });
    }
}

}


