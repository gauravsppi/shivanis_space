import { TestBed } from '@angular/core/testing';

import { EmployerSignupLoginService } from './employer-signup-login.service';

describe('EmployerSignupLoginService', () => {
  let service: EmployerSignupLoginService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EmployerSignupLoginService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
