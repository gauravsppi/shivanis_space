import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import{ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  currentUrl:string;
  dashboard:boolean=false;
  adminManage:boolean=false;
  careWorker:boolean=false;
  careReceiver:boolean=false;
  religion:boolean=false;
  showAgency:boolean=false;
  postRequest:boolean=false;
  complete:boolean=false;
  pending:boolean=false;
  newrequest:boolean=false;
  inprogress:boolean=false;
  role:string;
  constructor(private router:Router) { }

  ngOnInit(): void {
    this.currentUrl=this.router.url;
    this.checkRole();
  }
  ngAfterViewInit() {
    $('.sidebar-inner li.has_sub').click(function(){
      $(this).toggleClass('open');
      $(this).find('ul').toggle(500);
    });
    $('.button-menu-mobile').click(function(){
      $('.side-menu').toggleClass('menuopen')
    });
  }
  checkRole(){
    this.role=localStorage.getItem('role');
    if(this.role=='agency'){
      this.showAgency=true;
    }else{
      this.showAgency=false;
    }
  }
}
