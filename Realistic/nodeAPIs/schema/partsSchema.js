const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var partsSchema = new mongoose.Schema({
    part_image: {
        type: String     
    }, 
    company_id: {
        type: String     
    }, 
    model_id: {
        type: String     
    }, 
    car_name: {
        type: String     
    }, 
    description: {
        type: String     
    }, 
    link: {
        type: String     
    },
    status: {
        type: String     
    },  
    created_at: {
        type: Date,
        default: Date.now
    }, 
    updated_at: {
        type: Date,
        default: Date.now
    },    
});


partsSchema.set('toJSON', { virtuals: true });

partsSchema.virtual('company_details',{
    ref: 'car_company',
    localField: 'company_id',
    foreignField: '_id',
    justOne: true
});
partsSchema.virtual('models_details',{
    ref: 'car_models',
    localField: 'model_id',
    foreignField: '_id',
    justOne: true
});


module.exports = partsSchema;
