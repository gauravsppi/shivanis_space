import { Injectable, Input } from '@angular/core';
import { BaseHttpService } from '../base-http.service';
import { environment } from '../../../environments/environment';
import { candidateLogin_model } from '../../models/candidateLogin_model';
import { admin_model } from '../../models/admin_model';
import { employer_model } from '../../models/employer_model';
@Injectable({
  providedIn: 'root'
})
export class EmployerSignupLoginService {
  private screenStatus = environment.apiBaseUrl + "Employer/updateStatus";
  private updateScreen = environment.apiBaseUrl + "Employer/updateScreenStatus";

  constructor(private baseHttpService: BaseHttpService) { }
  statusUpdateVerify(employerModel: employer_model): Promise<any> {
    return this.baseHttpService.Post(this.screenStatus, employerModel)
      .then(function (response) {
        return response.json();
      });
  }
  updateStatusOfScreen(employerModel: employer_model): Promise<any> {
    return this.baseHttpService.Post(this.updateScreen, employerModel)
      .then(function (response) {
        return response.json();
      });
  }
  
}
