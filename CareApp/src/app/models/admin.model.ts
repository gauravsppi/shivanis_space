export class Admin {
    email:string;
    password:string;
    image:any;
    name:string;
    phoneno:string;
    id:string;
    religion_name:string;
    religion_description:string;
}
