import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostDetailsEditComponent } from './post-details-edit.component';

describe('PostDetailsEditComponent', () => {
  let component: PostDetailsEditComponent;
  let fixture: ComponentFixture<PostDetailsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostDetailsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostDetailsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
