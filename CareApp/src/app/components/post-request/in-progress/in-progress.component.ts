import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "../../../shared/services/spinner.service";
import { PostRequestService } from '../../../shared/services/post-request/post-request.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-in-progress',
  templateUrl: './in-progress.component.html',
  styleUrls: ['./in-progress.component.scss']
})
export class InProgressComponent implements OnInit {
  ProgressRequestArray: any = [];
  page = 1;
  count : number;
  pageSize = 10;
  postDetail: any;
  selectedIndex: any;
  constructor(
    private PostRequestService: PostRequestService,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.spinner.show();
    this.fetchRecords().toPromise().then((res) => {
      console.log('res++++++++++++',res);
      this.spinner.hide();
      if(res.status == 1){
        this.ProgressRequestArray = res.data
        this.count = res.numOfPendingRequest
      }else{
        Swal.fire({ icon: 'error', title: 'Oops...', text: res.message })
      }
    })
    .catch(err=> { 
      this.spinner.hide();
      console.log(err) 
    });
  }

  fetchRecords () {
    var Params = {
      page:  this.page, 
      pageSize: this.pageSize,
    }
    return this.PostRequestService.fetch_progress_requests(Params);
  }


  onTableDataChange(event){
    this.page = event;
    this.ngOnInit();
  }

  pickCancel(post, index): void{
    this.postDetail = post;
    this.selectedIndex = index
  }



  cancelPost(status): void {
    this.spinner.show();
    this.cancelPostRequest(status).toPromise().then((res) => {
      console.log('res++++++++++++',res);
      this.spinner.hide();
      if(res.status == 1){
        this.ngOnInit();
        Swal.fire( 'Canceled!', 'Post has been Canceled.', 'success');
        this.ProgressRequestArray.splice(this.selectedIndex , 1);

      }else{
        Swal.fire({ icon: 'error', title: 'Oops...', text: res.message })
      }
    })
    .catch(err=> { 
      this.spinner.hide();
      console.log(err) 
    });
  }

  cancelPostRequest (status) {
    var Params = {
      postId:  this.postDetail.postId, 
      receiverId:  this.postDetail.receiverId, 
      workerId:  this.postDetail.workerId, 
      _id:  this.postDetail._id, 
      status: status,
    }
    return this.PostRequestService.cancel_post_requests(Params);
  }



  completePost(status): void {
    this.spinner.show();
    this.completePostRequest(status).toPromise().then((res) => {
      console.log('res++++++++++++',res);
      this.spinner.hide();
      if(res.status == 1){
        this.ngOnInit();
        Swal.fire( 'Completed!', 'Post has been completed.', 'success');
        this.ProgressRequestArray.splice(this.selectedIndex , 1);

      }else{
        Swal.fire({ icon: 'error', title: 'Oops...', text: res.message })
      }
    })
    .catch(err=> { 
      this.spinner.hide();
      console.log(err) 
    });
  }

  completePostRequest (status) {
    var Params = {
      postId:  this.postDetail.postId, 
      receiverId:  this.postDetail.receiverId, 
      workerId:  this.postDetail.workerId, 
      _id:  this.postDetail._id, 
      status: status,
    }
    return this.PostRequestService.complete_post_requests(Params);
  }


}
