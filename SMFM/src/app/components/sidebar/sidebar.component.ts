import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Headers, Http } from '@angular/http';
import { AccountService } from '../../services/account.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public path = '';
  dashboardActive : boolean = true;
  selActive : boolean = false;
  recommendActive : boolean = false;
  abuseActive : boolean = false;
  IEPActive : boolean = false;
  courseActive : boolean = false;
  contentActive : boolean = false;
  announcementActive : boolean = false;
  observationActive : boolean = false;
  interventionActive : boolean = false;
  contactsActive : boolean = false;
  observationsActive : boolean = false;
  adminShow:boolean=false;
  menuShow: boolean = false; 


  constructor(
    private router: Router, private accountService: AccountService, private route: ActivatedRoute, private http: Http) {  }
  
  admin_id = localStorage.getItem('admin_id');

  ngOnInit(): void {
    this.menuCheck(this.admin_id);
    console.log(this.menuCheck(this.admin_id));
  }
  
  menuCheck(admin_id){ 
    this.path =  this.route.snapshot.url[0].path;
    console.log('current URL>>>',this.path);
    if(this.path == 'list-feedback'){
      this.contactsActive = true;
      this.dashboardActive = false;
    }
    if(this.path == 'school-view' || this.path == 'school-list' || this.path == 'teacher-list'){
      this.dashboardActive = true
    }
    if(this.path == 'abuse-words-list'){
      this.abuseActive = true;
      this.dashboardActive = false;
    }
    if(this.path == 'recommendations' ){
      this.dashboardActive = false;      
      this.recommendActive = true;      
    }
    if(this.path == 'category' || this.path == 'lesson' || this.path == 'add-lesson' || this.path == 'edit-lesson' || this.path == 'view-lesson'){      
      this.dashboardActive = false;
      this.selActive = true;
    }
    if(this.path == 'student-iep' ){
      this.dashboardActive = false;
      this.IEPActive = true;      
    }
    if(this.path == 'courses-list' ){      
      this.dashboardActive = false;      
      this.courseActive = true;
    }
    if(this.path == 'announcement-list' ||  this.path == 'add-announcement' || this.path == 'edit-announcement'){ 
      this.dashboardActive = false;
      this.announcementActive = true;
    }
    if(this.path == 'list-content' ||  this.path == 'edit-content'){
      this.dashboardActive = false;
      this.contentActive = true;      
    }
    if(this.path == 'antecedent-category' ||  this.path == 'list-observation-sub-category' || this.path == 'list-observation-sub-sub-category'){
      this.dashboardActive = false;     
      this.observationActive = true;
    }
    if(this.path == 'list-intervention'){
      this.dashboardActive = false;
      this.interventionActive = true;
    }
    if(this.path=='observations-list'){
      this.dashboardActive = false;     
      this.observationsActive = true;
    }
    if(admin_id == 1){
      this.menuShow =  true;
    }else if(admin_id==3){
      this.adminShow=true;
    }
  }  

}
