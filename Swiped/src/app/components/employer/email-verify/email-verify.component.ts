import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from '../../../services/account.service';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import { candidateLogin_model } from '../../../models/candidateLogin_model';

@Component({
  selector: 'app-email-verify',
  templateUrl: './email-verify.component.html',
  styleUrls: ['./email-verify.component.scss']
})
export class EmailVerifyComponent implements OnInit {
  public loginModel: candidateLogin_model;
  public addValidationError: candidateLogin_model;
  sucMsg: boolean = false;
  errMsg: boolean = false;
  message: string;
  id: any;
  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService) { }

  ngOnInit(): void {
    // this.id = setInterval(() => {
    //   this.getalldata();
    // }, 2000);
    // console.log(localStorage.getItem('token'));
  }
  ngOnDestroy() {
    if (this.id) {
      clearInterval(this.id);
    }
  }

  //-----------------------get status--------------------
  getalldata() {
    this.accountService.StatusGet().then((response) => {
      if (response.code == 200) {
        console.log(response.result);
        if (response.result.login_type_status == 0 && response.result.status == 0  ) {
          this.router.navigate(['/verify_email']);
        }  else {
          //this.router.navigate(['/candidate_dashboard']);
        }
        this.sucMsg = true;
      } else {
        this.errMsg = true;
      }
    })
  }

 //------------------------- after email verify sign in button------------------
 signin(){
  localStorage.clear();
  this.router.navigate(['/employer-login']);
}

}
