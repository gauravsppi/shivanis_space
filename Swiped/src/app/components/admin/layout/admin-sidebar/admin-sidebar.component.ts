import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-admin-sidebar',
  templateUrl: './admin-sidebar.component.html',
  styleUrls: ['./admin-sidebar.component.scss']
})
export class AdminSidebarComponent implements OnInit {
  url: string;
  Employer: boolean = false;
  Candidate: boolean = false;
  constructor( private route: ActivatedRoute, private router: Router)  { }
  
  ngOnInit(): void {
    this.url = this.router.url;
    //alert(this.url)
    if (this.url.includes("/admin/employers/new") || this.url.includes("/admin/employers/all") || this.url.includes("/admin/employers/approved") || this.url.includes("/admin/employers/rejected")) {
      this.Employer = true;
      }else if (this.url.includes("/admin/candidates/new") || this.url.includes("/admin/candidates/all") || this.url.includes("/admin/candidates/approved") || this.url.includes("/admin/candidates/rejected")) {
        this.Candidate = true;
        }
  }
  ngAfterViewInit() {
    $('.sidebar-inner li.has_sub').click(function(){
      $(this).toggleClass('open');
      $(this).find('ul').toggle(500);
    });
    $('.button-menu-mobile').click(function(){
      $('.side-menu').toggleClass('menuopen')
    });
  }
}

