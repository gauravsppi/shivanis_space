import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidateEmailVerifyComponent } from './candidate-email-verify.component';

describe('CandidateEmailVerifyComponent', () => {
  let component: CandidateEmailVerifyComponent;
  let fixture: ComponentFixture<CandidateEmailVerifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidateEmailVerifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidateEmailVerifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
