export class IEP_model {    
    name: string;
    admin_id: string;
    school_id: string;
    type: string;
    records: [];
    IEP_id: string;
    page: number;
    searchKey: string;
    sortKey: string;
    sortBy: string;
    fileName: File;
 }