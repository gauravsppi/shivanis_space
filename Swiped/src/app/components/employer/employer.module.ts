import { NgModule } from '@angular/core';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CampaignDetailComponent } from '../../components/employer/campaign-detail/campaign-detail.component';
import { ChoosePlanComponent } from '../../components/employer/choose-plan/choose-plan.component';
import { EmailVerifyComponent } from '../../components/employer/email-verify/email-verify.component';
import { HeaderComponent } from '../../components/employer/header/header.component';
import { CandidateListComponent } from '../../components/employer/candidate-list/candidate-list.component';
import { EmployerRoutingModule } from './employer-routing.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { EmployerDashboardComponent } from './employer-dashboard/employer-dashboard.component';
import { JobManageComponent } from './job-manage/job-manage.component';
import { ViewJobComponent } from './view-job/view-job.component';
import { EmployerSignupComponent } from './employer-signup/employer-signup.component';
import { EmployerLoginComponent } from './employer-login/employer-login.component';
import { EmployerAdminVerifyComponent } from './employer-admin-verify/employer-admin-verify.component';
import { EmployerWelcomeComponent } from './employer-welcome/employer-welcome.component';
import { CompanyDetailsComponent } from './company-details/company-details.component';

@NgModule({
  declarations: [
    HeaderComponent,
    CampaignDetailComponent,
    ChoosePlanComponent,
    EmailVerifyComponent,
    CandidateListComponent,
    EmployerDashboardComponent,
    JobManageComponent,
    ViewJobComponent,
    EmployerSignupComponent,
    EmployerLoginComponent,
    EmployerAdminVerifyComponent,
    EmployerWelcomeComponent,
    CompanyDetailsComponent
  ],
  imports: [
    CommonModule,
    EmployerRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgxDropzoneModule
  ]
})
export class EmployerModule { }
