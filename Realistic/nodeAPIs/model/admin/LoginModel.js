var dateFormat = require('dateformat');
var md5 = require('md5');
const current_datetime = dateFormat(new Date().toLocaleString('en-US'),"yyyy-mm-dd hh:MM:ss");

const mongoose = require('mongoose');
var adminSchema = require('../../schema/adminSchema');
var adminDB = mongoose.model('admin', adminSchema);

var usersSchema = require('../../schema/usersSchema');
var userDB = mongoose.model('users', usersSchema);

// var tryPartsSchema = require('../../schema/tryPartsSchema');
// var tryPartsDB = mongoose.model('try_parts', tryPartsSchema);

var tryPartsUseSchema = require('../../schema/tryPartsUseSchema');
var tryPartsUseDB = mongoose.model('try_parts_use', tryPartsUseSchema);

var userCarsSchema = require('../../schema/userCarsSchema');
var userCarsDB = mongoose.model('user_cars', userCarsSchema);

// Contact Table
var contactSchema = require('../../schema/contactSchema');
var contactTable = mongoose.model('contact', contactSchema);

// Parts Table
var partsSchema = require('../../schema/partsSchema');
var partsDB = mongoose.model('parts', partsSchema);

var multiCarCompanyModelSchema = require('../../schema/multiCarCompanyModelSchema');
var multiCarCompanyModelDB = mongoose.model('multiCarCompanyModel', multiCarCompanyModelSchema);


module.exports = {
	/*** Admin added into db *****/
	adminAdd: function(con, data, callback) { //for user signup
        var Admin = new adminDB();
         Admin.name = data.name;
         Admin.password = md5(data.password);
         Admin.uesrname = data.username;
         Admin.email = data.email;
         Admin.phone =  data.phone;         
         Admin.status =  '1';         
         Admin.image =  ''; 
         Admin.device_token = '';    
         Admin.device_id = '';    
         Admin.device_type = '';  
         Admin.created_at = current_datetime;    
         Admin.save(callback);
    },
  /******************* Get Records all******************/
   checkExitsByID:function(con, data, callback) { // for check email exists or not
        adminDB.find({ '_id': data.admin_id }, callback)
    },
	login: function(con, data, callback) {
		var password = md5(data.password);
	  	adminDB.find({ 'email': data.email, 'password': password  }, callback)
	},
	getInfo: function(con, data, callback) {
	  	adminDB.find({ '_id': data.admin_id }, callback)
	},
	updateProfileImage: function(con, imagename, admin_id, callback) {	
		var myquery = {  _id: admin_id  };
        var newvalues = { $set: 
            { 
                image: imagename,                
            } 
        };
        adminDB.updateOne(myquery, newvalues, callback)
	},	
	updateProfile: function(con, data, callback) {	
		var myquery = {  _id: data.admin_id  };
        var newvalues = { $set: 
            { 
                name: data.name, 
                phone: data.phone, 
                image: data.image, 
                email: data.email
            } 
        };
        adminDB.updateOne(myquery, newvalues, callback)
	},
    changePassword: function(con, data, callback) {  
        var myquery = {  _id: data.admin_id  };
        var newvalues = { $set: 
            { 
                password: md5(data.password),               
            } 
        };
        adminDB.updateOne(myquery, newvalues, callback)
    },
    getAllUsers: function(con, data, callback) {
        var start = (data.page-1)*10;
        var sortKey = data.sortKey;
        var sortBy = data.sortBy;
        if(sortBy == 'asc'){ sortBy = 1; }else{ sortBy = -1; }
        if(data.searchKey == ''){ 
            userDB.find({}).sort({ [sortKey]: sortBy  }).skip(start).limit(10).exec(callback)
        }else{
            userDB.find({$or: [{'name': { $regex : '.*' +data.searchKey, $options: 'i' } }, {'email': { $regex : '.*' +data.searchKey, $options: 'i' } }] }).sort({ [sortKey]: sortBy }).skip(start).limit(10).exec(callback)
        }    
    },
    getUserInfo: function(con, data, callback) {
        userDB.find({'_id': data.user_id }, callback)
    },
    getUsers: function(con, data, callback) {
        if(data.searchKey == ''){ 
            userDB.find({ }).exec(callback)     
        }else{
            userDB.find({$or: [{'name': { $regex : '.*' +data.searchKey, $options: 'i' } }, {'email': { $regex : '.*' +data.searchKey, $options: 'i' } }] }).exec(callback)        
        }         
    },
    getTryParts:function(con, data, callback) { //Get try parts
        partsDB.find({ }, callback)
    },
    tryPartsStatics: function(con, data, callback) {
        var arr = []; var count_res =1;
        data.forEach((row) => {
            console.log('row : ', row);
            tryPartsUseDB.find({'part_id': { $in: row.seqId } }).countDocuments(function(err, count) {
                if(count > 0){ 
                    var st = {'y':count, 'label': row._id};
                    arr.push(st);
                } 
                if(count_res == data.length){
                    callback('', arr);
                }
                count_res = count_res +1;
            })
        })
    },
    getCarDetails:function(con, data, callback) { //Get try parts
        userCarsDB.find({ 'company_id': data.company_id, 'model_id': data.model_id  }).exec(callback)
    },
    tryPartsWithCarStatics: function(con, data, callback) {
        var arr = []; var count_res =1;
        partsDB.find({}).exec(function(err, result){
            if(result.length > 0){     
                data.forEach((row) => { // array of cars               
                    result.forEach((parts) => { //array of parts
                        tryPartsUseDB.find({'car_id': row._id}).countDocuments(function(err, count) {
                            if(count > 0){ 
                                var st = {'y':count, 'label': row._id};
                                arr.push(st);
                            } 
                            if(count_res == data.length){
                                callback('', arr);
                            }
                            count_res = count_res +1;
                        })
                    })
                })    
            }else{
                callback('', arr);
            }
        })    
    },
    getContactData: function(con, data, callback) {
        var start = (data.page-1)*10;
        var sortKey = data.sortKey;
        var sortBy = data.sortBy;
        if(sortBy == 'asc'){ sortBy = 1; }else{ sortBy = -1; }

        if(data.searchKey == ''){ 
            contactTable.find({}).sort({ [sortKey]: sortBy }).populate({path: 'user_details', select: '_id name email'}).skip(start).limit(10).exec(callback)
        }else{
            contactTable.find( {'subject': { $regex : '.*' +data.searchKey+'*.', $options: 'i' } } ).populate({path: 'user_details', select: '_id name email'}).sort({ [sortKey]: sortBy }).skip(start).limit(10).exec(callback)
        }    
    }, 
    countContactData: function(con, data, callback) {        
        if(data.searchKey == ''){ 
            contactTable.find({}).exec(callback)
        }else{
            contactTable.find({'subject': { $regex : '.*' +data.searchKey, $options: 'i' } } ).exec(callback)
        }    
    },
     getUserCars:function(con, data, callback) { // Get user added car
        userCarsTable.find({ 'user_id': data.user_id }).populate([ {path:'company_details', select:'_id name'},{path:'models_details', select:'_id model_name'} ]).exec(callback)   
    },
    /*** Admin added into db *****/
    addParts: function(con, data, callback) {
        var PartsDB = new partsDB();
         PartsDB.company_id = data.company_name;
         PartsDB.model_id = data.model_name;
         PartsDB.car_name = data.car_name;
         PartsDB.description = data.description;
         PartsDB.part_image = data.part_image;
         PartsDB.link =  data.link;         
         PartsDB.status =  '1';         
         PartsDB.created_at = current_datetime;    
         PartsDB.save(callback);
    },
    getParts: function(con, data, callback) {
        var start = (data.page-1)*10;
        var sortKey = data.sortKey;
        var sortBy = data.sortBy;
        if(sortBy == 'asc'){ sortBy = 1; }else{ sortBy = -1; }

        if(data.searchKey == ''){ 
            partsDB.find({}).sort({ [sortKey]: sortBy }).populate([{path: 'company_details', select: '_id name'},{path: 'models_details', select: '_id model_name'} ]).skip(start).limit(10).exec(callback)
        }else{
            partsDB.find( { 'car_name': { $regex : '.*' +data.searchKey } } ).populate([{path: 'company_details', select: '_id name'},{path: 'models_details', select: '_id model_name'} ]).sort({ [sortKey]: sortBy }).skip(start).limit(10).exec(callback)
        }    
    }, 
    countParts: function(con, data, callback) {        
        if(data.searchKey == ''){ 
            partsDB.find({}).exec(callback)
        }else{
            partsDB.find({'car_name': { $regex : '.*' +data.searchKey } } ).exec(callback)
        }    
    },
    deleteParts:function(con, data, callback=null) { //delete part
        partsDB.deleteOne({ _id : data.part_id }).exec(function(err, res){
            if(res){
        multiCarCompanyModelDB.deleteMany({ add_part_id : data.part_id }).exec(callback);
            }
        })
      
    },
    // viewParts:function(con, data, callback) { //view part
    //     partsDB.find({ _id : data.part_id }).populate([{path: 'company_details', select: '_id name'},{path: 'models_details', select: '_id model_name'} ]).exec(callback)
    // },

    viewParts:function(con, data, callback) { //view part
        
        multiCarCompanyModelDB.find({ add_part_id : data.part_id }).populate([{path: 'company_details', select: '_id name'},{path: 'models_details', select: '_id model_name'},{path: 'part_details', select: '_id car_name link part_image description'} ]).exec(callback)

    },

    updateParts: function(con, data, callback) {
       var myquery = { _id: data.part_id  };
        var newvalues = { $set: 
            { 
                company_id: data.company_id,                
                model_id: data.model_id,                
                link: data.link,                
                car_name: data.car_name,                
                description: data.description,                
                part_image: data.part_image,   
                updated_at:current_datetime,              
            } 
        };
        partsDB.updateOne(myquery, newvalues, callback)
    }, 

      updateMultiCarModelStatus: function(con, data, callback) { 
        var myquery = {  status: "0"};
        var newvalues = { $set: 
            { 
                add_part_id: data,                
                status: 1,                
            } 
        };
        multiCarCompanyModelDB.updateMany(myquery, newvalues, callback)
    },  
     update_edit_status: function(con, data, callback) { 
        console.log('Current Sanerios',data);
        var myquery = {add_part_id: data.part_id};
        var newvalues = { $set: 
            {             
                status: 1,                
            } 
        };
        multiCarCompanyModelDB.updateMany(myquery, newvalues, callback)
    },  
    
}    


  // console.log('current values',data);

  //    var mymultipartquery = {add_part_id: data.part_id, status:"0"};
  //    var newvalue = { $set:  {  status: "1" } };
  //    multiCarCompanyModelDB.update({mymultipartquery, newvalue}).exec(function(err,res){
  //       console.log('mtlbb',res);
  //         console.log('mtlbb',err);
  //    });
