import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from '../../../services/account.service';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2'
import { employer_model } from '../../../models/employer_model';


@Component({
  selector: 'app-job-manage',
  templateUrl: './job-manage.component.html',
  styleUrls: ['./job-manage.component.scss']
})
export class JobManageComponent implements OnInit {

 
  public employerModel: employer_model;
  public addValidationError: employer_model;
  public headers = new Headers();
  errMsg:boolean=false;
  message:string; 
  employerlist:any =[] ;
  campaigndetailstabdata:any=[] ; 
  skillsneedtabdata:any=[] ; 
  companyroledesctabdata:any=[] ; 
  campaignquestabdata:any=[] ; 
  getId:any;
  
  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService, private spinner:NgxSpinnerService) { }
 
  ngOnInit(): void {
    // this.route.paramMap.subscribe(params => {
    //   this.getId = params.get('id');
    //   });

    this.employerModel = <employer_model>{
      id: '',
      name:''
    }
    this.addValidationError = <employer_model>{
      id: '',
      name:''
      
    }
    
    this.getEmployerbyId();
  }

  //-----------------------get employer details API------------------------
  getEmployerbyId(){
    this.employerModel.id = this.getId;
    this.spinner.show();
    debugger;
    this.accountService.getEmployerWithID(this.employerModel).then((response) => {
      console.log(response);
      debugger;
      if (response.code == 200) {
        console.log(response);
        this.spinner.hide();
        this.employerlist=response.result;
        this.campaigndetailstabdata=response.campaigndetailstabdata;
        this.skillsneedtabdata=response.skillsneedtabdata;
        this.companyroledesctabdata=response.companyroledesctabdata;
        this.campaignquestabdata=response.campaignquestabdata;
        
        
      } else {
        this.errMsg = true;
        this.message = "No Data Found!!!";
      }
    })
  }

  viewPostDetails(id)
  {
    console.log(id);
   this.router.navigate(['/view-job/'+id]);
  }

}
