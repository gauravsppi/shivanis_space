import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from '../../../services/account.service';
import { DataService } from '../../../services/data.service';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
//import { candidateLogin_model } from '../../../models/candidateLogin_model';
import { employer_model } from '../../../models/employer_model';
import { environment } from '../../../../environments/environment';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-campaign-detail',
  templateUrl: './campaign-detail.component.html',
  styleUrls: ['./campaign-detail.component.scss']
})
export class CampaignDetailComponent implements OnInit {
  public loginModel: employer_model;
  public addValidationError: employer_model;
  public headers = new Headers({
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  });

  public headers1 = new Headers({ 'Access-Control-Allow-Origin':'*', 'Content-Type': '*' });
  optionhow: any = [true, false, false];
  files: any[] = [];
  categories = [];
  experience=[];
 
  jobtype: any;
  s: number = 1;
  file: number=0;
  campaign: boolean = true;
  skills: boolean = true;
  companyroledesc: boolean = true;
  ques: boolean = true;
  poststatus: string;

  sucMsg: boolean = false;
  errMsg: boolean = false;
  message: string;
  w: number=0;
  filename:string;

  employerlist:any =[] ;  
  getId:any;
employerId:any;
  disabledrop: boolean =false;

  //----------validation for first tab--------------
  validationpost: boolean = false;
  validationjobtype: boolean = false;
  validationofficeaddress: boolean = false;
  validationcurrency: boolean = false;
  validationsalary1: boolean = false;
  validationsalary2: boolean = false;
  validationexperience: boolean = false;

  //--------------- second tab---------
  validationsector: boolean = false;
  validationskills: boolean = false;

//----------validation for third tab-------------
  validationwebsite: boolean = false;
  validationcompanydesc: boolean = false;
  validationroledesc: boolean = false;

//----------validation for foruth tab-------------
  validationques: boolean = false;
 
  //------ For campaign details And All Types Of Dropown -----
  sectorlists = [
    'Software Engineering',
    'Software Development',
    'Product and Design',
    'Graphic Designer',
    'Head of UX Design'
  ];
  currency = [
    '£',
    '€',
  ];
  job_type = [
    'Permanent',
    'Contract',
  ];
  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService, private data:DataService) { }

  ngOnInit(): void {

    this.route.paramMap.subscribe(params => {
      this.getId = params.get('id');
      });

    this.loginModel = < employer_model>{
      id:'',
      name:'',
      post: '',
      job_type: null,
      office_address: '',
      e_currencylist:[],
      salary: [],
      experience_level: 'beginner',
      job_sector: [],
      e_specialism: [],
      website: '',
      company_desc: 'demo',
      role_desc: 'demo',
      ques: []
      
    }
    this.addValidationError = <employer_model>{
      post: '',
      job_type: '',
      office_address: '',
      e_currencylist:[],
      salary: [],
      experience_level: '',
      website: '',
      company_desc: '',
      role_desc: '',
      ques: '',
      job_sector: [],
      e_specialism:[]
    }
    this.categories = [
      'Web Developer',
      'Web Designers',
      'Digital Marketing',
      'Designing',
      'Angular',
      'Vue',
      'JQuery',
      'Javascript',
    ];
    this.experience = [
      '0-12 months',
      '1-2 year',
      '2-4 year',
      '4-6 year',
      '6 years+',
    ];
    this.enabledisable();
    this.postStatus();
    this.getEmployerbyId();
  }

//   //-----------check validate------------
checkvalidation(){
 
  var NAME_REGEXP = /^[A-Za-z ]+$/;
  var NAMENUM_REGEXP = /^[A-Za-z0-9./ ]+$/;
  var WEBSITE_REGEXP = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[.\!\/\\w]*))?)/;
  if (this.loginModel.post == "") {
    this.validationpost=true;
  }else{
    this.validationpost=false;
  }
  // if (NAMENUM_REGEXP.test(this.loginModel.post)===false) {
  //   this.validationpost=true;
  // }
  if (this.loginModel.job_type == "" || this.loginModel.job_type== null) {
    this.validationjobtype=true;
  }else{
    this.validationjobtype=false;
  }
  if (NAME_REGEXP.test(this.loginModel.job_type)===false) {
    this.validationjobtype=true;
  }
  if (this.loginModel.office_address == "") {
    this.validationofficeaddress=true;
  }else{
    this.validationofficeaddress=false;
  }
  // if (NAMENUM_REGEXP.test(this.loginModel.office_address)===false) {
  //   this.validationofficeaddress=true;
  // }
  if (this.loginModel.salary[0] == "") {
    this.validationsalary1=true;
  }else{
    this.validationsalary1=false;
  }
  if (this.loginModel.salary[1] == "") {
    this.validationsalary2=true;
  }else{
    this.validationsalary2=false;
  }
  if (this.loginModel.salary[0].toString().length <=7) {
    this.validationsalary1=false;
  }else{
    this.validationsalary1=true;
  }
  if (this.loginModel.salary[1].toString().length <=7) {
    this.validationsalary2=false;
  }else{
    this.validationsalary2=true;
  }
  if (this.loginModel.experience_level == "") {
    this.validationexperience=true;
  }else{
    this.validationexperience=false;
  }
  if (this.loginModel.experience_level.toString().length <=2) {
    this.validationexperience=false;
  }else{
    this.validationexperience=true;
  }
  if (this.loginModel.website == "") {
    this.validationwebsite=true;
  }else{
    this.validationwebsite=false;
  }
  if (WEBSITE_REGEXP.test(this.loginModel.website)===false) {
    this.validationwebsite=true;
  }
  if (this.loginModel.company_desc == "") {
    this.validationcompanydesc=true;
  }else{
    this.validationcompanydesc=false;
  }
  // if (NAMENUM_REGEXP.test(this.loginModel.company_desc)===false) {
  //   this.validationcompanydesc=true;
  // }
  if (this.loginModel.role_desc == "") {
    this.validationroledesc=true;
  }else{
    this.validationroledesc=false;
  }
  // if (NAMENUM_REGEXP.test(this.loginModel.role_desc)===false) {
  //   this.validationroledesc=true;
  // }
  if (this.loginModel.ques == "") {
    this.validationques=true;
  }else{
    this.validationques=false;
  }
  // if (NAMENUM_REGEXP.test(this.loginModel.ques)===false) {
  //   this.validationques=true;
  // }

} 

postvalidation(){
  if (this.loginModel.post == "") {
      this.validationpost=true;
     }else{
      this.validationpost=false;
   }
}
postTypevalidation(){
  if (this.loginModel.job_type == "" || this.loginModel.job_type == null) {
      this.validationjobtype=true;
     }else{
      this.validationjobtype=false;
   }
}
officevalidation(){
  if (this.loginModel.office_address == "") {
      this.validationofficeaddress=true;
     }else{
      this.validationofficeaddress=false;
   }
}

currencyvalidation(){
  if (this.loginModel.e_currencylist == '') {
   this.validationcurrency=false;    
  }else{
    this.validationcurrency=true;
     }
}
keyPressSalary(event: any) {
  debugger;
  if (this.loginModel.salary[0] == "") {
    this.validationsalary1=true;
  }else{
    this.validationsalary1=false;
  }
  if (this.loginModel.salary[1] == "") {
    this.validationsalary2=true;
  }else{
    this.validationsalary2=false;
  }
  const pattern = /[0-9]/;
  let inputChar = String.fromCharCode(event.charCode);
  if (event.keyCode != 8 && !pattern.test(inputChar)) {
  event.preventDefault();
  
  }
  }
  keyPressExperience(event: any) {
    //debugger;
    if (this.loginModel.experience_level == "") {
      this.validationexperience=true;
    }else{
      this.validationexperience=false;
    }
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
    event.preventDefault();
    
    }
    }

    sectorvalidation(){
      if (this.loginModel.job_sector == '') {
       this.validationsector=false;    
      }else{
        this.validationsector=true;
         }
    }  

    skillsvalidation(){
      if (this.loginModel.e_specialism == '') {
       this.validationskills=false;    
      }else{
        this.validationskills=true;
         }
    }  

    websitevalidation(){
      if (this.loginModel.website == "") {
          this.validationwebsite=true;
         }else{
          this.validationwebsite=false;
       }
    }
    companyDescvalidation(){
      if (this.loginModel.company_desc == "") {
          this.validationcompanydesc=true;
         }else{
          this.validationcompanydesc=false;
       }
    }
    roleDescvalidation(){
      if (this.loginModel.role_desc == "") {
          this.validationroledesc=true;
         }else{
          this.validationroledesc=false;
       }
    }
  //post type status
  postStatus() {
    this.accountService.StatusGet().then((response) => {
      if (response.code == 200) {
        if (response.result.post_type_status == 0) {
          this.poststatus = "Post your first job";
        }else{
          this.poststatus = "Post job";
        }
        //this.sucMsg = true;
      } else {
        this.errMsg = true;
      }
    })
  }

  disableDropdown(){
    //alert();
    this.disabledrop =true;
  }


  // ------ Tabs Enable Disable According To The Login Type Status ---------
  enabledisable() {
    this.accountService.StatusGet().then((response) => {
      if (response.code == 200) {
        if (response.result.login_type_status == 0) {
          this.campaign = false;
        } else if (response.result.login_type_status == 1) {          
          this.skills = false;
        } else if (response.result.login_type_status == 2) {
          this.companyroledesc = false;
        } else if (response.result.login_type_status == 3) {
          this.ques = false;
        }else if (response.result.login_type_status == 4 && response.result.post_type_status == 1) {
          this.campaign = false;
        }else if (response.result.login_type_status == 1 && response.result.post_type_status == 1) {
          this.skills = false;
        }else if (response.result.login_type_status == 2 && response.result.post_type_status == 1) {
          this.companyroledesc = false;
        }else if (response.result.login_type_status == 3 && response.result.post_type_status == 1) {
          this.ques = false;
        } else {
          
          this.router.navigate(['/campaign_details']);
        }
        //this.sucMsg = true;
      } else {
        this.errMsg = true;
      }
    })
  }

   // ----- Add Questions For Campaign Ques ------------
   counts(s) {
    return new Array(s);
  }
  addQues() {
    this.s++;
  }
  removeQues() {
    this.s--;
  }

   // ------ Campaign details Data Save -----------
   campaigndetailsDataSave() {
    var valid = true;
    var NAME_REGEXP = /^[A-Za-z ]+$/;
    var NAMENUM_REGEXP = /^[A-Za-z0-9./ ]+$/;
    this.addValidationError.post = '';
    this.addValidationError.job_type = '';
    this.addValidationError.office_address = '';
    this.addValidationError.e_currencylist = [];
    this.addValidationError.salary = [];
    this.addValidationError.experience_level = '';

    if (this.loginModel.post == "") {
      this.validationpost=true;
      valid = false;
    }
    // if (NAMENUM_REGEXP.test(this.loginModel.post)===false) {
    //   this.validationpost=true;
    //   valid = false;
    // }
    if (this.loginModel.job_type == "" || this.loginModel.job_type ==null) {
      this.validationjobtype=true;
      valid = false;
    }
    // if (NAME_REGEXP.test(this.loginModel.job_type)===false) {
    //   this.validationjobtype=true;
    //   valid = false;
    // }
    if (this.loginModel.office_address == "") {
      this.validationofficeaddress=true;
      valid = false;
    }

    if (this.loginModel.e_currencylist == "") {
      this.validationcurrency=true;
      valid = false;
    }
    // if (NAME_REGEXP.test(this.loginModel.office_address)===false) {
    //   this.validationofficeaddress=true;
    //   valid = false;
    // }
    // if (this.loginModel.salary[0] == "") {
    //   this.validationsalary1=true;
    //   valid = false;
    // }
    // if (this.loginModel.salary[1] == "") {
    //   this.validationsalary2=true;
    //   valid = false;
    // }
    if (this.loginModel.experience_level == "") {
      this.validationexperience=true;
      valid = false;
    }

    if(valid){
      // Swal.fire({
      //   title: 'Are you sure you want to submit?',
      //   text: 'You are not able to edit data here',
      //   icon: 'warning',
      //   showCancelButton: true,
      //   confirmButtonColor:'#D0021B',
      //   confirmButtonText: 'Yes, want to save record',
      //   cancelButtonColor: '#D0021B',
      //   cancelButtonText: 'No, keep it',
      // }).then((result) => {
      //   if (result.isConfirmed) {
          this.accountService.EmployerFirstTab(this.loginModel).then((response) => {
            if (response.code == 200) {
              //Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B' , icon: 'success', title: 'Record successfully saved' })
              this.campaign = true;
              this.skills = false;
              this.errMsg = false;
            } else {
              this.errMsg = true;
              this.message = response.message;
            }
          })
      //   } else if (result.isDismissed) {
      //     Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B', icon: 'success', title: 'Record not saved' })
      //   }
      // })
   }  
  }
  // --------- skills need Tab Data Save ---------
  skillsTabDataSave() {
    var valid = true;

    this.addValidationError.job_sector = [];
    this.addValidationError.e_specialism = [];

    if (this.loginModel.job_sector == '') {      
      this.validationsector=true;       
    }else{
      this.validationsector=false;
         }
    if (this.loginModel.e_specialism == '') {      
      this.validationskills=true;
         }else{
      this.validationskills=false;
    }
  
    if(valid){
      // Swal.fire({
      //   title: 'Are you sure you want to submit?',
      //   text: 'You are not able to edit data here',
      //   icon: 'warning',
      //   showCancelButton: true,
      //   confirmButtonColor:'#D0021B',
      //   confirmButtonText: 'Yes, want to save record',
      //   cancelButtonColor: '#D0021B',
      //   cancelButtonText: 'No, keep it',
      // }).then((result) => {
      //   if (result.isConfirmed) {
          this.accountService.EmployerSecondTab(this.loginModel).then((response) => {
            if (response.code == 200) {
             // Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B' , icon: 'success', title: 'Record successfully saved' })
              this.skills = true;
              this.companyroledesc = false;
              this.errMsg = false;
            } else {
              this.errMsg = true;
              this.message = response.message;
            }
          })
      //   } else if (result.isDismissed) {
      //     Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B', icon: 'success', title: 'Record not saved' })
      //   }
      // })
    }
  }
  // ---- Company and role description Tab Data Save ----------
  companyroleTabdata() {
    var valid = true;
    var WEBSITE_REGEXP = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[.\!\/\\w]*))?)/;
    var NAMENUM_REGEXP = /^[A-Za-z0-9./ ]+$/;
    this.addValidationError.website = '';
    this.addValidationError.company_desc = '';
    this.addValidationError.role_desc = '';

    if (this.loginModel.website == "") {
      this.validationwebsite=true;
      valid = false;
    }
    // if (WEBSITE_REGEXP.test(this.loginModel.website)===false) {
    //   this.validationwebsite=true;
    //   valid = false;
    // }
    // if (this.loginModel.company_desc == "") {
    //   this.validationcompanydesc=true;
    //   valid = false;
    // }
    // if (NAMENUM_REGEXP.test(this.loginModel.company_desc)===false) {
    //   this.validationcompanydesc=true;
    //   valid = false;
    // }
    // if (this.loginModel.role_desc == "") {
    //   this.validationroledesc=true;
    //   valid = false;
    // }
    // if (NAMENUM_REGEXP.test(this.loginModel.role_desc)===false) {
    //   this.validationroledesc=true;
    //   valid = false;
    // }
    console.log('value of the valid',valid);
    if(valid){
      // Swal.fire({
      //   title: 'Are you sure you want to submit?',
      //   text: 'You are not able to edit data here',
      //   icon: 'warning',
      //   showCancelButton: true,
      //   confirmButtonColor:'#D0021B',
      //   confirmButtonText: 'Yes, want to save record',
      //   cancelButtonColor: '#D0021B',
      //   cancelButtonText: 'No, keep it',
      // }).then((result) => {
      //   if (result.isConfirmed) {
          this.accountService.EmployerThirdTab(this.loginModel).then((response) => {
            if (response.code == 200) {
              //Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B' , icon: 'success', title: 'Record successfully saved' })
              this.companyroledesc = true;
              this.ques = false;
              this.errMsg = false;
            } else {
              this.errMsg = true;
              this.message = response.message;
            }
          })
      //   } else if (result.isDismissed) {
      //     Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B', icon: 'success', title: 'Record not saved' })
      //   }
      // })
    }  
  }
  // ------- question Tab Data Save -------
  questionTabdata() {
    // var valid = true;    
    // var NAMENUM_REGEXP = /^[A-Za-z0-9./ ]+$/;   
    // this.addValidationError.ques = '';
    // if (this.loginModel.ques == "") {
    //   this.validationques=true;
    //   valid = false;
    // }
    // if (NAMENUM_REGEXP.test(this.loginModel.ques)===false) {
    //   this.validationques=true;
    //   valid = false;
    // }

    // if(valid){
      // Swal.fire({
      //   title: 'Are you sure you want to submit?',
      //   text: 'You are not able to edit data here',
      //   icon: 'warning',
      //   showCancelButton: true,
      //   confirmButtonColor:'#D0021B',
      //   confirmButtonText: 'Yes, want to save record',
      //   cancelButtonColor: '#D0021B',
      //   cancelButtonText: 'No, keep it',
      // }).then((result) => {
      //   if (result.isConfirmed) {
          this.accountService.EmployerFourthTab(this.loginModel).then((response) => {
            if (response.code == 200) {
              //Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B' , icon: 'success', title: 'Record successfully saved' })
              this.ques = true;
              
              this.errMsg = false;
              console.log(response);
              this.router.navigate(['/employer-dashboard']);
            } else {
              this.errMsg = true;
              this.message = response.message;
            }
          })
      //   } else if (result.isDismissed) {
      //     Swal.fire({ showCancelButton: false, confirmButtonColor: '#D0021B',icon: 'success', title: 'Record not saved' })
      //   }
      // })
    //}  
  }
  

   //-----------------------get employer details API------------------------
   getEmployerbyId(){
    this.data.changeStatus(true);
    this.loginModel.id = this.getId;
    //this.spinner.show();
    debugger;
    this.accountService.getEmployerWithID(this.loginModel).then((response) => {
      console.log('here is data>>>>',response);
      debugger;
      if (response.code == 200) {
        this.employerId=response.result._id;
       // this.spinner.hide();
        this.employerlist=response.result;
        console.log(this.employerlist);
        // this.letsgoTabdata=response.letsgoTabdata;
        // this.degreeTabdata=response.degreeTabdata;
        // this.currentjobTabdata=response.currentjobTabdata;
        // this.interestsTabdata=response.interestsTabdata;
        
        
      } else {
        this.errMsg = true;
        this.message = "No Data Found!!!";
      }
    })
  }
   // ----------- Upload optional video Image -----------
   selectFile(event){
    console.log(event.target.files[0]);
    this.file=event.target.files.length;
    this.filename=event.target.files[0].name;
      const formData = new FormData();
      formData.append('video_upload', <File>event.target.files[0]);
  }
  removeFile(event){
    this.file=0;
    event.value="";
  }
}
