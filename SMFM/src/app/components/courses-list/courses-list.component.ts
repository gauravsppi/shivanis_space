import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Headers, Http } from '@angular/http';

import { environment } from '../../../environments/environment';
import { AccountService } from '../../services/account.service';
import { class_model } from '../../models/class_model';


@Component({
  selector: 'app-courses-list',
  templateUrl: './courses-list.component.html',
  styleUrls: ['./courses-list.component.scss']
})
export class CoursesListComponent implements OnInit {

  constructor(private router: Router, private accountService: AccountService, private route: ActivatedRoute, private http: Http) {  }
  
  public classModel: class_model;
  public loaderShow: boolean = false;
  total : number= 0;
  totalPages : number= 0;
  currentPage:number=1;
  page : number = 1;
  key: string = 'name'; //set default
  reverse: boolean = false;
  noRecords: boolean = false;
  courses= [];

  ngOnInit(): void {
    this.classModel = <class_model>{
      name: '',
      admin_id: '',
      authToken: '',
      school_id: '',
      searchKey: '',
      sortKey: 'course_name',
      sortBy: 'asc'  
    }    
    this.getCourses()
  }

  getGG4lAuthToken(){
    this.accountService.getGG4LToken().then((response) => {
      if(response.length > 0 ){
        this.classModel.authToken = response.result[0].token;
      }
    })
  }

  getCourses(){
    this.loaderShow = true;
    this.classModel.admin_id = localStorage.getItem('admin_id');
    this.classModel.school_id = localStorage.getItem('school_id');
    this.classModel.page = this.currentPage;
    this.classModel.searchKey = '';
    this.accountService.getCourses(this.classModel).then((response) => {
        if (response.code == 200 && response.status == 'success') {
          this.total = response.count;
          this.totalPages=Math.ceil( response.count/10);
          this.courses = response.result;  
          this.noRecords = false;                  
        }else{
          this.noRecords = true;
          this.total = 0
          this.totalPages=0
          this.courses = []; 
        }
        this.loaderShow = false;
    })
  }
  pageChange(currentPage){
    this.currentPage=currentPage;
    if(this.classModel.searchKey==""){
      this.getCourses();
    }else{
      this.searchFunction(this.currentPage);
    } 
  }
  counter(i: number) {
    return new Array(i);
  }
  searchFunction(currentPage){
    if(this.classModel.searchKey==""){
      this.currentPage=1;
      this.getCourses();
    }else{
      this.loaderShow = true;
      this.currentPage=currentPage;
      this.accountService.getCourses(this.classModel).then((response) => {
        if (response.code == 200 && response.status == 'success') {
          this.total = response.count;
          this.totalPages=Math.ceil( response.count/10);
          this.courses = response.result;  
          this.noRecords = false;                  
        }else{
          this.noRecords = true;
          this.total = 0
          this.totalPages=0
          this.courses = []; 
        }
        this.loaderShow = false;
      })
    }
  }
  sortFunction(sortKey){
    this.key = sortKey;
    this.classModel.sortKey = sortKey;
    if(this.reverse === true){
      this.classModel.sortBy = 'asc';
    }else{
      this.classModel.sortBy = 'desc';
    }
    this.reverse = !this.reverse;   
    if(this.classModel.searchKey== ""){
      this.currentPage=1;
      this.getCourses();
    }else{
      this.searchFunction(this.currentPage);
    }
  }

}
