import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from '../../services/account.service';
import { Headers, Http } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { Login_model } from '../../models/login_model';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-edit-part',
  templateUrl: './edit-part.component.html',
  styleUrls: ['./edit-part.component.scss']
})
export class EditPartComponent implements OnInit {

  public loginModel: Login_model;
  public addValidationError: Login_model;
  private headers = new Headers();
  
  record: any = [];
  toDoList: any = [];
  total: number = 0;
  loaderShow: boolean = false;
  sucMsg: boolean = false;
  errMsg: boolean = false;
  successMessage : string = '';
  errorMessage : string = '';
  buttonDisable: boolean=false;
  fileSelect:boolean = false;
  fileSelect_error:boolean = false;
  companyNames: any = [];
  models: any = [];
  
  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: HttpClientModule) { }

  getId: string;
  records: [];
  partImagePath: string='';

  ngOnInit(): void {
    this.loginModel = <Login_model>{
      company_name: '',
      model_name: '',
      car_name: '',
      link: '',
      part_image: File= null, 
      description: '',
    }
    this.addValidationError = <Login_model> {
      company_name: '', 
      model_name: '',
      car_name: '',
      link: '',    
      part_image: File= null,    
      description:''
    }
    this.deleteAllmultiCarModel();
    this.isLoggedIn();
    this.route.params.subscribe(params => {
      this.getId = params['id'];
    });
    this.getPart(this.getId);
    this.getCompanyNames();
    this.getMultiDetail(this.getId);
   
  }
   //Delete All where status O.
   deleteAllmultiCarModel(){
    this.accountService.deleteAllmultiCarModels().then((response)=>{
     console.log('success');
    }) 
  }

  getValues(event){
    this.loginModel.company_details=event.target.value.split("?");
    this.loginModel.companyNames=this.loginModel.company_details[1];
    this.loginModel.company_id = this.loginModel.company_details[0];
    this.loaderShow= true;
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    this.accountService.getModels(this.loginModel).then((response)=>{
      if(response.code==200){
        this.loaderShow = false;
        this.models = response.result;        
      }else{
        this.models = [];
      }
    })
  }

  getValuesModel(event: any){ 
    this.loginModel.companyNames
    this.loginModel.id
    this.loginModel.model_details=event.target.value.split("?");
    this.loginModel.models_name = this.loginModel.model_details[1];
    this.loginModel.model_id = this.loginModel.model_details[0];
    // Insert Record on click
    console.log('Edit Values',this.loginModel);
    this.accountService.edit_multiCarModels(this.loginModel).then((response)=>{
      this.accountService.get_edit_multiCarModels(this.loginModel).then((response)=>{
        console.log('this is awesome',response);
        if(response.code==200){
          this.loaderShow = false;
          this.toDoList = response.result;        
        }else{
          this.toDoList = [];
        }
      }) 
    })
    // Insert record on click close
  
  }


  getCompanyNames(){
    this.loaderShow= true;
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    this.accountService.getCarCompanyNames(this.loginModel).then((response)=>{
      console.log('names: ', response);
      if(response.code==200){
        this.loaderShow = false;
        this.companyNames = response.result;        
      }else{
        this.companyNames = [];
      }
    })
  }

  //Add Company Name
  isLoggedIn() {
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    if (this.loginModel.admin_id == null) {
      this.router.navigate(['/']);
    }
  }

  getPart(part_id){
    this.loaderShow = true;
    this.loginModel.part_id = part_id;
    this.loginModel.admin_id = localStorage.getItem('admin_id');;
    this.accountService.viewParts(this.loginModel).then((response)=>{
    this.loaderShow = false;
      if(response.code==200){
        this.records= response.result;
        this.loginModel.company_name = response.result[0].company_name;
        this.loginModel.model_name = response.result[0].model_name;
        this.loginModel.car_name = response.result[0].part_details.car_name;
        this.loginModel.link = response.result[0].part_details.link;
        this.loginModel.description = response.result[0].part_details.description;
        //this.loginModel.part_id = response.result[0]._id;
        this.loginModel.part_img = response.result[0].part_details.part_image;
        this.partImagePath =  environment.partUrl + response.result[0].part_details.part_image;
        this.loginModel.model_id = response.result[0].model_id;
        this.loginModel.company_id = response.result[0].company_id;
        this.accountService.getModels(this.loginModel).then((response)=>{
          if(response.code==200){
            this.loaderShow = false;
            this.models = response.result;        
          }else{
            this.models = [];
          }
        })
      }else{
        this.records= [];
      }
    })
  }

  onFileSelect(event) {
    this.buttonDisable = false;
    console.log('Target>> ', event.target.files);
    if (event.target.files.length > 0) {
      const video_file = <File>event.target.files[0];
      if(video_file.type == 'image/jpeg' || video_file.type == 'image/jpg'  || video_file.type == 'image/png'){
        this.loginModel.part_image = <File>video_file;
        this.fileSelect = false;
      }else{
        this.fileSelect = true;
        this.loginModel.part_image = null;
        this.buttonDisable = true;
      }  
    }
  }
  edit_part(){
    var valid = true;
    this.addValidationError.company_name = '';
    this.addValidationError.model_name = '';
    this.addValidationError.car_name = '';
    this.addValidationError.link = '';
    this.addValidationError.part_image_error = '';
    this.addValidationError.description = '';

    if (this.loginModel.company_id == "") {
      this.addValidationError.company_name = 'Please Select Company Name';
      valid = false;
    }
    if (this.loginModel.model_id == "") {
      this.addValidationError.model_name = 'Please Select Model Name';
      valid = false;
    }
    if (this.loginModel.car_name == "") {
      this.addValidationError.car_name = 'Please Enter Car Name';
      valid = false;
    }
    if (this.loginModel.link == "") {
      this.addValidationError.link = 'Please Enter Link';
      valid = false;
    } 
     if (this.loginModel.description == "") {
      this.addValidationError.description = 'Please Enter description';
      valid = false;
    }else{
      var regExp = new RegExp ("^https?:\/\/()?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?");
      if(!regExp.test(this.loginModel.link)){
        valid = false;
        this.addValidationError.link = 'Please type Link Like http://google.com';     
      }
    }
  
    if (valid) {
      var formData: FormData  = new FormData();
      formData.append('company_id', this.loginModel.company_id);
      formData.append('model_id', this.loginModel.model_id);
      formData.append('car_name', this.loginModel.car_name);
      formData.append('link', this.loginModel.link);
      formData.append('part_image', this.loginModel.part_image);
      formData.append('admin_id', this.loginModel.admin_id);
      formData.append('part_id', this.loginModel.part_id);
      formData.append('part_img', this.loginModel.part_img);
      formData.append('description', this.loginModel.description);

      console.log('Debug Update',formData);
      this.accountService.updateParts(formData).then((response) => {
        if (response.code == 200) {
          this.sucMsg = true;
          this.successMessage = response.message;
          setTimeout(() => { this.router.navigate(['/part']); }, 1500);
        } else {
          this.errMsg = true;
          this.errorMessage = response.message;   
          setTimeout(() => { this.errMsg = false; }, 1500);      
        }
      })
    }
  }


  //To fetch all Company Models of particular Car Part.
  getMultiDetail(id){
    this.loginModel.id = id;
    this.accountService.get_edit_multiCarModels(this.loginModel).then((response)=>{
      console.log('this is awesome',response);
      if(response.code==200){
        this.loaderShow = false;
        this.toDoList = response.result;        
      }else{
        this.toDoList = [];
      }
    }) 
  }

   delete(id){
    if(confirm("Are you sure to delete!")) {
      this.loaderShow = true;
      this.loginModel.id = id;
      this.loginModel.admin_id = localStorage.getItem('admin_id');;
      this.accountService.deleteMultiParts(this.loginModel).then((response)=>{
      this.loaderShow = false;
        if(response.code==200){
          this.sucMsg = true;
          this.successMessage = response.message;
         this.getMultiDetail(this.getId);
          setTimeout (() => {  this.sucMsg = false }, 1000);
        }else{
          this.errMsg = true;
          this.errorMessage = response.message;
          setTimeout (() => {  this.errMsg = false }, 1000);
        }
      })
    }
  }

  
  

}
