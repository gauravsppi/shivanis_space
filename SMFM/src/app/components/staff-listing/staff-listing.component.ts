import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Headers, Http } from '@angular/http';


import { environment } from '../../../environments/environment';
import { AccountService } from '../../services/account.service';

import { school_model } from '../../models/school_model';
import { Login_model } from '../../models/login_model';

@Component({
  selector: 'app-staff-listing',
  templateUrl: './staff-listing.component.html',
  styleUrls: ['./staff-listing.component.scss']
})
export class StaffListingComponent implements OnInit {

  public schoolModel: school_model;
  public loginModel: Login_model;
  members: [];  
  total : number= 0;
  totalPages : number= 0;
  currentPage:number=1;
  page : number =1;
  key: string = 'name'; //set default
  reverse: boolean = false;
  noRecords: boolean = false;
  loaderShow: boolean = false;
  sucMsg: boolean = false;
  errMsg: boolean = false;
  successMessage: string;   
  errorMessage: string; 

  constructor(private router: Router, private accountService: AccountService, private route: ActivatedRoute, private http: Http) { }


  ngOnInit(): void {
    this.schoolModel = <school_model>{
      name: '',
      identifier: '',
      phoneNumber: '',
      schoolNumber: '',
      admin_id: '',
      school_id: '',
      searchKey: '',
      sortKey: 'name',
      sortBy: 'asc'  
    }
    this.getStaff();
  }
  
  getStaff() {
    this.loaderShow = true;
    this.schoolModel.admin_id = localStorage.getItem('admin_id');
    this.schoolModel.school_id = localStorage.getItem('school_id');
    this.schoolModel.page = this.currentPage;
    this.schoolModel.searchKey = '';
    this.accountService.getStaffMembers(this.schoolModel).then((response) => {
      console.log('Staff List:: ', response);
        if (response.code == 200 && response.status == 'success') {
          this.total = response.count;
          this.totalPages=Math.ceil( response.count/10);
          this.members = response.result;  
          this.noRecords = false;                  
        }else{
          this.noRecords = true;
          this.total = 0
          this.totalPages=0
          this.members = []; 
        }
        this.loaderShow = false;
    })
  }
 
  pageChange(currentPage){
    this.currentPage=currentPage;
    if(this.schoolModel.searchKey==""){
      this.getStaff();
    }else{
      this.searchFunction(this.currentPage);
    } 
  }
  counter(i: number) {
    return new Array(i);
  }
  searchFunction(currentPage){
    if(this.schoolModel.searchKey==""){
      this.currentPage=1;
      this.getStaff();
    }else{
      this.loaderShow = true;
      this.currentPage=currentPage;
      this.accountService.getStaffMembers(this.schoolModel).then((response) => {
        if (response.code == 200 && response.status == 'success') {
          this.total = response.count;
          this.totalPages=Math.ceil( response.count/10);
          this.members = response.result;  
          this.noRecords = false;                  
        }else{
          this.noRecords = true;
          this.total = 0
          this.totalPages=0 
          this.members = []; 
        }
        this.loaderShow = false;
      })
    }
  }
  sortFunction(sortKey){
    this.key = sortKey;
    this.schoolModel.sortKey = sortKey;
    if(this.reverse === true){
      this.schoolModel.sortBy = 'asc';
    }else{
      this.schoolModel.sortBy = 'desc';
    }
    this.reverse = !this.reverse;   
    if(this.schoolModel.searchKey== ""){
      this.currentPage=1;
      this.getStaff();
    }else{
      this.searchFunction(this.currentPage);
    }
  }

  getStaffId(student_id){
    this.schoolModel.staff_id = student_id;
    this.accountService.getStaffMembers(this.schoolModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
         this.schoolModel.name = response.result[0].name;                    
         //this.schoolModel.image = environment.studentImagePath + response.result[0].image;                    
         this.schoolModel.email = response.result[0].email;                    
      }
    })
  }

  blockStaff(staff_id){
    this.schoolModel.admin_id = localStorage.getItem('admin_id');
    this.schoolModel.staff_id = staff_id;
    this.accountService.blockStaff(this.schoolModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
          this.sucMsg= true;  
          this.successMessage = response.message;
          setTimeout (() => { this.sucMsg = false; }, 2000); 
          this.getStaff();       
      }else{
          this.errMsg= false;  
          this.errorMessage = response.message; 
          setTimeout (() => { this.errMsg = false; }, 2000);       
      }
    })
  }

}
