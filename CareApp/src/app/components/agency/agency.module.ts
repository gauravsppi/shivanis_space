import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { AgencyRoutingModule } from './agency-routing.module';
import { AddAgencyComponent } from './add-agency/add-agency.component';
import { ManageAgencyComponent } from './manage-agency/manage-agency.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { UiSwitchModule } from 'ngx-ui-switch';
import { LazyLoadImageModule } from 'ng-lazyload-image';
@NgModule({
  declarations: [AddAgencyComponent, ManageAgencyComponent],
  imports: [
    CommonModule,
    AgencyRoutingModule,
    NgxPaginationModule,
    UiSwitchModule,
    LazyLoadImageModule,
    ReactiveFormsModule,
  ]
})
export class AgencyModule { }
