import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReceiverService } from '../../../shared/services/receiver-service/receiver.service';
import { AgencyService } from '../../../shared/services/agency-service/agency.service'
import { NgxSpinnerService } from "../../../shared/services/spinner.service";
import Swal from 'sweetalert2';
import { Router } from "@angular/router";

@Component({
  selector: 'app-add-agency',
  templateUrl: './add-agency.component.html',
  styleUrls: ['./add-agency.component.scss']
})
export class AddAgencyComponent implements OnInit {
  addAgencyForm: FormGroup;
  submitted = false;
  passworedType = 'password';
  displayImage: any;
  selectImage: any;
  constructor(
    private AgencyService: AgencyService,
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder, 
    private router: Router,

  ) {
    const reg = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
    this.addAgencyForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      name: ['',[Validators.required]],
      phoneno: ['',[Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      password: ['',[Validators.required]],
      image: ['',[Validators.required]],
      url: ['', [Validators.pattern(reg)]],
      address: ['',[Validators.required, Validators.maxLength(300)]],
    });
   }

  ngOnInit(): void {
  }

  get f() { return this.addAgencyForm.controls; }



  onSubmit() {
    console.log(this.addAgencyForm.value)
    this.submitted = true;

    // stop here if form is invalid
    if (this.addAgencyForm.invalid) {
        return;
    }

    const formData = new FormData();
    formData.append('fileUpload', this.selectImage);
    formData.append('email', this.addAgencyForm.get('email').value);
    formData.append('name', this.addAgencyForm.get('name').value);
    formData.append('phoneno', this.addAgencyForm.get('phoneno').value);
    formData.append('password', this.addAgencyForm.get('password').value);
    formData.append('address', this.addAgencyForm.get('address').value);
    formData.append('url', this.addAgencyForm.get('url').value);
    console.log('form data>>>>>',formData);
    this.spinner.show();
    this.AgencyService.add_new_agency(formData).subscribe((data: any) => {
      this.spinner.hide();
      if(data.status == 0){
        console.log(data);
        Swal.fire({ icon: 'error',  title: 'Oops...',  text: data.message })

      }else{
        console.log(data);
        this.submitted = false;
        this.addAgencyForm.reset()
        this.displayImage='';
        this.router.navigate(['/agency/create-agency'])
        Swal.fire('Admin!',  'Agency has been added.', 'success');
      }
    }, error => {
      this.spinner.hide()
      console.log('error ----------',error)
      
    })
  }

  hideShowPass(){
    this.passworedType = this.passworedType == 'password' ? 'text': 'password'
  }


  getImage(event: any) { //Angular 11, for stricter type
		if(!event.target.files[0] || event.target.files[0].length == 0) {
			// this.msg = 'You must select an image';
			return;
		}
		
		var mimeType = event.target.files[0].type;
		
		if (mimeType.match(/image\/*/) == null) {
			// this.msg = "Only images are supported";
			return;
		}
		
		var reader = new FileReader();
		reader.readAsDataURL(event.target.files[0]);
		
		reader.onload = (_event) => {
      this.selectImage = event.target.files[0];
			this.displayImage = reader.result; 
		}
	}
 
}
