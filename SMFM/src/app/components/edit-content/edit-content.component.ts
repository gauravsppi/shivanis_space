import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import  * as $ from 'jquery';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';

import { AccountService } from '../../services/account.service';
import { Headers, Http } from '@angular/http';
import { Login_model } from '../../models/login_model';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ToolbarService, LinkService, ImageService, HtmlEditorService,TableService, QuickToolbarService } from '@syncfusion/ej2-angular-richtexteditor';

@Component({
  selector: 'app-edit-content',
  templateUrl: './edit-content.component.html',
  styleUrls: ['./edit-content.component.scss'],
  providers: [ ToolbarService, LinkService, ImageService, HtmlEditorService,TableService, QuickToolbarService]

})
export class EditContentComponent implements OnInit {
  public loginModel: Login_model;
  public addValidationError: Login_model;
  public Editor = ClassicEditor;
  public tools: object = {
    items: ['Undo', 'Redo', '|',
        'Bold', 'Italic', 'Underline', 'StrikeThrough', '|',
        'FontName', 'FontSize', 'FontColor', 'BackgroundColor', '|',
        'SubScript', 'SuperScript', '|',
        'LowerCase', 'UpperCase', '|',
        'Formats', 'Alignments', '|', 'OrderedList', 'UnorderedList', '|',
        'Indent', 'Outdent', '|', 'CreateLink','CreateTable',
        'Image', '|', 'ClearFormat', 'Print', 'SourceCode', '|', 'FullScreen']
  };
 
  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http) { }

  sucMsg: boolean = false;
  errMsg: boolean = false;
  successMessage: string;   
  errorMessage: string; 
  content: [];  
  noRecordsShow: boolean = false;
  loaderShow: boolean = false;
  total : number= 0;
  totalPages : number= 0;
  currentPage:number=1;
  page : number =1;
  key: string = 'id'; //set default
  reverse: boolean = false;

  ngOnInit(): void {
    this.loginModel = <Login_model>{
      admin_id: '',
      school_id: '',
      title:'',
      content:'',
      content_id:'',
      sortKey: 'title',
      sortBy: 'desc'    
    }

    this.addValidationError = <Login_model>{
      lesson_name:'',
      lesson:'',
    }  
    this.getAllContent();
  }

  getAllContent(){
    this.loaderShow = true;
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    this.loginModel.content_id = this.route.snapshot.params.id;
    this.loginModel.page = this.currentPage;
    this.accountService.getAllContent(this.loginModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
        for(var i=0; i< response.result.length; i++){
          if( this.loginModel.content_id == response.result[i].id){ 
            this.loginModel.title = response.result[i].title;
            this.loginModel.content = response.result[i].content;
          }  
        }       
      }
      this.loaderShow = false;
    })
  }

  onSubmitUpdate() {
    var valid = true;
    this.addValidationError.title = '';
    this.addValidationError.content = '';
    if (this.loginModel.title == "") {
        this.addValidationError.title = 'Please enter title';
        valid = false;
    }

    if (this.loginModel.content == "") {
      this.addValidationError.content = 'Please enter content';
      valid = false;
    }

    if(valid) {    
      this.accountService.updateContent(this.loginModel).then((response) => { 
        if (response.code == 200 && response.status == 'success') { 
          this.sucMsg = true;
          this.successMessage = response.message;
          setTimeout (() => { this.sucMsg = false; }, 2000);           
        } else {                  
          this.errMsg = true;
          this.errorMessage = response.message;
          setTimeout (() => { this.errMsg = false;  }, 2000); 
        }          
      })
    }
  }


}
