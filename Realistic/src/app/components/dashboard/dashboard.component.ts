import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { AccountService } from '../../services/account.service';
import { Headers, Http } from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import { Login_model } from '../../models/login_model';
import { Validation_model } from '../../models/Validation_model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public loginModel:Login_model;
  public Validation_model:Validation_model;

  constructor(private accountService:AccountService,private route: ActivatedRoute,private router: Router, private http: HttpClientModule) { }
  sucMsg:string=null;
  errMsg:string=null; 
  successMessage='Record updated Successfully'; 
  record:any=[];

  ngOnInit(): void {
    this.loginModel=<Login_model>{
      admin_id: '',
    }
    this.isLoggedIn();
    this.show_counters();
 
  }

  isLoggedIn() {
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    if(this.loginModel.admin_id == null){
    this.router.navigate(['/']);
    }
    } 
    
 
 
//  conponent to show  All User counter
show_counters(){
  this.accountService.user_counts().then((response)=>{
    if(response.code==200){
      this.record = response.total_users;
    }else{  
      this.errMsg = 'No Records';
    }
  }) 
}

}
