import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Headers, Http } from '@angular/http';

import { AccountService } from '../../services/account.service';

import { Login_model } from '../../models/login_model';
import Swal  from 'sweetalert2';

@Component({
  selector: 'app-sel-recommendations',
  templateUrl: './sel-recommendations.component.html',
  styleUrls: ['./sel-recommendations.component.scss']
})
export class SelRecommendationsComponent implements OnInit {
  public loginModel: Login_model;

  sucMsg: boolean =  false; 
  successMessage: string='';
  errMsg: boolean = false; 
  errorMessage: string=''; 
  noRecords: boolean =  false; 
  recommendations: [];
  total : number= 0;
  totalPages : number= 0;
  currentPage:number=1;
  page : number =1;
  key: string = 'recommend'; //set default
  reverse: boolean = false;
  loaderShow: boolean = false;

  constructor(private router: Router, private accountService: AccountService, private route: ActivatedRoute, private http: Http) { }


  ngOnInit(): void {
    this.loginModel = <Login_model>{
      admin_id: '',     
      school_id: '', 
      searchKey: '',
      sortKey: 'id',
      sortBy: 'desc'    
    }
    this.getSELrecommendations();
  }

  getSELrecommendations() {
    this.loaderShow = true;
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    this.loginModel.school_id = localStorage.getItem('school_id');
    this.loginModel.page = this.currentPage;
    this.loginModel.searchKey = "";
    this.accountService.getSELrecommendations(this.loginModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
        this.recommendations = response.result;
        this.total = response.count;
        this.totalPages=Math.ceil( response.count/10); 
        this.noRecords = false;
      }
      if(response.code == 200 && response.status == 'notFound'){
          this.noRecords = true;
          this.total =0
          this.totalPages=0  
          this.recommendations = [];     
      }
      this.loaderShow = false;
    })
  }

  pageChange(currentPage){
    this.currentPage=currentPage;
    if(this.loginModel.searchKey==""){
      this.getSELrecommendations();
    }else{
      this.searchFunction(this.currentPage);
    } 
  }

  counter(i: number) {
    return new Array(i);
  }

  searchFunction(currentPage){
    if(this.loginModel.searchKey== ""){
      this.currentPage=1;
      this.getSELrecommendations();
    }else{
      this.loaderShow = true;
      this.currentPage = 1;
      this.loginModel.page = currentPage;
      this.loginModel.school_id = localStorage.getItem('school_id');
      this.loginModel.admin_id = localStorage.getItem('admin_id');
      this.accountService.getSELrecommendations(this.loginModel).then((response) => {
        if (response.code == 200 && response.status == 'success') {
          this.total = response.count;
          this.totalPages=Math.ceil(response.count/10);
          this.recommendations = response.result;  
          this.noRecords = false;                  
        }
        if(response.code == 200 && response.status == 'notFound'){
            this.noRecords = true;
            this.total =0
            this.totalPages=0  
            this.recommendations = [];     
        }
        this.loaderShow = false;
      })
    }
  }

  sortFunction(sortKey){
    this.key = sortKey;
    this.loginModel.sortKey = sortKey;
    if(this.reverse === true){
      this.loginModel.sortBy = 'asc';
    }else{
      this.loginModel.sortBy = 'desc';
    }
    this.reverse = !this.reverse;   
    if(this.loginModel.searchKey== ""){
      this.currentPage=1;
      this.getSELrecommendations();
    }else{
      this.searchFunction(this.currentPage);
    }
  }

viewRecommendation(id){  
  this.loginModel.recommend_id = id;
  this.accountService.viewSELrecommendations(this.loginModel).then((response) => {  
    if (response.code == 200 && response.status == 'success') {
        this.loginModel.user_name = response.result[0].user_name; 
        this.loginModel.user_type = response.result[0].user_type; 
        this.loginModel.school_name = response.result[0].name; 
        this.loginModel.recommend = response.result[0].recommend; 
    }
  })        
}

deleteRecommendation(id){
    this.loginModel.recommend_id = id;
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to delete!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
    }).then((result) => {
      this.loaderShow = true;
      if (result.isConfirmed) {
        this.accountService.deleteSELrecommendations(this.loginModel).then((response) => {
          this.loaderShow = false;
          if (response.code == 200 && response.status == 'success') {
            this.sucMsg = true;
            this.successMessage = response.message;
            setTimeout (() => { this.sucMsg = false;  }, 3000); 
            this.getSELrecommendations();                
          }else{
            this.errMsg = true;
            this.errorMessage = "Something went wrong";
            setTimeout (() => { this.errMsg = false;  }, 3000); 
          }
        })
      } else if (result.isDismissed) {
        this.loaderShow = false;
        Swal.fire({  showCancelButton: false,  icon: 'success',  title: 'Your file is safe!'})
      }
    })
  }
}
