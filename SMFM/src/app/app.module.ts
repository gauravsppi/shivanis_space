import { BrowserModule } from '@angular/platform-browser';
import { NgModule,LOCALE_ID } from '@angular/core';
import { HttpModule } from '@angular/http';
import { DatePipe } from '@angular/common';
import { HttpClientModule } from  '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import {LocationStrategy,PathLocationStrategy, HashLocationStrategy} from '@angular/common';
import { BaseHttpService } from './services/base-http.service';
import { AccountService } from './services/account.service';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { UsersComponent } from './components/users/users.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { TopbarComponent } from './components/topbar/topbar.component';
import { ProfileComponent } from './components/profile/profile.component';
import { SchoolListingComponent } from './components/school-listing/school-listing.component';
import { TeacherListingComponent } from './components/teacher-listing/teacher-listing.component';
import { StudentListingComponent } from './components/student-listing/student-listing.component';
import { AbuseWordsComponent } from './components/abuse-words/abuse-words.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CategoryComponent } from './components/category/category.component';
import { LessonComponent } from './components/lesson/lesson.component';
import { AddLessonComponent } from './components/add-lesson/add-lesson.component';
import { EditLessonComponent } from './components/edit-lesson/edit-lesson.component';
import { ViewLessonComponent } from './components/view-lesson/view-lesson.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { SafePipe } from './safe.pipe';
//import { DatePipe } from './date_pipe';
import { FormatDatePipe } from './formatDate.pipe'


import { SelRecommendationsComponent } from './components/sel-recommendations/sel-recommendations.component';
import { StudentViewComponent } from './components/student-view/student-view.component';
import { SchoolViewComponent } from './components/school-view/school-view.component';
import { TeacherViewComponent } from './components/teacher-view/teacher-view.component';
import { ClassViewComponent } from './components/class-view/class-view.component';
import { LoaderComponent } from './components/loader/loader.component';
import { StudentIEPComponent } from './components/student-iep/student-iep.component';
import { CoursesListComponent } from './components/courses-list/courses-list.component';
import { ClassListComponent } from './components/class-list/class-list.component';
import { AddAnnouncementComponent } from './components/add-announcement/add-announcement.component';
import { EditAnnouncementComponent } from './components/edit-announcement/edit-announcement.component';
import { ListAnnouncementComponent } from './components/list-announcement/list-announcement.component';
import { TeachersListComponent } from './components/teachers-list/teachers-list.component';
import { StaffListingComponent } from './components/staff-listing/staff-listing.component';
import { AddIepComponent } from './components/add-iep/add-iep.component';
import { EditContentComponent } from './components/edit-content/edit-content.component';
import { ListContentComponent } from './components/list-content/list-content.component';
import { ListObservationCategoryComponent } from './components/list-observation-category/list-observation-category.component';
import { ListObservationSubCategoryComponent } from './components/list-observation-sub-category/list-observation-sub-category.component';
import { ListObservationSubSubCategoryComponent } from './components/list-observation-sub-sub-category/list-observation-sub-sub-category.component';
import { ListInterventionsComponent } from './components/list-interventions/list-interventions.component';
// Imported Syncfusion RichTextEditorModule from Rich Text Editor package
//import { RichTextEditorModule } from '@syncfusion/ej2-angular-richtexteditor';
import { RichTextEditorModule } from "@syncfusion/ej2-angular-richtexteditor";
import { ListContactsInfoComponent } from './components/list-contacts-info/list-contacts-info.component';
import { ObservationsCategoryComponent } from './components/observations-category/observations-category.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgxPopper } from 'angular-popper';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    HeaderComponent,
    FooterComponent,
    UsersComponent,
    SidebarComponent,
    TopbarComponent,
    ProfileComponent,
    SchoolListingComponent,
    TeacherListingComponent,
    StudentListingComponent,
    AbuseWordsComponent,
    CategoryComponent,
    LessonComponent,
    AddLessonComponent,
    EditLessonComponent,
    ViewLessonComponent,
    SafePipe,
    SelRecommendationsComponent,
    StudentViewComponent,
    SchoolViewComponent,
    TeacherViewComponent,
    ClassViewComponent,
    LoaderComponent,
    StudentIEPComponent,
    CoursesListComponent,
    ClassListComponent,
    AddAnnouncementComponent,
    EditAnnouncementComponent,
    ListAnnouncementComponent,
    TeachersListComponent,
    StaffListingComponent,
    AddIepComponent,
    EditContentComponent,
    ListContentComponent,
    ListObservationCategoryComponent,
    ListObservationSubCategoryComponent,
    ListObservationSubSubCategoryComponent,
    ListInterventionsComponent,
    FormatDatePipe,
    ListContactsInfoComponent,
    ObservationsCategoryComponent
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    NoopAnimationsModule,
    CKEditorModule,
    RichTextEditorModule,
    NgMultiSelectDropDownModule.forRoot(),
    NgxPopper,
    

  ],
  providers: [AccountService,BaseHttpService,
    SelRecommendationsComponent, 
    StudentIEPComponent,
    AbuseWordsComponent,
    SchoolListingComponent,
    DatePipe,
    { provide: LOCALE_ID, useValue: 'nl-NL' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
