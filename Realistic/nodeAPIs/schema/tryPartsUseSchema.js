const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tryPartsUseSchema = new mongoose.Schema({
    part_id : [], //store parts id array
    user_id: {
        type: String     
    }, 
    car_id: {
        type: String     
    }, 
    image: {
        type: String     
    }, 
    created_at: {
        type: Date,
        default: Date.now
    },    
});




tryPartsUseSchema.virtual('tryParts_details',{
    ref: 'parts',
    localField: 'part_id',
    foreignField: '_id',
    justOne: true
});
tryPartsUseSchema.virtual('user_details',{
    ref: 'users',
    localField: 'user_id',
    foreignField: '_id',
    justOne: true
});
tryPartsUseSchema.virtual('user_car_details',{
    ref: 'user_cars',
    localField: 'car_id',
    foreignField: '_id',
    justOne: true
});

tryPartsUseSchema.set('toObject', { virtuals: true });
tryPartsUseSchema.set('toJSON', { virtuals: true });

module.exports = tryPartsUseSchema;
