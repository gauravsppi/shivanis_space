const CarsModel = require("../../model/CarsModel"); //for define car model
const UsersModel = require("../../model/UsersModel"); //for define users model

const path = require('path'); //for define path
const fs = require('fs'); //for file system 
var nodemailer = require('nodemailer'); 
var formidable = require('formidable'); 

module.exports = {	
	test: function(req, res) { //Add car company
		res.send({code: 200, message:'test api...'});
	},
	addCarCompanyName: function(req, res) { //Add car company name
	 	var data=req.body;	 
	 	if(Object.keys(data).length > 0){ //Check if parameter passes or not
		  	if(data.company_name != ''){ 
				CarsModel.addCarCompanyName(req.con, data, function(err, added) { 
					if (err) throw err;	
					if(added._id != ''){															
						res.send({
							"code": 200,									
							"status": 'success',
							"message":"Car company name saved Successfully"
						});
					}else{
						res.send({
							"code": 400,
							"status": 'notInserted',
							"message":"Record Not Insert"
						});
					}
			    })	
			}else{
				res.send({
					"code": 400,
					"status": 'notEmpty',
					"message":"Please fill all the required fileds"
				});
			}
		}else{
			res.send({
				"code": 400,
				"status": 'noParameter',
				"message":"Something went wrong"
			});
		}	
	},
	updateCarCompanyName: function(req, res) { //Update car company
	 	var data=req.body;	 
	 	if(Object.keys(data).length > 0){ //Check if parameter passes or not
		  	if(data.company_name != '' && data.company_id != ''){ 
				CarsModel.updateCarCompanyName(req.con, data, function(err, updated) { //Update company name
					if (err) throw err;	
					if(updated.ok > 0 || updated.nModified > 0 || updated.n > 0){ 													
						res.send({
							"code": 200,									
							"status": 'success',
							"message":"Car company updated Successfully"
						});
					}else{
						res.send({
							"code": 400,
							"status": 'notUpdated',
							"message":"Record not updated"
						});
					}
			    })	
			}else{
				res.send({
					"code": 400,
					"status": 'notEmpty',
					"message":"Please fill all the required fileds"
				});
			}
		}else{
			res.send({
				"code": 400,
				"status": 'noParameter',
				"message":"Something went wrong"
			});
		}	
	},
	getCarCompanyNames: function(req, res) { //Get car company names
	 	var data=req.body;	 
	 	console.log('???',data);
		CarsModel.getCarCompanyNames(req.con, data, function(err, result) { //Get company name
			if(err) throw err;	
			if(result.length > 0){ 													
				res.send({
					"code": 200,									
					"status": 'success',
					"result": result,
					"message":"Car company names fetch Successfully"
				});
			}else{
				res.send({
					"code": 400,
					"status": 'notFound',
					"message":"Record not found"
				});
			}
	    })		
	},
	getCarCompanyName: function(req, res) { //For admin: Get car company names
	 	var data=req.body;	 
		CarsModel.getCarCompanyName(req.con, data, function(err, result) { //Get company name
			if(err) throw err;	
			if(result.length > 0){ 	
				CarsModel.countCarCompanyName(req.con, data, function(err, result_count) { //Get company name
					res.send({
						"code": 200,									
						"status": 'success',
						"result": result,
						"count": result_count.length,
						"message":"Car company names fetch Successfully"
					});
				})	
			}else{
				res.send({
					"code": 400,
					"status": 'notFound',
					"message":"Record not found"
				});
			}
	    })		
	},
	deleteCarCompanyName: function(req, res) { //Add car company name
	 	var data=req.body;	 
	 	if(Object.keys(data).length > 0){ //Check if parameter passes or not
		  	if(data.company_id != ''){ 
				CarsModel.deleteCarCompanyName(req.con, data, function(err, added) { 
					if (err) throw err;	
					if(Object.keys(added).length > 0){															
						res.send({
							"code": 200,									
							"status": 'success',
							"message":"Car company name deleted successfully"
						});
					}else{
						res.send({
							"code": 400,
							"status": 'notDeleted',
							"message":"Something went wrong"
						});
					}
			    })	
			}else{
				res.send({
					"code": 400,
					"status": 'notEmpty',
					"message":"Please fill all the required fileds"
				});
			}
		}else{
			res.send({
				"code": 400,
				"status": 'noParameter',
				"message":"Something went wrong"
			});
		}	
	},
	viewCarCompanyName: function(req, res) { //Add car company name
	 	var data=req.body;	 
	 	if(Object.keys(data).length > 0){ //Check if parameter passes or not
		  	if(data.company_id != ''){ 
				CarsModel.viewCarCompanyName(req.con, data, function(err, result) { 
					if (err) throw err;	
					if(result.length > 0){															
						res.send({
							"code": 200,									
							"status": 'success',
							"result": result,
							"message":"Car company name fetch successfully"
						});
					}else{
						res.send({
							"code": 400,
							"status": 'notFound',
							"message":"Records not found"
						});
					}
			    })	
			}else{
				res.send({
					"code": 400,
					"status": 'notEmpty',
					"message":"Please fill all the required fileds"
				});
			}
		}else{
			res.send({
				"code": 400,
				"status": 'noParameter',
				"message":"Something went wrong"
			});
		}	
	},
	addCarModel: function(req, res) { //Add car company
	 	var data=req.body;	 
	 	if(Object.keys(data).length > 0){ //Check if parameter passes or not
		  	if(data.model_name != '' && data.company_id != ''){ 
				CarsModel.addCarModel(req.con, data, function(err, added) { 
					if (err) throw err;	
					if(added._id != ''){															
						res.send({
							"code": 200,									
							"status": 'success',
							"message":"Car model saved Successfully"
						});
					}else{
						res.send({
							"code": 400,
							"status": 'notInserted',
							"message":"Record Not Insert"
						});
					}
			    })	
			}else{
				res.send({
					"code": 400,
					"status": 'notEmpty',
					"message":"Please fill all the required fileds"
				});
			}
		}else{
			res.send({
				"code": 400,
				"status": 'noParameter',
				"message":"Something went wrong"
			});
		}	
	},
	updateCarModel: function(req, res) { //Update car model
	 	var data=req.body;	 
	 	if(Object.keys(data).length > 0){ //Check if parameter passes or not
		  	if(data.model_name != ''){ 
				CarsModel.updateCarModel(req.con, data, function(err, updated) { 
					if (err) throw err;	
					if(updated.ok > 0 || updated.nModified > 0 || updated.n > 0){											
						res.send({
							"code": 200,									
							"status": 'success',
							"message":"Car model updated Successfully"
						});
					}else{
						res.send({
							"code": 400,
							"status": 'notUpdated',
							"message":"Something went wrong"
						});
					}
			    })	
			}else{
				res.send({
					"code": 400,
					"status": 'notEmpty',
					"message":"Please fill all the required fileds"
				});
			}
		}else{
			res.send({
				"code": 400,
				"status": 'noParameter',
				"message":"Something went wrong"
			});
		}	
	},
	getCarModels: function(req, res) { //Get car model
	 	var data=req.body;	 
		CarsModel.getCarModels(req.con, data, function(err, result) { //Get car models
			if(err) throw err;	
			console.log('model name: ', result);
			if(result.length > 0){ 													
				res.send({
					"code": 200,									
					"status": 'success',
					"result": result,
					"message":"Car models fetch Successfully"
				});
			}else{
				res.send({
					"code": 400,
					"status": 'notFound',
					"message":"Record not found"
				});
			}
	    })		
	},
	getCarModel: function(req, res) { //Admin: Get car model
	 	var data=req.body;	 
		CarsModel.getCarModel(req.con, data, function(err, result) { //Get car models
			if(err) throw err;	
			if(result.length > 0){ 
				CarsModel.countCarModel(req.con, data, function(err, result_count) { //Get car models	
					res.send({
						"code": 200,									
						"status": 'success',
						"result": result,
						"count": result_count.length,
						"message":"Car models fetch Successfully"
					});
				});	
			}else{
				res.send({
					"code": 400,
					"status": 'notFound',
					"message":"Record not found"
				});
			}
	    })		
	},
	// deleteCarModels: function(req, res) { //Delete car model
	//  	var data=req.body;	 
	// 	CarsModel.deleteCarModels(req.con, data, function(err, result) { //Delete car models
	// 		if(err) throw err;	
	// 		if(Object.keys(result).length > 0){															
	// 			res.send({
	// 				"code": 200,									
	// 				"status": 'success',
	// 				"result": result,
	// 				"message":"Car models deleted successfully"
	// 			});
	// 		}else{
	// 			res.send({
	// 				"code": 400,
	// 				"status": 'notDeleted',
	// 				"message":"Something went wrong"
	// 			});
	// 		}
	//     })		
	// },
	deleteCarModels: function(req, res) { //Delete car model
	 	var data=req.body;	 
		CarsModel.deleteCarModels(req.con, data, function(err, result) { //Delete car models
			if(err) throw err;	
			if(Object.keys(result).length > 0){															
				res.send({
					"code": 200,									
					"status": 'success',
					"result": result,
					"message":"Car models deleted successfully"
				});
			}else{
				res.send({
					"code": 400,
					"status": 'notDeleted',
					"message":"Something went wrong"
				});
			}
	    })		
	},
	viewCarModels: function(req, res) { //View car model
	 	var data=req.body;	 
		CarsModel.viewCarModels(req.con, data, function(err, result) { //view car models
			if(err) throw err;	
			if(result.length > 0){ 													
				res.send({
					"code": 200,									
					"status": 'success',
					"result": result,
					"message":"Car models info fetch successfully"
				});
			}else{
				res.send({
					"code": 400,
					"status": 'notFound',
					"message":"Record not found"
				});
			}
	    })		
	},

	addTryParts: function(req, res) { //Add Try parts
	 	var form = new formidable.IncomingForm();
	 	form.parse(req, function(err, fields, files){
	 		var fileName = '';
	    	var data = fields;
		 	
	  		/**** Upload Car image ********/					  							  		
	  		if(Object.keys(files).length > 0){ 
		  		//fileName = Date.now()+path.extname(files.image.name); 	           
		  		fileName = files.image.name; 	           
    			var rawData = fs.readFileSync(files.image.path) 
    			fs.writeFile("public/uploads/tryParts/"+fileName, rawData, function(err) {
    				if(err){ 
	                    res_data = {'code': 400,'error': err,'message': 'File not uploaded', status: 'error'}
	                 	res.send(res_data);
	                }else{ 
	                	data.image = fileName;
	                	CarsModel.addTryParts(req.con, data, function(err, added) { 
							if (err) throw err;	
							if(added._id != ''){															
								res.send({"code": 200,"status": 'success',"message":"Try parts saved Successfully" });
							}else{
								res.send({"code": 400,"status": 'notInserted',"message":"Something went wrong" });
							}
					    })	
	                }
    			})
    		}else{
    			res.send({"code": 400,"status": 'imageFieldEmpty',"message":"Image field is required" });
    		}	
		})	
	},
	getTryParts: function(req, res) { //Get try parts
	 	var data=req.body;	 
		CarsModel.getTryParts(req.con, data, function(err, result) { //Get try parts
			if(err) throw err;	
			if(result.length > 0){ 													
				res.send({
					"code": 200,									
					"status": 'success',
					"result": result,
					"message":"Try parts fetch Successfully"
				});
			}else{
				res.send({
					"code": 400,
					"status": 'notFound',
					"message":"Record not found"
				});
			}
	    })		
	},

	saveUsedTryParts: function(req, res) { //Saved user try parts uses
	 	var data=req.body;	
	 	console.log('save used try parts data : ', data);
	 	if(Object.keys(data).length > 0){ //Check if parameter passes or not
			if(data.user_id != ''){ 
				UsersModel.checkIDExits(req.con, data, function(err, exits) { //check ID exists or not
			  		if (err) throw err;
			  		if(exits._id != '' ){ 
						CarsModel.saveUsedTryParts(req.con, data, function(err, saved) { 
							if(err) throw err;	
							if(saved._id != ''){ 													
								res.send({
									"code": 200,									
									"status": 'success',								
									"message":"Info saved Successfully"
								});
							}else{
								res.send({
									"code": 400,
									"status": 'notInserted',
									"message":"Record not notInserted"
								});
							}
					    })	
					}else{
						res.send({"code": 400,"status": 'notExists',"message":"User ID not exists" });
					}  
				})
			}else{
				res.send({"code": 400,"status": 'notEmpty',	"message":"Please fill all the required fileds"	});
			}
		}else{
			res.send({"code": 400,"status": 'noParameter',"message":"Something went wrong"});
		}			  	
	},
	saveTryPartsOfImage: function(req, res) { //Saved user try parts uses
	 	var form = new formidable.IncomingForm();
	 	form.parse(req, function(err, fields, files){
		 	console.log('save used try parts with image : ', files);
		 	console.log('save used try parts with fileds : ', fields);

	 		var fileName = '';
	    	var data = fields;
		 	if(Object.keys(data).length > 0){ //Check if parameter passes or not
				if(data.user_id != ''){ 
					UsersModel.checkIDExits(req.con, data, function(err, exits) { //check ID exists or not
				  		if(err){
				  			res.send({"code": 400,"status": "error",'error': err, "message":"Something went wrong" });
				  		}else{ 
					  		if(exits._id != '' ){ 
						  		/***** Upload try parts used image ****/
					  			if(Object.keys(files).length > 0){ 
							  		fileName = Date.now()+path.extname(files.image.name); 	           
					    			var rawData = fs.readFileSync(files.image.path) 
					    			fs.writeFile("public/uploads/userCarImages/"+fileName, rawData, function(err) {
					    				if(err){ 
						                    res_data = {'code': 400,'error': err,'message': 'File not uploaded', status: 'error'}
						                 	res.send(res_data);
						                }else{ 
						                	data.image = fileName;
											CarsModel.saveTryPartsOfImage(req.con, data, function(err, saved) { 
												if(err) throw err;	
												if(saved._id != ''){ 													
													res.send({
														"code": 200,									
														"status": 'success',								
														"message":"Info saved Successfully"
													});
												}else{
													res.send({
														"code": 400,
														"status": 'notInserted',
														"message":"Something went wrong"
													});
												}
										    })	
										}
									})	    

								}else{
									res.send({"code": 400,"status": 'ImageParameterEmpty',"message":"Image parameter not empty" });
								}  
						    }else{
						    	res.send({"code": 400,"status": 'notExists',"message":"User ID not exists" });
						    }		
						}    
					})
				}else{
					res.send({"code": 400,"status": 'notEmpty',	"message":"Please fill all the required fileds"	});
				}
			}else{
				res.send({"code": 400,"status": 'noParameter',"message":"Something went wrong"});
			}	
		})			  	
	},

	save_multiCarModel: function(req, res) { //Saved user try parts uses
	 	var data=req.body;	
	 	if(Object.keys(data).length > 0){ //Check if parameter passes or not
			if(data.user_id != ''){ 
					CarsModel.savemultiCarCompanyModel(req.con, data, function(err, saved) { 
							if(err) throw err;	
							if(saved._id != ''){ 													
								res.send({
									"code": 200,									
									"status": 'success',								
									"message":"Info saved Successfully"
								});
							}else{
								res.send({
									"code": 400,
									"status": 'notInserted',
									"message":"Record not notInserted"
								});
							}
					})	
			}else{
				res.send({"code": 400,"status": 'notEmpty',	"message":"Please fill all the required fileds"	});
			}
		}else{
			res.send({"code": 400,"status": 'noParameter',"message":"Something went wrong"});
		}			  	
	},
	

	//Add Multipart Cases 550 to 612 line Apx.
	getmultiCarModel: function(req, res) { //Get try parts
	 	var data=req.body;	 
		CarsModel.getmultiCarModels(req.con, data, function(err, result) { //Get try parts
			if(err) throw err;	
			if(result.length > 0){ 													
				res.send({
					"code": 200,									
					"status": 'success',
					"result": result,
					"message":"Multi CarCompany Models fetch Successfully"
				});
			}else{
				res.send({
					"code": 400,
					"status": 'notFound',
					"message":"Record not found"
				});
			}
	    })		
	},

	deleteMultiParts: function(req, res) { //Delete car model
	 	var data=req.body;	 
		CarsModel.deleteMultiPart(req.con, data, function(err, result) { //Delete Multi Part
			if(err) throw err;	
			if(Object.keys(result).length > 0){															
				res.send({
					"code": 200,									
					"status": 'success',
					"result": result,
					"message":"CarCompany models deleted successfully"
				});
			}else{
				res.send({
					"code": 400,
					"status": 'notDeleted',
					"message":"Something went wrong"
				});
			}
	    })		
	},
	deleteAllmultiCarModels: function(req, res) { //Delete car model
	 	var data=req.body;	 
		CarsModel.deleteAllMultiPart(req.con, data, function(err, result) { //Delete Multi Part
			if(err) throw err;	
			if(Object.keys(result).length > 0){															
				res.send({
					"code": 200,									
					"status": 'success',
					"result": result,
					"message":"CarCompany models deleted successfully"
				});
			}else{
				res.send({
					"code": 400,
					"status": 'notDeleted',
					"message":"Something went wrong"
				});
			}
	    })		
	},

	// Update Multip Part Cases
	get_edit_multiCarModel: function(req, res) { //Get try parts
	 	var data=req.body;	 
		CarsModel.get_edit_multiCarModels(req.con, data, function(err, result) { //Get try parts
			if(err) throw err;	
			if(result.length > 0){ 													
				res.send({
					"code": 200,									
					"status": 'success',
					"result": result,
					"message":"Multi CarCompany Models fetch Successfully"
				});
			}else{
				res.send({
					"code": 400,
					"status": 'notFound',
					"message":"Record not found"
				});
			}
	    })		
	},
	save_multiCarModelwithID: function(req, res) { //Saved user try parts uses
	 	var data=req.body;	
	 	if(Object.keys(data).length > 0){ //Check if parameter passes or not
			if(data.user_id != ''){ 
					CarsModel.save_edit_multiCarCompanyModel(req.con, data, function(err, saved) { 
							if(err) throw err;	
							if(saved._id != ''){ 													
								res.send({
									"code": 200,									
									"status": 'success',								
									"message":"Info saved Successfully"
								});
							}else{
								res.send({
									"code": 400,
									"status": 'notInserted',
									"message":"Record not notInserted"
								});
							}
					})	
			}else{
				res.send({"code": 400,"status": 'notEmpty',	"message":"Please fill all the required fileds"	});
			}
		}else{
			res.send({"code": 400,"status": 'noParameter',"message":"Something went wrong"});
		}			  	
	},


	
	
}

