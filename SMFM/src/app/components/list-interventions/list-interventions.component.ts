import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Headers, Http } from '@angular/http';
import Swal  from 'sweetalert2';
import  * as $ from 'jquery';
declare const modalOpen_add: any;
declare const modalOpen_edit: any;
import { AccountService } from '../../services/account.service';
import { school_model } from '../../models/school_model';
@Component({
  selector: 'app-list-interventions',
  templateUrl: './list-interventions.component.html',
  styleUrls: ['./list-interventions.component.scss']
})
export class ListInterventionsComponent implements OnInit {

  public schoolModel: school_model;
  public addValidationError: school_model;
  sucMsg: boolean =  false; 
  successMessage: string='';
  errMsg: boolean = false; 
  errorMessage: string=''; 
  noRecords: boolean = false;

  total : number= 0;
  totalPages : number= 0;
  currentPage:number=1;
  page : number =1;
  key: string = 'position'; //set default
  reverse: boolean = false; 
  loaderShow: boolean = false; 

  checkStatus1: boolean = false; 
  checkStatus0: boolean = false; 
  ObCategories = [];
  listView = [];
intereventionId:number;
  constructor(private router: Router, private accountService: AccountService, private route: ActivatedRoute, private http: Http) { }

  ngOnInit(): void {
    this.schoolModel = <school_model>{
      user_id: '',
      strategy_name: '',        
      intervention_id: '',      
      status: '1' ,
      searchKey: '',
      sortKey: 'position',
      sortBy: 'ASC', 
      strategy_unique_id:'',
      strategy_description:'',
      newPosition:'',
    }
    this.addValidationError = <school_model>{
      user_id: '',
      strategy_name: '',   
      intervention_id: '',      
      status: '' ,  
      strategy_description:'',
    }
    this.getInterventions();
    modalOpen_add();
    modalOpen_edit();
  }

  add_btn(){
    this.schoolModel.strategy_name = '';
    this.schoolModel.strategy_unique_id = '';
    this.schoolModel.strategy_description = '';
    this.schoolModel.status = '1';
  }
  update_btn(strategy_name, strategy_unique_id, strategy_description,status, id){
    this.schoolModel.strategy_name = strategy_name;
    this.schoolModel.strategy_unique_id = strategy_unique_id;
    this.schoolModel.strategy_description = strategy_description;
    this.schoolModel.status =status;
    this.schoolModel.intervention_id = id;
    if(status == 1){ this.checkStatus1 = true }else{ this.checkStatus0 = true }   
  }

  updateFunction(){
    var valid = true;
    this.addValidationError.strategy_name = '';
    this.addValidationError.strategy_description = '';
    if (this.schoolModel.strategy_name == "") {
        this.addValidationError.strategy_name = 'Please enter intervention name';
        valid = false;
    }
    if (this.schoolModel.strategy_description == "") {
      this.addValidationError.strategy_description = 'Please enter intervention description';
      valid = false;
  }
    if (valid) {  
      this.schoolModel.user_id = localStorage.getItem('admin_id');
      this.schoolModel.user_type = 'admin';    
      this.schoolModel.type = 'edit';  
      this.accountService.saveInterventionStrategy(this.schoolModel).then((response) => {  
        if (response.code == 200 && response.status == 'success') {        
          this.getInterventions();  
          this.sucMsg = true;
          this.successMessage = response.message;
          setTimeout (() => { this.sucMsg = false;  }, 3000); 
        } else {           
          this.errMsg = true;
          this.errorMessage = response.message;
          setTimeout (() => { this.errMsg = false;  }, 3000); 
        }         
        modalOpen_edit();
      })
    }
  }

  saveFunction(){
    var valid = true;
    this.addValidationError.strategy_name = '';
    this.addValidationError.strategy_description = '';
    if (this.schoolModel.strategy_name == "") {
        this.addValidationError.strategy_name = 'Please enter intervention name';
        valid = false;
    }
    if (this.schoolModel.strategy_description == "") {
      this.addValidationError.strategy_description = 'Please enter intervention description';
      valid = false;
  }
    if (valid) {  
      this.schoolModel.user_id = localStorage.getItem('admin_id');
      this.schoolModel.user_type = 'admin';    
      this.schoolModel.type = 'add';      
      console.log('incoming request >>>',this.schoolModel);
      this.accountService.saveInterventionStrategy(this.schoolModel).then((response) => {  
        if (response.code == 200 && response.status == 'success') {        
          this.getInterventions();  
          this.sucMsg = true;
          this.successMessage = response.message;
          setTimeout (() => { this.sucMsg = false;  }, 3000); 
        } else {           
          this.errMsg = true;
          this.errorMessage = response.message;
          setTimeout (() => { this.errMsg = false;  }, 3000); 
        }         
        modalOpen_add();
      })
    }
  }

  getInterventions() {
    this.loaderShow = true;
    this.schoolModel.page= this.currentPage;
    this.schoolModel.user_id = localStorage.getItem('admin_id');
    this.schoolModel.user_type = 'admin';
    console.log('enter here with values>>>',this.schoolModel);
    this.accountService.getInterventionStrategy(this.schoolModel).then((response) => {
      console.log('incoming data',response);
      if (response.code == 200 && response.status == 'success') {
        this.ObCategories = response.result; 
        this.total = response.count;
        this.totalPages=Math.ceil( response.count/10);
        this.noRecords = false;
      }else{
        this.noRecords = true;
        this.ObCategories = []; 
        this.total = 0;
        this.totalPages=0;
      }
      this.loaderShow = false;
    })
  }

  pageChange(currentPage){
    this.currentPage=currentPage;
    if(this.schoolModel.searchKey==""){
      this.getInterventions();
    }else{
      this.searchFunction(this.currentPage);
    } 
  }

  counter(i: number) {
    return new Array(i);
  }

  searchFunction(currentPage){
    if(this.schoolModel.searchKey== ""){
      this.currentPage=1;
      this.getInterventions();
    }else{
      this.loaderShow = true;
      this.currentPage=currentPage;
      this.schoolModel.page = this.currentPage;
      this.accountService.getInterventionStrategy(this.schoolModel).then((response) => {
        if (response.code == 200 && response.status == 'success') {
          this.total = response.count;
          this.totalPages=Math.ceil(response.count/10);
          this.ObCategories = response.result;  
          this.noRecords = false;                  
        }else{
          this.noRecords = true;
          this.ObCategories = []; 
          this.total = 0;
          this.totalPages=0;
        }
        this.loaderShow = false;
      })
    }
  }

  sortFunction(sortKey){
    this.key = sortKey;
    this.schoolModel.sortKey = sortKey;
    if(this.reverse === true){
      this.schoolModel.sortBy = 'ASC';
    }else{
      this.schoolModel.sortBy = 'ASC';
    }
    this.reverse = !this.reverse;   
    if(this.schoolModel.searchKey== ""){
      this.currentPage=1;
      this.getInterventions();
    }else{
      this.searchFunction(this.currentPage);
    }
  }

  changeStatus(id, status){ 
    this.schoolModel.user_id = localStorage.getItem('admin_id');
    this.schoolModel.user_type = "admin";
    this.schoolModel.intervention_id = id;
    this.schoolModel.status = status;
    this.accountService.changeStatusInterventionStrategy(this.schoolModel).then((response) => {
      this.loaderShow = false;
      if (response.code == 200 && response.status == 'success') {
        this.getInterventions();  
        this.sucMsg = true;
        this.successMessage = response.message;                    
        setTimeout (() => { this.sucMsg = false;  }, 3000);        
      }else{
        this.errMsg = true;
        this.errorMessage = response.message;
        setTimeout (() => { this.errMsg = false;  }, 3000); 
      }
    })
  }

  deleteFunction(id,position){
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to delete!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
    }).then((result) => {
      this.loaderShow = true;
      if (result.isConfirmed) {        
        this.schoolModel.user_id = localStorage.getItem('admin_id');
        this.schoolModel.user_type = "admin";
        this.schoolModel.intervention_id = id;
        this.schoolModel.newPosition = position;
        this.accountService.deleteInterventionStrategy(this.schoolModel).then((response) => {
          this.loaderShow = false;
          if (response.code == 200 && response.status == 'success') {
            this.getInterventions();  
            this.sucMsg = true;
            this.successMessage = response.message;                    
            setTimeout (() => { this.sucMsg = false;  }, 3000);        
          }else{
            this.errMsg = true;
            this.errorMessage = response.message;
            setTimeout (() => { this.errMsg = false;  }, 3000); 
          }
        })
      } else if (result.isDismissed) {
        this.loaderShow = false;
        Swal.fire({  showCancelButton: false,  icon: 'success',  title: 'Your file is safe!'})
      }
    })      
  }
  addPosition(id, position, e){
    console.log('first value',position);
    console.log('second value',e);
    this.schoolModel.newPosition=position;
    this.intereventionId=id;
  }
  updatePosition(id){
    console.log('click here>>>',this.schoolModel);
    this.schoolModel.intervention_id=id;
    this.loaderShow = true;
    this.accountService.changePositions(this.schoolModel).then((response) => {
      if(response.code==200){
        this.getInterventions();  
        this.loaderShow = false;
        this.sucMsg = true;
        this.successMessage = response.message;                    
        setTimeout (() => { this.sucMsg = false;  }, 2000); 
      }else{
        this.errMsg = true;
        this.errorMessage = response.message;
        setTimeout (() => { this.errMsg = false;  }, 3000); 
      }
    })
  }
  viewList(){
    this.loaderShow = true;
    this.accountService.getInterventionListPosition(this.schoolModel).then((response) => {
      console.log('get response here',response);
      if(response.code==200){
        this.loaderShow = false;
        this.listView=response.result;
      }else{

      }
    })
  }
}
