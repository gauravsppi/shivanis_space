import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCarCompanyComponent } from './add-car-company.component';

describe('AddCarCompanyComponent', () => {
  let component: AddCarCompanyComponent;
  let fixture: ComponentFixture<AddCarCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCarCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCarCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
