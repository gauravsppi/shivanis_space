import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {ProfileService} from '../../../shared/services/profile/profile.service';
import { NgxSpinnerService } from "../../../shared/services/spinner.service";
import Swal from 'sweetalert2';
import { Router } from "@angular/router";

@Component({
  selector: 'app-admin-profile',
  templateUrl: './admin-profile.component.html',
  styleUrls: ['./admin-profile.component.scss']
})
export class AdminProfileComponent implements OnInit {
  updateAgencyProfileForm: FormGroup;
  submitted = false;
  isShowDivIf = true;
  isHideDivIf = false;
  role:string;
  showAgency:boolean;
  showAdmin:boolean;
  profileData:any;
  displayImage: any;
  selectImage: any;
  constructor(private ProfileService: ProfileService,
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder, 
    private router: Router,) { 
      const reg = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
    this.updateAgencyProfileForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      phone: ['',[Validators.required]],
      url: ['', [Validators.pattern(reg)]],
      address: ['',[Validators.required]]
    });
  }

  ngOnInit(): void {
    this.checkRole();
    this.spinner.show();
    this.fetchRecords().toPromise().then((res) => {
      console.log('response getting>>>',res);
      this.spinner.hide();
      if(res.status == 1){
        if(res.role=='agency'){
          // role is agency
          this.profileData = res.data;
          this.displayImage = this.profileData.image;
          this.selectImage = this.profileData.image;
          this.updateAgencyProfileForm.patchValue({
            name: this.profileData.agency_name,
            phone: this.profileData.phone,
            url: this.profileData.agency_url,
            address: this.profileData.agency_address,
          });
          console.log(this.profileData.email);
          console.log('response is >>>>',res);
        }else{
          // role is admin
          this.profileData = res.data;
          this.displayImage = this.profileData.image;
          this.selectImage = this.profileData.image;
          this.updateAgencyProfileForm.patchValue({
            name: this.profileData.agency_name,
            phone: this.profileData.phone,
            url: this.profileData.agency_url,
            address: this.profileData.agency_address,
          });
        }
      }else{
         Swal.fire({ icon: 'error', title: 'Oops...', text: res.message })
      }
    })
    .catch(err=> { 
      this.spinner.hide();
      console.log(err) 
    });
  }
  get f() { return this.updateAgencyProfileForm.controls; }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.updateAgencyProfileForm.invalid) {
        return;
    }
    const formData = new FormData();
    formData.append('fileUpload', this.selectImage);
    formData.append('name', this.updateAgencyProfileForm.get('name').value);
    formData.append('phone', this.updateAgencyProfileForm.get('phone').value);
    formData.append('url', this.updateAgencyProfileForm.get('url').value);
    formData.append('address', this.updateAgencyProfileForm.get('address').value);
    console.log('form data>>>>>',formData);
    this.spinner.show();
    this.ProfileService.update_profile(formData).subscribe((data: any) => {
      this.spinner.hide();
      if(data.status == 0){
        console.log(data);
        Swal.fire({ icon: 'error',  title: 'Oops...',  text: data.message })

      }else{
        console.log(data);
        this.submitted = false;
        this.toggleHideDivIf();
        this.ngOnInit();
        this.router.navigate(['/admin-profile'])
        Swal.fire('Agency!',  'Agency profile has been added.', 'success');
      }
    }, error => {
      this.spinner.hide()
    })
  }

  toggleDisplayDivIf() {
    this.isShowDivIf = false;
    this.isHideDivIf = true;
  }
    toggleHideDivIf() {
    this.isHideDivIf = false;
    this.isShowDivIf = true;
  }
  checkRole(){
    this.role=localStorage.getItem('role');
    if(this.role=='agency'){
      this.showAgency=true;
    }else{
      this.showAdmin=true;
    }
  }
  fetchRecords (){
    return this.ProfileService.fetch_user_profile_data();
  }

  getImage(event: any) { //Angular 11, for stricter type
		if(!event.target.files[0] || event.target.files[0].length == 0) {
			// this.msg = 'You must select an image';
			return;
		}
		
		var mimeType = event.target.files[0].type;
		
		if (mimeType.match(/image\/*/) == null) {
			// this.msg = "Only images are supported";
			return;
		}
		
		var reader = new FileReader();
		reader.readAsDataURL(event.target.files[0]);
		
		reader.onload = (_event) => {
      this.selectImage = event.target.files[0];
			this.displayImage = reader.result; 
		}
	}
}
