
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';


@Injectable({ providedIn: 'root' })
export class AdminDashboardService {
    constructor(
        private http: HttpClient
    ) {}

 
    //*********** Fetch Total Counts ************************
    fetch_total_count() {
        return this.http.get<any>(environment.apiBaseUrl + 'fetchTotalCounts')
            .pipe(map((res: any) => {
                return res;
            }));
    }


}