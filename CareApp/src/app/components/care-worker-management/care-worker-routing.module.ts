import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CareWorkerManagementComponent } from './care-worker/care-worker-management.component';
import { AddWorkerComponent } from './add-worker/add-worker.component';
 

const routes: Routes = [
  {
    path: '',
    component: CareWorkerManagementComponent
  },

  {
    path: 'add-worker',
    component: AddWorkerComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CareWorkerRoutingModule { }