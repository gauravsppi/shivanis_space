import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CareWorkerManagementComponent } from './care-worker-management.component';

describe('CareWorkerManagementComponent', () => {
  let component: CareWorkerManagementComponent;
  let fixture: ComponentFixture<CareWorkerManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CareWorkerManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CareWorkerManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
