import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Headers, Http } from '@angular/http';

import { AccountService } from '../../services/account.service';

import { school_model } from '../../models/school_model';

@Component({
  selector: 'app-add-announcement',
  templateUrl: './add-announcement.component.html',
  styleUrls: ['./add-announcement.component.scss']
})
export class AddAnnouncementComponent implements OnInit {
  public schoolModel: school_model;
  public addValidationError: school_model;
  sucMsg: boolean =  false; 
  successMessage: string='';
  errMsg: boolean = false; 
  errorMessage: string=''; 
  urgencyActive: boolean= true; 

  constructor(private router: Router, private accountService: AccountService, private route: ActivatedRoute, private http: Http) { }


  ngOnInit(): void {
    this.schoolModel = <school_model>{
      user_id: '',
      title: '',
      details: '',
      urgent: '0',
      status: ''   
    }
    this.addValidationError = <school_model>{
      title: '',
      details: '',
      urgent: '',
      status: ''   
    }
  }

  onSubmit() {
   
    var valid = true;
    this.schoolModel.searchKey== "";
    if (this.schoolModel.title == "") {
      this.addValidationError.title = 'Please enter title';
      valid = false;
    }
    if (this.schoolModel.details == "") {
      this.addValidationError.details = 'Please enter details';
      valid = false;
    }
  
    if(valid){ 
     // this.loaderShow = true;
      this.schoolModel.user_id = localStorage.getItem('admin_id');
      this.schoolModel.school_id = localStorage.getItem('school_sourcedId');
      this.schoolModel.user_type = 'admin';
      this.accountService.saveAnnouncement(this.schoolModel).then((response) => {
        if (response.code == 200) {
          this.sucMsg = true;
          this.successMessage = response.message;
          setTimeout (() => { this.sucMsg = false;  }, 3000); 
          this.addValidationError.title = '';
          this.addValidationError.details = '';
          this.addValidationError.urgent = '';
          this.addValidationError.status = '';

          this.schoolModel.title = '';
          this.schoolModel.details = '';
          this.schoolModel.urgent = '';
          this.schoolModel.status = '';
          this.router.navigate(['/announcement-list']);
        }else{
          this.errMsg = true;
          this.errorMessage = "Something went wrong";
          setTimeout (() => { this.errMsg = false;  }, 3000); 
          
        }
       // this.loaderShow = false;
      })  
    }
  }

}
