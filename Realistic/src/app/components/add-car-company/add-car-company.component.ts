import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { AccountService } from '../../services/account.service';
import { Headers, Http } from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import { Login_model } from '../../models/login_model';
import { Validation_model } from '../../models/Validation_model';

@Component({
  selector: 'app-add-car-company',
  templateUrl: './add-car-company.component.html',
  styleUrls: ['./add-car-company.component.scss']
})
export class AddCarCompanyComponent implements OnInit {
  public loginModel:Login_model;
  public addValidationError:Validation_model;
  private headers = new Headers();
  record:any=[];
  total : number= 0;
  loaderShow: boolean = false;
  sucMsg:boolean=false;
  errMsg:boolean=false;
  successMessage='Record Added successfully'; 
  errorMessage='Something went wrong'; 
  constructor(private accountService:AccountService,private route: ActivatedRoute,private router: Router, private http: HttpClientModule) { }
 
  getId: string;
  ngOnInit(): void {
    this.loginModel=<Login_model>{
      company_name:'',
    }
    this.addValidationError = <Validation_model>{
      company_name:'',
    }

    this.isLoggedIn();
  }
    
  //Add Company Name
  isLoggedIn() {
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    if(this.loginModel.admin_id == null){
    this.router.navigate(['/']);
    }
    } 


    add_company(){
          var valid=true;
          this.addValidationError.company_name='';
          if(this.loginModel.company_name==""){
            this.addValidationError.company_name='Please Enter Company Name';
            valid=false; 
          }
          if(valid){
            this.sucMsg = true;
            this.accountService.add_company_name(this.loginModel).then((response)=>{
              if (response.code == 200) {
                this.sucMsg = false;
                setTimeout (() => { this.router.navigate(['/view-company-list']);  }, 1500);
              } else {
                this.errMsg = true;
                this.router.navigate(['/login']);
              }
            })
           }
        }
  }


