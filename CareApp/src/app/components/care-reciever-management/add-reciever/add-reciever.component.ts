import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReceiverService } from '../../../shared/services/receiver-service/receiver.service';
import { NgxSpinnerService } from "../../../shared/services/spinner.service";
import Swal from 'sweetalert2';
import { Router } from "@angular/router";

@Component({
  selector: 'app-add-reciever',
  templateUrl: './add-reciever.component.html',
  styleUrls: ['./add-reciever.component.scss']
})
export class AddRecieverComponent implements OnInit {
  addCareReceiverForm: FormGroup;
  submitted = false;
  passworedType = 'password';
  displayImage: any;
  selectImage: any;
  constructor(
    private ReceiverService: ReceiverService,
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder, 
    private router: Router,

  ) {

    this.addCareReceiverForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      name: ['',[Validators.required]],
      phoneno: ['',[Validators.required]],
      password: ['',[Validators.required]],

    });
   }

  ngOnInit(): void {
  }

  get f() { return this.addCareReceiverForm.controls; }



  onSubmit() {
    console.log(this.addCareReceiverForm.value)
    this.submitted = true;

    // stop here if form is invalid
    if (this.addCareReceiverForm.invalid) {
        return;
    }

    const formData = new FormData();
    formData.append('fileUpload', this.selectImage);
    formData.append('email', this.addCareReceiverForm.get('email').value);
    formData.append('name', this.addCareReceiverForm.get('name').value);
    formData.append('phoneno', this.addCareReceiverForm.get('phoneno').value);
    formData.append('password', this.addCareReceiverForm.get('password').value);


    this.spinner.show();
    this.ReceiverService.add_receiver(formData).subscribe((data: any) => {
      this.spinner.hide();
      if(data.status == 0){
        Swal.fire({ icon: 'error',  title: 'Oops...',  text: data.message })

      }else{
        this.submitted = false;
        this.addCareReceiverForm.reset()
        this.router.navigate(['care-reciever-management'])
        Swal.fire('Receiver!',  'Receiver has been added.', 'success');
      }
    }, error => {
      this.spinner.hide()
      console.log('error ----------',error)
      
    })
  }

  hideShowPass(){
    this.passworedType = this.passworedType == 'password' ? 'text': 'password'
  }


  getImage(event: any) { //Angular 11, for stricter type
		if(!event.target.files[0] || event.target.files[0].length == 0) {
			// this.msg = 'You must select an image';
			return;
		}
		
		var mimeType = event.target.files[0].type;
		
		if (mimeType.match(/image\/*/) == null) {
			// this.msg = "Only images are supported";
			return;
		}
		
		var reader = new FileReader();
		reader.readAsDataURL(event.target.files[0]);
		reader.onload = (_event) => {
      this.selectImage = event.target.files[0];
			this.displayImage = reader.result; 
		}
	}
 
}
