const mongoose = require('mongoose');
var multiCarCompanyModelSchema = new mongoose.Schema({
    company_name: {
        type: String,       
    },
     company_id: {
        type: String,       
    },
     model_name: {
        type: String,       
    },
     model_id: {
        type: String,       
    },
     add_part_id: {
        type: String,
        default:0       
    },
    status: {
        type: String,
        default:0
    },
    created_at: {
        type: Date,
        default: Date.now,
    },
});



multiCarCompanyModelSchema.virtual('part_details',{
    ref: 'parts',
    localField: 'add_part_id',
    foreignField: '_id',
    justOne: true
});
multiCarCompanyModelSchema.virtual('company_details',{
    ref: 'car_company',
    localField: 'company_id',
    foreignField: '_id',
    justOne: true
});
multiCarCompanyModelSchema.virtual('model_details',{
    ref: 'car_models',
    localField: 'model_id',
    foreignField: '_id',
    justOne: true
});

multiCarCompanyModelSchema.set('toObject', { virtuals: true });
multiCarCompanyModelSchema.set('toJSON', { virtuals: true });

module.exports = multiCarCompanyModelSchema;
