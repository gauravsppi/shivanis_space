const PagesModel = require("../../model/PagesModel"); //for define pages model
var formidable = require('formidable');

module.exports = {	
	/**** Add User *****/
	addContent: function(req, res) { //User signup api
	 	var data=req.body;	 
	 	var form = new formidable.IncomingForm();
	 	form.parse(req, function(err, fields, files){
	 		var fileName = '';
	    	var data = fields;
	 		if(Object.keys(data).length > 0){ //Check if parameter passes or not
			  	if(data.title  != ''){ 				  					                
	                PagesModel.contentAdd(req.con, data, function(err, added) { //register User data
						if (err) throw err;	
						if(added._id != ''){															
							res.send({
								"code": 200,									
								"status": 'success',								
								"message":"Content saved successfully"
							});
						}else{
							res.send({
								"code": 400,
								"status": 'notInserted',
								"message":"Record Not Insert"
							});
						}
				    })
				}				
			}else{
				res.send({
					"code": 400,
					"status": 'notEmpty',
					"message":"Please fill all the required fileds"
				});
			}
		})		
	},
	/**** Update User *****/
	updateContent: function(req, res) { //User signup api
			//console.log('kuch likh');
	 	var data = req.body;
	 		if(Object.keys(data).length > 0){ //Check if parameter passes or not
			  	//if(data.title  != ''){ 				  					                
	                PagesModel.contentUpdate(req.con, data, function(err, updated) { //register User data
						if (err) throw err;	
						if(updated.ok > 0 && updated.nModified > 0 && updated.n > 0){ 													
							res.send({
								"code": 200,									
								"status": 'success',								
								"message":"Content updated successfully"
							});
						}else{
							res.send({
								"code": 400,
								"status": 'notUpdated',
								"message":"Record not updated"
							});
						}
				    })
				//}				
			}else{
				res.send({
					"code": 400,
					"status": 'notEmpty',
					"message":"Please fill all the required fileds"
				});
			}
				
	},
	/***** Get content ******/
	getContent: function(req, res){
		var data=req.body;	 
	 	if(Object.keys(data).length > 0){ //Check if parameter passes or not						
			PagesModel.getContent(req.con,data, function(err, result){ 
				if(err){
					res.send({
						"code": 400,
						"error": err,
						"status": 'sqlError',							
						"message":"Something went wrong"
					});
				}else{
					if(result.length > 0){ 
						res.send({
							"code": 200,
							"result": result,
							"status": 'success',
							"message": "Content fetch successfully"
						});
					}else{
						res.send({
							"code": 400,										
							"status": 'notFound',							
							"message":"Content not found"
						});
					}	
				}
			})			
		}else{
			res.send({"code": 400,"status": 'noParameter',"message":"Something went wrong"});
		}
	},					
}	