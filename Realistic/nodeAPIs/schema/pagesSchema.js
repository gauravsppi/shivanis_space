const mongoose = require('mongoose');
var pagesSchema = new mongoose.Schema({
    title: {
        type: String       
    },
    content: {
        type: String
    }, 
    type: {
        type: String
    },  
    status: {
        type: String
    },
    created_at: {
        type: Date,
        default: Date.now
    },
});
module.exports = pagesSchema;
