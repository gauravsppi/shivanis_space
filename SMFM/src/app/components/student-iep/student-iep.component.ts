import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Headers, Http } from '@angular/http';


import { environment } from '../../../environments/environment';
import { AccountService } from '../../services/account.service';
import { IEP_model } from '../../models/IEP_model';
import Swal  from 'sweetalert2';


@Component({
  selector: 'app-student-iep',
  templateUrl: './student-iep.component.html',
  styleUrls: ['./student-iep.component.scss']
})
export class StudentIEPComponent implements OnInit {
  csvContent: string = '';
  public IEPModel: IEP_model;
  public records;
  public records_arr=[];
  CSVFileSelect: boolean= false;
  sucMsg: boolean =  false; 
  successMessage: string='';
  errMsg: boolean = false; 
  errorMessage: string='';
  noRecords: boolean =  false; 
  total : number= 0;
  totalPages : number= 0;
  currentPage:number=1;
  page : number =1;
  key: string = 'type'; //set default
  reverse: boolean = false;
  loaderShow: boolean = false;

  constructor(private router: Router, private accountService: AccountService, private route: ActivatedRoute, private http: Http) { }

  ngOnInit(): void {
    this.IEPModel = <IEP_model>{
      type: '',
      name: '',
      fileName: File = null,
      searchKey: '',
      sortKey: 'type',
      sortBy: 'asc'    
    }
    this.getIEPRecords()
  }
  getIEPRecords(){
    this.loaderShow = true;
    this.IEPModel.admin_id = localStorage.getItem('admin_id');
    this.IEPModel.page = this.currentPage;
    this.IEPModel.searchKey = '';
    this.accountService.getIEPStudents(this.IEPModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
        this.records_arr = response.result;
        this.total = response.count;
        this.totalPages=Math.ceil( response.count/10); 
        this.noRecords = false;
      }
      if (response.code == 200 && response.status == 'notFound') {
        this.records_arr = [];
        this.noRecords = true;
        this.total = 0;
        this.totalPages=0; 
      }
      this.loaderShow = false;
    })  
  }

  
  pageChange(currentPage){
    this.currentPage=currentPage;
    if(this.IEPModel.searchKey==""){
      this.getIEPRecords();
    }else{
      this.searchFunction(this.currentPage);
    } 
  }

  counter(i: number) {
    return new Array(i);
  }

  searchFunction(currentPage){
    if(this.IEPModel.searchKey== ""){
      this.currentPage=1;
      this.getIEPRecords();
    }else{
      this.loaderShow = true;
      this.currentPage=currentPage;
      this.IEPModel.page = this.currentPage;
      this.IEPModel.school_id = this.route.snapshot.params.id;
      this.accountService.getIEPStudents(this.IEPModel).then((response) => {
        if (response.code == 200 && response.status == 'success') {
          this.total = response.count;
          this.totalPages=Math.ceil(response.count/10);
          this.records_arr = response.result;  
          this.noRecords = false;
        }
        if (response.code == 200 && response.status == 'notFound') {
          this.records_arr = [];
          this.noRecords = true;
          this.total =0
          this.totalPages=0
        }
        this.loaderShow = false;
      })
    }
  }

  sortFunction(sortKey){
    this.key = sortKey;
    this.IEPModel.sortKey = sortKey;
    if(this.reverse === true){
      this.IEPModel.sortBy = 'asc';
    }else{
      this.IEPModel.sortBy = 'desc';
    }
    this.reverse = !this.reverse;   
    if(this.IEPModel.searchKey== ""){
      this.currentPage=1;
      this.getIEPRecords();
    }else{
      this.searchFunction(this.currentPage);
    }
  }


  getHeaderArray(csvRecordsArr: any) {  
    let headers = (<string>csvRecordsArr[0]).split(',');  
    let headerArray = [];  
    for (let j = 0; j < headers.length; j++) {  
      headerArray.push(headers[j]);  
    }  
    return headerArray;  
  } 

  onFileSelect(event) {
    const files = event.target.files;
    var content = this.csvContent;
    var type = files[0].name.split('.').pop();
    if (files && files.length) {
      if(type == 'csv'){ 
        this.loaderShow = true;
        this.CSVFileSelect= false;
        const fileToRead = files[0];
        const reader = new FileReader();
        reader.readAsText(files[0]);
        reader.onload = () => {  
          var csvData = reader.result;   
          console.log('csvData:: ', csvData);
          var csvRecordsArray = (<string>csvData).split(/\r\n|\n/);  
          console.log('csvRecordsArray:: ', csvRecordsArray);
          //var headersRow = this.getHeaderArray(csvRecordsArray); 
          this.records = this.getDataRecordsArrayFromCSVFile(csvRecordsArray, 2);
          console.log('Records:: ', this.records);
          if(this.records.length > 0){
            this.IEPModel.records = this.records;
            /***Save IEP Records ****/
            this.accountService.saveIEPStudents(this.IEPModel).then((response) => {
              if (response.code == 200 && response.status == 'success') {
                this.sucMsg = true;
                this.successMessage = response.message;
                setTimeout (() => { this.sucMsg = false;  }, 3000); 
                this.getIEPRecords();
                this.IEPModel.records = [];
               this.IEPModel.fileName = null;
              }else{
                this.IEPModel.records = [];
                const files = '';
                this.errMsg = true;
                this.errorMessage = "Something went wrong";
                setTimeout (() => { this.errMsg = false;  }, 3000); 
              }
              this.loaderShow = false;
            }) 
          }  
        };
      }else{
         this.CSVFileSelect= true;
      }   
    }
  }

  getDataRecordsArrayFromCSVFile(csvRecordsArray: any, headerLength: any) {  
    var csvArr = [];  
    for (var i = 1; i < csvRecordsArray.length; i++) {  
     // if(i != 3 ){ 
        var curruntRecord = (<string>csvRecordsArray[i]).split(','); 
        //console.log('val i = ',i+' varible : '+curruntRecord)
      //}else{
        //console.log('val i = ',i+' varible : '+curruntRecord)
      //} 
        csvArr.push( curruntRecord );
    }
    return csvArr;    
  }  
  
  delete(IEP_id){
    this.IEPModel.IEP_id = IEP_id;
    this.IEPModel.admin_id = localStorage.getItem('admin_id');
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to delete!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
    }).then((result) => {
      this.loaderShow = true;
      if (result.isConfirmed) {
        this.accountService.deleteIEP(this.IEPModel).then((response) => {
          this.loaderShow = false;
          if (response.code == 200 && response.status == 'success') {
            this.sucMsg = true;
            this.successMessage = response.message;
            setTimeout (() => { this.sucMsg = false;  }, 3000); 
            this.getIEPRecords();                
          }else{
            this.errMsg = true;
            this.errorMessage = "Something went wrong";
            setTimeout (() => { this.errMsg = false;  }, 3000); 
          }          
        })
      } else if (result.isDismissed) {
        this.loaderShow = false;
        Swal.fire({  showCancelButton: false,  icon: 'success',  title: 'Your file is safe!'})
      }
    })
  }

  deleteAll(){ 
    this.IEPModel.admin_id = localStorage.getItem('admin_id');
    this.IEPModel.school_id = localStorage.getItem('school_id');
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to delete!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
    }).then((result) => {
      this.loaderShow = true;
      if (result.isConfirmed) {
        this.accountService.deleteAllIEP(this.IEPModel).then((response) => {
          this.loaderShow = false;
          if (response.code == 200 && response.status == 'success') {
            this.sucMsg = true;
            this.successMessage = response.message;
            setTimeout (() => { this.sucMsg = false;  }, 3000); 
            this.getIEPRecords();                
          }else{
            this.errMsg = true;
            this.errorMessage = "Something went wrong";
            setTimeout (() => { this.errMsg = false;  }, 3000); 
            this.getIEPRecords(); 
          }
        })
      } else if (result.isDismissed) {
        this.loaderShow = false;
        Swal.fire({  showCancelButton: false,  icon: 'success',  title: 'Your file is safe!'})
      }
    })
  }

}
