import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import  * as $ from 'jquery';
declare const modalOpen_add: any;
declare const modalOpen_edit: any;


import { AccountService } from '../../services/account.service';
import { Headers, Http } from '@angular/http';
import { Login_model } from '../../models/login_model';
import Swal  from 'sweetalert2';


@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  public loginModel: Login_model;
  public addValidationError: Login_model;
  private headers = new Headers();

  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http) { }
  sucMsg: boolean = false;
  errMsg: boolean = false;
  successMessage: string;   
  errorMessage: string; 
  categories: [];  
  loaderShow: boolean = false;
  noRecords: boolean = false;

  ngOnInit(): void {
    this.loginModel = <Login_model>{
      admin_id: '',
      school_id: '',
      school_sourcedId: '',
      category_name:'',
      category_des:'',
      category_id:'',
    }

    this.addValidationError = <Login_model>{
        category_name:'',
        category_des:'',
    }

    this.getSELCategory();
    modalOpen_add();
    modalOpen_edit();
  }

  getSELCategory() {
    this.loaderShow = true;
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    this.loginModel.school_id = localStorage.getItem('school_id');
    this.accountService.getSELCategory(this.loginModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
        this.categories = response.result; 
        this.noRecords = false 
      }else{
        this.noRecords = true; 
        this.categories = [];                 
      }
      this.loaderShow = false;
    })
  }
  getSELCategory_id(id){
    this.addValidationError.category_name = '';
    this.addValidationError.category_des = '';
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    this.loginModel.school_id = localStorage.getItem('school_id');
    this.accountService.getSELCategory(this.loginModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
        this.categories = response.result; 
        for (var index in this.categories) {
          var vals = this.categories[index]
            if(vals['id'] == id){
              this.loginModel.category_id = vals['id'];
              this.loginModel.category_name = vals['category_name'];
              this.loginModel.category_des = vals['description'];
            }
        }
      }
    })
  }

  deleteSELCategory(id){
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to delete!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
    }).then((result) => {
      this.loaderShow = true;
      if (result.isConfirmed) {
        this.loginModel.admin_id = localStorage.getItem('admin_id');
        this.loginModel.sel_cat_id = id;
        this.accountService.deleteSELCategory(this.loginModel).then((response) => {
          this.loaderShow = false;
          if (response.code == 200 && response.status == 'success') {
            this.getSELCategory();  
            this.sucMsg = true;
            this.successMessage = response.message;                    
            setTimeout (() => { this.sucMsg = false;  }, 3000);        
          }else{
            this.errMsg = true;
            this.errorMessage = response.message;
            setTimeout (() => { this.errMsg = false;  }, 3000); 
          }
        })
      } else if (result.isDismissed) {
        this.loaderShow = false;
        Swal.fire({  showCancelButton: false,  icon: 'success',  title: 'Your file is safe!'})
      }
    })      
  }

  add_SEL(){
    this.loginModel.category_name = '';
    this.loginModel.category_des = '';
    this.addValidationError.category_name = '';
    this.addValidationError.category_des = '';
  }
 
  modalOpen1(){
    $('#add_cate').hide();
  }

  onSubmitAdd() {
    var valid = true;
    this.addValidationError.category_name = '';

    if (this.loginModel.category_name.trim() == "") {
        this.addValidationError.category_name = 'Please enter category name';
        valid = false;
    }
    if (valid) {      
      this.accountService.addSELCategory(this.loginModel).then((response) => {          
        if (response.code == 200 && response.status == 'success') {        
          this.getSELCategory();  
          this.sucMsg = true;
          this.successMessage = response.message;
          setTimeout (() => { this.sucMsg = false;  }, 3000); 
        } else {           
          this.errMsg = true;
          this.errorMessage = response.message;
          setTimeout (() => { this.errMsg = false;  }, 3000); 
        }         
        modalOpen_add();
      })
    }
  }

  onSubmit() {
    var valid = true;
    this.addValidationError.category_name = '';
    if (this.loginModel.category_name.trim() == "") {
        this.addValidationError.category_name = 'Please enter category name';
        valid = false;
    }
    if (valid) {  
      this.loginModel.category_name = this.loginModel.category_name.trim();    
      this.loginModel.category_des = this.loginModel.category_des.trim();    
      this.accountService.updateSELCategory(this.loginModel).then((response) => {  
        if (response.code == 200 && response.status == 'success') { 
            this.getSELCategory();
            this.sucMsg = true;
            this.successMessage = response.message;
            this.loginModel.category_name = '';
            this.loginModel.category_des = '';
            setTimeout (() => { this.sucMsg = false;  }, 3000); 
        } else {
            this.errMsg = true;
            this.errorMessage = response.message;
            setTimeout (() => { this.errMsg = false;  }, 3000); 
        }  
        modalOpen_edit();        
      })
    }
  }
}
