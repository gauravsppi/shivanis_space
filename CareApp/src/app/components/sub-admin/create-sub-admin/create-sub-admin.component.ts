import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import { Admin } from '../../../models/admin.model';
import { AccountService } from '../../../services/account.service';

@Component({
  selector: 'app-create-sub-admin',
  templateUrl: './create-sub-admin.component.html',
  styleUrls: ['./create-sub-admin.component.scss']
})
export class CreateSubAdminComponent implements OnInit {
  public adminModel: Admin;
  public addValidationError: Admin;
  private headers = new Headers();
  isTextFieldType: boolean = false;
  urls:String;
  sucMsg:boolean=false;
  errMsg:boolean=false;
  successMessage:string;
  errorMessage:string;
  imageSelect:boolean=false;
  staticImg:boolean=false;
  validationname:boolean=false;
  validationemail:boolean=false;
  validationpassword:boolean=false;
  validationphone:boolean=false;
  validationimage:boolean=false;
  loaderShow:boolean=false;
  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService) { }
  ngOnInit(): void {
    this.adminModel = <Admin>{
      email: '',
      password: '',
      image:'',
      name:'',
      phoneno:''
    }
    this.addValidationError=<Admin>{
      email: '',
      password: '',
      image:'',
      name:'',
      phoneno:''
    }
  }
  ngAfterViewInit() {
    $(function () {
      $('.showonhover').click(function (event) {
        $("#selectfile").trigger('click');
      });

    });
  }
  togglePasswordFieldType() {
    console.log(this.isTextFieldType);
    this.isTextFieldType = !this.isTextFieldType;
  }
  selectImage(event){
    console.log("enter here");
    if (event.target.files.length > 0) {
      console.log(event.target.files[0].name);
      const formData = new FormData();
      formData.append('file', <File>event.target.files[0]);
      console.log(formData);
      this.http.post('https://www.thecareapps.com/nodeapis/Admin/imageUpload', formData, { headers: this.headers }).subscribe((res: any) => {
        var obj = res.json();
        this.urls = obj.imgurl + obj.result;
        this.imageSelect=true;
        this.staticImg=true;  
        this.adminModel.image = obj.result;
      });
    }
  }
  addsubadmin(){
    var valid = true;
    var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    if (this.adminModel.name == "") {
      this.validationname=true;
      valid = false;
    }else{ 
      this.validationname=false;
    }
    if(!EMAIL_REGEXP.test(this.adminModel.email)){
      this.validationemail=true;
      valid=false;
    }else{
      this.validationemail=false;
    }
    if(this.adminModel.password == ""){
      this.validationpassword=true;
      valid=false;
    }else {
      this.validationpassword=false;
    }
    if(this.adminModel.phoneno == ""){
      this.validationphone=true;
      valid=false;
    }else{ 
      this.validationphone=false;
    }
    if(this.adminModel.image == ""){
      this.validationimage=true;
      valid=false;
    }else{
      this.validationimage=false;
    }
    if(valid){
      this.loaderShow=true;
      this.accountService.Subadmin(this.adminModel).then((response) => {
        if(response.code==200){
          this.loaderShow=false;
          this.imageSelect=false;
          this.staticImg=false;
          this.adminModel.name='';
          this.adminModel.email='';
          this.adminModel.password='';
          this.adminModel.phoneno='';
          this.adminModel.image='';
          this.sucMsg=true;
          this.successMessage=response.message;
          setTimeout (() => { this.sucMsg=false; this.successMessage = '';  }, 3000);
          this.router.navigate(['/create-sub-admin']);
        }else{
          this.loaderShow=false;
          this.errMsg=true;
          this.errorMessage=response.message;
          setTimeout(()=> { this.errMsg=false; this.errorMessage=''; }, 3000);
        }
      })      
    }
  }
}
