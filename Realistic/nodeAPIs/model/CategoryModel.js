var dateFormat = require('dateformat'); //for using date format
const current_datetime = dateFormat(new Date().toLocaleString('en-US'),"yyyy-mm-dd hh:MM:ss"); //get current date and time
const md5 = require('md5'); //for encrypt passwords

const mongoose = require('mongoose');
var categorySchema = require('../schema/categorySchema');
var categoryDB = mongoose.model('category', categorySchema);


module.exports = {
    checkCategoryNameExits:function(con, data, callback) { // for check category exists or not
        categoryDB.find({ 'name': data.name }, callback)
    },
    checkIDExits:function(con, data, callback) { // for check category by id exists or not
        categoryDB.find({ '_id': data.category_id }, callback)
    },
    categoryAdd: function(con, data, callback) { //add category
        var Category = new categoryDB();
        Category.name = data.name;       
        Category.image = data.image;       
        Category.status = '1';       
        Category.created_at = current_datetime;    
        Category.save(callback);
    },
    updateCategory:function(con, data, callback) { //Update category
        var myquery = {  _id: data.category_id  };
        var newvalues = { $set: 
            { 
                name: data.name
            } 
        };
        categoryDB.updateOne(myquery, newvalues, callback)
    },
    deleteCategory:function(con, data, callback) { //delete category
        categoryDB.deleteOne({ _id : data.category_id }, callback )
    },
    getCategory:function(con,data, callback) { //get category
       var start = (data.page-1)*10;
        if(data.searchKey == ''){ 
            categoryDB.find({}).sort({ 'name': data.name }).skip(start).limit(10).exec(callback)
        }else{
            categoryDB.find({'name': new RegExp(data.searchKey) }).sort({ 'name': data.name }).skip(start).limit(10).exec(callback)
        }
    },
    getAllCategories:function(con, data, callback) { //get category
        categoryDB.find({ 'status': '1' }, callback )        
    },
    countCategories: function(con, data, callback) {
        if(data.searchKey == ''){ 
            categoryDB.find({}, callback )   
        }else{
            categoryDB.find({ 'name': new RegExp(data.searchKey) }, callback )      
        }         
    },
}
