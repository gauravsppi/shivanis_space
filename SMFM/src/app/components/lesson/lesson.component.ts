import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import  * as $ from 'jquery';

import { AccountService } from '../../services/account.service';
import { Headers, Http } from '@angular/http';
import { Login_model } from '../../models/login_model';
import Swal  from 'sweetalert2';


@Component({
  selector: 'app-lesson',
  templateUrl: './lesson.component.html',
  styleUrls: ['./lesson.component.scss']
})
export class LessonComponent implements OnInit {

  public loginModel: Login_model;
  public addValidationError: Login_model;

  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http) { }
  sucMsg: boolean = false;
  errMsg: boolean = false;
  successMessage: string;   
  errorMessage: string; 
  lessons: [];  
  noRecordsShow: boolean = false;
  loaderShow: boolean = false;
  total : number= 0;
  totalPages : number= 0;
  currentPage:number=1;
  page : number =1;
  key: string = 'lesson_name'; //set default
  reverse: boolean = false;
  category_name: string = ''; //set default

  ngOnInit(): void {
    this.loginModel = <Login_model>{
      admin_id: '',
      school_id: '',
      school_sourcedId: '',
      lesson_name:'',
      lesson:'',
      video: File = null,
      searchKey: '',
      sortKey: 'lesson_name',
      sortBy: 'desc'    
    }
    this.getSELCategoryLessons();
    this.getSELCategoryLessonsName();
  }

  getSELCategoryLessonsName() {
    //this.loaderShow = true;
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    this.loginModel.sel_cat_id = this.route.snapshot.params.id;
    this.accountService.getSELCategoryLessonsName(this.loginModel).then((response) => {
      console.log('cat name: ', response);
      if (response.code == 200 && response.status == 'success') {
        this.category_name = response.result[0].category_name; 
      }    
      //this.loaderShow = false;
    })
  }
  getSELCategoryLessons() {
    this.loaderShow = true;
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    this.loginModel.sel_cat_id = this.route.snapshot.params.id;
    this.loginModel.page = this.currentPage;
    this.accountService.getSELCategoryLessons(this.loginModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
        this.lessons = response.result; 
        this.total = response.count;
        this.totalPages=Math.ceil(response.count/10);
        this.noRecordsShow = false;
      }else {
        this.noRecordsShow = true;
        this.lessons = []; 
        this.total = 0;
        this.totalPages= 0;
      }
      this.loaderShow = false;
    })
  }

  pageChange(currentPage){
    this.currentPage=currentPage;
    if(this.loginModel.searchKey==""){
      this.getSELCategoryLessons();
    }else{
      this.searchFunction(this.currentPage);
    } 
  }

  counter(i: number) {
    return new Array(i);
  }

  searchFunction(currentPage){
    if(this.loginModel.searchKey== ""){
      this.currentPage=1;
      this.getSELCategoryLessons();
    }else{
      this.loaderShow = true;
      this.currentPage=currentPage;
      this.loginModel.page = this.currentPage;
      this.loginModel.school_id = this.route.snapshot.params.id;
      this.accountService.getSELCategoryLessons(this.loginModel).then((response) => {
        if (response.code == 200 && response.status == 'success') {
          this.total = response.count;
          this.totalPages=Math.ceil(response.count/10);
          this.lessons = response.result;
          this.noRecordsShow = false;                    
        }else{
          this.noRecordsShow = true;
          this.total = 0;
          this.totalPages=0;
          this.lessons = [];  
        }
        this.loaderShow = false;
      })
    }
  }

  sortFunction(sortKey){
    this.key = sortKey;
    this.loginModel.sortKey = sortKey;
    if(this.reverse === true){
      this.loginModel.sortBy = 'asc';
    }else{
      this.loginModel.sortBy = 'desc';
    }
    this.reverse = !this.reverse;   
    if(this.loginModel.searchKey== ""){
      this.currentPage=1;
      this.getSELCategoryLessons();
    }else{
      this.searchFunction(this.currentPage);
    }
  }

  deleteSELCategoryLessons(id){
    
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to delete!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
    }).then((result) => {
      this.loaderShow = true;
      if (result.isConfirmed) {        
        this.loginModel.admin_id = localStorage.getItem('admin_id');
        this.loginModel.sel_lesson_id = id;
        this.accountService.deleteSELCategoryLessons(this.loginModel).then((response) => {
          this.loaderShow = false;
          if (response.code == 200 && response.status == 'success') {
            this.getSELCategoryLessons();  
            this.sucMsg = true;
            this.successMessage = response.message;                    
            setTimeout (() => { this.sucMsg = false;  }, 3000);        
          }else{
            this.errMsg = true;
            this.errorMessage = response.message;
            setTimeout (() => { this.errMsg = false;  }, 3000); 
          }
        })
      } else if (result.isDismissed) {
        this.loaderShow = false;
        Swal.fire({  showCancelButton: false,  icon: 'success',  title: 'Your file is safe!'})
      }
      
    })      
  }

}
