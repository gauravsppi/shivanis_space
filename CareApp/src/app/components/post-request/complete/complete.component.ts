import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "../../../shared/services/spinner.service";
import { PostRequestService } from '../../../shared/services/post-request/post-request.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-complete',
  templateUrl: './complete.component.html',
  styleUrls: ['./complete.component.scss']
})
export class CompleteComponent implements OnInit {
  CompletedRequestArray: any = [];
  page = 1;
  count : number;
  pageSize = 10;
  postDetail: any;
  selectedIndex: any;
  constructor(
    private PostRequestService: PostRequestService,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.spinner.show();
    this.fetchRecords().toPromise().then((res) => {
      console.log('res++++++++++++',res);
      this.spinner.hide();
      if(res.status == 1){
        this.CompletedRequestArray = res.data
        this.count = res.numOfCompleteRequest
      }else{
        Swal.fire({ icon: 'error', title: 'Oops...', text: res.message })
      }
    })
    .catch(err=> { 
      this.spinner.hide();
      console.log(err) 
    });
  }

  fetchRecords () {
    var Params = {
      page:  this.page, 
      pageSize: this.pageSize,
    }
    return this.PostRequestService.fetch_complete_requests(Params);
  }


  onTableDataChange(event){
    this.page = event;
    this.ngOnInit();
  }

}
