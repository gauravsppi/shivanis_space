export class teacher_model {    
   name: string;
   phone: string;   
   email: string;
   school_id: string;
   teacher_id: string;   
   admin_id: string;   
   teacher_sourcedId: string;   
   image: string;   
   sex: string;   
   identifier: string;   
   birthDate: string;   
   cityOfBirth: string;   
   countryOfBirthCode: string;   
   stateOfBirthAbbreviation: string;   
   username: string;   
   page: number;
   searchKey: string;
   sortKey: string;
   sortBy: string;
}