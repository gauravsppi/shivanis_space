import { Component, OnInit } from '@angular/core';
import {Http , Headers } from  '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Login_model} from'../../models/login_model';
import{ AccountService} from'../../services/account.service';

@Component({
  selector: 'app-edit-company-name',
  templateUrl: './edit-company-name.component.html',
  styleUrls: ['./edit-company-name.component.scss']
})

export class EditCompanyNameComponent implements OnInit {
  public loginModel:Login_model;
  public addValidationError:Login_model;
  private headers = new Headers();
  public getId: any = [];
  sucMsg: boolean = false;
  errMsg:boolean = false; 
  successMessage='Record updated successfully'; 
  errorMessage='Something went wrong'; 
 record:any=[];
 loaderShow: boolean = false
  constructor(private accountService:AccountService,private route: ActivatedRoute,private router: Router,private http: Http ) { }

  ngOnInit(): void {
    this.loginModel=<Login_model>{
      company_name: '',
    }
    this.addValidationError = <Login_model>{
      company_name: '',
      }
     this.route.params.subscribe(params => {
       this.getId = params['_id'];
     });
     this.getCompoanyName(this.getId);
     this.isLoggedIn();
  }
  isLoggedIn() {
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    if(this.loginModel.admin_id == null){
    this.router.navigate(['/']);
    }
    } 
    

    
  getCompoanyName(id) {
    this.loaderShow=true;
    this.loginModel.company_id=id;
    this.loginModel.admin_id=localStorage.getItem('admin_id');
    //debugger;
    this.accountService.getCompanyNames(this.loginModel).then((response) => {
      console.log(response);
      this.loaderShow=false;
        if(response.code==200){
         this.record=response.result[0];
         this.loginModel.company_name=this.record.name;
        }
      })
  }
 
  update_company(){
    this.loaderShow=true;
    var valid=true;
    this.addValidationError.company_name	='';
    if(this.loginModel.company_name==""){
      this.addValidationError.company_name='Please Enter Company Name';
      valid=false; 
    }
    if(valid){ 
      console.log(this.loginModel);
      this.accountService.updateCarCompanyNames(this.loginModel).then((response)=>{
        if (response.code == 200) {
          this.sucMsg=true;
          setTimeout (() => {  
           this.sucMsg = false; 
            this.router.navigate(['/view-company-list']); 
           }, 2000);
      } else {
          this.errMsg = true;
          this.errorMessage;
              setTimeout (() => {
                 this.sucMsg = false;
            this.router.navigate(['/view-company-list']); 
          }, 2000);
      }
      })
    }
    }
}




