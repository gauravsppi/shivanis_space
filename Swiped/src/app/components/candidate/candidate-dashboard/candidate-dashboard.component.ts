import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from '../../../services/account.service';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import { candidateLogin_model } from '../../../models/candidateLogin_model';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2'

import { OwlOptions } from 'ngx-owl-carousel-o';
import * as $ from 'jquery';
@Component({
  selector: 'app-candidate-dashboard',
  templateUrl: './candidate-dashboard.component.html',
  styleUrls: ['./candidate-dashboard.component.scss']
})
export class CandidateDashboardComponent implements OnInit {

  public loginModel: candidateLogin_model;
  public addValidationError: candidateLogin_model;
  public headers = new Headers();
  errMsg:boolean=false;
  message:string; 
  candidatelist:any =[] ;
  // letsgoTabdata:any [] ;
  // degreeTabdata:any [] ;
  // currentjobTabdata:any [] ;
  // interestsTabdata:any [] ;
  getId:any;

  status: boolean = false;
  checkclass: string;
  names: any = [
    { 'company_name': 'Digipost Studio', 'company_url': 'www.digipoststudio.com', 'company_mail': 'mail@digipoststudio.com', 'post': 'Manager', 'job_type': 'Permanent', 'role': 'Accounts & Data Analytics', 'salary': '$ 22,000 To $ 25,000', 'address': '108 Hudson St, New York, NY, USA' },
    { 'company_name': 'Digipost Studio', 'company_url': 'www.digipoststudio.com', 'company_mail': 'mail@digipoststudio.com', 'post': 'Manager', 'job_type': 'Permanent', 'role': 'Accounts & Data Analytics', 'salary': '$ 22,000 To $ 25,000', 'address': '108 Hudson St, New York, NY, USA' },
    { 'company_name': 'Digipost Studio', 'company_url': 'www.digipoststudio.com', 'company_mail': 'mail@digipoststudio.com', 'post': 'Manager', 'job_type': 'Permanent', 'role': 'Accounts & Data Analytics', 'salary': '$ 22,000 To $ 25,000', 'address': '108 Hudson St, New York, NY, USA' },
    { 'company_name': 'Digipost Studio', 'company_url': 'www.digipoststudio.com', 'company_mail': 'mail@digipoststudio.com', 'post': 'Manager', 'job_type': 'Permanent', 'role': 'Accounts & Data Analytics', 'salary': '$ 22,000 To $ 25,000', 'address': '108 Hudson St, New York, NY, USA' },
    { 'company_name': 'Digipost Studio', 'company_url': 'www.digipoststudio.com', 'company_mail': 'mail@digipoststudio.com', 'post': 'Manager', 'job_type': 'Permanent', 'role': 'Accounts & Data Analytics', 'salary': '$ 22,000 To $ 25,000', 'address': '108 Hudson St, New York, NY, USA' },
    { 'company_name': 'Digipost Studio', 'company_url': 'www.digipoststudio.com', 'company_mail': 'mail@digipoststudio.com', 'post': 'Manager', 'job_type': 'Permanent', 'role': 'Accounts & Data Analytics', 'salary': '$ 22,000 To $ 25,000', 'address': '108 Hudson St, New York, NY, USA' },
    { 'company_name': 'Digipost Studio', 'company_url': 'www.digipoststudio.com', 'company_mail': 'mail@digipoststudio.com', 'post': 'Manager', 'job_type': 'Permanent', 'role': 'Accounts & Data Analytics', 'salary': '$ 22,000 To $ 25,000', 'address': '108 Hudson St, New York, NY, USA' },
    { 'company_name': 'Digipost Studio', 'company_url': 'www.digipoststudio.com', 'company_mail': 'mail@digipoststudio.com', 'post': 'Manager', 'job_type': 'Permanent', 'role': 'Accounts & Data Analytics', 'salary': '$ 22,000 To $ 25,000', 'address': '108 Hudson St, New York, NY, USA' },
    { 'company_name': 'Digipost Studio', 'company_url': 'www.digipoststudio.com', 'company_mail': 'mail@digipoststudio.com', 'post': 'Manager', 'job_type': 'Permanent', 'role': 'Accounts & Data Analytics', 'salary': '$ 22,000 To $ 25,000', 'address': '108 Hudson St, New York, NY, USA' },
    { 'company_name': 'Digipost Studio', 'company_url': 'www.digipoststudio.com', 'company_mail': 'mail@digipoststudio.com', 'post': 'Manager', 'job_type': 'Permanent', 'role': 'Accounts & Data Analytics', 'salary': '$ 22,000 To $ 25,000', 'address': '108 Hudson St, New York, NY, USA' },
    { 'company_name': 'Digipost Studio', 'company_url': 'www.digipoststudio.com', 'company_mail': 'mail@digipoststudio.com', 'post': 'Manager', 'job_type': 'Permanent', 'role': 'Accounts & Data Analytics', 'salary': '$ 22,000 To $ 25,000', 'address': '108 Hudson St, New York, NY, USA' },
    { 'company_name': 'Digipost Studio', 'company_url': 'www.digipoststudio.com', 'company_mail': 'mail@digipoststudio.com', 'post': 'Manager', 'job_type': 'Permanent', 'role': 'Accounts & Data Analytics', 'salary': '$ 22,000 To $ 25,000', 'address': '108 Hudson St, New York, NY, USA' },
    { 'company_name': 'Digipost Studio', 'company_url': 'www.digipoststudio.com', 'company_mail': 'mail@digipoststudio.com', 'post': 'Manager', 'job_type': 'Permanent', 'role': 'Accounts & Data Analytics', 'salary': '$ 22,000 To $ 25,000', 'address': '108 Hudson St, New York, NY, USA' },
    { 'company_name': 'Digipost Studio', 'company_url': 'www.digipoststudio.com', 'company_mail': 'mail@digipoststudio.com', 'post': 'Manager', 'job_type': 'Permanent', 'role': 'Accounts & Data Analytics', 'salary': '$ 22,000 To $ 25,000', 'address': '108 Hudson St, New York, NY, USA' },
    { 'company_name': 'Digipost Studio', 'company_url': 'www.digipoststudio.com', 'company_mail': 'mail@digipoststudio.com', 'post': 'Manager', 'job_type': 'Permanent', 'role': 'Accounts & Data Analytics', 'salary': '$ 22,000 To $ 25,000', 'address': '108 Hudson St, New York, NY, USA' },
    { 'company_name': 'Digipost Studio', 'company_url': 'www.digipoststudio.com', 'company_mail': 'mail@digipoststudio.com', 'post': 'Manager', 'job_type': 'Permanent', 'role': 'Accounts & Data Analytics', 'salary': '$ 22,000 To $ 25,000', 'address': '108 Hudson St, New York, NY, USA' },
    { 'company_name': 'Digipost Studio', 'company_url': 'www.digipoststudio.com', 'company_mail': 'mail@digipoststudio.com', 'post': 'Manager', 'job_type': 'Permanent', 'role': 'Accounts & Data Analytics', 'salary': '$ 22,000 To $ 25,000', 'address': '108 Hudson St, New York, NY, USA' },
    { 'company_name': 'Digipost Studio', 'company_url': 'www.digipoststudio.com', 'company_mail': 'mail@digipoststudio.com', 'post': 'Manager', 'job_type': 'Permanent', 'role': 'Accounts & Data Analytics', 'salary': '$ 22,000 To $ 25,000', 'address': '108 Hudson St, New York, NY, USA' },
    { 'company_name': 'Digipost Studio', 'company_url': 'www.digipoststudio.com', 'company_mail': 'mail@digipoststudio.com', 'post': 'Manager', 'job_type': 'Permanent', 'role': 'Accounts & Data Analytics', 'salary': '$ 22,000 To $ 25,000', 'address': '108 Hudson St, New York, NY, USA' },
    { 'company_name': 'Digipost Studio', 'company_url': 'www.digipoststudio.com', 'company_mail': 'mail@digipoststudio.com', 'post': 'Manager', 'job_type': 'Permanent', 'role': 'Accounts & Data Analytics', 'salary': '$ 22,000 To $ 25,000', 'address': '108 Hudson St, New York, NY, USA' },

  ];
  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService, private spinner:NgxSpinnerService) { }
  productsOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    nav: true,
    dots:false,
    navText:['<img src="/assets/img/top-arrow.png">','<img src="/assets/img/top-arrow.png">'],
    navSpeed: 700,
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 3
      }
    },
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.getId = params.get('id');
      });

    this.loginModel = <candidateLogin_model>{
      id: '',
      name:''
    }
    this.addValidationError = <candidateLogin_model>{
      id: '',
      name:''
      
    }
    
    this.getCandidatebyId();
  }
  // removeItem(i: number): void {
  //   this.names.splice(i, 1);
  //   this.checkclass = 'success';
  // }
  // downItem(i: number): void {
  //   this.names.splice(i, 1);
  //   this.checkclass = 'down';
  // }

   //-----------------------get candidate details API------------------------
   getCandidatebyId(){
    this.loginModel.id = this.getId;
    this.spinner.show();
    debugger;
    this.accountService.getCandidateWithID(this.loginModel).then((response) => {
      console.log(response);
      debugger;
      if (response.code == 200) {
        console.log(response);
        this.spinner.hide();
        this.candidatelist=response.result;
        // this.letsgoTabdata=response.letsgoTabdata;
        // this.degreeTabdata=response.degreeTabdata;
        // this.currentjobTabdata=response.currentjobTabdata;
        // this.interestsTabdata=response.interestsTabdata;
        
        
      } else {
        this.errMsg = true;
        this.message = "No Data Found!!!";
      }
    })
  }
  ngAfterViewInit() {
    $('.owl-carousel').owlCarousel({
      loop: true,
      margin: 10,
      responsiveClass: true,
      responsive: {
        0: {
          items: 1,
          nav: true
        },
        600: {
          items: 3,
          nav: false
        },
        1000: {
          items: 5,
          nav: true,
          loop: false
        }
      }
    })


  
  }
}

