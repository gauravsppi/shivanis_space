import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
const routes: Routes = [
  {path:'', redirectTo: '/candidate', pathMatch:'full'},
  {
    path: '',
    loadChildren: () => import(`./components/candidate/candidate.module`).then(m => m.CandidateModule)
  },
  {
    path: 'admin',
    loadChildren: () => import(`./components/admin/admin.module`).then(m => m.AdminsModule)
  },
  {
    path: '',
    loadChildren: () => import(`./components/employer/employer.module`).then(m => m.EmployerModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
