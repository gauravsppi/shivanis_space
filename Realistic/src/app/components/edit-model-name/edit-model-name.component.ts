import { Component, OnInit } from '@angular/core';
import {Http , Headers } from  '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Login_model} from'../../models/login_model';
import{ AccountService} from'../../services/account.service';

@Component({
  selector: 'app-edit-model-name',
  templateUrl: './edit-model-name.component.html',
  styleUrls: ['./edit-model-name.component.scss']
})

export class EditModelNameComponent implements OnInit {
  public loginModel:Login_model;
  public addValidationError:Login_model;
  private headers = new Headers();
  public getId: any = [];
  sucMsg: boolean = false;
  errMsg:boolean = false; 
  successMessage='Record updated successfully'; 
  errorMessage='Something went wrong'; 
 record:any=[];
 loaderShow: boolean = false
  constructor(private accountService:AccountService,private route: ActivatedRoute,private router: Router,private http: Http ) { }

  ngOnInit(): void {
    this.loginModel=<Login_model>{
      model_id: '',
    }
    this.addValidationError = <Login_model>{
      model_id: '',
      }
     this.route.params.subscribe(params => {
       this.getId = params['_id'];
     });
     this.getModelName(this.getId);
     this.loginModel.company_id=this.getId;
     this.isLoggedIn();
  }
  
  isLoggedIn() {
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    if(this.loginModel.admin_id == null){
    this.router.navigate(['/']);
    }
    } 
    

    
  getModelName(id) {
    this.loaderShow=true;
    this.loginModel.model_id=id;
    this.loginModel.admin_id=localStorage.getItem('admin_id');
    //debugger;
    this.accountService.getModelNames(this.loginModel).then((response) => {
      this.loaderShow=false;
        if(response.code==200){
         this.record=response.result[0];
         this.loginModel.model_name=this.record.model_name;
         this.loginModel.company_id=this.record.company_id;
        }
      })
  }
 
  update_model(){
    this.loaderShow=true;
    var valid=true;
    this.addValidationError.company_name	='';
    if(this.loginModel.company_name==""){
      this.addValidationError.company_name='Please Enter model Name';
      valid=false; 
    }
    var company_id=this.loginModel.company_id
    alert(company_id);
    if(valid){ 
      console.log(this.loginModel);
      this.accountService.updateCarModelNames(this.loginModel).then((response)=>{
        if (response.code == 200) {
          this.sucMsg=true;
          setTimeout (() => {  
           this.sucMsg = false; 
            this.router.navigate(['/view-model-list/',company_id]); 
           }, 2000);
      } else {
          this.errMsg = true;
          this.errorMessage;
              setTimeout (() => {
                 this.sucMsg = false;
            this.router.navigate(['/view-model-list']); 
          }, 2000);
      }
      })
    }
    }
}





