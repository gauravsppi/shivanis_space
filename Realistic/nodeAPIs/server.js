const express = require("express")
const app = express()
var methodOverride = require("method-override")
const path = require("path")
const con = require("./config/db.js") //connect with database

// connecting route to database 
app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Headers", "Origin,Content-Type, Content-Length, X-Requested-With");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
  req.con = con
  next()
})

// parsing body request
app.use(express.json({limit: '60mb'}))
app.use(express.urlencoded({ extended: true, limit: '60mb' }))
app.use(methodOverride("_method"))

// include router
const routesRouter = require("./routes/routesRouter")

// routing
app.use("/realsticCarWeb", routesRouter)

//starting server
// app.listen(3000, function() {
//   console.log("server listening on port 3000")
// })

// starting server
con.connectToServer( function(err){	
    if(err){
        console.log('Db connection error', err);
    }else{
        app.listen(3000, function() {
            console.log("server listening on port 3000")
        })         
    }       
})