
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { AccountService } from '../../services/account.service';
import { Headers, Http } from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import { Login_model } from '../../models/login_model';
import { Validation_model } from '../../models/Validation_model';
@Component({
  selector: 'app-car-company-model',
  templateUrl: './car-company-model.component.html',
  styleUrls: ['./car-company-model.component.scss']
})

export class CarCompanyModelComponent implements OnInit {
  public loginModel:Login_model;
  public addValidationError:Validation_model;
  private headers = new Headers();
  record:any=[];
  total : number= 0;
  loaderShow: boolean = false;
  constructor(private accountService:AccountService,private route: ActivatedRoute,private router: Router, private http: HttpClientModule) { }
  sucMsg:boolean=false;
  errMsg:string='';
  getId: string;
  ngOnInit(): void {
    this.loginModel=<Login_model>{
      c_password: '',
      password:'',
    }
    this.route.params.subscribe(params => {
      this.getId = params['_id'];
      //  this.getUser()
    });
    this.addValidationError = <Login_model>{
      // c_password: '',
      // password:'',
    }
      this.isLoggedIn();
  }
  
  isLoggedIn() {
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    if(this.loginModel.admin_id == null){
    this.router.navigate(['/']);
    }
    }

    add_company(){
      //alert('dffd');
    var valid=true;
    this.addValidationError.company_name='';
    if(this.loginModel.company_name==""){
      this.addValidationError.company_name='Please Enter Company Name';
      valid=false; 
    }
    if(valid){
    this.loginModel.admin_id=localStorage.getItem('admin_id');
      this.accountService.update_admin_password(this.loginModel).then((response)=>{
        // debugger;
        // console.log(response);
        if(response.code==200){
         // this.router.navigate(['/admin-profile']);
          this.addValidationError.success='Record Added Successfully';
          this.sucMsg = true;
          setTimeout (() => { this.router.navigate(['/admin-profile']);  }, 3000);
          window.location.reload();
        }else{
          this.errMsg = 'Something Went Wrong !';
          this.router.navigate(['/admin-profile']);
        }
      })
    }

    }

}

