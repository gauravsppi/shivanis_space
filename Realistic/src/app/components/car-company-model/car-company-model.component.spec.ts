import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarCompanyModelComponent } from './car-company-model.component';

describe('CarCompanyModelComponent', () => {
  let component: CarCompanyModelComponent;
  let fixture: ComponentFixture<CarCompanyModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarCompanyModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarCompanyModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
