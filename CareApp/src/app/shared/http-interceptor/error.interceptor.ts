import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
// import { ToastrManager } from 'ng6-toastr-notifications';
import { Router } from "@angular/router";
@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private router : Router) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            console.log('err',err);
            if(err.status === 400 || err.status === 401) {
                if(err.error === 'No token provided.' || err.error === 'Failed to authenticate token.') {
                    // this.toaster.errorToastr('Your session has expired. Please login again', 'Oops!', {
                    //     position: 'top-full-width', toastTimeout: 3000, animate: 'slideFromTop', showCloseButton: true
                    // });
                    localStorage.removeItem("token");
                    localStorage.removeItem("role");
                    this.router.navigate(['login']);
                } else {
                    // this.toaster.errorToastr('Something went wrong. Plaese try again.', 'Oops!', {
                    //     position: 'top-full-width', toastTimeout: 3000, animate: 'slideFromTop', showCloseButton: true
                    // });
                }
            }else{
                console.log('err 400',err);
                // this.toaster.errorToastr('Something went wrong. Plaese check you network connection.', 'Oops!', {
                //     position: 'top-full-width', toastTimeout: 3000, animate: 'slideFromTop', showCloseButton: true
                // });
            }
            const error = err.error.error_description || err.error.message || err.statusText;
            return throwError(error);
        }));
    }
}
