const CategoryModel = require("../../model/CategoryModel"); 
const Login = require("../../model/admin/LoginModel")
const path = require('path'); //for define path
const fs = require('fs'); //for file system 
var formidable = require('formidable');


module.exports = {	
	/**** Add User *****/
	addCategory: function(req, res) { //patient signup api
	 	var form = new formidable.IncomingForm();
	 	form.parse(req, function(err, fields, files){
	 		var data = fields;
		 	if(Object.keys(data).length > 0){ //Check if parameter passes or not
			  	if(data.name != '' && data.admin_id != ''){
				  	Login.checkExitsByID( req.con, data, function(err,result) {
						if(err){
							res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
						}else{
							if(result.length > 0){ 
							  	CategoryModel.checkCategoryNameExits(req.con, data, function(err, result) { //check email exists or not
							  		if (err) throw err;
							  		if(result.length >0 ){
							  			res.send({
											"code": 200,
											"status": 'alreadyExits',
											"message":"Category already exists"
										});
							  		}else{ 
							  			/**** Upload category image ********/
								  		if(Object.keys(files).length > 0){ 
									  		fileName = Date.now()+path.extname(files.image.name); 	           
					            			var rawData = fs.readFileSync(files.image.path) 
					            			data.image = fileName;
					            			fs.writeFile("public/uploads/category/"+fileName, rawData, function(err) {
								                if(err){ 
								                    res_data = {'code': 400,'error': err,'message': 'File not uploaded', status: 'error'}
								                 	res.send(res_data);
								                }else{  
								                	console.log('file uploaded successfully'); 			  			
													CategoryModel.categoryAdd(req.con, data, function(err, added) { //add category
														if (err) throw err;	
														if(added._id != ''){															
															res.send({
																"code": 200,									
																"status": 'success',
																"message":"Category added Successfully"
															});
														}else{
															res.send({
																"code": 400,
																"status": 'notInserted',
																"message":"Record Not Insert"
															});
														}
												    })				 
												}
											})//end fs function
										}else{
											data.image = '';
											CategoryModel.categoryAdd(req.con, data, function(err, added) { //add category data
												if (err) throw err;	
												if(added._id != ''){															
													res.send({
														"code": 200,									
														"status": 'success',
														"message":"Category added Successfully"
													});
												}else{
													res.send({
														"code": 400,
														"status": 'notInserted',
														"message":"Record Not Insert"
													});
												}
										    })	
										}
									}	
								})
							}else{
								res.send({"code": 200, "status": 'notExits',"message": "ID does not exits" });
							}
						}		
					})		
				}else{
					res.send({
						"code": 400,
						"status": 'notEmpty',
						"message":"Please fill all the required fileds"
					});
				}
			}else{
				res.send({
					"code": 400,
					"status": 'noParameter',
					"message":"Something went wrong"
				});
			}
		})//end form parse		
	},
	/**** Update category ***/
	updateCategory: function(req, res){
		var data=req.body;	 
	 	if(Object.keys(data).length > 0){ //Check if parameter passes or not
	 		if(data.category_id != '' && data.admin_id != ''){ 
 				Login.checkExitsByID( req.con, data, function(err,result) {
					if(err){
						res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
					}else{
						if(result.length > 0){ 
				 			CategoryModel.checkIDExits(req.con, data, function(err, result) { //check email exists or not
						  		if (err) throw err;
						  		if(result.length >0 ){
						  			/** update user info **/
						  			CategoryModel.updateCategory(req.con, data, function(err, updated){ 
						  				if(err){
											res.send({
												"code": 400,
												"error": err,
												"status": 'sqlError',							
												"message":"Something went wrong"
											});
										}else{
											if(updated.ok > 0 && updated.nModified > 0 && updated.n > 0){ 
												res.send({
													"code": 200,
													"result": data,
													"status": 'success',
													"message":"Category updated successfully"
												});
											}else{
												res.send({
													"code": 400,										
													"status": 'notUpdated',							
													"message":"Something went wrong"
												});
											}	
										}
						  			})	
						  		}else{
						  			res.send({"code": 400,"status": 'notExits',"message":"ID not exits" });
						  		}
						  	})
						}
					}
				})		  		
	 		}else{
	 			res.send({"code": 400,"status": 'notEmpty',"message":"Please fill all the required fileds" });
	 		}
	 	}else{
	 		res.send({"code": 400,"status": 'noParameter',"message":"Something went wrong"});
	 	}	
	},
	/**** Delete category *****/	
	deleteCategory: function(req, res){
		var data=req.body;	 
	 	if(Object.keys(data).length > 0){ //Check if parameter passes or not
	 		if(data.category_id != '' && data.admin_id != ''){ 
	 			Login.checkExitsByID( req.con, data, function(err,result) {
					if(err){
						res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
					}else{
						if(result.length > 0){ 
				 			CategoryModel.checkIDExits(req.con, data, function(err, result) { //check email exists or not
						  		if (err) throw err;
						  		if(result.length > 0 ){
						  			/** update user info **/
						  			CategoryModel.deleteCategory(req.con, data, function(err, deleted){ 
						  				if(err){
											res.send({
												"code": 400,
												"error": err,
												"status": 'sqlError',							
												"message":"Something went wrong"
											});
										}else{
											if(deleted.ok > 0 && deleted.deletedCount > 0 && deleted.n > 0){ 
												res.send({
													"code": 200,
													"status": 'success',
													"message": "Category deleted successfully"
												});
											}else{
												res.send({
													"code": 400,										
													"status": 'notDeleted',							
													"message":"Something went wrong"
												});
											}	
										}
						  			})	
						  		}else{
						  			res.send({"code": 400,"status": 'notExits',"message":"ID not exits" });
						  		}
						  	})	
						}
					}
				})		  	
	 		}else{
	 			res.send({"code": 400,"status": 'notEmpty',"message":"Please fill all the required fileds" });
	 		}
	 	}else{
	 		res.send({"code": 400,"status": 'noParameter',"message":"Something went wrong"});
	 	}	
	},
	/*** Get categories list ***/
	getCategory: function(req, res){
		var data=req.body;	 
	 	if(Object.keys(data).length > 0){ //Check if parameter passes or not
	 		if(data.admin_id != ''){ 
				Login.checkExitsByID( req.con, data, function(err,result) {
					if(err){
						res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
					}else{
						if(result.length > 0){ 
							CategoryModel.getCategory(req.con,data, function(err, result){ 
								if(err){
									res.send({
										"code": 400,
										"error": err,
										"status": 'sqlError',							
										"message":"Something went wrong"
									});
								}else{
									if(result.length > 0){ 
										CategoryModel.getAllCategories(req.con, data, function(err, result_count){ 
											res.send({
												"code": 200,
												"result": result,
												"count": result_count.length,
												"status": 'success',
												"message": "Category fetch successfully"
											});
										});
									}else{
										res.send({
											"code": 400,										
											"status": 'notFound',							
											"message":"Category not found"
										});
									}	
								}
							})
						}
					}
				})
			}else{
				res.send({"code": 400,"status": 'notEmpty',"message":"Please fill all the required fileds" });
			}	
		}else{
			res.send({"code": 400,"status": 'noParameter',"message":"Something went wrong"});
		}					
	},
}

