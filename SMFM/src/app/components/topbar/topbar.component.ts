import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import { environment } from "../../../environments/environment";
import { AccountService } from "../../services/account.service";
import { Login_model } from "../../models/login_model";
import { ImageChangeService } from '../../services/image-change.service';
import { concat } from "rxjs";


@Component({
  selector: "app-topbar",
  templateUrl: "./topbar.component.html",
  styleUrls: ["./topbar.component.scss"],
})
export class TopbarComponent implements OnInit {
  public loginModel: Login_model;
  ImagePath: string = "";
  isShown: boolean = false;
  isImageShown: boolean = true;
  demoImagePath = environment.adminImagePath + 'no-user.jpg';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private accountService: AccountService,
    private ImageChangeService: ImageChangeService,
  ) {}

  ngOnInit(): void {
    this.loginModel = <Login_model>{
      admin_id: "",
      name: "",
      phone: "",
      email: "",
      image: File = null,
    };
    //this.isLoggedIn();
    this.getProfileInfo();
      $(".button-menu-mobile").click(function () {
        $(".left.side-menu").addClass("open");
      });
      $(".left.side-menu .button-menu-mobile").click(function () {
        $(".left.side-menu").removeClass("open");
      });

      this.ImageChangeService.imageChangeEmitter.subscribe( msg=>{
        this.ImagePath = msg;
        //this.ImageChangeService.imageChangeEmitter.unsubscribe();
      })
  }

  updateImage(image) {
    this.isImageShown = false;
    this.ImagePath = environment.imagePath + image;
  }
  showdiv() {
    if (this.isShown == true) {
      this.isShown = false;
    } else {
      this.isShown = true;
    }
  }

  adminLogout() {
    localStorage.removeItem("admin_id");
    localStorage.clear();
    this.router.navigate([""]);
  }

  getProfileInfo() {
    this.loginModel.admin_id = localStorage.getItem("admin_id");
    this.accountService.adminProfile(this.loginModel).then((response) => {
      if (response.code == 200) {
        this.loginModel.name = response.result.name;
        this.loginModel.email = response.result.email;
        if (
          response.result.image == "" ||
          response.result.image == undefined ||
          response.result.image == null
        ) {
          this.ImagePath = environment.adminImagePath + "admin.png";
        } else {
          this.ImagePath = environment.adminImagePath + response.result.image;
        }
        this.isImageShown = false;
        this.loginModel.phone = response.result.phone;
        this.loginModel.password = response.result.password;
        this.loginModel.cpassword = response.result.password;
      }
    });
  }
  isLoggedIn() {
    this.loginModel.admin_id = localStorage.getItem("admin_id");
    if (this.loginModel.admin_id == null) {
      this.router.navigate(["/"]);
    }
  }
}
