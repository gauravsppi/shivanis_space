const mongoose = require('mongoose');
var userSchema = new mongoose.Schema({
    name: {
        type: String,       
    },
    email: {
        type: String
    },  
    password: {
        type: String
    },  
    address: {
        type: String
    }, 
    postal: {
        type: String
    },
    state: {
        type: String
    },
    country: {
        type: String
    },
    image: {
        type: String
    },
    otp: {
        type: String
    },
    device_id: {
        type: String
    },
    device_token: {
       type: String
    },
    status: {
        type: String
    },
    created_at: {
        type: Date,
        default: Date.now
    },
});
module.exports = userSchema;
