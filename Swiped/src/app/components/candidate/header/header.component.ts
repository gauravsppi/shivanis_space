import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { candidateLogin_model } from '../../../models/candidateLogin_model';
import { DataService } from "../../../services/data.service";
import { Headers, Http } from '@angular/http';
import * as $ from 'jquery';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public loginModel: candidateLogin_model;
  public addValidationError: candidateLogin_model;
  public localvar = localStorage.getItem("token");
  login: boolean;
  logout: boolean;
  headerdata: boolean=true;
  getId:any;
  constructor(private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService,private data: DataService) { 
    this.data.currentMessage.subscribe(message => this.headerdata = message)
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.getId = params.get('id');
      console.log('dhdghj',this.getId);
      });
    if (this.localvar == null) {
      console.log(this.localvar);
      this.login = true;
    } else {
      console.log(this.localvar);
      this.logout = true;
    }
    console.log('current url',this.router.url);
    this.headerdata=this.router.url=='/dashboard/';
    if(this.headerdata && this.getId==''){
      this.headerdata=false;
    }else if(this.getId!==''){
      this.headerdata=true;
    }else{

    }
    console.log('vhchcvhgi',this.headerdata);

    this.loginModel = <candidateLogin_model>{
      id: ''
    }
    this.addValidationError = <candidateLogin_model>{
      id: '',
      
    }
    this.loginModel.id = this.getId;
  }
  logoutclick(){
    localStorage.clear();
    //this.cookieService.deleteAll();    
    this.router.navigate(['/sign-in']);
  }
  loginclick(){
    this.router.navigate(['/sign-in']);
  }
  ngAfterViewInit() {
    $(".open-notification").click(function () {
      $(".notfication-area").addClass("show-notification");
  });
    $(".cross").click(function () {
      $(".notfication-area").removeClass("show-notification");
  });
   }
}
