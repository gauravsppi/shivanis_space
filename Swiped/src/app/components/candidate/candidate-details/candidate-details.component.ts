import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from '../../../services/account.service';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import { candidateLogin_model } from '../../../models/candidateLogin_model';
import { environment } from '../../../../environments/environment';
import { DataService } from "../../../services/data.service";
import { NgxSpinnerService } from "ngx-spinner";
import * as moment from 'moment';

import Swal from 'sweetalert2'

@Component({
  selector: 'app-candidate-details',
  templateUrl: './candidate-details.component.html',
  styleUrls: ['./candidate-details.component.scss']
})
export class CandidateDetailsComponent implements OnInit {
  
  public loginModel: candidateLogin_model;
  public addValidationError: candidateLogin_model;
  private headers = new Headers();
  public headers1 = new Headers({ 'Access-Control-Allow-Origin':'*', 'Content-Type': '*' });
  optionhow: any = [true, false, false];
  files: any[] = [];
  skills = [];
  fulltime: boolean = false;
  parttime: boolean = false;
  internship: boolean = false;
  disabledrop: boolean =false;
  jobtype: any;
  optionlength: number = 0;
  i: number = 1;
  q: number = 0;
  r: number = 1;
  s: number = 1;
  t: number = 1;
  lets: boolean = true;
  degree: boolean = true;
  current: boolean = true;
  target: boolean = true;
  upload: boolean = true;
  university1: any[];
  marked: boolean = false;
  endvalue: string;
  sucMsg: boolean = false;
  errMsg: boolean = false;
  message: string;
  w: number=0;
  x: number=0;
  y: number=0;
  z: number=0;
  filename1:string;
  filename2:string;
  filename3:string;
  filename4:string;
  availabilityDate=[];
  //----------validation for first tab-------------
  validationcurrentjobtitle: boolean = false;
  validationforwho: boolean = false;
  validationgithub: boolean = false;
  validationlinkedin: boolean = false;
  validationsector: boolean = false;
  validationskills: boolean = false;
  validationexperience: boolean = false;
  validationlooking: boolean = false;
  validationcvupload: boolean = false;
  validationrefupload: boolean = false;
  validationwebsiteupload: boolean = false;
//----------validation for second tab-------------
  validationqualification: boolean = false;
  validationuniversity: boolean = false;
  validationgrade: boolean = false;
//----------validation for third tab-------------
  validationjobtitle: boolean = false;
  validationcurrency: boolean = false;
  validationsalary: boolean = false;
  validationpreflocation: boolean = false;
  validationculture: boolean = false;
  validationmaindrivers: boolean = false;
  validationindustry: boolean = false;
  validationsize: boolean = false;
  //validationdate: boolean = false;
  logout:boolean=true;

//----------validation for foruth tab-------------
  validationfriend: boolean = false;
  validationdowell: boolean = false;
  validationpassion: boolean = false;
  validationgoals: boolean = false;
  validationanythingelse: boolean = false;
  validationvideo: boolean = false;
  validationavailability: boolean = false;
  validationidealjob: boolean = false;

  //--------------disable tab-------
  disablefirstTab: boolean = true;
  disablesecondTab: boolean = true;
  disablethirdTab: boolean = true;
  disablefourthTab: boolean = true;


  candidatelist:any ;
  letsgoTabdata:any [] ;
  degreeTabdata:any [] ;
  currentjobTabdata:any [] ;
  interestsTabdata:any [] ;
  getId:any;
  idGet:any;
  skillswithsector:any = [];
  public todayDate:any = new Date();
  minDate = new Date();
  
  //------ For Qualifications And All Types Of Dropown -----
  
  employmenttypes =[
    'Permanent',
    'Contract',
    'Temporary'
  ]
  sectorlists = [
    'DATA',
    'TECHNOLOGY',
    'MARKETING',
    'CREATIVE',
      ];
  selectedSector: [];
    sectorlist = [
        { name: 'Product Analytics',  category: 'DATA' },
        { name: 'Big Data Engineering ',  category: 'DATA' },
        { name: 'CRM & Marketing Analytics',  category: 'DATA' },
        { name: 'BI & Data Warehousing',  category: 'DATA' },
        { name: 'Data Science',  category: 'DATA' },
        { name: 'Customer Insight',  category: 'DATA' },
        { name: 'Machine Learning & AI',  category: 'DATA' },
        { name: 'Digital Analytics & Optimization',  category: 'DATA' },
        { name: 'Front-End Developmet/ UI ',  category: 'TECHNOLOGY' },
        { name: 'Back-End Software Developmet',  category: 'TECHNOLOGY' },
        { name: 'Infrastructure',  category: 'TECHNOLOGY' },
        { name: 'Full Stack Development',  category: 'TECHNOLOGY' },
        { name: 'Mobile Developmet',  category: 'TECHNOLOGY' },
        { name: 'Devops',  category: 'TECHNOLOGY' },
        { name: 'Enterprise Technologies',  category: 'TECHNOLOGY' },
        { name: 'Testing & QA',  category: 'TECHNOLOGY' },
        { name: 'AD-Technology', category: 'MARKETING' },
        { name: 'Digital Marketing',  category: 'MARKETING' },
        { name: 'Paid Social', category: 'MARKETING' },
        { name: 'PPC', category: 'MARKETING' },
        { name: 'Performance Marketing', category: 'MARKETING' },
        { name: 'Content Marketing',  category: 'MARKETING' },
        { name: 'SEO', category: 'MARKETING' },
        { name: 'Programmatic', category: 'MARKETING' },
        { name: 'Product Management', category: 'CREATIVE' },
        { name: 'Project Management',  category: 'CREATIVE' },
        { name: 'Digital Production', category: 'CREATIVE' },
        { name: 'Studio Management', category: 'CREATIVE' },
        { name: 'Visual Design', category: 'CREATIVE' },
        { name: 'Product & Service Design',  category: 'CREATIVE' },
        { name: 'UX & Research', category: 'CREATIVE' },
        { name: 'Art Direction', category: 'CREATIVE' },
       
    ];
   
  
  prefferedIndustrylist = [
    'DATA',
    'TECHNOLOGY',
    'MARKETING',
    'CREATIVE',
   
  ];
  currency = [
    '£',
    '€',
 
   
  ];

  currentStatuslist = [
    'Yes',
    'No',
    'Passively Looking'   
   
  ];

  experiencelist = [
    '0-12 months',
    '1-2 years',
    '2-4 years',
    '4-6 years',
    '6 years+'
  ];

  locationlist = [
    'uk',
    'poland',
    'ireland',
    'Austria'   
  ];

  sizelist = [
    '30',
    '50',
    '100',
    '500 & above'   
  ];

  culturelist = [
    'Corporate',
    'Start Up ',
    'Remote',
    'Open to all cultures'   
  ];

  driverlist = [
    'Salary Increase',
    'Progression ',
    'Culture',
    'Career Change'
  ];

   //prelocationindex: any;
  // prequalificationindex: any;

//----------------- filter skills as per sector ------------------------

  filterskillswithsector(event){
    this.skillswithsector =[];
    this.sectorlist.forEach((elment,id)=>{  
           
      if(event ==elment.category){
          this.skillswithsector.push(elment.name);
      }
    
    });
  }

  /**
   * on file drop handler
   */
  
  onFileDropped($event) {
    this.prepareFilesList($event);
  }

  /**
   * handle file from browsing
   */
  fileBrowseHandler(files) {
    this.prepareFilesList(files);
  }

  /**
   * Delete file from files list
   * @param index (File index)
   */
  deleteFile(index: number) {
    this.files.splice(index, 1);
  }

  /**
   * Simulate the upload process
   */
  uploadFilesSimulator(index: number) {
    setTimeout(() => {
      if (index === this.files.length) {
        return;
      } else {
        const progressInterval = setInterval(() => {
          if (this.files[index].progress === 100) {
            clearInterval(progressInterval);
            this.uploadFilesSimulator(index + 1);
          } else {
            this.files[index].progress += 5;
          }
        }, 200);
      }
    }, 1000);
  }

  /**
   * Convert Files list to normal array list
   * @param files (Files List)
   */
  prepareFilesList(files: Array<any>) {
    for (const item of files) {
      item.progress = 0;
      this.files.push(item);
    }
    this.uploadFilesSimulator(0);
  }

  /**
   * format bytes
   * @param bytes (File size in bytes)
   * @param decimals (Decimals point)
   */
  formatBytes(bytes, decimals) {
    if (bytes === 0) {
      return '0 Bytes';
    }
    const k = 1024;
    const dm = decimals <= 0 ? 0 : decimals || 2;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService,private spinner: NgxSpinnerService,private data: DataService) { }

  ngOnInit(): void {
    this.loginModel = <candidateLogin_model>{
     id:'',
     current_job_title:'',
     job_type:'',
     for_who:'',
     sector: [],
     skills: [],
     experience:[],
     
    // specific_project_experience: [],
    // first_language: [],
     link_github:'',
     link_linkedin:'',
     your_website:'',
     relevent_project_work_links:[],
     cv_upload:[],
     skill_assessment_upload: [],
     reference_upload:[],
     qualification: [],
     preffered_location:[],
     call_request:[],
     grade: [],
     university_school: [],
     date: [],
     job_title: '',
     currencylist:[],
     target_salary: '',
     salary_type:'',
     type_role:'',
     current_status: [],
     ideal_start_date: [],
     preffered_industry: [],
     size1: [],
     type_of_industry: '',
     main_driver: '',
     describe_friend: '',
     do_well: '',
     passion: '',
     goals: '',
     anything_else: '',
     video_upload: [],
     availability:[],
     ideal_job:''
    }
    this.addValidationError = <candidateLogin_model>{
      current_job_title:'',
      job_type: '',
      for_who: '',     
      experience: [],    
      link_github:'',
      link_linkedin:'',
      //your_website: '',
      grade: '',
      university_school: '',
      qualification: [],
      job_title: '',
      currencylist:'',
      target_salary: '',     
      current_status: [],
      preffered_industry: '',
      date:[],
      preffered_location:[],
      size1: [],
      type_of_industry: '',
      main_driver: '',
      describe_friend: '',
      do_well: '',
      passion: '',
      goals: '',
      anything_else: '',
      ideal_job:'',
      sector:[],
      video_upload:[],
      availability:[]
     
    }
    this.skills = [
          'Product Analytics',
          'Big Data Engineering ',  
         'CRM & Marketing Analytics',
         'BI & Data Warehousing', 
        'Data Science', 
        'Customer Insight',  
        'Machine Learning & AI', 
         'Digital Analytics & Optimization', 
         'Front-End Developmet/ UI ', 
         'Back-End Software Developmet',  
         'Infrastructure', 
        'Full Stack Development',  
         'Mobile Developmet',  
         'Devops',  
         'Enterprise Technologies',  
         'Testing & QA',  
         'AD-Technology', 
         'Digital Marketing', 
         'Paid Social', 
          'PPC', 
         'Performance Marketing', 
         'Content Marketing',  
         'SEO', 
         'Programmatic', 
         'Product Management', 
         'Project Management',  
         'Digital Production', 
         'Studio Management', 
         'Visual Design', 
         'Product & Service Design',  
         'UX & Research', 
         'Art Direction',
    ];
    this.route.paramMap.subscribe(params => {
      this.getId = '';
      this.idGet = params.get('id');
      console.log(this.getId);
      if(this.idGet=='' || this.idGet==null){
          this.enabledisable();
          this.getCandidatebyId();
      }else{
        this.loginModel.id = this.idGet;
        this.accountService.updatedAccountStatus(this.loginModel).then((response) => {
          if (response.code == 200) {
            localStorage.setItem('token', response.token);
            this.data.changeMessage(true);
            this.enabledisable();
            this.getCandidatebyId();
          }else{
            this.router.navigate(['/sign-in']);
          }
        })
      }
    });
  }
  file: File[] = [];
  

checkvalidation(){
    if (this.loginModel.cv_upload !== "") {
        this.validationcvupload=true;
      }else{
        this.validationcvupload=false;
      }
      // if (this.loginModel.skill_assessment_upload !== "") {
      //   this.validationwebsiteupload=true;
      // }else{
      //   this.validationwebsiteupload=false;
      // }
      // if (this.loginModel.reference_upload !== "") {
      //   this.validationrefupload=true;
      // }else{
      //   this.validationrefupload=false;
      // }
      if (this.loginModel.video_upload !== "") {
        this.validationvideo=true;
      }else{
        this.validationvideo=false;
      }
}

currentjobtitlevalidation(){
  if (this.loginModel.current_job_title == "") {
      this.validationcurrentjobtitle=true;
     }else{
      this.validationcurrentjobtitle=false;
   }
}

forwhovalidation(){
  if (this.loginModel.for_who == "") {
      this.validationforwho=true;
     }else{
      this.validationforwho=false;
   }
}

sectorvalidation(){
  if (this.loginModel.sector == '') {
   this.validationsector=false;    
  }else{
    this.validationsector=true;
     }
}

skillsvalidation(){
  if (this.loginModel.skills == '') {
    this.validationskills=false;    
   }else{
     this.validationskills=true;
      }
}

experiencevalidation(){
  if (this.loginModel.experience == '') {
    this.validationexperience=false;    
   }else{
     this.validationexperience=true;
      }
}

lookingvalidation(){
  if (this.loginModel.current_status == '') {
    this.validationlooking=false;    
   }else{
     this.validationlooking=true;
      }
}

idealjobvalidation(){
  if (this.loginModel.job_title == "") {
        this.validationjobtitle=true;
      }else{
        this.validationjobtitle=false;
      }
}

// prefferedlocationvalidation(){
//   if (this.loginModel.preffered_location.length !== 0) {
//       this.validationpreflocation=true;
//       }else{
//         this.validationpreflocation=false;
//       }
// }

// datevalidation(){
//   if (this.loginModel.date == '') {
//       this.validationdate=true;
//      }else{
//       this.validationdate=false;
//    }
// }

industryvalidation(){
  if (this.loginModel.preffered_industry == '') {
    //alert("no");
   this.validationindustry=false;    
  }else{
    //alert("yes");
    this.validationindustry=true;
     }
}

sizevalidation(){
  if (this.loginModel.size1 == '') {
        this.validationsize=false;
      }else{
        this.validationsize=true;
      }
}

maindrivervalidation(event: any) {
  if (this.loginModel.main_driver != "") {
    this.validationmaindrivers=false;
  }else{
    this.validationmaindrivers=true;
  }
  const pattern = /[a-zA-Z ]/;
  let inputChar = String.fromCharCode(event.charCode);
  if (event.keyCode != 8 && !pattern.test(inputChar)) {
  event.preventDefault();
  
  }
  }

// maindrivervalidation1(){
//   if (this.loginModel.main_driver == "") {
//         this.validationmaindrivers=true;
//       }else{
//         this.validationmaindrivers=false;
//       }
// }

// culturevalidation1(){
//   var NAME_REGEXP = /^[A-Za-z ]+$/;
//   if (this.loginModel.type_of_industry == "") {
//         this.validationculture=true;
//       }else{
//         this.validationculture=false;
//       }
//       if (NAME_REGEXP.test(this.loginModel.type_of_industry)===false) {
//         this.validationculture=true;
//            }    
// }

culturevalidation(event: any) {
  if (this.loginModel.type_of_industry != "") {
    this.validationculture=false;
  }else{
    this.validationculture=true;
  }
  const pattern = /[a-zA-Z ]/;
  let inputChar = String.fromCharCode(event.charCode);
  if (event.keyCode != 8 && !pattern.test(inputChar)) {
  event.preventDefault();
  
  }
  }

bestfriendvalidation(){
  if (this.loginModel.describe_friend == "") {
        this.validationfriend=true;
      }else{
        this.validationfriend=false;
      }
}

dowellvalidation(){
  if (this.loginModel.do_well == "") {
        this.validationdowell=true;
      }else{
        this.validationdowell=false;
      }
}

passionvalidation(){
  if (this.loginModel.passion == "") {
        this.validationpassion=true;
      }else{
        this.validationpassion=false;
      }
}

goalvalidation(){
  if (this.loginModel.goals == "") {
        this.validationgoals=true;
      }else{
        this.validationgoals=false;
      }
}
idealvalidation(){
  if (this.loginModel.ideal_job == "") {
        this.validationidealjob=true;
      }else{
        this.validationidealjob=false;
      }
}

anythingvalidation(){
  if (this.loginModel.anything_else == "") {
        this.validationanythingelse=true;
      }else{
        this.validationanythingelse=false;
      }
}

currencyvalidation(){
  if (this.loginModel.currencylist == '') {
   this.validationcurrency=false;    
  }else{
    this.validationcurrency=true;
     }
}

keyPressSalary(event: any) {
  if (this.loginModel.target_salary == "") {
        this.validationsalary=true;
      }else{
        this.validationsalary=false;
      }
  const pattern = /[0-9]/;
  let inputChar = String.fromCharCode(event.charCode);
  if (event.keyCode != 8 && !pattern.test(inputChar)) {
  event.preventDefault();
  
  }
  }

  //--------------- disable currency list after selection----------------
  disableDropdown(){
    //alert();
    this.disabledrop =true;
  }

  //--------------- to check wheather entered value already exist or not----------------
  alreadyExistQualification(){

  }
  
  // ------ Tabs Enable Disable According To The Login Type Status ---------
  enabledisable() {
    this.accountService.StatusGet().then((response) => {
      console.log('tab status>>>>>',response);
      if (response.code == 200) {
        if (response.result.login_type_status == 0) {
          this.lets = false;
        } else if (response.result.login_type_status == 1) {
          // window.location.reload();
          this.degree = false;
        } else if (response.result.login_type_status == 2) {
          this.current = false;
        } else if (response.result.login_type_status == 3) {
          this.target = false;
        } 
        else if (response.result.login_type_status == 4) {
          this.upload = false;
        } 
        else {
          //this.router.navigate(['/account_created']);
          this.router.navigate(['/email_verify']);
        }
        //this.sucMsg = true;
      } else {
        this.errMsg = true;
      }
    })
  }
  // ---- Counter For Degree Tab ----------
  counter(i) {
    return new Array(i);
  }
  appendData() {
    this.i++;
  }
  closetab() {
    this.i--;
  }
  // ------ Counter For --------
  counters(q) {
    return new Array(q);
  }
  addData() {
    this.q++;
  }
  removeData() {
    this.q--;
  }
  // -------- Counter For Qualification Tab ------------
  countes(r) {
    return new Array(r);
  }
  addQualification() {
    this.r++;
    //this.prequalificationindex=j
  }
  removeQualification() {
    this.r--;
  }
  // ----- Add relevent project links for you tab ------------
  counts(s) {
    return new Array(s);
  }
  addWork() {
    this.s++;
  }
  removeWork() {
    this.s--;
  }
  // ----------- Add preffered location Counter ----------
  count(t) {
    return new Array(t);
  }
  addlocation( ){
    this.t++;
    //this.prelocationindex=i;
  }
  removelocation() {
    this.t--;
  }
  // ----------- Upload CV Image -----------
  selectcvImage(event){
    console.log(event.target.files[0]);
    this.w=event.target.files.length;
    this.filename1=event.target.files[0].name;
    this.loginModel.cv_upload = event.target.files[0];
    //if(event.target.files.length<1) {
      const formData = new FormData();
      formData.append('cv_upload', <File>event.target.files[0]);
      //formData.append('type', 'CV');
      //this.accountService.uploadCVFiles(formData).then((response) => {
      this.http.post(environment.apiBaseUrl+'Candidate/uploadCVFiles', formData , { headers: this.headers }).subscribe(res => {
      //this.http.post(environment.apiBaseUrl+'Candidate/uploadCVFiles', formData, { headers: this.headers }).subscribe((res: any) => {
        
      var obj = res.json();
        //this.urls = obj.url + obj.image;
        this.loginModel.cv_upload = obj.result;
        console.log('response recevied is ', this.loginModel.cv_upload);
      });
    //}
  }
  removecvfile(event){
    this.w=0;
    event.value="";
  }

  // ----------- Upload Skills Image -----------
  selectSkillsImage(event){
    console.log(event.target.files[0]);
    this.x=event.target.files.length;
    this.filename2=event.target.files[0].name;
    this.loginModel.skill_assessment_upload = event.target.files[0];
    //if(event.target.files.length<1) {
      const formData = new FormData();
      formData.append('skill_assessment_upload', <File>event.target.files[0]);
      //this.http.post(environment.apiBaseUrl+'Candidate/uploadVideo', formData, { headers: this.headers }).subscribe((res: any) => {
        this.http.post(environment.apiBaseUrl+'Candidate/uploadSkillFiles', formData , { headers: this.headers }).subscribe(res => { 
        var obj = res.json();
        //this.urls = obj.url + obj.image;
        this.loginModel.skill_assessment_upload = obj.result;
        console.log('response recevied is ', this.loginModel.skill_assessment_upload);
      });
    //}
  }
  removeskillsfile(event){
    this.x=0;
    event.value="";
  }
 
  // ----------- Upload Reference Image -----------
  selectReferenceImage(event){
    console.log(event.target.files[0]);
    this.y=event.target.files.length;
    this.filename3=event.target.files[0].name;
    this.loginModel.reference_upload = event.target.files[0];
    //if(event.target.files.length<1) {
      const formData = new FormData();
      formData.append('reference_upload', <File>event.target.files[0]);
      //this.http.post(environment.apiBaseUrl+'Candidate/uploadReferenceFiles', formData, { headers: this.headers }).subscribe((res: any) => {
        this.http.post(environment.apiBaseUrl+'Candidate/uploadReferenceFiles', formData , { headers: this.headers }).subscribe(res => { 
        var obj = res.json();
        //this.urls = obj.url + obj.image;
        this.loginModel.reference_upload = obj.result;
        console.log('response recevied is ', this.loginModel.reference_upload);
      });
    //}
  }
  removereferencefile(event){
    this.y=0;
    event.value="";
  }

  // ----------- Upload optional video Image -----------
  selectVideoImage(event){
    console.log(event.target.files[0]);
    this.z=event.target.files.length;
    this.filename4=event.target.files[0].name;
    this.loginModel.video_upload = event.target.files[0];
    //if(event.target.files.length<1) {
      const formData = new FormData();
      formData.append('video_upload', <File>event.target.files[0]);
      //this.http.post(environment.apiBaseUrl+'Candidate/uploadVideoFiles', formData, { headers: this.headers }).subscribe((res: any) => {
        this.http.post(environment.apiBaseUrl+'Candidate/uploadVideoFiles', formData , { headers: this.headers }).subscribe(res => { 
          
      var obj = res.json();
        //this.urls = obj.url + obj.image;
        this.loginModel.video_upload = obj.result;
        console.log('response recevied is ', this.loginModel.video_upload);
      });
    //}
  }
  removevideofile(event){
    this.z=0;
    event.value="";
  }
  // ------ Let's Go Tab Data Save -----------
  letgotabdatasave() {
    console.log('enter here');
    var valid = true;
    var NAME_REGEXP = /^[A-Za-z ]+$/;
    var NAMENUM_REGEXP = /^[A-Za-z0-9 ]+$/;
    var GIT_REGEXP = /(www\.)?github\.([a-z])+\/([A-Za-z0-9]{1,})+\/?$/;
    var LINKEDIN_REGEXP = /(((www|\w\w)\.)?linkedin\.com\/)((([\w]{2,3})?)|([^\/]+\/(([\w|\d-&#?=])+\/?){1,}))$/;
    this.addValidationError.current_job_title = '';
    this.addValidationError.for_who = '';
     this.addValidationError.link_github = '';
     this.addValidationError.link_linkedin = '';  
    this.addValidationError.sector = [];
    this.addValidationError.skills = [];
    this.addValidationError.experience = []; 
    this.addValidationError.current_status = [];
   
    this.addValidationError.cv_upload = [];
    this.addValidationError.reference_upload = [];
    this.addValidationError.skill_assessment_upload = []; 
    console.log('login model here>>>',this.loginModel);
    if (this.loginModel.current_job_title == "") {
      this.validationcurrentjobtitle=true;
      valid = false;
    }
    if (NAMENUM_REGEXP.test(this.loginModel.current_job_title)===false) {
      this.validationcurrentjobtitle=true;
      valid = false;
    }
    if (this.loginModel.for_who == "") {
      this.validationforwho=true;
      valid = false;
    }
    if (NAMENUM_REGEXP.test(this.loginModel.for_who)===false) {
      this.validationforwho=true;
      valid = false;
    }
    if (this.loginModel.sector == '') {
      console.log(this.loginModel.sector.length);
      this.validationsector=true;
      //alert("no");
  
    }else{
      this.validationsector=false;
      //alert("yes");
    }

    if (this.loginModel.skills == '') {
      console.log(this.loginModel.skills.length);
      this.validationskills=true;
      //alert("no");
  
    }else{
      this.validationskills=false;
      //alert("yes");
    }
    if (this.loginModel.experience == '') {
      console.log(this.loginModel.sector.experience);
      this.validationexperience=true;
      //alert("no");
  
    }else{
      this.validationexperience=false;
      //alert("yes");
    }

    if (this.loginModel.current_status == '') {
      console.log(this.loginModel.current_status.experience);
      this.validationlooking=true;
      //alert("no");
  
    }else{
      this.validationlooking=false;
      //alert("yes");
    }
   
    // if (this.loginModel.link_github == "" || GIT_REGEXP.test(this.loginModel.link_github)===true) {
    //   this.validationgithub=false;
    // }else{
    //   this.validationgithub=true;
    //   valid = false;
    // }
   
    // if (this.loginModel.link_linkedin == "" || LINKEDIN_REGEXP.test(this.loginModel.link_linkedin)===true) {
    //   this.validationlinkedin=false;
    // }else{
    //   this.validationlinkedin=true;
    //   valid = false;
    // }
    // if (GIT_REGEXP.test(this.loginModel.link_github)===false) {
    //   this.validationgithub=true;
    //   valid = false;
    // }
    // if (this.loginModel.link_linkedin == "") {
    //   this.validationlinkedin=true;
    //   valid = false;
    // }
    // if (LINKEDIN_REGEXP.test(this.loginModel.link_linkedin)===false) {
    //   this.validationlinkedin=true;
    //   valid = false;
    // }
    // if (this.loginModel.skill_assessment_upload == "") {
    //   this.validationwebsiteupload=true;
    //   valid = false;
    // }else{
    //   this.validationwebsiteupload=false;
    // }
    if (this.loginModel.cv_upload == "") {
      this.validationcvupload=true;
      valid = false;
    }else{
      this.validationcvupload=false;
    }
    if(this.loginModel.skill_assessment_upload.length==0){
      this.loginModel.skill_assessment_upload='null';
    }
    if(this.loginModel.reference_upload.length==0){
      this.loginModel.reference_upload='null';
    }
    // if (this.loginModel.reference_upload == "") {
    //   this.validationrefupload=true;
    //   valid = false;
    // }else{
    //   this.validationrefupload=false;
    // }
    if(valid){
      console.log('enter here with response>>>',this.loginModel);
      Swal.fire({
        title: 'Are you sure you want to submit?',
        text: 'You are not able to edit data here',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor:'#D0021B',
        confirmButtonText: 'Yes, want to save record',
        cancelButtonColor: '#D0021B',
        cancelButtonText: 'No, keep it',
      }).then((result) => {
        if (result.isConfirmed) {
          this.accountService.FirstTab(this.loginModel).then((response) => {
            if (response.code == 200) {
              Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B' , icon: 'success', title: 'Record successfully saved' })
              this.lets = true;
              this.degree = false;
              this.errMsg = false;
            } else {
              this.errMsg = true;
              this.message = response.message;
              console.log(this.message,"111");
            }
          })
        } else if (result.isDismissed) {
          Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B', icon: 'success', title: 'Record not saved' })
        }
      })
    }  
  }
  // --------- Qualification Tab Data Save ---------
  degreetabdatasave() {
    // var valid = true;
    // var NAME_REGEXP = /^[A-Za-z ]+$/;
    // var NAMENUM_REGEXP = /^[A-Za-z0-9 ]+$/;
    // this.addValidationError.qualification = [];
    // this.addValidationError.university_school = [];
    // this.addValidationError.grade = [];

    // if (this.loginModel.qualification == "") {
    //   this.validationqualification=true;
    //   valid = false;
    // }
    // if (NAMENUM_REGEXP.test(this.loginModel.qualification)===false) {
    //   console.log(this.loginModel.qualification);
    //   this.validationqualification=true;
    //   valid = false;
    // }
    // if (this.loginModel.university_school == "") {
    //   this.validationuniversity=true;
    //   valid = false;
    // }
    // if (NAMENUM_REGEXP.test(this.loginModel.university_school)===false) {
    //   this.validationuniversity=true;
    //   valid = false;
    // }
    // if (this.loginModel.grade == "") {
    //   this.validationgrade=true;
    //   valid = false;
    // }
    // if (NAMENUM_REGEXP.test(this.loginModel.grade)===false) {
    //   this.validationgrade=true;
    //   valid = false;
    // }
    // if(valid){

      Swal.fire({
        title: 'Are you sure you want to submit?',
        text: 'You are not able to edit data here',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor:'#D0021B',
        confirmButtonText: 'Yes, want to save record',
        cancelButtonColor: '#D0021B',
        cancelButtonText: 'No, keep it',
      }).then((result) => {
        if (result.isConfirmed) {
          this.accountService.SecondTab(this.loginModel).then((response) => {
            if (response.code == 200) {
              Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B' , icon: 'success', title: 'Record successfully saved' })
              
              this.degree = true;
              this.current = false;
              this.errMsg = false;
            } else {
              this.errMsg = true;
              this.message = response.message;
            }
          })
        } else if (result.isDismissed) {
          Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B', icon: 'success', title: 'Record not saved' })
        }
      })
    //}  
  }
  // ---- Current Ideal Job Tab Data Save ----------
  currentTabdata() {
    var valid = true;
    var NUM_REGEXP = /^[0-9]+$/;
    var NAME_REGEXP = /^[A-Za-z ]+$/;
    var NAMENUM_REGEXP = /^[A-Za-z0-9 ]+$/;
    this.addValidationError.job_title = '';
    this.addValidationError.preffered_location = [];
    this.addValidationError.target_salary = '';
    this.addValidationError.date = [];
    this.addValidationError.type_of_industry = '';
    this.addValidationError.main_driver = '';
    console.log(this.loginModel);
    if (this.loginModel.job_title == "") {
      this.validationjobtitle=true;
      valid = false;
    }
    if (NAMENUM_REGEXP.test(this.loginModel.job_title)===false) {
      this.validationjobtitle=true;
      valid = false;
    }
    if (this.loginModel.currencylist == "") {
      this.validationcurrency=true;
      valid = false;
    }
    if (this.loginModel.target_salary == "") {
      this.validationsalary=true;
      valid = false;
    }
    
    // if (NUM_REGEXP.test(this.loginModel.target_salary)===false) {
    //   this.validationsalary=true;
    //   valid = false;
    // }
    // if (this.loginModel.preffered_location.length !== 0) {
    //   this.validationpreflocation=true;
    //   valid = false;
    // }
    // if (NAME_REGEXP.test(this.loginModel.preffered_location)===false) {
    //   this.validationpreflocation=true;
    //   valid = false;
    // }
    // if (this.loginModel.type_of_industry == "") {
    //   this.validationculture=true;
    //   valid = false;
    // }
    // if (NAME_REGEXP.test(this.loginModel.type_of_industry)===false) {
    //   this.validationculture=true;
    //   valid = false;
    // }
    if (this.loginModel.preffered_industry == '') {
    
      this.validationindustry=true;     
      }else{
      this.validationindustry=false;     
    }
    // if (NAME_REGEXP.test(this.loginModel.type_of_industry)===false) {
    //   this.validationculture=true;
    //   valid = false;
    // }
    // if (this.loginModel.date == '') {
    //   alert("111");
    //   this.validationdate=true;
    //   valid = false;
    // }
    // if (this.loginModel.main_driver == "") {
    //   this.validationmaindrivers=true;
    //   valid = false;
    // }
    // if (this.loginModel.size1 == '') {
    
    //   this.validationsize=true;    
    // }else{
    //   this.validationsize=false;
    //   }
    // if (NAME_REGEXP.test(this.loginModel.main_driver)===false) {
    //   this.validationmaindrivers=true;
    //   valid = false;
    // }
    if(valid){
      Swal.fire({
        title: 'Are you sure you want to submit?',
        text: 'You are not able to edit data here',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor:'#D0021B',
        confirmButtonText: 'Yes, want to save record',
        cancelButtonColor: '#D0021B',
        cancelButtonText: 'No, keep it',
      }).then((result) => {
        if (result.isConfirmed) {
          this.accountService.ThirdTab(this.loginModel).then((response) => {
            if (response.code == 200) {
              Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B' ,icon: 'success', title: 'Record successfully saved' })
              this.current = true;
              this.target = false;
              this.errMsg = false;
            } else {
              this.errMsg = true;
              this.message = response.message;
            }
          })
        } else if (result.isDismissed) {
          Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B', icon: 'success', title: 'Record not saved' })
        }
      })
    } 
  }
  // ------- Interest Tab Data Save -------
  interestTabdata() {
    this.loginModel.availability=this.availabilityDate;
    console.log('length of dated selecte6d>>',this.loginModel);
    var valid = true;
    var NAME_REGEXP = /^[A-Za-z ]+$/;
    var NAMENUM_REGEXP = /^[A-Za-z0-9 ]+$/;
    this.addValidationError.describe_friend = '';
    this.addValidationError.do_well = '';
    this.addValidationError.passion = '';
    this.addValidationError.goals = '';
    this.addValidationError.anything_else = '';
    this.addValidationError.anything_else = '';
    //this.addValidationError.video_upload = [];

    if (this.loginModel.describe_friend == "") {
      this.validationfriend=true;
      valid = false;
    }
    // if (NAMENUM_REGEXP.test(this.loginModel.describe_friend)===false) {
    //   this.validationfriend=true;
    //   valid = false;
    // }
    if (this.loginModel.do_well == "") {
      this.validationdowell=true;
      valid = false;
    }
    if(this.availabilityDate.length!==3){
      this.validationavailability=true;
      valid = false;
    }else{
      this.validationavailability=false;
      valid = true;
    }
    // if (NAMENUM_REGEXP.test(this.loginModel.do_well)===false) {
    //   this.validationdowell=true;
    //   valid = false;
    // }
    if (this.loginModel.passion == "") {
      this.validationpassion=true;
      valid = false;
    }
    // if (NAMENUM_REGEXP.test(this.loginModel.passion)===false) {
    //   this.validationpassion=true;
    //   valid = false;
    // }
    if (this.loginModel.goals == "") {
      this.validationgoals=true;
      valid = false;
    }
    // if (NAMENUM_REGEXP.test(this.loginModel.goals)===false) {
    //   this.validationgoals=true;
    //   valid = false;
    // }
    if (this.loginModel.ideal_job == "") {
      this.validationidealjob=true;
      valid = false;
    }
    if (this.loginModel.anything_else == "") {
      this.validationanythingelse=true;
      valid = false;
    }
    // if (this.loginModel.video_upload == "") {
    //   this.validationvideo=true;
    //   valid = false;
    // }else{
    //   this.validationvideo=false;
    // }
    
    // if (NAMENUM_REGEXP.test(this.loginModel.anything_else)===false) {
    //   this.validationanythingelse=true;
    //   valid = false;
    // }
    if(valid){
      Swal.fire({
        title: 'Are you sure you want to submit?',
        text: 'You are not able to edit data here',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor:'#D0021B',
        confirmButtonText: 'Yes, want to save record',
        cancelButtonColor: '#D0021B',
        cancelButtonText: 'No, keep it',
        
      }).then((result) => {
        if (result.isConfirmed) {
          this.accountService.FourthTab(this.loginModel).then((response) => {
            if (response.code == 200) {
              Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B' , icon: 'success', title: 'Record successfully saved' })
              this.target = true;           
              this.errMsg = false;
              console.log(response);
              this.router.navigate(['/email_verify']);
            } else {
              this.errMsg = true;
              this.message = response.message;
            }
          })
        } else if (result.isDismissed) {
          Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B', icon: 'success', title: 'Record not saved' })
        }
      })
    }  
  }

  //---------------------change salary type monthly/annually----------------------
  onItemChange(value){
    console.log(value);    
    this.loginModel.salary_type = value;
    console.log(this.loginModel);
  }

  //---------------------change job type permanent/contract----------------------
  onRoleTypeChange(value){
    console.log(value);    
    this.loginModel.type_role = value;
    console.log(this.loginModel);
  }

   //---------------------change jobtype type perm/free/contract----------------------
   onJobtypeChange(value){
    console.log(value);    
    this.loginModel.job_type = value;
    console.log(this.loginModel);
  }

 //-----------------------get candidate details API------------------------
 getCandidatebyId(){
  this.loginModel.id = this.getId;
  //this.spinner.show();
  this.accountService.getCandidateWithID(this.loginModel).then((response) => {
    console.log('enter here for response',response);
    if (response.code == 200) {
      console.log('get candidate details',response);
      //this.spinner.hide();
      this.candidatelist=response.result;
      this.loginModel.email=this.candidatelist.email;
      console.log('candidate list here>>>',this.candidatelist.email);
      this.letsgoTabdata=response.letsgoTabdata;
      this.degreeTabdata=response.degreeTabdata;
      this.currentjobTabdata=response.currentjobTabdata;
      this.interestsTabdata=response.interestsTabdata;

      //firsttab/about you fields data
      // this.loginModel.current_job_title=response.letsgoTabdata[0].c_job_title;
      // this.loginModel.job_type=response.letsgoTabdata[0].job_type;
      // console.log(this.loginModel.job_type);
      // this.loginModel.for_who=response.letsgoTabdata[0].for_who;
      // this.loginModel.sector=response.letsgoTabdata[0].sector;
      // console.log(this.loginModel.sector);
      // this.loginModel.skills=response.letsgoTabdata[0].skills;
      // this.loginModel.experience=response.letsgoTabdata[0].experience;
      // console.log(this.loginModel.experience);
      // this.loginModel.current_status=response.letsgoTabdata[0].current_status;
      // this.loginModel.link_github=response.letsgoTabdata[0].link_github;
      // this.loginModel.link_linkedin=response.letsgoTabdata[0].link_linkedin;
      // this.loginModel.your_website=response.letsgoTabdata[0].your_website;
      // this.loginModel.relevent_project_work_links=response.letsgoTabdata[0].relevent_project_work_links;
      // console.log(this.loginModel.relevent_project_work_links);
      // this.loginModel.skill_assessment_upload=response.letsgoTabdata[0].skill_assessment_upload;
      // this.loginModel.cv_upload=response.letsgoTabdata[0].cv_upload;
      // this.loginModel.reference_upload=response.letsgoTabdata[0].reference_upload;
    

      //qualification fields data
      // this.loginModel.qualification=response.degreeTabdata[0].candidate_qualification;
      // this.loginModel.grade=response.degreeTabdata[0].grade;
      // this.loginModel.university_school=response.degreeTabdata[0].university;
      // this.loginModel.date= response.degreeTabdata[0].date;
      // //this.loginModel.date= moment(response.degreeTabdata[0].date).format("YYYY-MM-DD");
      // console.log(this.loginModel.date);
      // // let myMoment: moment.Moment = moment(this.loginModel.date);
      // // console.log( moment(response.degreeTabdata[0].date).format("YYYY-MM-DD"));
      

      // //ideal job fields data
      // this.loginModel.job_title=response.currentjobTabdata[0].t_job_title;
      // this.loginModel.target_salary=response.currentjobTabdata[0].target_salary;
      // this.loginModel.salary_type=response.currentjobTabdata[0].salary_type;
      // this.loginModel.type_role=response.currentjobTabdata[0].type_role;
      // this.loginModel.preffered_location=response.currentjobTabdata[0].preferred_location;
      // console.log(this.loginModel.preffered_location);
      // this.loginModel.ideal_start_date=response.currentjobTabdata[0].ideal_start_date;
      // this.loginModel.preffered_industry=response.currentjobTabdata[0].preffered_industry;
      // this.loginModel.size1=response.currentjobTabdata[0].size1;
      // this.loginModel.type_of_industry=response.currentjobTabdata[0].company_type;
      // this.loginModel.main_driver=response.currentjobTabdata[0].main_driver;
      
      // //interests/you fields data
      // this.loginModel.describe_friend=response.interestsTabdata[0].describe_friend;
      // this.loginModel.do_well=response.interestsTabdata[0].do_well;
      // this.loginModel.passion=response.interestsTabdata[0].passion;
      // this.loginModel.goals=response.interestsTabdata[0].goals;
      // this.loginModel.anything_else=response.interestsTabdata[0].anything_else;
      // this.loginModel.video_upload=response.interestsTabdata[0].video_upload;
      
      
    } else {
      //this.errMsg = true;
      //this.message = "No Data Found!!!";
    }
  })
}
//---------------------update you/firsttab data-----------------------
updateletgotabdatasave(){
  this.spinner.show();
  this.accountService.updateCandidateFirstTab(this.loginModel).then((response) => {
    if (response.code == 200) {
      this.spinner.hide();
        //this.router.navigate(['/admin/show_package']);
        Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B' , icon: 'success', title: 'Record successfully updated' })
    } else {
      //this.errmsg=true;
      this.message="Something Went Wrong";
    }
  })
}

//---------------------update qualification tab data-----------------------
updatedegreetabdatasave(){
  this.spinner.show();
  this.accountService.updateQualificationTab(this.loginModel).then((response) => {
    if (response.code == 200) {
      this.spinner.hide();
        //this.router.navigate(['/admin/show_package']);
        Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B' , icon: 'success', title: 'Record successfully updated' })
    } else {
      //this.errmsg=true;
      this.message="Something Went Wrong";
    }
  })
}

  
//---------------------update ideal job tab data-----------------------
updatecurrentTabdata(){
  this.spinner.show();
  this.accountService.updateCandidateIdealjob(this.loginModel).then((response) => {
    if (response.code == 200) {
      this.spinner.hide();
        //this.router.navigate(['/admin/show_package']);
        Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B' , icon: 'success', title: 'Record successfully updated ' })
    } else {
      //this.errmsg=true;
      this.message="Something Went Wrong";
    }
  })
}

//---------------------update interst/about you tab data-----------------------
updateinterestTabdata(){
  this.spinner.show();
  this.accountService.updateCandidateInterest(this.loginModel).then((response) => {
    if (response.code == 200) {
      this.spinner.hide();
        //this.router.navigate(['/admin/show_package']);
        Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B' , icon: 'success', title: 'Record successfully updated' })
    } else {
      //this.errmsg=true;
      this.message="Something Went Wrong";
    }
  })
}
dateChanged(event){
  console.log(event.value);
  const date = event.value;
  let now = moment(date).format('DD/MMM/YYYY hh:mm a');
  if(this.availabilityDate.length<=2){
    this.availabilityDate.push(now);
  }
}
}
