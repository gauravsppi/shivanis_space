import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { AccountService } from '../../services/account.service';
import { Headers, Http } from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import { Login_model } from '../../models/login_model';
import { Validation_model } from '../../models/Validation_model';
//import * as ClassicEditor from '../../@ckeditor/ckeditor5-build-classic';
import { AngularEditorModule } from '@kolkov/angular-editor';

@Component({
  selector: 'app-term-condition',
  templateUrl: './term-condition.component.html',
  styleUrls: ['./term-condition.component.scss']
})

export class TermConditionComponent implements OnInit {
  public loginModel:Login_model;
  public addValidationError:Login_model;
  private headers = new Headers();
  sucMsg:string=null;
  errMsg:string=null; 
  successMessage='Record updated successfully'; 
  errorMessage='Something went wrong'; 
  record:any=[];
  searchKey:'';
  public getId: any = [];
  total : number= 0;
  totalPages : number= 0;
  currentPage:number=1;
  page : number = 1;
  key: string = 'name'; //set default
  reverse: boolean = false;
  loaderShow: boolean = false;
  recordcheck:boolean = false;
  sortKey: 'desc';
  sortBy:string;
  constructor(private accountService:AccountService,private route: ActivatedRoute,private router: Router,private http: Http ) { }
  
  ngOnInit(): void {
    this.loginModel=<Login_model>{
    }
    this.route.params.subscribe(params => {
      this.getId = params['_id'];
      //  this.getUser()
    });
    this.addValidationError = <Login_model>{
      // c_password: '',
      // password:'',
    }
     this.isLoggedIn();
    this.get_contents_policy();
  }
  
  isLoggedIn() {
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    if(this.loginModel.admin_id == null){
    this.router.navigate(['/']);
    }
    }
  
    get_contents_policy(){
      this.loginModel.type='term';
      this.accountService.getContentdetails(this.loginModel).then((response)=>{
        console.log(response);
        if(response.code==200){
          this.loginModel.title=response.result[0].title;
          this.loginModel.content=response.result[0].content;
        }else{
          this.sucMsg = 'success';
          this.router.navigate(['/']);
        }
      }) 
    }

    update_policy(){
      this.loaderShow=true;
      var valid=true;
      this.addValidationError.title	='';
      this.addValidationError.content	='';
      if(this.loginModel.title==""){
        this.addValidationError.title='Please Enter Title Name';
        valid=false; 
      }
      if(this.loginModel.content==""){
        this.addValidationError.content='Please Enter Policy Content';
        valid=false; 
      }
      if(valid){ 
        console.log(this.loginModel);
        this.accountService.update_contents(this.loginModel).then((response)=>{
          if(response.code==200){
            this.sucMsg = 'success';
            setTimeout (() => { this.sucMsg = '';  }, 2000);
            this.get_contents_policy();
          }else{
            this.sucMsg = 'success';
            setTimeout (() => { this.sucMsg = '';  }, 2000);
            this.get_contents_policy();
          }
        }) 
      }
 }
}



