import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRecieverComponent } from './add-reciever.component';

describe('AddRecieverComponent', () => {
  let component: AddRecieverComponent;
  let fixture: ComponentFixture<AddRecieverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddRecieverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRecieverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
