import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListObservationSubSubCategoryComponent } from './list-observation-sub-sub-category.component';

describe('ListObservationSubSubCategoryComponent', () => {
  let component: ListObservationSubSubCategoryComponent;
  let fixture: ComponentFixture<ListObservationSubSubCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListObservationSubSubCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListObservationSubSubCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
