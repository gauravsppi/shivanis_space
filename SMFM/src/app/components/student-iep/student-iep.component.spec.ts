import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentIepComponent } from './student-iep.component';

describe('StudentIepComponent', () => {
  let component: StudentIepComponent;
  let fixture: ComponentFixture<StudentIepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentIepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentIepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
