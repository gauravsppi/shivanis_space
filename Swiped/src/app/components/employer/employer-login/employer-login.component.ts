import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from '../../../services/account.service';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import { candidateLogin_model } from '../../../models/candidateLogin_model';

@Component({
  selector: 'app-employer-login',
  templateUrl: './employer-login.component.html',
  styleUrls: ['./employer-login.component.scss']
})
export class EmployerLoginComponent implements OnInit {
  public loginModel: candidateLogin_model;
  public headers = new Headers();
  sucMsg: boolean = false;
  errMsg: boolean = false;
  marked: boolean = false;
  message: string;
  cookiecheck:any;
  buttonDisable:boolean=false;
  loginForm: FormGroup;
  submitted = false;
  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService,private formBuilder: FormBuilder) { 
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      checked: [''],
      role: ['employer']
    });
  }

  ngOnInit(): void {
    //this.loginForm.patchValue({email:'shivani@gmail.com'})(assign value)
    this.loginModel = <candidateLogin_model>{
      email: '',
      password: '',
      signuprole:''
    }
    localStorage.clear();
    this.checkcookies();
  }
  checkcookies(){
    this.cookiecheck=this.cookieService.getAll();
    if(this.cookiecheck==null){
      
    }else{
      this.loginForm.patchValue({email:this.cookiecheck.email})
    }
  }
  get f() { return this.loginForm.controls; }
    onSubmit() {
      this.submitted = true;
      // stop here if form is invalid
      if (this.loginForm.invalid) {
          return;
      }
      console.log('enter here with login form values',this.loginForm.value);
      this.loginModel.email=this.loginForm.value.email;
      this.loginModel.password=this.loginForm.value.password;
      this.loginModel.signuprole=this.loginForm.value.role;
      console.log('enter here with values',this.loginModel);
      this.accountService.LoginCandidate(this.loginModel).then((response) => {
        console.log('response here>>>',response.code);
        if (response.code == 200) {
          localStorage.setItem('token', response.data);
          if(response.result[0].screen_status==0){
            this.router.navigate(['/verify_email']); 
          }else if(response.result[0].screen_status==1){
            this.router.navigate(['/verify']); 
          }else if(response.result[0].screen_status==2 && response.result[0].status==1){
            localStorage.setItem('token', response.data);
            this.router.navigate(['/employer-profile/'+response.result[0]._id]); 
          }else{
            this.router.navigate(['/employer-dashboard']);
          }
        }else{

        }
      })
    }
}
