import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import {Admin} from '../../../models/admin.model';
import { AccountService } from '../../../services/account.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public adminModel: Admin;
  public addValidationError:Admin;
  public userRole:string;
  agencyShow:boolean;
  constructor(private accountService: AccountService,private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService) { }

  ngOnInit(): void {
    this.userRole=localStorage.getItem('role');
    if(this.userRole=='agency'){
      this.agencyShow=true;
    }else{
      this.agencyShow=false;
    }
  }
  logout(){
    localStorage.clear();
    this.router.navigate(['/login']);
  }
}
