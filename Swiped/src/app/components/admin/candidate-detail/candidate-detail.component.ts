import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from '../../../services/account.service';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import { admin_model } from '../../../models/admin_model';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2'

@Component({
  selector: 'app-candidate-detail',
  templateUrl: './candidate-detail.component.html',
  styleUrls: ['./candidate-detail.component.scss']
})
export class CandidateDetailComponent implements OnInit {

  public adminModel: admin_model;
  public addValidationError: admin_model;
  public headers = new Headers();
  errMsg:boolean=false;
  message:string;
  candidatelist:any ;
  
  letsgoTabdata:any [] ;
  degreeTabdata:any [] ;
  currentjobTabdata:any [] ;
  interestsTabdata:any [] ;
  availabilityDate= [] ;
  getId:any;
  sendmail:boolean=false;
  
  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService, private spinner:NgxSpinnerService) { }

  ngOnInit(): void {
   
     this.route.paramMap.subscribe(params => {
      this.getId = params.get('id');
      });

    this.adminModel = <admin_model>{
      id: '',
      availabilitytime:'',
      candidate_email:'',
    }
    this.addValidationError = <admin_model>{
      id: '',
      admin_password: ''
    }
    this.getCandidatedetails();
    
  }

  //-----------------------get candidate details API------------------------
  getCandidatedetails(){
    this.adminModel.id = this.getId;
    this.spinner.show();
    this.accountService.candidateDetailsWithID(this.adminModel).then((response) => {
      if (response.code == 200) {
        console.log('here is the response',response.result);
        this.spinner.hide();
        this.candidatelist=response.result;
        console.log('here is the status>>>',this.candidatelist.status);
        this.letsgoTabdata=response.letsgoTabdata;
        this.degreeTabdata=response.degreeTabdata;
        this.currentjobTabdata=response.currentjobTabdata;
        this.interestsTabdata=response.interestsTabdata;
        this.availabilityDate=this.interestsTabdata[0].availabilityTime;
        //this.availabilityDate=[];
        console.log('datacacha bsds',this.interestsTabdata[0].availabilityTime);
        
      } else {
        this.errMsg = true;
        this.message = "No Data Found!!!";
      }
    })
  }

//---------------approve candidate ----------
  approveCandidate(id,status){
    this.adminModel.id=id;
    this.adminModel.status=status;
    //debugger;
    Swal.fire({
      title: 'Are you sure you want to approve candidate?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, move it to approved!',
      cancelButtonText: 'No, keep it',   
      confirmButtonColor:'#D0021B',
      cancelButtonColor: '#D0021B',
    
    }).then((result) => {
      if (result.isConfirmed) {
          this.accountService.CandidateStatus(this.adminModel).then((response) => {
            //debugger;
              if (response.code == 200) {
                    this.getCandidatedetails();
                    Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B' , icon: 'success', title: 'Approved' })
              } else {
                  //this.errMsg = true;
                  //this.message = response.message;
                }
            })
      } else if (result.isDismissed) {
                        Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B', icon: 'success', title: 'Your data is same as before!' })
              }
      })
  }

  //---------------reject candidate ----------
  rejectCandidate(id,status){
    this.adminModel.id=id;
    this.adminModel.status=status;
    //debugger;
    Swal.fire({
      title: 'Are you sure you want to reject candidate?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, move it to rejected!',
      cancelButtonText: 'No, keep it',   
      confirmButtonColor:'#D0021B',
      cancelButtonColor: '#D0021B',
    
    }).then((result) => {
      if (result.isConfirmed) {
             // this.accountService.CandidateStatus(this.adminModel).then((response) => {
              //debugger;
                    //if (response.code == 200) {
                        this.accountService.Statusreject(this.adminModel).then((response) => {
                              if (response.code == 200) {
                              //this.sucMsg = true;
                                   Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B' , icon: 'success', title: 'Rejected! Successfully saved in rejected' })
                                  this.getCandidatedetails();
                              } else {
                                  //this.errMsg = true;
                                  //this.errmessage = 'Something went wrong';
                                }
                        })
                            // this.getCandidatedetails();
                            // Swal.fire('Rejected!');
          
                //     } else {
                //         //this.errMsg = true;
                //         //this.message = response.message;
                //       }
                // })
        } else if (result.isDismissed) {
              Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B', icon: 'success', title: 'Your data is same as before!' })
              }
        })
    }
    // send mail to candidate for call //
    sendMail(email){
      this.adminModel.candidate_email=email;
      console.log('datta here>>> ',this.adminModel);
      this.accountService.mailsendForVerify(this.adminModel).then((response) => {
        if (response.code == 200) {
          document.getElementById('closemodal').click();
        } else {

        }
      })
    }
    check(){
      this.sendmail=true;
        }
}

  

