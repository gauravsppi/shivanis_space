import { Component } from '@angular/core';
import { NgxSpinnerService } from "../app/shared/services/spinner.service";
import { Router, NavigationStart, NavigationCancel, NavigationEnd } from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'CareApp-dashboard';

  constructor(
    public spinner: NgxSpinnerService,
    private router: Router,

	) {
  }


  ngAfterViewInit() {
		this.router.events
			.subscribe((event) => {
				if (event instanceof NavigationStart) {
					this.spinner.show();
				}
				else if (event instanceof NavigationEnd || event instanceof NavigationCancel) {
					this.spinner.hide();
				}
			});
	}
}
