import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { UsersComponent } from './components/users/users.component';
import { ProfileComponent } from './components/profile/profile.component';
import { SchoolListingComponent } from './components/school-listing/school-listing.component';
import { TeacherListingComponent } from './components/teacher-listing/teacher-listing.component';
import { TeachersListComponent } from './components/teachers-list/teachers-list.component';
import { StudentListingComponent } from './components/student-listing/student-listing.component';
import { AbuseWordsComponent } from './components/abuse-words/abuse-words.component';
import { CategoryComponent } from './components/category/category.component';
import { LessonComponent } from './components/lesson/lesson.component';
import { AddLessonComponent } from './components/add-lesson/add-lesson.component';
import { EditLessonComponent } from './components/edit-lesson/edit-lesson.component';
import { ViewLessonComponent } from './components/view-lesson/view-lesson.component';
import { SelRecommendationsComponent } from './components/sel-recommendations/sel-recommendations.component';
import { TeacherViewComponent } from './components/teacher-view/teacher-view.component';
import { StudentViewComponent } from './components/student-view/student-view.component';
import { SchoolViewComponent } from './components/school-view/school-view.component';
import { ClassViewComponent } from './components/class-view/class-view.component';
import { StudentIEPComponent } from './components/student-iep/student-iep.component';
import { CoursesListComponent } from './components/courses-list/courses-list.component';
import { ClassListComponent } from './components/class-list/class-list.component';
import { AddAnnouncementComponent } from './components/add-announcement/add-announcement.component';
import { EditAnnouncementComponent } from './components/edit-announcement/edit-announcement.component';
import { ListAnnouncementComponent } from './components/list-announcement/list-announcement.component';
import { StaffListingComponent } from './components/staff-listing/staff-listing.component';
import { AddIepComponent } from './components/add-iep/add-iep.component';
import { EditContentComponent } from './components/edit-content/edit-content.component';
import { ListContentComponent } from './components/list-content/list-content.component';
import { AuthGuardService } from './services/authguard.service';
import { ListObservationCategoryComponent } from './components/list-observation-category/list-observation-category.component';
import { ListObservationSubCategoryComponent } from './components/list-observation-sub-category/list-observation-sub-category.component';
import { ListObservationSubSubCategoryComponent } from './components/list-observation-sub-sub-category/list-observation-sub-sub-category.component';
import { ListInterventionsComponent } from './components/list-interventions/list-interventions.component';
import {ListContactsInfoComponent } from './components/list-contacts-info/list-contacts-info.component';
import { ObservationsCategoryComponent } from './components/observations-category/observations-category.component';

const routes: Routes = [
  {path:"", component:LoginComponent},
  {path:"dashboard", component:DashboardComponent, canActivate:[AuthGuardService]},
  {path:"user", component:UsersComponent, canActivate:[AuthGuardService]},
  {path:"profile", component:ProfileComponent, canActivate:[AuthGuardService]},
  {path:"school-list", component:SchoolListingComponent, canActivate:[AuthGuardService]},
  {path:"teacher-list/:id", component:TeacherListingComponent, canActivate:[AuthGuardService]},
  { 
    path:"teachers-list", 
    component:TeachersListComponent, 
    canActivate:[AuthGuardService],
  },
  {path:"student-list", component:StudentListingComponent, canActivate:[AuthGuardService]},
  {path:"staff-list", component:StaffListingComponent, canActivate:[AuthGuardService]},
  {path:"abuse-words-list", component:AbuseWordsComponent, canActivate:[AuthGuardService] },
  {path:"category", component:CategoryComponent, canActivate:[AuthGuardService] },
  {path:"lesson/:id", component:LessonComponent, canActivate:[AuthGuardService] },
  {path:"add-lesson/:id", component:AddLessonComponent, canActivate:[AuthGuardService] },
  {path:"edit-lesson/:id", component:EditLessonComponent, canActivate:[AuthGuardService] },
  {path:"view-lesson/:id", component:ViewLessonComponent, canActivate:[AuthGuardService] },
  {path:"recommendations", component:SelRecommendationsComponent, canActivate:[AuthGuardService] },
  {path:"teacher-view/:id", component:TeacherViewComponent, canActivate:[AuthGuardService] },
  {path:"student-view/:id", component:StudentViewComponent, canActivate:[AuthGuardService] },
  {path:"school-view/:id", component:SchoolViewComponent, canActivate:[AuthGuardService] },
  {path:"class-view/:id", component:ClassViewComponent, canActivate:[AuthGuardService] },
  {path:"student-iep", component:StudentIEPComponent, canActivate:[AuthGuardService] },
  {path:"courses-list", component:CoursesListComponent, canActivate:[AuthGuardService] },
  {path:"class-list/:id", component:ClassListComponent, canActivate:[AuthGuardService] },
  {path:"class-list", component:ClassListComponent, canActivate:[AuthGuardService] },
  {path:"add-announcement", component:AddAnnouncementComponent, canActivate:[AuthGuardService] },
  {path:"edit-announcement/:id", component:EditAnnouncementComponent, canActivate:[AuthGuardService] },
  {path:"announcement-list", component:ListAnnouncementComponent, canActivate:[AuthGuardService] },
  {path:"add-iep", component:AddIepComponent, canActivate:[AuthGuardService] },
  {path:"edit-iep/:id", component:AddIepComponent, canActivate:[AuthGuardService] },
  {path:"edit-content/:id", component:EditContentComponent, canActivate:[AuthGuardService] },
  {path:"list-content", component:ListContentComponent, canActivate:[AuthGuardService] },
  {path:"antecedent-category", component:ListObservationCategoryComponent, canActivate:[AuthGuardService] },
  {path:"antecedent/:id", component:ListObservationSubCategoryComponent , canActivate:[AuthGuardService] },
  {path:"list-observation-sub-sub-category/:id", component:ListObservationSubSubCategoryComponent , canActivate:[AuthGuardService] },
  {path:"list-intervention", component:ListInterventionsComponent , canActivate:[AuthGuardService] },
  {path:"list-feedback", component:ListContactsInfoComponent , canActivate:[AuthGuardService] },
  {path:"observations-list", component:ObservationsCategoryComponent , canActivate:[AuthGuardService] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
