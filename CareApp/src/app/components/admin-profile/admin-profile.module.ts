import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { AdminProfileRoutingModule } from './admin-profile-routing.module';
import { AdminProfileComponent } from './admin-profile/admin-profile.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { UiSwitchModule } from 'ngx-ui-switch';
import { LazyLoadImageModule } from 'ng-lazyload-image';

@NgModule({
  imports: [
    CommonModule,
    AdminProfileRoutingModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    UiSwitchModule,
    LazyLoadImageModule
  ],
  declarations: [AdminProfileComponent]
})
export class AdminProfileModule { }