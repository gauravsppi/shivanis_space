import { Injectable, EventEmitter  } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageChangeService {

  imageChangeEmitter = new EventEmitter<String>(); 

  constructor() { }
}
