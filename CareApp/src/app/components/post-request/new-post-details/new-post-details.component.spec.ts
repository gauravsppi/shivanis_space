import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPostDetailsComponent } from './new-post-details.component';

describe('NewPostDetailsComponent', () => {
  let component: NewPostDetailsComponent;
  let fixture: ComponentFixture<NewPostDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPostDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPostDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
