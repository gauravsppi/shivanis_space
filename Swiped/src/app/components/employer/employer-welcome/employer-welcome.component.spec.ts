import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployerWelcomeComponent } from './employer-welcome.component';

describe('EmployerWelcomeComponent', () => {
  let component: EmployerWelcomeComponent;
  let fixture: ComponentFixture<EmployerWelcomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployerWelcomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployerWelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
