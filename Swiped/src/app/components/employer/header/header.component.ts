import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { employer_model } from '../../../models/employer_model';
import { Headers, Http } from '@angular/http';
import { DataService } from "../../../services/data.service";
import { AccountService } from '../../../services/account.service';
import { NgxSpinnerService } from "ngx-spinner";
import * as $ from 'jquery';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public employerModel: employer_model;
  public addValidationError: employer_model;
  public localvar = localStorage.getItem("token");
  login: boolean;
  logout: boolean;
  headerdata: boolean=true;
  errMsg:boolean=false;
  message:string; 
  employerlist:any=[];
  getId:any;
  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService, private spinner:NgxSpinnerService,private data:DataService) { 
    this.data.headerMessage.subscribe(messages => {
      this.headerdata = messages
    })
  }
 
  ngOnInit(): void {
    console.log('header console here>>>>',this.headerdata);
    this.route.paramMap.subscribe(params => {
      this.getId = params.get('id');
      console.log(this.getId);
      });
    if (this.localvar == null) {
      console.log(this.localvar);
      this.login = true;
    } else {
      console.log('entre here',this.localvar);
      this.logout = true;
    }
    this.employerModel = <employer_model>{
      id: ''
    }
    this.addValidationError = <employer_model>{
      id: '',
      
    }
    this.employerModel.id = this.getId;
  }
  logoutclick(){
    localStorage.clear();
    //this.cookieService.deleteAll();    
    this.router.navigate(['/employer-login']);
  }
  loginclick(){
    this.router.navigate(['/employer-login']);
  }
  ngAfterViewInit() {
    $(".open-notification").click(function () {
      $(".notfication-area").addClass("show-notification");
  });
    $(".cross").click(function () {
      $(".notfication-area").removeClass("show-notification");
  });
}
}
