import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AgencyService {

  constructor( private http: HttpClient) { }
   //*********** fetch care receiver list service ************************
  add_new_agency(formdata) {
    return this.http.post<any>(environment.apiBaseUrl + 'addAgency', formdata)
        .pipe(map((res: any) => {
            return res;
    }));
  }
  //*********** fetch agency list service ************************
  fetch_agency(paramsData) {
    return this.http.post<any>(environment.apiBaseUrl + 'getAgenciesData', paramsData)
        .pipe(map((res: any) => {
            return res;
        }));
  }
//*********** delete/active/deactive receiver service *************
delete_agency(id, status){
  return this.http.delete<any>(environment.apiBaseUrl+ 'deleteAgencyData/' + id + '/' + status)
      .pipe(map((res: any) => {
          return res;
      }));
}
} 
