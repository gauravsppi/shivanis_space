
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';


@Injectable({ providedIn: 'root' })
export class ReceiverService {
    constructor(
        private http: HttpClient
    ) {}

 

    //*********** fetch care receiver list service ************************
    fetch_care_receiver(paramsData) {
        return this.http.post<any>(environment.apiBaseUrl + 'fetchCareReceiverList', paramsData)
            .pipe(map((res: any) => {
                return res;
            }));
    } 

    //*********** delete/active/deactive receiver service *************
    delete_receiver(id, status){
        return this.http.delete<any>(environment.apiBaseUrl+ 'deleteReceiver/' + id + '/' + status)
            .pipe(map((res: any) => {
                return res;
            }));
    }


    //******************* Edit care receiver ********************
    edit_care_receiver(formData){
        return this.http.put<any>(environment.apiBaseUrl + 'updateCareReceiver/' + formData._id, formData)
            .pipe(map((res: any) => {
                return res;
            }));
    }

    //******************* Add Receiver ********************
    add_receiver(formdata) {
        return this.http.post<any>(environment.apiBaseUrl + 'addReceiver', formdata)
            .pipe(map((res: any) => {
                return res;
            }));
    }


    //******************* Fetch Receiver Details ********************
    fetch_receiver_details(formdata) {
        return this.http.post<any>(environment.apiBaseUrl + 'receiverDetails', formdata)
            .pipe(map((res: any) => {
                return res;
            }));
    }


     //*********** fetch post by receiver list service ************************
     fetch_post_by_receiver(paramsData) {
        return this.http.post<any>(environment.apiBaseUrl + 'fetchPostByReceiver', paramsData)
            .pipe(map((res: any) => {
                return res;
            }));
    }


     //*********** delete posts service *************
     delete_receiver_post(id, status){
        return this.http.delete<any>(environment.apiBaseUrl+ 'deleteReceiverPost/' + id + '/' + status)
            .pipe(map((res: any) => {
                return res;
            }));
    }



     //*********** fetch single post details service ************************
     fetch_single_post_details(paramsData) {
        return this.http.post<any>(environment.apiBaseUrl + 'fetchSinglePostDetails', paramsData)
            .pipe(map((res: any) => {
                return res;
            }));
    }


     //*********** fetch matching worker service ************************
     fetch_matching_workers(paramsData) {
        return this.http.post<any>(environment.apiBaseUrl + 'fetchMatchingWorkers', paramsData)
            .pipe(map((res: any) => {
                return res;
            }));
    }


    //*********** fetch direct assign worker service ************************
    fetch_direct_assign_workers(paramsData) {
        return this.http.post<any>(environment.apiBaseUrl + 'fetchDirectAssignWorkers', paramsData)
            .pipe(map((res: any) => {
                return res;
            }));
    }


    //*********** Assign matching worker service ************************
    assign_matching_workers(paramsData) {
        return this.http.post<any>(environment.apiBaseUrl + 'assignWorkers', paramsData)
            .pipe(map((res: any) => {
                return res;
            }));
    }



      //*********** fetch assigned worker service ************************
      fetch_assigned_workers(paramsData) {
        return this.http.post<any>(environment.apiBaseUrl + 'fetchAssignedMatchingWorkers', paramsData)
            .pipe(map((res: any) => {
                return res;
            }));
    }



    direct_assign_matching_workers(paramsData) {
        return this.http.post<any>(environment.apiBaseUrl + 'directAssignToWorkers', paramsData)
            .pipe(map((res: any) => {
                return res;
            }));
    }

    fetch_matching_complete_workers(paramsData) {
        return this.http.post<any>(environment.apiBaseUrl + 'fetchCompleteWorkers', paramsData)
            .pipe(map((res: any) => {
                return res;
            }));
    }    
}