// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  apiBaseUrl:"https://wholestudentlearning.com/",
  apiBasePath:"/nodeAPI_api/seeMeForMe/",
  imagePath:"https://wholestudentlearning.com/public/images/admin/",
  lessonVideoPath:"https://wholestudentlearning.com/uploads/lessons/",
  teacherImagePath:"https://wholestudentlearning.com/public/images/teachers/",
  adminImagePath:"https://wholestudentlearning.com/public/images/admin/",
  studentImagePath:"https://wholestudentlearning.com/public/images/students/",
  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
