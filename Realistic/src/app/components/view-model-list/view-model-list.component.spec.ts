import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewModelListComponent } from './view-model-list.component';

describe('ViewModelListComponent', () => {
  let component: ViewModelListComponent;
  let fixture: ComponentFixture<ViewModelListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewModelListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewModelListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
