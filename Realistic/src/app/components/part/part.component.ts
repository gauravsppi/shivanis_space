import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { AccountService } from '../../services/account.service';
import { Headers, Http } from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import { Login_model } from '../../models/login_model';
import { Validation_model } from '../../models/Validation_model';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-part',
  templateUrl: './part.component.html',
  styleUrls: ['./part.component.scss']
})

export class PartComponent implements OnInit {
  public loginModel:Login_model;
  public addValidationError:Login_model;
  private headers = new Headers();
  sucMsg: boolean = false;
  errMsg:boolean = false; 
  successMessage=''; 
  errorMessage=''; 
  record:any=[];
  searchKey:'';
  public getId: any = [];
  total : number= 0;
  totalPages : number= 0;
  currentPage:number=1;
  page : number = 1;
  key: string = '_id'; //set default
  reverse: boolean = false;
  loaderShow: boolean = false;
  recordcheck:boolean = false;
  sortKey: 'desc';
  sortBy:string; 
  records: [];
  car_name:string;
  link:string;
  partImagePath: string='';
  partImagePath1: string='';
  images_path:string;
  description:string;
  constructor(private accountService:AccountService,private route: ActivatedRoute,private router: Router,private http: Http ) { }

  ngOnInit(): void {
    this.loginModel=<Login_model>{
      admin_id:'',
      searchKey:'',
      sortKey:'_id',
      sortBy:'desc',
      car_name:'',
    }
    this.searchKey='';
    this.sortKey='desc';
    this.partImagePath = environment.partUrl;
    this.isLoggedIn();
    this.getAllParts();
  }

  isLoggedIn() {
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    if(this.loginModel.admin_id == null){
    this.router.navigate(['/']);
    }
  } 

  getAllParts(){
    this.loaderShow= true;
    this.loginModel.page=this.currentPage;
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    this.accountService.getParts(this.loginModel).then((response)=>{
      console.log('parts respone:: ', response);
      if(response.code==200){
        this.loaderShow = false;
        this.total = response.count;  
        this.images_path=environment.partUrl;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
        this.totalPages=Math.ceil( response.count/10);
        this.record = response.result;
        this.recordcheck=false;
      }else{
        this.record = [];
        this.total = 0;
        this.totalPages= 0;
       this.loaderShow = false;
       this.recordcheck=true;
      }
    })
  }


  delete(part_id,name){
    if(confirm("Are you sure to delete!")) {
      this.loaderShow = true;
      this.loginModel.part_id = part_id;
      this.loginModel.admin_id = localStorage.getItem('admin_id');;
      this.accountService.deleteParts(this.loginModel).then((response)=>{
      this.loaderShow = false;
        if(response.code==200){
          this.sucMsg = true;
          this.successMessage = response.message;
          this.getAllParts();
          setTimeout (() => {  this.sucMsg = false }, 1000);
        }else{
          this.errMsg = true;
          this.errorMessage = response.message;
          setTimeout (() => {  this.errMsg = false }, 1000);
        }
      })
    }
  }

  view(part_id){
    this.loaderShow = true;
    this.loginModel.part_id = part_id;
    this.loginModel.admin_id = localStorage.getItem('admin_id');;
    this.accountService.viewParts(this.loginModel).then((response)=>{
    this.loaderShow = false;
      if(response.code==200){
        this.records= response.result;
        console.log(this.records);
        this.car_name =   response.result[0].part_details.car_name;
        this.link =   response.result[0].part_details.link;
        this.description =   response.result[0].part_details.description;
        if(response.result[0].part_details.part_image=='n/a'){
          this.partImagePath1='null';
        }else{
          this.partImagePath1 =  environment.partUrl + response.result[0].part_details.part_image;
        }
        console.log(this.description);
      }else{
        this.records= [];
      }
    })
  }


  getSearching(currentPage){
    this.loaderShow= true;
    this.loginModel.searchKey= this.loginModel.searchKey;
    this.loginModel.page=1;
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    this.accountService.getParts(this.loginModel).then((response)=>{
      if(response.code==200){
        this.loaderShow = false;
        this.total = response.count;
        this.totalPages=Math.ceil( response.count/10);
       this.record = response.result;
       this.recordcheck=false;
      
      }else{
        this.record = []; this.total = 0;
        this.totalPages= 0;
        this.recordcheck=true;
       this.loaderShow = false;
      }
    })
  }

  pageChange(currentPage){
    this.currentPage=currentPage;
    if(this.loginModel.searchKey==""){
      this.getAllParts();
    }else{
      this.getAllParts();
    } 
  }

  counter(i: number) {
    return new Array(i);
  }

  sortFunction(sortKey){
    this.key = sortKey;
    this.loginModel.sortKey = sortKey;
    if(this.reverse === true){
    this.loginModel.sortBy = 'asc';
    }else{
    this.loginModel.sortBy = 'desc';
    }
    this.reverse = !this.reverse;
    if(this.loginModel.searchKey== ""){
    this.currentPage=1;
    this.getSearching(this.currentPage);
    }else{
    this.getSearching(this.currentPage);
    }
  }

  

}


