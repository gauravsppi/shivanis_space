import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from '../../../services/account.service';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import { admin_model } from '../../../models/admin_model';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2'

@Component({
  selector: 'app-employer-detail',
  templateUrl: './employer-detail.component.html',
  styleUrls: ['./employer-detail.component.scss']
})
export class EmployerDetailComponent implements OnInit {

  public adminModel: admin_model;
  public addValidationError: admin_model;
  public headers = new Headers();
  sucMsg:boolean=false;
  errMsg:boolean=false;
  message:string;
  employerlist:any ;
  getId:any;
  packesdetails:any=[];
  //campaigndetailstabdata:any=[] ;
  campaigndetailstabdata:any=[] ;
  skillsneedtabdata:any [] ;
  companyroledesctabdata:any [] ;
  campaignquestabdata:any [] ;

  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService, private spinner:NgxSpinnerService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.getId = params.get('id');
     
    });

    this.adminModel = <admin_model>{
      id: ''
    }
    this.addValidationError = <admin_model>{
      id: '',
      admin_password: ''
    }
    this.getEmployerdetailsWithID();
    this.getPackagedetails();
  }

  //--------------- Get All Packages Details API------------
  getPackagedetails(){
    //this.spinner.show();
    //debugger;
    this.accountService.getPlans().then((response) => {
      //debugger;
      if (response.code == 200) {
        this.packesdetails=response.result;
      } else {
        //this.errMsg = true;
        //this.message = response.message;
      }
    })
  }
  

  getEmployerdetailsWithID(){
    this.adminModel.id = this.getId;
    this.spinner.show();
    //debugger;
    this.accountService.getEmployerdetailsWithID(this.adminModel).then((response) => {
      //debugger;
    if (response.code == 200) {
      this.spinner.hide();
      this.employerlist=response.result;
      console.log('enter here with status>>>',this.employerlist)
      this.campaigndetailstabdata=response.campaigndetailstabdata;
      this.skillsneedtabdata=response.skillsneedtabdata;
      this.companyroledesctabdata=response.companyroledesctabdata;
      this.campaignquestabdata=response.campaignquestabdata;
    } else {
      //this.errMsg = true;
      //this.message = response.message; 
      }
    })
  }

//---------------approve candidate ----------
  approveEmployer(id,status){
    this.adminModel.id=id;
    this.adminModel.status=status;
    //debugger;
    Swal.fire({
          title: 'Are you sure you want to approve employer?',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, move it to approved!',
          cancelButtonText: 'No, keep it',   
          confirmButtonColor:'#D0021B',
          cancelButtonColor: '#D0021B',
        
        }).then((result) => {
          if (result.isConfirmed) {
              this.accountService.CandidateStatus(this.adminModel).then((response) => {
              //debugger;
              if (response.code == 200) {
                this.getEmployerdetailsWithID();
                Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B' , icon: 'success', title: 'Approved' })
              } else {
                  //this.errMsg = true;
                  //this.message = response.message;
               }
              })
            } else if (result.isDismissed) {
                Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B', icon: 'success', title: 'Your data is same as before!' })
             }
          })
    }


  //---------------reject candidate ----------
  rejectEmployer(id,status){
    this.adminModel.id=id;
    this.adminModel.status=status;
    //debugger;
    Swal.fire({
      title: 'Are you sure you want to reject employer?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, move it to rejected!',
      cancelButtonText: 'No, keep it',   
      confirmButtonColor:'#D0021B',
      cancelButtonColor: '#D0021B',
    
    }).then((result) => {
      if (result.isConfirmed) {
          //this.accountService.CandidateStatus(this.adminModel).then((response) => {
            //debugger;
              //if (response.code == 200) {
                  this.accountService.Statustrash(this.adminModel).then((response) => {
                      if (response.code == 200) {
                        //this.sucMsg = true;
                        Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B' , icon: 'success', title: 'Rejected! Successfully saved in rejected' })
                        this.getEmployerdetailsWithID();
                      } else {
                        //this.errMsg = true;
                        //this.errmessage = 'Something went wrong';
                      }
                  })
                      // this.getCandidatedetails();
                      // Swal.fire('Rejected!');
          
            //   } else {
            //       //this.errMsg = true;
            //       //this.message = response.message;
            //      }
            // })
        } else if (result.isDismissed) {
                Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B', icon: 'success', title: 'Your data is same as before!' })
          }
   })
  }
  sendMail(id){
    this.spinner.show();
    this.adminModel.id=id;
    this.accountService.planSendMail(this.adminModel).then((response) => {
      //debugger;
      if (response.code == 200) {
        this.spinner.hide();
        this.message = response.mailmessage;   
        this.sucMsg = true;                 
        setTimeout (() => { this.sucMsg = false;  }, 2000); 
      } else {
        this.message = response.mailmessage;   
        this.errMsg = true;                 
        setTimeout (() => { this.errMsg = false;  }, 2000); 
      }
    })
  }

   //---------------change package ----------

  changePackage(){
    this.router.navigate(['']);
  }

 //---------------to show post details-----------------

 viewPostDetails(id)
 {
   console.log(id);
  this.router.navigate(['/admin/view_post/'+id]);
 }

}
