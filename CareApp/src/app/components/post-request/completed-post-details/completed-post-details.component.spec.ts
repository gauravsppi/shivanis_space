import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletedPostDetailsComponent } from './completed-post-details.component';

describe('CompletedPostDetailsComponent', () => {
  let component: CompletedPostDetailsComponent;
  let fixture: ComponentFixture<CompletedPostDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletedPostDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletedPostDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
