import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { candidateLogin_model } from '../../../models/candidateLogin_model';
import { Headers, Http } from '@angular/http';

@Component({
  selector: 'app-candidate-email-verify',
  templateUrl: './candidate-email-verify.component.html',
  styleUrls: ['./candidate-email-verify.component.scss']
})
export class CandidateEmailVerifyComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService) { }

  ngOnInit(): void {
  }
  move(){
    localStorage.clear();
    //this.cookieService.deleteAll();    
    this.router.navigate(['/sign-in']);
  }
}
