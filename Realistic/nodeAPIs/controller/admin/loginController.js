const Login = require("../../model/admin/LoginModel")
const fs = require('fs'); 
var formidable = require('formidable');
var path = require('path');


module.exports = {
	/**** Add User *****/
	addAdmin: function(req, res) { //patient signup api
	 	var data=req.body;	 
	 	if(Object.keys(data).length > 0){ //Check if parameter passes or not
	 		console.log('data:', data);
		  	if(data.email != ''){ 
				Login.adminAdd(req.con, data, function(err, added) { //register patient data
					if (err) throw err;	
					if(added._id != ''){															
						res.send({
							"code": 200,									
							"status": 'success',
							"message":"Admin saved Successfully"
						});
					}else{
						res.send({
							"code": 400,
							"status": 'notInserted',
							"message":"Record Not Insert"
						});
					}
			    })				 
				
			}else{
				res.send({
					"code": 400,
					"status": 'notEmpty',
					"message":"Please fill all the required fileds"
				});
			}
		}else{
			res.send({
				"code": 400,
				"status": 'noParameter',
				"message":"Something went wrong"
			});
		}	
	},


	/**************** Get User Record *****************/
	login: function(req, res) {
		var data = req.body;
  		if(req.body.email != '' && req.body.password != ''){ 
			Login.login(req.con, req.body, function(err,result) {
				if (err){
					res.send({
						"code": 400,
						"status": 'error',
						"result": err,						
						"message":"Something went wrong."
					});
				}else{ 			 
					if(result.length > 0){					
						const setlogindetails = {
							'id': result[0].id
						}			
						res.send({
							"code": 200,
							"status": 'success',
							"result": setlogindetails,
							"message":"Login Successfully"
						});	
								
					}else{
						res.send({
							"code": 400,
							"status": 'notMatch',
							"message":"Email and password not match"
						});
					}		
				}//end else	
	    	})// end function
	    }else{
	    	res.send({
				"code": 400,
				"status": 'empty',				
				"message":"Please fill all the required fileds"
			});
	    }	
  	},
  	getProfile: function(req, res) {
  		if(req.body.admin_id != ''){ 
			Login.getInfo(req.con, req.body, function(err,result) {
				if (err){
					res.send({
						"code": 400,
						"status": 'error',
						"result": err,						
						"message":"Something went wrong"
					});
				}else{ 			
					if(result.length > 0){											
						res.send({
							"code": 200,
							"status": 'success',
							"result": result[0],
							"message":"Info sent Successfully"
						});					
					}else{
						res.send({
							"code": 400,
							"status": 'notExits',						
							"message": "ID not exits"
						});
					}
				}		
			})
	    }else{
	    	res.send({
				"code": 400,
				"status": 'empty',				
				"message":"Please fill all the required fileds"
			});
	    }		
    },
    updateProfileImage: function(req, res){
      	if(req.body.admin_id != '' && req.body.imgFile != ''){
      		var extension = req.body.imgType;
	    	var fileName = Date.now()+'.'+extension;     	    	
	        fs.writeFile("public/images/admin/"+fileName, req.body.imgFile, 'base64', function(err) {
	        	if(err) throw err;
	        	if(err){
	        		res.send({
						"code": 400,
						"error": err,
						"status": 'uploadError',				
						"message": "Error on file uploading"
					});
	        	}else{ 
	        		Login.updateProfileImage(req.con, fileName, req.body.admin_id, function(err, updated) {
	        			if(err) throw err;
	        			if(updated.ok > 0 || updated.nModified > 0 || updated.n > 0){ 		
							res.send({
								"code": 200,
								"status": 'success',
								"imageName": fileName,
								"message": "Profile image updated successfully"
							});					
						}else{
							res.send({
								"code": 400,
								"status": 'error',						
								"message":"Something went wrong"
							});
						}
	        	    })		
	        	}//end function
	        }) //file upload function end here
	    }else{
	    	res.send({
				"code": 400,
				"status": 'empty',				
				"message":"Please fill all the required fields"
			});
	    } 
    },
    updateProfile: function(req, res){
    	var data = req.body;
      	if(data.admin_id != '' && data.name != '' && data.email != '' && data.phone != ''){
      		if(data.imgFile != ''){ 
      			if(req.body.imgType != ''){
      				var extension = req.body.imgType;
      			}else{
      				var extension = 'jpg';
      			}	      		
		    	var fileName = Date.now()+'.'+extension;     	    	
		        fs.writeFile("public/uploads/images/admin/"+fileName, req.body.imgFile, 'base64', function(err) {
		        	if(err) throw err;
		        	if(err){
		        		res.send({
							"code": 400,
							"error": err,
							"status": 'uploadError',				
							"message": "Error on file uploading"
						});
		        	}else{ 
		        		data.image = fileName;
		        		Login.updateProfile(req.con, data, function(err,updated) {
			    			if(err) throw err;
			    			if(updated.ok > 0 || updated.nModified > 0 || updated.n > 0){ 		
			    				data = req.body;
			    				details = {
									'id': data.admin_id,
									'name': data.name,
									'email': data.email,
									'phone': data.phone,
									'image': data.image,
								}		
								res.send({
									"code": 200,
									"status": 'success',						
									"result": details,						
									"message": "Profile updated successfully"
								});					
							}else{
								res.send({
									"code": 400,
									"status": 'error',						
									"message":"Something went wrong"
								});
							}
			    	    })	
		        	}
		        })	
      		}else{
      			Login.updateProfile(req.con, data, function(err,updated) {
	    			if(err) throw err;
	    			if(updated.ok > 0 || updated.nModified > 0 || updated.n > 0){ 		
	    				data = req.body;
	    				details = {
							'id': data.admin_id,
							'name': data.name,
							'email': data.email,
							'phone': data.phone,
							'image': data.image,
						}		
						res.send({
							"code": 200,
							"status": 'success',						
							"result": details,						
							"message": "Profile updated successfully"
						});					
					}else{
						res.send({
							"code": 400,
							"status": 'error',						
							"message":"Something went wrong"
						});
					}
	    	    })	
      		}
      	}else{
	    	res.send({
				"code": 400,
				"status": 'empty',				
				"message":"Please fill all the required fields"
			});
	    } 
    },
    /***** Change Password ******/
    changePassword: function(req, res){
		var data = req.body;
		if (data.admin_id != '') {
			Login.checkExitsByID( req.con, data, function(err,result) {
				if(err){
					res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
				}else{
					if(result.length > 0){
						Login.changePassword( req.con, data, function(err, updated) {
							if(err){
								res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
							}else{
								if(updated.ok > 0 || updated.nModified > 0 || updated.n > 0){ 		
									res.send({"code": 200,"status": 'success',"message": "Password updated successfully" });	
								}else{
									res.send({"code": 400, "status": 'notUpdated', "message":"Something went wrong" });
								}	
							}
						})	
					}else{
						res.send({"code": 400, "status": 'notExits',"message": "ID does not exits" });	
					}
				}
			})
		}else{
			res.send({"code": 401,"status": 'notEmpty',"message":"Please fill all the required fileds" });
		}
	},	
	/***** Get all Users ******/
    getAllUsers: function(req, res){
		var data = req.body;
		if (data.admin_id != '') {
			Login.checkExitsByID( req.con, data, function(err,result) {
				if(err){
					res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
				}else{
					if(result.length > 0){
						Login.getAllUsers( req.con, data, function(err, result) {
							if(err){
								res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
							}else{
								if(result.length > 0){ 
									Login.getUsers( req.con, data, function(err, user_count) {
										res.send({"code": 200,"status": 'success', 'result':result,"count": user_count.length, "message": "Users fetch successfully" });
									})											
								}else{
									res.send({"code": 400, "status": 'notFound', "message":"Records not found" });
								}	
							}
						})	
					}else{
						res.send({"code": 400, "status": 'notExits',"message": "ID does not exits" });	
					}
				}
			})
		}else{
			res.send({"code": 401,"status": 'notEmpty',"message":"Please fill all the required fileds" });
		}
	},	
	/***** Get User Info ******/
    getUserInfo: function(req, res){
		var data = req.body;
		if (data.admin_id != '') {
			Login.checkExitsByID( req.con, data, function(err,result) {
				if(err){
					res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
				}else{
					if(result.length > 0){
						Login.getUserInfo( req.con, data, function(err, userInfo) {
							if(err){
								res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
							}else{
								if(userInfo.length > 0){ 
									res.send({"code": 200,"status": 'success', 'result':userInfo[0],"message": "User Info fetch successfully" });	
								}else{
									res.send({"code": 400, "status": 'notFound', "message":"Something went wrong" });
								}	
							}
						})	
					}else{
						res.send({"code": 400, "status": 'notExits',"message": "ID does not exits" });	
					}
				}
			})
		}else{
			res.send({"code": 401,"status": 'notEmpty',"message":"Please fill all the required fileds" });
		}
	},
	/***** Count all Users ******/
    countAllUsers: function(req, res){
		var data = req.body;
		if (data.admin_id != '') {
			Login.checkExitsByID( req.con, data, function(err,result) {
				if(err){
					res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
				}else{
					if(result.length > 0){
						Login.getUsers( req.con, data, function(err, result) {
							if(err){
								res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
							}else{
								if(result.length > 0){ 								
									res.send({"code": 200,"status": 'success', 'total_users':result.length, "message": "Users count successfully" });
								}else{
									res.send({"code": 400, "status": 'notFound', "message":"no records found" });
								}	
							}
						})	
					}else{
						res.send({"code": 400, "status": 'notExits',"message": "ID does not exits" });	
					}
				}
			})
		}else{
			res.send({"code": 401,"status": 'notEmpty',"message":"Please fill all the required fileds" });
		}
	},	
	/***** graph api of try parts ******/
    tryPartsStatics: function(req, res){
		var data = req.body;
		if (data.admin_id != '') {
			Login.checkExitsByID( req.con, data, function(err,result) {
				if(err){
					res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
				}else{
					if(result.length > 0){
						Login.getTryParts( req.con, data, function(err, parts) {
							console.log('Partss',parts);
							if(parts.length > 0){ 
								Login.tryPartsStatics( req.con, parts, function(err, result) {
									if(err){
										res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
									}else{
										if(result.length > 0){ 								
											res.send({"code": 200,"status": 'success', 'data':result, "message": "Info fetch successfully" });
										}else{
											res.send({"code": 400, "status": 'notFound', "message":"no records found" });
										}	
									}
								})	
							}else{
								res.send({"code": 400, "status": 'notFound', "message":"no records found" });
							}	
						})	
					}else{
						res.send({"code": 400, "status": 'notExits',"message": "ID does not exits" });	
					}
				}
			})
		}else{
			res.send({"code": 401,"status": 'notEmpty',"message":"Please fill all the required fileds" });
		}
	},	
	/***** graph api of try parts using car company and models ******/
    tryPartsWithCarStatics: function(req, res){
		var data = req.body; 
		console.log(data);
		if (data.admin_id != '') {
			Login.checkExitsByID( req.con, data, function(err,result) {
				if(err){
					res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
				}else{
					if(result.length > 0){
						Login.getCarDetails( req.con, data, function(err, parts) {
							if(parts.length > 0){ 
								Login.tryPartsWithCarStatics( req.con, parts, function(err, result) {
									if(err){
										res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
									}else{
										if(result.length > 0){ 								
											res.send({"code": 200,"status": 'success', 'data':result, "message": "Info fetch successfully" });
										}else{
											res.send({"code": 400, "status": 'notFound', "message":"no records found" });
										}	
									}
								})	
							}else{
								res.send({"code": 400, "status": 'notFound', "message":"no records found" });
							}	
						})	
					}else{
						res.send({"code": 400, "status": 'notExits',"message": "ID does not exits" });	
					}
				}
			})
		}else{
			res.send({"code": 401,"status": 'notEmpty',"message":"Please fill all the required fileds" });
		}
	},	
	/***** graph api of try parts using car company and models ******/
    getContactData: function(req, res){
		var data = req.body; 
		if (data.admin_id != '') {
			Login.checkExitsByID( req.con, data, function(err, result) {
				if(err){
					res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
				}else{
					if(result.length > 0){
						Login.getContactData( req.con, data, function(err, result) {
							if(err){
								res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
							}else{
								Login.countContactData( req.con, data, function(err, result_count) {
									if(result.length > 0){ 								
										res.send({"code": 200,"status": 'success', 'result':result,'count': result_count.length, "message": "Info fetch successfully" });
									}else{
										res.send({"code": 400, "status": 'notFound', "message":"no records found" });
									}
								})		
							}
						})								
					}else{
						res.send({"code": 400, "status": 'notExits',"message": "ID does not exits" });	
					}
				}
			})
		}else{
			res.send({"code": 401,"status": 'notEmpty',"message":"Please fill all the required fileds" });
		}
	},	
	/**** Get User car dashboard details *****/
	getUserCars: function(req, res) { 
	 	var data=req.body;	
		if(Object.keys(data).length > 0){ //Check if parameter passes or not
			if(data.user_id != ''){ 
				UsersModel.checkIDExits(req.con, data, function(err, exits) { //check email exists or not
			  		if (err) {
			  			res.send({"code": 400,"status": 'sqlError',"error": err, "message":"Something went wrong"	});
			  		}else{ 
				  		if(exits._id != '' ){
				  			LoginModel.getUserCars(req.con, data, function(err, result) { //check email exists or not
						  		console.log('data: ', result);
						  		if (err) throw err;
						  		if(result.length > 0 ){
						  			res.send({"code": 200,"result": result , "status": 'success', "message":"Records fetch Successfully" });
						  		}else{
						  			res.send({"code": 200,"status": 'notFound',"message":"Record not found"	});
						  		}
						  	})
				  		}else{
				  			res.send({ "code": 400,"status": 'notExits',"message":"User ID not exists" });
				  		}
				  	}	
		  		})
		  	}else{
		  		res.send({ "code": 400,"status": 'notEmpty',"message":"Please fill all the required fileds" });
		  	}	
	 	}else{
	 		res.send({ "code": 400,"status": 'noParameter',"message":"Something went wrong" });
	 	}
	}, 
	/**** Add Parts details *****/
	addParts: function(req, res) {
		console.log(res.body);
	 	var form = new formidable.IncomingForm();   
	    var mediaFolder = 'parts';
       	var extension = '';
	    form.parse(req, function(err, fields, files){
	    	var fileName = '';
	    	var data = fields;
		 	if(Object.keys(data).length > 0){ //Check if parameters passes or not
		 		console.log('data:', data);
		 		console.log('files:', files);
		 		if (data.admin_id != '') {
					Login.checkExitsByID( req.con, data, function(err, result) {
						if(err){
							res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
						}else{
							if(result.length > 0){
						 		var fileName = Date.now()+path.extname(files.part_image.name); 	           
					            var rawData = fs.readFileSync(files.part_image.path) 
					            data.part_image = fileName;
								fs.writeFile("public/uploads/"+mediaFolder+"/"+fileName, rawData, function(err) {
					                if(err){ 
					                    res_data = {'code': 400, 'message': 'something went wrong', status: 'error'}
					                 	res.send(res_data);
					                }else{  
					                	console.log('file uploaded successfully');
					                	data.part_image = fileName;
					                }
					            })  	         
							  	if(data.part_image != ''){ 
									Login.addParts(req.con, data, function(err, added) { //register patient data
										if (err) throw err;	
										if(added._id != ''){
											let data=added._id;

									Login.updateMultiCarModelStatus(req.con, data, function(err, result) {
											res.send({
												"code": 200,									
												"status": 'success',
												"message":"Parts saved Successfully"
											});
								    })
								}
								  })
								}else{
									res.send({
										"code": 400,
										"status": 'notEmpty',
										"message":"Please fill all the required fileds"
									});
								}
							}else{
								res.send({"code": 400, "status": 'notExits', "message": "ID does not exists" });
							}
						}
					})
				}		
			}else{
				res.send({
					"code": 400,
					"status": 'noParameter',
					"message":"Something went wrong"
				});
			}	
		})	
	},
	/**** Update Parts details *****/
	updateParts: function(req, res) {
	 	var form = new formidable.IncomingForm();   
	    var mediaFolder = 'parts';
       	var extension = '';
	    form.parse(req, function(err, fields, files){
	    	var fileName = '';
	    	var data = fields;
		 	if(Object.keys(data).length > 0){ //Check if parameters passes or not
		 		console.log('data:', data);
		 		console.log('files:', files);
		 		if (data.admin_id != '') {
					Login.checkExitsByID( req.con, data, function(err, result) {
						if(err){
							res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
						}else{
							if(result.length > 0){
								if(Object.keys(files).length > 0){ 
							 		var fileName = Date.now()+path.extname(files.part_image.name); 	           
						            var rawData = fs.readFileSync(files.part_image.path) 
						            data.part_image = fileName;
									fs.writeFile("public/uploads/"+mediaFolder+"/"+fileName, rawData, function(err) {
						                if(err){ 
						                    res_data = {'code': 400, 'message': 'something went wrong', status: 'error'}
						                 	res.send(res_data);
						                }else{  
						                	console.log('file uploaded successfully');
						                	data.part_image = fileName;
						                }
						            })  	
						        } else{
						        	data.part_image = data.part_img;
						        }           
							  	if(data.part_image != ''){ 
									Login.updateParts(req.con, data, function(err, added) { //register patient data
										if (err) throw err;	
									Login.update_edit_status(req.con, data, function(err, added) {
										if(added._id != ''){
											res.send({
												"code": 200,									
												"status": 'success',
												"message":"Parts saved Successfully"
											});
										}else{
											res.send({
												"code": 400,
												"status": 'notInserted',
												"message":"Something wet wrong"
											});
										}
									})			
									
								    })
								}else{
									res.send({
										"code": 400,
										"status": 'notEmpty',
										"message":"Please fill all the required fileds"
									});
								}
							}else{
								res.send({"code": 400, "status": 'notExits', "message": "ID does not exists" });
							}
						}
					})
				}		
			}else{
				res.send({
					"code": 400,
					"status": 'noParameter',
					"message":"Something went wrong"
				});
			}	
		})	
	},
	/**** Get parts  ******/
    getParts: function(req, res){
		var data = req.body; 
		console.log('get parts data::', data);
		if (data.admin_id != '') {
			Login.checkExitsByID( req.con, data, function(err, result) {
				if(err){
					res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
				}else{
					if(result.length > 0){
						Login.getParts( req.con, data, function(err, result) {
						  // console.log('part err;', err);
						  // console.log('part result;', result);
							if(err){
								res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
							}else{
								Login.countParts( req.con, data, function(err, result_count) {
									if(result.length > 0){ 								
										res.send({"code": 200,"status": 'success', 'result':result,'count': result_count.length, "message": "Info fetch successfully" });
									}else{
										res.send({"code": 400, "status": 'notFound', "message":"no records found" });
									}
								})		
							}
						})								
					}else{
						res.send({"code": 400, "status": 'notExits',"message": "ID does not exits" });	
					}
				}
			})
		}else{
			res.send({"code": 401,"status": 'notEmpty',"message":"Please fill all the required fileds" });
		}
	},	
	/**** Delete parts  ******/
   	deleteParts: function(req, res){
		var data = req.body; 
		if (data.admin_id != '') {
			Login.checkExitsByID( req.con, data, function(err, result) {
				if(err){
					res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
				}else{
					if(result.length > 0){
						Login.deleteParts( req.con, data, function(err, result) {
							if(err){
								res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
							}else{				
								if(result.n > 0 || result.ok > 0 || result.deletedCount > 0){ 								
									res.send({"code": 200,"status": 'success', "message": "Part deleted successfully" });
								}else{
									res.send({"code": 400, "status": 'notDelete', "message":"Something went wrong" });
								}								
							}
						})								
					}else{
						res.send({"code": 400, "status": 'notExits',"message": "ID does not exits" });	
					}
				}
			})
		}else{
			res.send({"code": 401,"status": 'notEmpty',"message":"Please fill all the required fileds" });
		}
	},	
	/**** View parts  ******/
   	viewParts: function(req, res){
		var data = req.body; 
		if (data.admin_id != '') {
			Login.checkExitsByID( req.con, data, function(err, result) {
				if(err){
					res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
				}else{
					if(result.length > 0){
						Login.viewParts( req.con, data, function(err, result) {
							console.log('Let Seee reslt',result);
							if(err){
								res.send({"code": 400, "error": err, "status": 'sqlError', "message":" Something went wrong" });
							}else{				
								if(result.length > 0){ 								
									res.send({"code": 200,"status": 'success', "result": result, "message": "Part info fetch successfully" });
								}else{
									res.send({"code": 400, "status": 'notFound', "message":"Something went wrong" });
								}								
							}
						})								
					}else{
						res.send({"code": 400, "status": 'notExits',"message": "ID does not exits" });	
					}
				}
			})
		}else{
			res.send({"code": 401,"status": 'notEmpty',"message":"Please fill all the required fileds" });
		}
	},	
}	