import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import {Admin} from '../../../models/admin.model';
import { AccountService } from '../../../services/account.service';
import { NgxSpinnerService } from "../../../shared/services/spinner.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
public adminModel: Admin;
public addValidationError: Admin;
validationemail: boolean = false;
validationpassword: boolean = false;
marked:boolean=false;
cookiecheck:any;
errMsg:boolean=false;
errorMsg:string;
  constructor(private accountService: AccountService,private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService, 
    private spinner: NgxSpinnerService,
    ) { }
  ngOnInit(): void {
    this.adminModel = <Admin>{
    email: '',
    password: ''
  }
  this.addValidationError = <Admin>{
    email: '',
    password: ''
  }
  this.checkcookies();
  }
  checkcookies(){
    this.cookiecheck=this.cookieService.getAll();
    if(this.cookiecheck==null){
      
    }else{
      this.adminModel.email=this.cookiecheck.email;
    }
  }
  toggleVisibility(e) {
    this.marked = e.target.checked;
    console.log(this.marked);
  }
  checkValidation(){
    var valid = true;
    this.addValidationError.email = '';
    this.addValidationError.password = '';
    console.log("Password>>>",this.adminModel.password);
    var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    if (!EMAIL_REGEXP.test(this.adminModel.email)) {
      this.validationemail = true;
      this.addValidationError.email='Please Enter Valid Email Id';
      valid = false;
    } else {
      this.validationemail = false;
    }
    if (this.adminModel.password == "" || this.adminModel.password == undefined) {
      this.validationpassword = true;
      this.addValidationError.password='Please Enter Password';
      valid = false;
    } else {
      this.validationpassword = false;
    }
    if(valid){
      if(this.marked==true){
        this.spinner.show();
        this.accountService.LoginAdmin(this.adminModel).then((response) => {
          //console.log(response.result[0].email);
        this.spinner.hide();

          if (response.code == 200) {
            console.log('dataaaaaaa>>>>',response);
          localStorage.setItem('token', response.token);
          localStorage.setItem('role', response.result[0].role);
          this.cookieService.set("email",response.result[0].email);
          if(response.result[0].role=='Subadmin'){
            this.router.navigate(['/agency/create-agency']);
          }else{
            this.router.navigate(['/dashboard']);
          }
          }else{
            this.errMsg=true;
            this.errorMsg=response.message;
            setTimeout (() => { this.errMsg=false; this.errorMsg = '';  }, 3000);
          }
        })
      }else{
        this.spinner.show();
        this.accountService.LoginAdmin(this.adminModel).then((response) => {
        this.spinner.hide();

          if (response.code == 200) {
            console.log('dataaaaaaa>>>>',response);
            localStorage.setItem('token', response.token);
            localStorage.setItem('role', response.result[0].role);
            this.cookiecheck=this.cookieService.getAll();
            if(this.cookiecheck==null){
              if(response.result[0].role=='Subadmin'){
                this.router.navigate(['/agency/create-agency']);
              }else{
                this.router.navigate(['/dashboard']);
              }
            }else{
              if(response.result[0].role=='Subadmin'){
                this.cookieService.deleteAll();
                this.router.navigate(['/agency/create-agency']);
              }else{
                this.cookieService.deleteAll();
                this.router.navigate(['/dashboard']);
              }
            }
          }else{
            this.errMsg=true;
            this.errorMsg=response.message;
            setTimeout (() => { this.errMsg=false; this.errorMsg = '';  }, 3000);
          }
        })
      }
      //console.log(this.adminModel);
    }
  }
}
