const mongoose = require('mongoose');
var Schema = mongoose.Schema;
var autoIncrement = require('mongoose-auto-increment');
var url = 'mongodb://127.0.0.1/carsDB';
var connection = mongoose.createConnection('mongodb://127.0.0.1/carsDB');
autoIncrement.initialize(connection);

var tryPartsSchema = new mongoose.Schema({
    image_name: {
        type: String     
    }, 
    partsSeqId: {
        type: String     
    },  
    created_at: {
        type: Date,
        default: Date.now
    },    
});


tryPartsSchema.set('toJSON', { virtuals: true });
tryPartsSchema.plugin(autoIncrement.plugin, {
    model: 'try_parts',
    field: 'partsSeqId',
    startAt: 1,
    incrementBy: 1
});

module.exports = tryPartsSchema;
