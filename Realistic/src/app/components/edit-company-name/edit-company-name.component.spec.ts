import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCompanyNameComponent } from './edit-company-name.component';

describe('EditCompanyNameComponent', () => {
  let component: EditCompanyNameComponent;
  let fixture: ComponentFixture<EditCompanyNameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCompanyNameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCompanyNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
