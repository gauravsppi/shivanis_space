
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';


@Injectable({ providedIn: 'root' })
export class WorkerService {
    constructor(
        private http: HttpClient
    ) {}

 
    //*********** fetch care worker list service ************************
    fetch_care_worker(paramsData) {
        return this.http.post<any>(environment.apiBaseUrl + 'fetchCareWorkerList', paramsData)
            .pipe(map((res: any) => {
                return res;
            }));
    } 


     //*********** delete/active/deactive worker service *************
     delete_worker(id, status){
        return this.http.delete<any>(environment.apiBaseUrl+ 'deleteWorker/' + id + '/' + status)
            .pipe(map((res: any) => {
                return res;
            }));
    }


    //******************* Edit care worker ********************
    edit_care_worker(formData){
        return this.http.put<any>(environment.apiBaseUrl + 'updateCareWorker/' + formData._id, formData)
            .pipe(map((res: any) => {
                return res;
            }));
    }
}