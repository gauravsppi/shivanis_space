import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostRequestRoutingModule } from './post-request-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

import { CompleteComponent } from './complete/complete.component';
import { InProgressComponent } from './in-progress/in-progress.component';
import { NewRequestComponent } from './new-request/new-request.component';
import { PendingComponent } from './pending/pending.component';
import { PendingPostDetailsComponent } from './pending-post-details/pending-post-details.component';
import { CompletedPostDetailsComponent } from './completed-post-details/completed-post-details.component';

import { NewPostDetailsComponent } from './new-post-details/new-post-details.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { LazyLoadImageModule } from 'ng-lazyload-image';



@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    PostRequestRoutingModule,
    NgxPaginationModule,
    LazyLoadImageModule
  ],
  declarations: [CompleteComponent, InProgressComponent, NewRequestComponent , PendingComponent, NewPostDetailsComponent, PendingPostDetailsComponent, CompletedPostDetailsComponent]
})
export class PostRequestModule { }