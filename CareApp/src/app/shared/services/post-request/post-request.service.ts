
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';


@Injectable({ providedIn: 'root' })
export class PostRequestService {
    constructor(
        private http: HttpClient
    ) {}

 
    //*********** Fetch list of pending request ************************
    fetch_pending_requests(paramsData) {
        return this.http.post<any>(environment.apiBaseUrl + 'fetchPendingRequests', paramsData)
            .pipe(map((res: any) => {
                return res;
            }));
    }


    //*********** Fetch list of new request ************************
    fetch_new_requests(paramsData) {
        return this.http.post<any>(environment.apiBaseUrl + 'fetchNewRequests', paramsData)
            .pipe(map((res: any) => {
                return res;
            }));
    }


    //*********** Fetch list of progress request ************************
    fetch_progress_requests(paramsData) {
        return this.http.post<any>(environment.apiBaseUrl + 'fetchProgressRequests', paramsData)
            .pipe(map((res: any) => {
                return res;
            }));
    }


    //*********** Fetch list of complete request ************************
    fetch_complete_requests(paramsData) {
        return this.http.post<any>(environment.apiBaseUrl + 'fetchCompleteRequests', paramsData)
            .pipe(map((res: any) => {
                return res;
            }));
    }


    //*********** Cancel post request ************************
    cancel_post_requests(paramsData) {
        return this.http.post<any>(environment.apiBaseUrl + 'cancelRequests', paramsData)
            .pipe(map((res: any) => {
                return res;
            }));
    }


     //*********** Complete post request ************************
     complete_post_requests(paramsData) {
        return this.http.post<any>(environment.apiBaseUrl + 'completeRequests', paramsData)
            .pipe(map((res: any) => {
                return res;
            }));
    }

}