import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddAgencyComponent } from './add-agency/add-agency.component'
import { ManageAgencyComponent } from './manage-agency/manage-agency.component';

const routes: Routes = [
  {
    path: 'create-agency',
    component: AddAgencyComponent
  },
  {
    path: 'manage-agency',
    component: ManageAgencyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgencyRoutingModule { }
