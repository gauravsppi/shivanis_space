import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from '../../../services/account.service';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import { candidateLogin_model } from '../../../models/candidateLogin_model';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2'

@Component({
  selector: 'app-account-created',
  templateUrl: './account-created.component.html',
  styleUrls: ['./account-created.component.scss']
})
export class AccountCreatedComponent implements OnInit {
  public loginModel: candidateLogin_model;
  public addValidationError: candidateLogin_model;
 
  public headers = new Headers();
  errMsg:boolean=false;
  message:string; 
  candidatelist:any=[] ;
 
  getId:any;

  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService, private spinner:NgxSpinnerService) { 
    
  }

  ngOnInit(): void {
  
    this.route.paramMap.subscribe(params => {

      
      this.getId = params.get('id');
      console.log(this.getId);
      });

    this.loginModel = <candidateLogin_model>{
      id: '',
      name:''
    }
    this.addValidationError = <candidateLogin_model>{
      id: '',
      name:''
      
    }
    this.getCandidatebyId();
  }

  //-----------------------get candidate details API------------------------
  getCandidatebyId(){
    this.loginModel.id = this.getId;
    this.spinner.show();
    debugger;
    this.accountService.getCandidateWithID(this.loginModel).then((response) => {
      debugger;
      if (response.code == 200) {
        //console.log(response);
        this.spinner.hide();
        this.candidatelist=response.result;                    
      } else {
        this.errMsg = true;
        this.message = "No Data Found!!!";
      }
    })
  }

  //--------------------view candidate dashboard--------------------------
  dashboardCandidate()
  {
    //this.loginModel.id = this.getId;
    this.router.navigate(['/candidate_dashboard/'+this.getId]);
  }

}
