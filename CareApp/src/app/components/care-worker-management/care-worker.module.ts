import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';
import { ReactiveFormsModule } from '@angular/forms';
import { UiSwitchModule } from 'ngx-ui-switch';
import { LazyLoadImageModule } from 'ng-lazyload-image';

import { CareWorkerRoutingModule } from './care-worker-routing.module';
import { AddWorkerComponent } from './add-worker/add-worker.component';
import { CareWorkerManagementComponent } from './care-worker/care-worker-management.component';

@NgModule({
  imports: [
    CommonModule,
    CareWorkerRoutingModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    UiSwitchModule,
    LazyLoadImageModule
  ],
  declarations: [CareWorkerManagementComponent, AddWorkerComponent]
})
export class CareWorkerModule { }