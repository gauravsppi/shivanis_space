export class class_model {    
   name: string;
   classCode: string;
   subjectCode: string;
   subjectName: string;
   periods: string;
   grades: string;
   classType: string;
   classRoom: string;
   phone: string;   
   email: string;
   admin_id: string;
   school_id: string;
   school_sourcedId: string;
   class_id: string;
   teacher_id: string;   
   class_sourcedId: string;   
   image: string;   
   page: number;
   searchKey: string;
   sortKey: string;
   sortBy: string;
   authToken: string;
   course_id: string;
}