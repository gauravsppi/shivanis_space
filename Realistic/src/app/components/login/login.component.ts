import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { AccountService } from '../../services/account.service';
import { Headers, Http } from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import { Login_model } from '../../models/login_model';
import { Validation_model } from '../../models/Validation_model';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public Login_model:Login_model;
  public Validation_model:Validation_model;
  constructor(private accountService:AccountService,private route: ActivatedRoute,private router: Router, private http: HttpClientModule) { }
  sucMsg:boolean=false;
  errMsg:string='';

  ngOnInit(): void {
    this.Login_model=<Login_model>{
      email: '',
      password:'',
      admin_id:'',
    }
    this.Validation_model = <Validation_model>{
      email: '',
      password:'',
    }
    
  }

  loginAdmin(){
    var valid=true;
    this.Validation_model.email='';
    this.Validation_model.password='';
    if(this.Login_model.email==""){
      this.Validation_model.email='Please Enter Email';
      valid=false; 
    }
    if(this.Login_model.password==""){
      this.Validation_model.password='Please Enter Password';
      valid=false; 
    }
    if(valid){
      this.accountService.adminLogin(this.Login_model).then((response)=>{
        
        console.log(response);
        if(response.code==200){
          this.sucMsg = true;
          localStorage.setItem('admin_id', response.result.id);               
          this.router.navigate(['/dashboard']);
        }else{
          this.errMsg = 'Invalid Credential';
          this.router.navigate(['/']);
        }
      })
    }
}
} 