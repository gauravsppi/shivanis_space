import { NgModule } from '@angular/core';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CandidateRoutingModule } from './candidate-routing.module';
import { HeaderComponent } from '../../components/candidate/header/header.component';
import { LoginComponent } from '../../components/candidate/login/login.component';
import { SignUpComponent } from '../../components/candidate/sign-up/sign-up.component';
import { EmailVerifyComponent } from '../../components/candidate/email-verify/email-verify.component';
import { AccountCreatedComponent } from '../../components/candidate/account-created/account-created.component';
import { CandidateEmailVerifyComponent } from '../../components/candidate/candidate-email-verify/candidate-email-verify.component';
import { CandidateDetailsComponent } from '../../components/candidate/candidate-details/candidate-details.component';
import { CandidateDashboardComponent } from '../../components/candidate/candidate-dashboard/candidate-dashboard.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
@NgModule({
  declarations: [
    HeaderComponent,
    LoginComponent,
    SignUpComponent,
    EmailVerifyComponent,
    AccountCreatedComponent,
    CandidateDetailsComponent,
    CandidateDashboardComponent,
    CandidateEmailVerifyComponent
  ],
  imports: [
    CommonModule,
    CandidateRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgxDropzoneModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    CarouselModule 
  ]
})
export class CandidateModule { }
