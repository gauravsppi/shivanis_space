import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListObservationCategoryComponent } from './list-observation-category.component';

describe('ListObservationCategoryComponent', () => {
  let component: ListObservationCategoryComponent;
  let fixture: ComponentFixture<ListObservationCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListObservationCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListObservationCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
