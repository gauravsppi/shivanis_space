
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class NgxSpinnerService {
    isLoading: boolean = true;
    timeoutIns : any;
    constructor() { }

    show() {
        setTimeout(() => {
            this.isLoading = true;
        }, 0);
    }

    hide() {
        setTimeout(() => {
            this.isLoading = false;
        }, 0);
    }
}