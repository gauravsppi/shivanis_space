import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RoutesRecognized, NavigationEnd} from '@angular/router';
import { Headers, Http } from '@angular/http';
import { filter, pairwise } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { AccountService } from '../../services/account.service';
import { class_model } from '../../models/class_model';
import { RoutingService } from '../../services/routing.service';

@Component({
  selector: 'app-class-view',
  templateUrl: './class-view.component.html',
  styleUrls: ['./class-view.component.scss']
})
export class ClassViewComponent implements OnInit {

  public classModel: class_model;
  public students = [];
  student_count = 0;
  public teachers = [];
  teacher_count = 0;
  noRecords: boolean = true;
  noRecordsTeachers: boolean = true;
  class_id = this.route.snapshot.params.id;
  periods = [];
  grades = [];
  subjects = [];
  subject_code = [];
  authToken='';

 
  previousUrl: string = '';
  currentUrl: string = '';
  arr = [];
  breadcrumb_teacher : boolean = false;
  breadcrumb_student : boolean = false;
  breadcrumb_class : boolean = false;
  class_Id : string = '';



  constructor(private router: Router, private accountService: AccountService, private route: ActivatedRoute, private http: Http, private routingService : RoutingService) {  }

  ngOnInit(): void {
   
    this.classModel = <class_model>{      
      teacher_id : '',
      class_id : '',
      school_id : '',
      name:'',
      email:'',
      phone:'',
      image:'',
      class_sourcedId:''
    }
    this.previousUrl = this.routingService.getPreviousUrl(); //get previous url
    this.currentUrl = this.routingService.getCurrentUrl(); //get current url
    
    console.log('get previuo URL :: ', this.previousUrl);
    console.log('get current URL :: ', this.currentUrl);
    if(this.previousUrl != undefined){ 
      this.arr = this.previousUrl.split('/');
      this.previousUrl = this.arr[1];
      this.class_Id = this.arr[2];
      if(this.previousUrl == 'teacher-view'){
        this.breadcrumb_teacher = true;
        this.breadcrumb_class = false;
        this.breadcrumb_student = false;
      }else if(this.previousUrl == 'student-view'){
        this.breadcrumb_teacher = false;
        this.breadcrumb_class = false;
        this.breadcrumb_student = true;
      }else{          
        this.breadcrumb_class = true;
      }   
    }else{
      this.breadcrumb_class = true;
    }  


    this.getClassesById(this.class_id);
  }  

  getClassesById(class_id){
    this.classModel.class_sourcedId =class_id;
    this.classModel.authToken = localStorage.getItem('authToken');
    this.classModel.admin_id = localStorage.getItem('admin_id');
    this.accountService.getClassInfo(this.classModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
         this.classModel.name = response.result[0].classes[0].className;
         this.classModel.classCode = response.result[0].classes[0].classCode;
         this.classModel.classType = response.result[0].classes[0].classType;
         this.classModel.classRoom = response.result[0].classes[0].location;
         this.classModel.class_sourcedId = response.result[0].classes[0].sourcedId;
         if(response.result[0].periods.length > 0 || response.result[0].periods != ''){
            for (var i = 0; i < response.result[0].periods.length; i++) {
              this.periods.push(response.result[0].periods[i].periods);
            }
            this.classModel.periods = this.periods.join(',');
         }
         if(response.result[0].grades.length > 0 || response.result[0].periods != ''){
          for (var i = 0; i < response.result[0].grades.length; i++) {
            this.grades.push(response.result[0].grades[i].grades);
          }
          this.classModel.grades = this.grades.join(',');
        }
        if(response.result[0].subjects.length > 0 || response.result[0].periods != ''){
          for (var i = 0; i < response.result[0].subjects.length; i++) {
            this.subjects.push(response.result[0].subjects[i].subject_name);
            this.subject_code.push(response.result[0].subjects[i].subject_code);
          }
          this.classModel.subjectName = this.subjects.join(',');
          this.classModel.subjectCode = this.subject_code.join(',');
        }
        if(this.classModel.class_sourcedId != ''){
          this.accountService.getStudentsByClassID(this.classModel).then((response) => {
              if(response.code == 200 && response.status == 'success'){ 
                this.students = response.students.users;
                this.student_count = response.students.users.length;
                this.noRecords = false;
              }
              if(response.code == 200 && response.status == 'notFound'){ 
                this.noRecords = false;
                this.students = [];
              }
          })
        } 
        if(this.classModel.class_sourcedId != ''){
          this.accountService.getTeachersByClassID(this.classModel).then((response) => {
              if(response.code == 200 && response.status == 'success'){ 
                this.teachers = response.teachers.users;
                this.teacher_count = response.teachers.users.length;
                this.noRecordsTeachers = false;
              }
              if(response.code == 200 && response.status == 'notFound'){ 
                this.noRecordsTeachers = false;
                this.teachers = [];
              }
          })
        } 
      }
    })  
  }

}
