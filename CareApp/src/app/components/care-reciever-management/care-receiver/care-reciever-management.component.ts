import { Component, OnInit, ViewChild } from '@angular/core';
import { NgxSpinnerService } from "../../../shared/services/spinner.service";
import { ReceiverService } from '../../../shared/services/receiver-service/receiver.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-care-reciever-management',
  templateUrl: './care-reciever-management.component.html',
  styleUrls: ['./care-reciever-management.component.scss']
})
export class CareRecieverManagementComponent implements OnInit {
  @ViewChild('closebutton') closebutton;
  editCareReceiverForm: FormGroup;
  submitted = false;
  enable: false
  receiverArray: any = [];
  page = 1;
  count : number;
  pageSize = 10;
  selectedIndex: number = 0;
  defaultImage = 'assets/images/loader.gif';
  receiverLanguage: any = [];

  receiverDetail = {
    name: '',
    email: '',
    mobileno: '',
    profileimage:'',
    gender:'',
    dob:'',
    customsandtraditions: {},
    hairandgroomings:{},
    healthanddetails: {},
    preferences: {},
    rateandavailabilities:{},
    religions:{},
    specialoccasionholidays: {}
  };

  constructor(
    private ReceiverService: ReceiverService,
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder, 


  ) {
    this.editCareReceiverForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      name: ['',[Validators.required]],
      phone: ['',[Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      _id: [''],
      profileimage:['']
    });
   }

  ngOnInit(): void {
    this.spinner.show();
    this.fetchRecords().toPromise().then((res) => {
      console.log(res)
      this.spinner.hide();
      if(res.code == 200){
        this.receiverArray = res.result
        this.count = res.numOfReceiver
      }else{
        // Swal.fire({ icon: 'error', title: 'Oops...', text: res.message })
      }
    })
    .catch(err=> { 
      this.spinner.hide();
      console.log(err) 
    });
  }

  get f() { return this.editCareReceiverForm.controls; }



  fetchRecords (){
    var Params = {
      page:  this.page, 
      pageSize: this.pageSize,
    }
    return this.ReceiverService.fetch_care_receiver(Params);
  }

  onTableDataChange(event){
    this.page = event;
    this.ngOnInit();
  }  


  editCareReceiver(receiver, index){
    this.selectedIndex =  index;
    console.log('receiver data',receiver);
    console.log('index data',index);
    this.editCareReceiverForm.patchValue({name: receiver.name, email: receiver.email, phone: receiver.mobileno, _id : receiver._id, profileimage: receiver.profileimage})
  }


  onSubmit() {
    console.log(this.editCareReceiverForm.value)
    this.submitted = true;

    // stop here if form is invalid
    if (this.editCareReceiverForm.invalid) {
        return;
    }

    this.spinner.show();
    this.ReceiverService.edit_care_receiver(this.editCareReceiverForm.value).subscribe((data: any) => {
      console.log('data++++++++',data)
      this.spinner.hide();
      if(data.status == 0){
        Swal.fire({ icon: 'error',  title: 'Oops...',  text: data.message })

      }else{
        this.closebutton.nativeElement.click();
        this.receiverArray[this.selectedIndex].name =  data.data.name;
        this.receiverArray[this.selectedIndex].email =  data.data.email;
        this.receiverArray[this.selectedIndex].mobileno =  data.data.phone;
        Swal.fire('Receiver!',  'Receiver has been updated.', 'success')

        // this.router.navigate(['dashboard'])
      }
      
    }, error => {
      this.spinner.hide();
      console.log('login data ----------',error)
    
    })
  }


  //******** Detele receiver function **************
  deleteReceiver(id, status): void {
    // this.page = 1;
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {

        this.spinner.show();
        this.ReceiverService.delete_receiver(id, status).subscribe((data: any) => {
          this.spinner.hide();
          console.log(data);
          if(data.status == 1){
            this.ngOnInit()
            var foundIndex = this.receiverArray.findIndex(x => x._id == id);
            this.receiverArray.splice(foundIndex , 1)
            Swal.fire( 'Deleted!', 'Receiver has been deleted.', 'success')
          }else{
            Swal.fire({  icon: 'error', title: 'Oops...', text: data.message })
          }
        }, error => {
          this.spinner.hide();
          console.log(error)
        })
      }
    })
  }


  ViewReceiver(receiver): void {
    console.log('receiver++++++', receiver)
    this.receiverDetail = receiver;
    this.receiverLanguage = receiver.languageandculturalcuisines ? receiver.languageandculturalcuisines.language : [];
  }

  onChangeEvent(reveiver){
    console.log('reveiver+++++++', reveiver);
    var reqParams = {
      _id : reveiver._id,
      status: reveiver.status == false ? 1 : 0
    }

    console.log(reqParams);

    this.spinner.show();
        this.ReceiverService.delete_receiver(reqParams._id, reqParams.status).subscribe((data: any) => {
          this.spinner.hide();
          console.log(data);
          if(reqParams.status == 1){
            Swal.fire( 'Receiver activated!', 'Receiver has been activated.', 'success')
          }else if(reqParams.status == 0){
            Swal.fire( 'Receiver dactivated!', 'Receiver has been dactivated.', 'success')

          }else{
            Swal.fire({  icon: 'error', title: 'Oops...', text: data.message })

          }
        }, error => {
          this.spinner.hide();
          console.log(error)
        })

  }

}
