import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { AccountService } from '../../services/account.service';
import { Headers, Http } from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import { Login_model } from '../../models/login_model';
import { Validation_model } from '../../models/Validation_model';

@Component({
  selector: 'app-view-company-list',
  templateUrl: './view-company-list.component.html',
  styleUrls: ['./view-company-list.component.scss']
})

export class ViewCompanyListComponent implements OnInit {
  public loginModel:Login_model;
  public addValidationError:Login_model;
  private headers = new Headers();
  sucMsg: boolean = false;
  errMsg:boolean = false; 
  successMessage='Record updated successfully'; 
  errorMessage='Something went wrong'; 
  record:any=[];
  searchKey:'';
  public getId: any = [];
  total : number= 0;
  totalPages : number= 0;
  currentPage:number=1;
  page : number = 1;
  key: string = 'name'; //set default
  reverse: boolean = false;
  loaderShow: boolean = false;
  recordcheck:boolean = false;
  sortKey: 'desc';
  sortBy:string; 
  constructor(private accountService:AccountService,private route: ActivatedRoute,private router: Router,private http: Http ) { }

  ngOnInit(): void {
    this.loginModel=<Login_model>{
      admin_id:'',
      searchKey:'',
      sortKey:'name',
      sortBy:'desc',
    }
    this.searchKey='';
    this.sortKey='desc';
    this.isLoggedIn();
    this.getAllCompany();
  }

  isLoggedIn() {
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    if(this.loginModel.admin_id == null){
    this.router.navigate(['/']);
    }
    } 

  getAllCompany(){
    this.loaderShow= true;
    this.loginModel.page=this.currentPage;
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    this.accountService.getCompany(this.loginModel).then((response)=>{
      console.log(response);
      if(response.code==200){
        this.loaderShow = false;
        this.total = response.count;
        this.totalPages=Math.ceil( response.count/10);
        this.record = response.result;
        this.recordcheck=false;
      }else{
        this.record = [];
        this.total = 0;
        this.totalPages= 0;
       this.loaderShow = false;
       this.recordcheck=true;
      }
    })
  }


  company_delete(company_id,name){
    if(confirm("Are you sure to delete!"+name)) {
      this.loaderShow = true;
     this.accountService.delete_company(company_id).then((response)=>{
      this.loaderShow = false;
        if(response.code==200){
          this.sucMsg = true;
                this.successMessage='Company Deleted Successfully';
                this.getAllCompany();
                setTimeout (() => {  this.sucMsg = false }, 1000);
        }else{
          this.errMsg = true;
          this.router.navigate(['/']);
        }
      })
    }
  }

  getSearching(currentPage){
    this.loaderShow= true;
    this.loginModel.searchKey= this.loginModel.searchKey;
    this.loginModel.page=1;
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    this.accountService.getCompany(this.loginModel).then((response)=>{
      if(response.code==200){
        this.loaderShow = false;
        this.total = response.count;
        this.totalPages=Math.ceil( response.count/10);
       this.record = response.result;
       this.recordcheck=false;
      
      }else{
        this.record = []; this.total = 0;
        this.totalPages= 0;
        this.recordcheck=true;
       this.loaderShow = false;
      }
    })
  }

  pageChange(currentPage){
    this.currentPage=currentPage;
    if(this.loginModel.searchKey==""){
      this.getAllCompany();
    }else{
      this.getAllCompany();
    } 
  }

  counter(i: number) {
    return new Array(i);
  }

  sortFunction(sortKey){
        this.key = sortKey;
        this.loginModel.sortKey = sortKey;
        if(this.reverse === true){
        this.loginModel.sortBy = 'asc';
        }else{
        this.loginModel.sortBy = 'desc';
        }
        this.reverse = !this.reverse;
        if(this.loginModel.searchKey== ""){
        this.currentPage=1;
        this.getSearching(this.currentPage);
        }else{
        this.getSearching(this.currentPage);
        }
  }

  

}

