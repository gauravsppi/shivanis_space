const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var contactSchema = new mongoose.Schema({
    user_id: {
        type: String     
    }, 
    subject: {
        type: String     
    }, 
    query: {
        type: String     
    }, 
    created_at: {
        type: Date,
        default: Date.now
    },    
});


contactSchema.virtual('user_details',{
    ref: 'users',
    localField: 'user_id',
    foreignField: '_id',
    justOne: true
});

contactSchema.set('toObject', { virtuals: true });
contactSchema.set('toJSON', { virtuals: true });

module.exports = contactSchema;
