import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from '../../../services/account.service';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import { candidateLogin_model } from '../../../models/candidateLogin_model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginModel: candidateLogin_model;
  public addValidationError: candidateLogin_model;
  public headers = new Headers();
  validationemail: boolean = false;
  validationpassword: boolean = false;
  ischecked: boolean = false;
  sucMsg: boolean = false;
  errMsg: boolean = false;
  marked: boolean = false;
  message: string;
  cookiedata: any[];
  cookiecheck:any;
  buttonDisable:boolean=false;
  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService) { }

  ngOnInit(): void {
    this.loginModel = <candidateLogin_model>{
      email: '',
      password: ''
    }
    this.addValidationError = <candidateLogin_model>{
      email: '',
      password: ''
    }
    localStorage.clear();
    this.checkcookies();
  }
  checkcookies(){
    this.cookiecheck=this.cookieService.getAll();
    if(this.cookiecheck==null){
      
    }else{
      this.loginModel.email=this.cookiecheck.email;
      this.loginModel.password=this.cookiecheck.password;
    }
  }
  toggleVisibility(e) {
    this.marked = e.target.checked;
  }
  checkvalidations() {
    var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    if (!EMAIL_REGEXP.test(this.loginModel.email)) {
      this.validationemail = true;
    } else {
      this.validationemail = false;
    }
    if (this.loginModel.password == "") {
      this.validationpassword = true;
    } else {
      this.validationpassword = false;
    }
  }
  logincandidate() {
    var valid = true;
    this.addValidationError.email = '';
    this.addValidationError.password = '';
    if (this.loginModel.email == "" || this.loginModel.email == undefined) {
      this.validationemail = true;
      valid = false;
    }
    if (this.loginModel.password == "" || this.loginModel.password == undefined) {
      this.validationpassword = true;
      valid = false;
    }
    if (valid) {
      if (this.marked == true) {
        this.accountService.LoginCandidate(this.loginModel).then((response) => {
          if (response.code == 200) {
            this.buttonDisable=true;
            localStorage.setItem('token', response.data);
            this.cookieService.set("email",response.email);
            this.cookieService.set("password",response.password);
            // -------- For Check Login Type Status ----------
            if(response.result[0].role =="candidate"){
              if(response.result[0].account_status == 0){
                localStorage.clear();
                this.router.navigate(['/verify_mail']); 
              }else{
              if (response.result[0].status == 0 && response.result[0].login_type_status == 4) {
                this.router.navigate(['/email_verify']); 
              }else if(response.result[0].login_type_status == 0 && response.result[0].status == 0){
                this.router.navigate(['/candidate_details']);
              } else if(response.result[0].login_type_status == 1 || response.result[0].login_type_status == 2 || response.result[0].login_type_status == 3 ){
                this.router.navigate(['/candidate_details']);
              }else if(response.result[0].login_type_status == 4 && response.result[0].status == 1){
                this.router.navigate(['/account_created/'+response.result[0]._id]);
              }else{
                //this.router.navigate(['/candidate_dashboard']);
              }
            }
            }else{
              if (response.result[0].status == 0 && response.result[0].package_name == '') {
                this.router.navigate(['/choose_plan']);
                }else if(response.result[0].login_type_status == 0 && response.result[0].status == 0){
                  this.router.navigate(['/verify_email']);
                }else if(response.result[0].login_type_status == 0 && response.result[0].status == 1){
                  this.router.navigate(['/campaign_details']);
                }else if(response.result[0].login_type_status == 4 && response.result[0].status == 1 && response.result[0].post_type_status == 0){
                  this.router.navigate(['/employer-dashboard']);
                }else if(response.result[0].login_type_status == 1 || response.result[0].login_type_status == 2 || response.result[0].login_type_status == 3  ){
                  this.router.navigate(['/campaign_details']);
                }else if(response.result[0].login_type_status == 4 && response.result[0].status == 1){
                  this.router.navigate(['/employer-dashboard']);
                }else{
                  
                }
            }
            } else {
              this.errMsg = true;
              this.message = response.message;
            }
          })
      }else{
          this.accountService.LoginCandidate(this.loginModel).then((response) => {
            if (response.code == 200) {
              this.buttonDisable=true;
              localStorage.setItem('token', response.data);
              this.cookiecheck=this.cookieService.getAll();
              if(this.cookiecheck==null){
                // -------- For Check Login Type Status ----------
                if(response.result[0].role =="candidate"){
                  if(response.result[0].account_status == 0){
                    localStorage.clear();
                    this.router.navigate(['/verify_mail']); 
                  }else{
                if (response.result[0].status == 0 && response.result[0].login_type_status == 4) {
                  this.router.navigate(['/email_verify']);
                }else if(response.result[0].login_type_status == 0 && response.result[0].status == 0){
                  this.router.navigate(['/candidate_details']);
                } else if(response.result[0].login_type_status == 1 || response.result[0].login_type_status == 2 || response.result[0].login_type_status == 3 ){
                  this.router.navigate(['/candidate_details']);
                }else if(response.result[0].login_type_status == 4 && response.result[0].status == 1){
                  this.router.navigate(['/account_created/'+response.result[0]._id]);
                }else{
                  //this.router.navigate(['/candidate_dashboard']);
                }
              }
              }else{
                if (response.result[0].status == 0 && response.result[0].package_name == '') {
                  this.router.navigate(['/choose_plan']);
                  }else if(response.result[0].login_type_status == 0 && response.result[0].status == 0){
                    this.router.navigate(['/verify_email']);
                  }else if(response.result[0].login_type_status == 0 && response.result[0].status == 1){
                    this.router.navigate(['/campaign_details']);
                  }else if(response.result[0].login_type_status == 4 && response.result[0].status == 1 && response.result[0].post_type_status == 0){
                    this.router.navigate(['/employer-dashboard']);
                  }else if(response.result[0].login_type_status == 1 || response.result[0].login_type_status == 2 || response.result[0].login_type_status == 3  ){
                    this.router.navigate(['/campaign_details']);
                  }else if(response.result[0].login_type_status == 4 && response.result[0].status == 1){
                    this.router.navigate(['/employer-dashboard']);
                  }else{
                    
                  }
              }
              }else{
                this.cookieService.deleteAll();
                // -------- For Check Login Type Status ----------
                if(response.result[0].role =="candidate"){
                  if(response.result[0].account_status == 0){
                    localStorage.clear();
                    this.router.navigate(['/verify_mail']); 
                  }else{
                if (response.result[0].status == 0 && response.result[0].login_type_status == 4) { 
                  this.router.navigate(['/email_verify']);
                }else if(response.result[0].login_type_status == 0 && response.result[0].status == 0){
                  this.router.navigate(['/candidate_details']);
                } else if(response.result[0].login_type_status == 1 || response.result[0].login_type_status == 2 || response.result[0].login_type_status == 3  ){
                  this.router.navigate(['/candidate_details']);
                }else if(response.result[0].login_type_status == 4 && response.result[0].status == 1){
                  this.router.navigate(['/account_created/'+response.result[0]._id]);
                }else{
                  //this.router.navigate(['/candidate_dashboard']);
                }
              }
              }else{
                if (response.result[0].status == 0 && response.result[0].package_name == '') {
                this.router.navigate(['/choose_plan']);
                }else if(response.result[0].login_type_status == 0 && response.result[0].status == 0){
                  this.router.navigate(['/verify_email']);
                }else if(response.result[0].login_type_status == 0 && response.result[0].status == 1){
                  this.router.navigate(['/campaign_details']);
                }else if(response.result[0].login_type_status == 4 && response.result[0].status == 1 && response.result[0].post_type_status == 0){
                  this.router.navigate(['/employer-dashboard']);
                }else if(response.result[0].login_type_status == 1 || response.result[0].login_type_status == 2 || response.result[0].login_type_status == 3  ){
                  this.router.navigate(['/campaign_details']);
                }else if(response.result[0].login_type_status == 4 && response.result[0].status == 1){
                  this.router.navigate(['/employer-dashboard']);
                }else{
  
                }
              }
              }
            }else{
              this.errMsg = true;
              this.message = response.message;
            }
          })
        }
      }
    }
  }
  