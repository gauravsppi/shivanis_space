var MongoClient = require('mongoose');
var url = 'mongodb://127.0.0.1/carsDB';

require("dotenv").config();
/******* Mongo DB Connection********/
var _db = '';
module.exports = {
	connectToServer: function( callback ) {
		MongoClient.connect( url,  {  useUnifiedTopology: true,  useNewUrlParser: true }, function( err, client ) {
		  return callback( err );
		} );
	},
	getDb: function() {
		return _db;
	}
};


//Import the mongoose module
// var mongoose = require('mongoose');

// //Set up default mongoose connection
// var mongoDB = 'mongodb://127.0.0.1/my_database';
// mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});

// //Get the default connection
// var db = mongoose.connection;

// //Bind connection to error event (to get notification of connection errors)
// db.on('error', console.error.bind(console, 'MongoDB connection error:'));