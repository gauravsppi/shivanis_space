import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "../../../shared/services/spinner.service";
import { PostRequestService } from '../../../shared/services/post-request/post-request.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-new-request',
  templateUrl: './new-request.component.html',
  styleUrls: ['./new-request.component.scss']
})
export class NewRequestComponent implements OnInit {
  newRequestArray: any = [];
  page = 1;
  count : number;
  pageSize = 10;
  constructor(
    private PostRequestService: PostRequestService,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.spinner.show();
    this.fetchRecords().toPromise().then((res) => {
      console.log('res++++++++++++',res);
      this.spinner.hide();
      if(res.status == 1){
        this.newRequestArray = res.data
        this.count = res.numOfNewRequest
      }else{
        Swal.fire({ icon: 'error', title: 'Oops...', text: res.message })
      }
    })
    .catch(err=> { 
      this.spinner.hide();
      console.log(err) 
    });
  }

  fetchRecords () {
    var Params = {
      page:  this.page, 
      pageSize: this.pageSize,
    }
    return this.PostRequestService.fetch_new_requests(Params);
  }


  onTableDataChange(event){
    this.page = event;
    this.ngOnInit();
  }

}
