import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from '../../../services/account.service';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import { admin_model } from '../../../models/admin_model';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-package',
  templateUrl: './edit-package.component.html',
  styleUrls: ['./edit-package.component.scss']
})
export class EditPackageComponent implements OnInit {
  public adminModel: admin_model;
  public addValidationError: admin_model;
  public headers = new Headers();
  candidatelist:any = [];
  getId:string;
  message:string;
  errmsg:boolean=false;

  validationplanamount: boolean = false;
  validationsuccessfee: boolean = false;
  plansname = [
    'Standard',
    'Premium',
    'Customised'
  ];
  plantime = [
    'One Time',
    'Monthly',
    'Annually'
    ];
  url: string;
  mainheading: string;
  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService,private spinner: NgxSpinnerService) { }
  ngOnInit(): void {
    this.adminModel = <admin_model>{
      plan_name:[],
      plan_time:[],
      fee:'',
      plan_description:'',
      plan_amount:'',
      success_fee:'',
    }
    this.addValidationError = <admin_model>{
      plan_name:'',
      plan_time:'',
      fee:'',
      plan_description:''
    }
    this.route.paramMap.subscribe(params => {
      this.getId = params.get('id');
      
    });
    //console.log(this.getId);
    this.url = this.router.url;
    //alert(this.url)
    if (this.url =="/admin/edit_package" ) {
      this.mainheading="Add Package";
      }else {
        this.mainheading="Edit Package";
        }
    this.getsingleRecord();
  }
  elementclick($event){
    this.adminModel.fee=$event.target.value;
  }
 
  keyPress(event: any) {
    debugger;
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
    event.preventDefault();
    
    }
    }
  
  //--------------------- Add new Package API---------------------------------
  Addpackage(){
    console.log(this.adminModel);    
    this.accountService.Plansdetailadd(this.adminModel).then((response) => {
      if (response.code == 200) {
        //Swal.fire('');
          this.router.navigate(['/admin/show_package']);
      } else {
        this.errmsg=true;
        this.message=response.message;
      }
    })
  }

  //--------------------- View Single package details API---------------------------------
  getsingleRecord(){
    this.adminModel.id=this.getId;
    debugger;
    this.accountService.singlePackage(this.adminModel).then((response) => {
      debugger;
      if (response.code == 200) {
          console.log(response.result.plan_name);
          this.adminModel.plan_name=response.result.plan_name;
          this.adminModel.plan_amount=response.result.plan_amount;
          this.adminModel.plan_description=response.result.plan_description;
          this.adminModel.fee=response.result.access_fee;
          this.adminModel.success_fee=response.result.success_fee;
          this.adminModel.plan_time=response.result.plan_time;
      } else {
      }
    })
  }

  //--------------------- edit/update package details API---------------------------------
  editpackage(){
    //this.spinner.show();
    this.accountService.updatePlan(this.adminModel).then((response) => {
      debugger;
      if (response.code == 200) {
        this.spinner.hide();
          this.router.navigate(['/admin/show_package']);
      } else {
        this.errmsg=true;
        this.message="Something Went Wrong";
      }
    })
  }
}
