const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userCarsSchema = new mongoose.Schema({
    user_id: {
        type: String
    }, 
    company_id: {
        type: String     
    },  
    model_id: {
        type: String
    },  
    color: {
        type: String,                           
    },  
    year: {
        type: String,       
    }, 
    description: {
        type: String,       
    },
    images: [],   
    created_at: {
        type: Date,
        default: Date.now
    },    
});

userCarsSchema.virtual('user_details',{
    ref: 'users',
    localField: 'user_id',
    foreignField: '_id',
    justOne: true
});
userCarsSchema.virtual('company_details',{
    ref: 'car_company',
    localField: 'company_id',
    foreignField: '_id',
    justOne: true
});
userCarsSchema.virtual('models_details',{
    ref: 'car_models',
    localField: 'model_id',
    foreignField: '_id',
    justOne: true
});


userCarsSchema.set('toObject', { virtuals: true });
userCarsSchema.set('toJSON', { virtuals: true });


module.exports = userCarsSchema;
