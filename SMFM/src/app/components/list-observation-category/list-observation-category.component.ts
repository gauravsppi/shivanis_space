import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Headers, Http } from '@angular/http';
import Swal  from 'sweetalert2';
import  * as $ from 'jquery';
declare const modalOpen_add: any;
declare const modalOpen_edit: any;
import { AccountService } from '../../services/account.service';
import { school_model } from '../../models/school_model';

@Component({
  selector: 'app-list-observation-category',
  templateUrl: './list-observation-category.component.html',
  styleUrls: ['./list-observation-category.component.scss']
})
export class ListObservationCategoryComponent implements OnInit {

  public schoolModel: school_model;
  public addValidationError: school_model;
  sucMsg: boolean =  false; 
  successMessage: string='';
  errMsg: boolean = false; 
  errorMessage: string=''; 
  noRecords: boolean = false;

  total : number= 0;
  totalPages : number= 0;
  currentPage:number=1;
  page : number =1;
  key: string = 'category_name'; //set default
  reverse: boolean = false; 
  loaderShow: boolean = false; 

  checkStatus1: boolean = false; 
  checkStatus0: boolean = false; 

  ObCategories = [];

  constructor(private router: Router, private accountService: AccountService, private route: ActivatedRoute, private http: Http) { }


  ngOnInit(): void {
    this.schoolModel = <school_model>{
      user_id: '',
      category_name: '',
      hint: '',      
      obCat_id: '',      
      status: '1' ,
      searchKey: '',
      sortKey: 'id',
      sortBy: 'desc', 
    }
    this.addValidationError = <school_model>{
      user_id: '',
      category_name: '',
      hint: '',      
      obCat_id: '',      
      status: '' ,  
    }
    this.getObCategories();
    modalOpen_add();
    modalOpen_edit();
  }

  add_btn(){
    this.schoolModel.category_name = '';
    this.schoolModel.hint = '';
  }
  update_btn(category_name, hint, status, id){
    this.schoolModel.category_name = category_name;
    this.schoolModel.hint = hint;
    this.schoolModel.status =status;
    this.schoolModel.obCat_id = id;
    if(status == 1){ this.checkStatus1 = true }else{ this.checkStatus0 = true }   
  }

  updateFunction(){
    var valid = true;
    this.addValidationError.category_name = '';
    if (this.schoolModel.category_name.trim() == "") {
        this.addValidationError.category_name = 'Please enter category name';
        valid = false;
    }
    if (valid) {  
      this.schoolModel.user_id = localStorage.getItem('admin_id');
      this.schoolModel.user_type = 'admin';    
      this.schoolModel.type = 'edit';    
      this.schoolModel.category_name = this.schoolModel.category_name.trim();      
      this.accountService.saveOBCategory(this.schoolModel).then((response) => {  
        if (response.code == 200 && response.status == 'success') {        
          this.getObCategories();  
          this.sucMsg = true;
          this.successMessage = response.message;
          setTimeout (() => { this.sucMsg = false;  }, 3000); 
        } else {           
          this.errMsg = true;
          this.errorMessage = response.message;
          setTimeout (() => { this.errMsg = false;  }, 3000); 
        }         
        modalOpen_edit();
      })
    }
  }

  saveFunction(){
    var valid = true;
    this.addValidationError.category_name = '';
    if (this.schoolModel.category_name.trim() == "") {
        this.addValidationError.category_name = 'Please enter category name';
        valid = false;
    }
    if (valid) {  
      this.schoolModel.user_id = localStorage.getItem('admin_id');
      this.schoolModel.user_type = 'admin';    
      this.schoolModel.type = 'add';    
      this.schoolModel.category_name = this.schoolModel.category_name.trim();
      this.accountService.saveOBCategory(this.schoolModel).then((response) => {  
        if (response.code == 200 && response.status == 'success') {        
          this.getObCategories();  
          this.sucMsg = true;
          this.successMessage = response.message;
          setTimeout (() => { this.sucMsg = false;  }, 3000); 
        } else {           
          this.errMsg = true;
          this.errorMessage = response.message;
          setTimeout (() => { this.errMsg = false;  }, 3000); 
        }         
        modalOpen_add();
      })
    }
  }

  getObCategories() {
    this.loaderShow = true;
    this.schoolModel.page= this.currentPage;
    this.schoolModel.user_id = localStorage.getItem('admin_id');
    this.schoolModel.user_type = 'admin';
    this.accountService.getOBCategory(this.schoolModel).then((response) => {
      console.log('OBcat data:', response)
      if (response.code == 200 && response.status == 'success') {
        this.ObCategories = response.result; 
        this.total = response.count;
        this.totalPages=Math.ceil( response.count/10);
        this.noRecords = false;
      }else{
        this.noRecords = true;
        this.ObCategories = []; 
        this.total = 0;
        this.totalPages=0;
      }
      this.loaderShow = false;
    })
  }

  pageChange(currentPage){
    this.currentPage=currentPage;
    if(this.schoolModel.searchKey==""){
      this.getObCategories();
    }else{
      this.searchFunction(this.currentPage);
    } 
  }

  counter(i: number) {
    return new Array(i);
  }

  searchFunction(currentPage){
    if(this.schoolModel.searchKey== ""){
      this.currentPage=1;
      this.getObCategories();
    }else{
      this.loaderShow = true;
      this.currentPage=currentPage;
      this.schoolModel.page = this.currentPage;
      this.accountService.getOBCategory(this.schoolModel).then((response) => {
        if (response.code == 200 && response.status == 'success') {
          this.total = response.count;
          this.totalPages=Math.ceil(response.count/10);
          this.ObCategories = response.result;  
          this.noRecords = false;                  
        }else{
          this.noRecords = true;
          this.ObCategories = []; 
          this.total = 0;
          this.totalPages=0;
        }
        this.loaderShow = false;
      })
    }
  }

  sortFunction(sortKey){
    this.key = sortKey;
    this.schoolModel.sortKey = sortKey;
    if(this.reverse === true){
      this.schoolModel.sortBy = 'asc';
    }else{
      this.schoolModel.sortBy = 'desc';
    }
    this.reverse = !this.reverse;   
    if(this.schoolModel.searchKey== ""){
      this.currentPage=1;
      this.getObCategories();
    }else{
      this.searchFunction(this.currentPage);
    }
  }

  changeStatusOBCategory(id, status){ 
    this.schoolModel.user_id = localStorage.getItem('admin_id');
    this.schoolModel.user_type = "admin";
    this.schoolModel.obCat_id = id;
    this.schoolModel.status = status;
    this.accountService.changeStatusOBCategory(this.schoolModel).then((response) => {
      this.loaderShow = false;
      if (response.code == 200 && response.status == 'success') {
        this.getObCategories();  
        this.sucMsg = true;
        this.successMessage = response.message;                    
        setTimeout (() => { this.sucMsg = false;  }, 3000);        
      }else{
        this.errMsg = true;
        this.errorMessage = response.message;
        setTimeout (() => { this.errMsg = false;  }, 3000); 
      }
    })
  }

  deleteOBCategory(id){
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to delete!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
    }).then((result) => {
      this.loaderShow = true;
      if (result.isConfirmed) {        
        this.schoolModel.user_id = localStorage.getItem('admin_id');
        this.schoolModel.user_type = "admin";
        this.schoolModel.obCat_id = id;
        this.accountService.deleteOBCategory(this.schoolModel).then((response) => {
          this.loaderShow = false;
          if (response.code == 200 && response.status == 'success') {
            this.getObCategories();  
            this.sucMsg = true;
            this.successMessage = response.message;                    
            setTimeout (() => { this.sucMsg = false;  }, 3000);        
          }else{
            this.errMsg = true;
            this.errorMessage = response.message;
            setTimeout (() => { this.errMsg = false;  }, 3000); 
          }
        })
      } else if (result.isDismissed) {
        this.loaderShow = false;
        Swal.fire({  showCancelButton: false,  icon: 'success',  title: 'Your file is safe!'})
      }
    })      
  }
}
