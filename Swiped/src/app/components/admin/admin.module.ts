import { NgModule } from '@angular/core';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AdminsRoutingModule } from './admin-routing.module';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import { AdminSidebarComponent } from './layout/admin-sidebar/admin-sidebar.component';
import { AdminHeaderComponent } from './layout/admin-header/admin-header.component';
import { CandidateListComponent } from './candidate-list/candidate-list.component';
import { CandidateDetailComponent } from './candidate-detail/candidate-detail.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ShowPackageComponent } from './show-package/show-package.component';
import { EditPackageComponent } from './edit-package/edit-package.component';
import { EmployerListComponent } from './employer-list/employer-list.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxSpinnerModule } from "ngx-spinner";
import { ViewCandidateComponent } from './view-candidate/view-candidate.component';
import { EmployerDetailComponent } from './employer-detail/employer-detail.component';
import { ViewPostComponent } from './view-post/view-post.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter'; 
import { Ng2OrderModule } from 'ng2-order-pipe'; 
@NgModule({
  imports: [
    CommonModule,
    AdminsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    NgxSpinnerModule,
    NgxPaginationModule,
    Ng2SearchPipeModule, 
    Ng2OrderModule
  ],
  declarations: [AdminLoginComponent, AdminSidebarComponent, AdminHeaderComponent, CandidateListComponent, CandidateDetailComponent, DashboardComponent, ShowPackageComponent, EditPackageComponent, EmployerListComponent, ViewCandidateComponent, EmployerDetailComponent, ViewPostComponent]
})
export class AdminsModule { }


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/