/**
  * Usage: dateString | dateFormat:'format'
 **/
import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({ name: 'formatdate' })

export class FormatDatePipe  implements PipeTransform {
  //transform() {}
  //constructor(protected DatePipe: DatePipe) {}

  // // adding a default format in case you don't want to pass the format
  // // then 'yyyy-MM-dd' will be used
  transform(date: Date | string,  format: string = 'yyyy-MM-dd'): string {
    date = new Date(date);  // if orginal type was a string
   // date.setDate(date.getDate()-day);
    return new DatePipe('en-US').transform(date, format);
  }
}