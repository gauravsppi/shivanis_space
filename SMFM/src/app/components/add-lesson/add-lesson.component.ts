import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import  * as $ from 'jquery';
import { AccountService } from '../../services/account.service';
import { Headers, Http } from '@angular/http';
import { Login_model } from '../../models/login_model';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ToolbarService, LinkService, ImageService, HtmlEditorService,TableService, QuickToolbarService } from '@syncfusion/ej2-angular-richtexteditor';


@Component({
  selector: 'app-add-lesson',
  templateUrl: './add-lesson.component.html',
  styleUrls: ['./add-lesson.component.scss'],
  providers: [ToolbarService, LinkService, ImageService, HtmlEditorService,TableService, QuickToolbarService]

})
export class AddLessonComponent implements OnInit {

  public loginModel: Login_model;
  public addValidationError: Login_model;
  public Editor = ClassicEditor;
  private headers = new Headers();
  radio=true;
  public tools: object = {
    items: ['Undo', 'Redo', '|',
        'Bold', 'Italic', 'Underline', 'StrikeThrough', '|',
        'FontName', 'FontSize', 'FontColor', 'BackgroundColor', '|',
        'SubScript', 'SuperScript', '|',
        'LowerCase', 'UpperCase', '|',
        'Formats', 'Alignments', '|', 'OrderedList', 'UnorderedList', '|',
        'Indent', 'Outdent', '|', 'CreateLink','CreateTable',
        'Image', '|', 'ClearFormat', 'Print', 'SourceCode', '|', 'FullScreen']
  };
 

  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http) {}
  fileSelect:boolean = false;
  fileSelect_error:boolean = false;
  fileSelectThumbnail:boolean = false;
  fileSelectThumbnail_error:boolean = false;
  sucMsg: boolean = false;
  errMsg: boolean = false;
  successMessage: string;   
  errorMessage: string; 
  lessons: [];  
  display: "none";
  model_class_show: string = "";
  noRecords: boolean = false;
  buttonDisable: boolean=false;
  category_name: string = ''; //set default
  upload:boolean=true;
  youtubeLink:boolean=false;
  vimeoLink:boolean=false;
  validatelink:boolean;
  validatelinks:boolean=true;
  normalupload:boolean=false;
  ngOnInit(): void {
    this.loginModel = <Login_model>{
      admin_id: '',
      school_id: '',
      school_sourcedId: '',
      lesson_name:'',
      lesson:'',
      video: File = null,
      thumbnail: File = null,
      type:0,
      video_link:'',
    }
    this.addValidationError = <Login_model>{
      lesson_name:'',
      lesson:'',
    }
    this.getSELCategoryLessonsName();
  }

  getSELCategoryLessonsName() {
    //this.loaderShow = true;
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    this.loginModel.sel_cat_id = this.route.snapshot.params.id;
    this.accountService.getSELCategoryLessonsName(this.loginModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
        this.category_name = response.result[0].category_name; 
      }    
      //this.loaderShow = false;
    })
  }
  
  onFileSelect(event) {
    this.buttonDisable = false;
    if (event.target.files.length > 0) {
      const video_file = <File>event.target.files[0];
      if(video_file.type == 'video/mp4' || video_file.type == 'video/ogg'  || video_file.type == 'video/webm'){
        this.loginModel.video = <File>video_file;
        this.fileSelect = false;
        this.normalupload=true;
      }else{
        this.fileSelect = true;
        this.loginModel.video = null;
        this.buttonDisable = true;
      }  
    }
  }

  onFileSelectThumbnail(event) {
    this.buttonDisable = false;
    if (event.target.files.length > 0) {
      const video_file = <File>event.target.files[0];
      //console.log('File type:: ', video_file.type);
      if(video_file.type == 'image/jpeg' || video_file.type == 'image/jpg'  || video_file.type == 'image/png'){
        this.loginModel.thumbnail = <File>video_file;
        this.fileSelectThumbnail = false;
        if(this.upload==false){
          this.validatelinks=false;
          this.fileSelect_error=false;
        }
      }else{
        this.fileSelectThumbnail = true;
        this.loginModel.thumbnail = null;
        this.buttonDisable = true;
      }  
    }
  }

  onSubmitAdd() {
    var valid = true;
    this.validatelink=false;
    this.fileSelectThumbnail = false;
    this.addValidationError.lesson_name = '';
    this.addValidationError.lesson = '';  
    this.addValidationError.video_link = ''; 
    if (this.loginModel.lesson_name.trim() == "") {
        this.addValidationError.lesson_name = 'Please enter lesson name';
        valid = false;
    }
    if (this.loginModel.lesson.trim() == "") {
      this.addValidationError.lesson = 'Please enter lesson description';
      valid = false;
    }
    if (this.loginModel.video_link!== "" || this.validatelinks==false) {
      var link = /^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/;
      var youtubeTest=link.test(this.loginModel.video_link);
      if(youtubeTest==true){
        this.youtubeLink=true;
        this.validatelink=true;
      }
      var regExp = /http:\/\/(www\.)?vimeo.com\/(\d+)($|\/)/;
      var vimeoTest = regExp.test(this.loginModel.video_link);
      if(vimeoTest==true){
        this.vimeoLink=true;
        this.validatelink=true;
      }
      if(this.validatelink==false){
      this.addValidationError.video_link = 'Please enter valid link here';
      if(this.fileSelectThumbnail == false && this.validatelinks==true){
        this.fileSelectThumbnail = true;
      }
      valid = false;
      }
    }
    if(this.loginModel.thumbnail != null && this.upload==true){
      if(this.loginModel.video != null ){

      }else{
        this.fileSelect_error = true;
        valid = false;
      }
    }
    if(this.loginModel.video != null ){
      if(this.loginModel.thumbnail != null ){

      }else{
        this.fileSelectThumbnail_error = true;
        valid = false;
      }
    }
    

    if (valid) {  
      if(this.youtubeLink==true){
        this.loginModel.type=1;
        this.loginModel.video_link=this.getYoutubeId();
      }else if(this.vimeoLink==true){
        this.loginModel.type=2;
        this.loginModel.video_link=this.getVimeoId();
      }else if(this.normalupload==true){
        this.loginModel.type=0;
        this.loginModel.video_link=null;
      }else{
        this.loginModel.type=3;
        this.loginModel.video_link=null;
      }
      this.noRecords = true;
      this.buttonDisable = false;
      this.loginModel.admin_id = localStorage.getItem('admin_id');
      this.loginModel.sel_cat_id = this.route.snapshot.params.id;  
      var formData: FormData  = new FormData();
      formData.append('lesson_name', this.loginModel.lesson_name.trim());
      formData.append('lesson', this.loginModel.lesson.trim());
      formData.append('admin_id', this.loginModel.admin_id);
      formData.append('sel_cat_id', this.loginModel.sel_cat_id);
      formData.append('video', this.loginModel.video);
      formData.append('thumbnail', this.loginModel.thumbnail);
      formData.append('type', this.loginModel.type);
      formData.append('videolink', this.loginModel.video_link);

      this.accountService.addSELCategoryLessons(formData).then((response) => { 
        if (response.code == 200 && response.status == 'success') {          
          this.sucMsg = true;
          this.successMessage = response.message;
          setTimeout (() => { this.sucMsg = false;  this.router.navigate(['/lesson/',this.loginModel.sel_cat_id]); }, 2000); 
          this.buttonDisable = false;      
          this.noRecords = false;       
        } else {   
          this.buttonDisable = false;
          this.noRecords = false;         
          this.errMsg = true;
          this.errorMessage = response.message;
          setTimeout (() => { this.errMsg = false;  }, 3000); 
        }          
      })
    }
  }
  change(event){
    console.log('jvdhvd>>>>>>',event);
    if(event==0){
      if(this.youtubeLink==true){
        this.youtubeLink=false;
      }
      this.upload=true;
    }else{
      this.normalupload=false;
      this.upload=false;
    }
  }
getYoutubeId(){
  var video_id = this.loginModel.video_link.split('v=')[1].split('&')[0];
  return video_id;
}
getVimeoId() {
  var match = /vimeo.*\/(\d+)/i.exec( this.loginModel.video_link );
  // If the match isn't null (i.e. it matched)
  if ( match ) {
    // The grouped/matched digits from the regex
    return match[1];
  }
}
}
