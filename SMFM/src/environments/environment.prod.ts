export const environment = {
  production: true,
  apiBaseUrl:"https://wholestudentlearning.com/",
  apiBasePath:"/nodeAPI_api/seeMeForMe/",
  imagePath:"https://wholestudentlearning.com/public/images/admin/",
  lessonVideoPath:"https://wholestudentlearning.com/uploads/lessons/",
  teacherImagePath:"https://wholestudentlearning.com/public/images/teachers/",
  adminImagePath:"https://wholestudentlearning.com/public/images/admin/",
  studentImagePath:"https://wholestudentlearning.com/public/images/students/",
  
};