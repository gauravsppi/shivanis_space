import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Headers, Http } from '@angular/http';


import { environment } from '../../../environments/environment';
import { AccountService } from '../../services/account.service';
import { TopbarComponent } from '../topbar/topbar.component';
import { Login_model } from '../../models/login_model';
import { ImageChangeService } from '../../services/image-change.service';


@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

    public loginModel: Login_model;
    public addValidationError: Login_model;
    ImagePath: string;   
    superAdmin: boolean= false;   
    successMessage: string;   
    errorMessage: string;   
    sucMsg: boolean = false;
    errMsg: boolean = false;
    isShownEdit: boolean = false;
    loaderShow: boolean = false;
    @Output() changeName = new EventEmitter();
    menuShow: boolean = false; 
    admin_Id = localStorage.getItem('admin_id');

    private headers = new Headers();

    constructor(private ImageChangeService: ImageChangeService,
        private router: Router, private accountService: AccountService, private route: ActivatedRoute, private http: Http) {
      
    }

    ngOnInit(): void {
        this.loginModel = <Login_model>{
            image: File = null,
            imgFile: '',
            imgType: '',
            admin_id: '',
            sex: '',
            name: '',
            phone: '',
            password: '',
            cpassword: '',
        }
        this.addValidationError = <Login_model>{
            email: '',
            password: '',
            cpassword: '',
            name: '',
            phone: '',
        }
        if(localStorage.getItem('admin_id') == '1'){
            this.superAdmin = true;
            this.isShownEdit = true;
        }
        this.getProfileInfo();   
        if(this.admin_Id == '1'){
            this.menuShow =  true;
        }          
    }

    getProfileInfo() {
        this.loginModel.admin_id = localStorage.getItem('admin_id');      
        this.accountService.adminProfile(this.loginModel).then((response) => {
            console.log('Admin respone:: ', response);
            if (response.code == 200) {
                this.loginModel.name = response.result.name;
                this.loginModel.email = response.result.email;
                this.loginModel.sex = response.result.sex;
                this.loginModel.birthDate = response.result.birthDate;
                this.loginModel.cityOfBirth = response.result.cityOfBirth;                    
                this.loginModel.countryOfBirthCode = response.result.countryOfBirthCode;                    
                this.loginModel.stateOfBirthAbbreviation = response.result.stateOfBirthAbbreviation;                    
                this.loginModel.username = response.result.username;                    
                this.loginModel.image =  response.result.image;
                this.loginModel.phone = response.result.phone;
                this.loginModel.password = response.result.original_password;
                this.loginModel.cpassword = response.result.original_password;
                if(localStorage.getItem('admin_id') != '1'){
                    this.loginModel.school_name = response.result.school_name;
                }
                
                //this.ImagePath = environment.imagePath+response.result.image;
                if(response.result.image == '' || response.result.image == undefined || response.result.image == null){ 
                   
                    this.ImagePath = environment.adminImagePath + 'admin.png';
                }else{ 
                    this.ImagePath = environment.adminImagePath+response.result.image;
                }
                
            }
        })
    }

    showEdit() {
        if (this.isShownEdit == true) {
            this.isShownEdit = false;
           // this.superAdmin = false;
        } else {
            //this.superAdmin = true;
            this.isShownEdit = true;
        }
    }

    editProfile() {
        var valid = true;
        this.addValidationError.email = '';
        this.addValidationError.name = '';
        this.addValidationError.username = '';
        this.addValidationError.phone = '';
        this.addValidationError.password = '';
        if (this.loginModel.email == "") {
            this.addValidationError.email = 'Please enter email';
            valid = false;
        }else{

        }
        if (this.loginModel.username == "") {
            this.addValidationError.username = 'Please enter username';
            valid = false;
        }
        if (this.loginModel.name == "") {
            this.addValidationError.name = 'Please enter name';
            valid = false;
        }
        if (this.loginModel.phone == "") {
            this.addValidationError.phone = 'Please enter phone';
            valid = false;
        }
        if (this.loginModel.password == "" ) {
            this.addValidationError.password = 'Please enter password';
            valid = false;
        }
        if (this.loginModel.cpassword == "" ) {
            this.addValidationError.cpassword = 'Please enter confirm password';
            valid = false;
        }
        if (this.loginModel.cpassword != this.loginModel.password ) {
            this.addValidationError.cpassword = "Password and confirm password doesn't match";
            valid = false;
        }
        if (valid) {
            this.accountService.adminProfileUpdate(this.loginModel).then((response) => {
                if (response.code == 200) {                                      
                    this.loginModel.name = response.result.name;
                    this.loginModel.email = response.result.email;
                    this.loginModel.phone = response.result.phone;
                    this.sucMsg = true;  
                    this.successMessage = response.message                 
                    this.isShownEdit = false;                 
                    setTimeout (() => { this.sucMsg = false;  }, 3000);                    
                } else {
                    this.errMsg = true;
                    this.errorMessage = response.message                    
                    this.isShownEdit = true;            
                    setTimeout (() => { this.errMsg = false;  }, 3000);                            
                }
            })
            this.addValidationError.name ='';
            this.addValidationError.email ='';
            this.addValidationError.phone ='';
            this.addValidationError.password ='';
            this.addValidationError.cpassword ='';
        }
    }

    onFileChange(event) {    
        this.loaderShow = true;   
        this.loginModel.admin_id = localStorage.getItem('admin_id');
        const reader = new FileReader();
        reader.onload = (e: any) => {
            const image = new Image();
            image.src = e.target.result;
            this.loginModel.imgFile = e.target.result;
            this.loginModel.imgFile = this.loginModel.imgFile.replace('data:' + event.target.files[0].type + ';base64,', '');            
            if (event.target.files[0].type == 'image/jpeg') {
                this.loginModel.imgType = 'jpg';
            }
            if (event.target.files[0].type == 'image/png') {
                this.loginModel.imgType = 'png';
            }
            this.accountService.adminProfileImageUpdate(this.loginModel).then((response) => {               
                if (response.code == 200) {                    
                    this.ImagePath = environment.imagePath+response.imageName;

                    this.ImageChangeService.imageChangeEmitter.emit(this.ImagePath);

                    this.sucMsg = true;
                    this.successMessage = response.message 
                    setTimeout (() => { this.sucMsg = false;  }, 3000);                    
                } else {
                    this.errMsg = true;
                    this.errorMessage = response.message 
                    setTimeout (() => { this.errMsg = false;  }, 3000);                     
                }
                this.loaderShow = false;   
            })
        }
        reader.readAsDataURL(event.target.files[0]);
    }

}
