import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Headers, Http } from '@angular/http';

import { environment } from '../../../environments/environment';
import { AccountService } from '../../services/account.service';
import { class_model } from '../../models/class_model';
import { RoutingService } from '../../services/routing.service';

@Component({
  selector: 'app-class-list',
  templateUrl: './class-list.component.html',
  styleUrls: ['./class-list.component.scss']
})
export class ClassListComponent implements OnInit {

  constructor(private router: Router, private accountService: AccountService, private route: ActivatedRoute, private http: Http, private routingService : RoutingService) {  }
  
  public classModel: class_model;
  public loaderShow: boolean = false;
  total : number= 0;
  totalPages : number= 0;
  currentPage:number=1;
  page : number = 1;
  key: string = 'name'; //set default
  reverse: boolean = false;
  noRecords: boolean = false;
  courses= [];

  previousUrl: string = '';
  currentUrl: string = '';
  arr = [];
  breadcrumb_course : boolean = false;
  breadcrumb_class_list : boolean = false;
  breadcrumb_class : boolean = false;
  class_id : string = '';


  ngOnInit(): void {
    this.classModel = <class_model>{
      name: '',
      admin_id: '',
      authToken: '',
      school_id: '',
      course_id: '',
      searchKey: '',
      sortKey: 'className',
      sortBy: 'asc'  
    }    
    this.classModel.course_id = this.route.snapshot.params.id;
    this.previousUrl = this.routingService.getPreviousUrl(); //get previous url
    this.currentUrl = this.routingService.getCurrentUrl(); //get current url
    
    console.log('get previuo URL :: ', this.previousUrl);
    console.log('get current URL :: ', this.currentUrl);
    if(this.previousUrl != undefined){ 
      this.arr = this.previousUrl.split('/');
      this.previousUrl = this.arr[1];
      this.class_id = this.arr[2];
      if(this.previousUrl == 'class-view'){
        this.breadcrumb_course = false;
        this.breadcrumb_class = true;  
        this.breadcrumb_class_list = false;        
      }else if(this.previousUrl == 'class-list'){
        this.breadcrumb_course = false;
        this.breadcrumb_class_list = true;        
        this.breadcrumb_class = false;        
      }else{          
        if(this.classModel.course_id != undefined){ 
          this.breadcrumb_course = true;
        }else{
          this.breadcrumb_class_list = false;
        }
      }   
    }else{
      if(this.classModel.course_id != undefined){ 
        this.breadcrumb_course = true;
      }else{
        this.breadcrumb_class_list = false;
      }   
    }  
    
    console.log('breadcrumb student ::', this.breadcrumb_course)

    this.getClasses()
    if(this.classModel.course_id == undefined){
      this.classModel.course_id = '';
    }
  }

  getClasses(){   
    this.loaderShow = true;
    this.classModel.admin_id = localStorage.getItem('admin_id');
    this.classModel.school_id = localStorage.getItem('school_id');
    this.classModel.page = this.currentPage;
    this.classModel.searchKey = '';
    if(this.classModel.course_id == undefined){
      this.classModel.course_id = '';
    }
    this.accountService.getClasses(this.classModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
        this.total = response.count;
        this.totalPages=Math.ceil( response.count/10);
        this.courses = response.result;  
        this.noRecords = false;                  
      }else{
        this.noRecords = true;
        this.total = 0
        this.totalPages=0
        this.courses = []; 
      }
      this.loaderShow = false;
    })
  }
  pageChange(currentPage){
    this.currentPage=currentPage;
    if(this.classModel.searchKey==""){
      this.getClasses();
    }else{
      this.searchFunction(this.currentPage);
    } 
  }
  counter(i: number) {
    return new Array(i);
  }
  searchFunction(currentPage){
    if(this.classModel.searchKey==""){
      this.currentPage=1;
      this.getClasses();
    }else{
      this.loaderShow = true;
      this.currentPage=currentPage;
      this.accountService.getClasses(this.classModel).then((response) => {
        if (response.code == 200 && response.status == 'success') {
          this.total = response.count;
          this.totalPages=Math.ceil( response.count/10);
          this.courses = response.result;  
          this.noRecords = false;                  
        }else{
          this.noRecords = true;
          this.total = 0
          this.totalPages=0
          this.courses = []; 
        }
        this.loaderShow = false;
      })
    }
  }
  sortFunction(sortKey){
    this.key = sortKey;
    this.classModel.sortKey = sortKey;
    if(this.reverse === true){
      this.classModel.sortBy = 'asc';
    }else{
      this.classModel.sortBy = 'desc';
    }
    this.reverse = !this.reverse;   
    if(this.classModel.searchKey== ""){
      this.currentPage=1;
      this.getClasses();
    }else{
      this.searchFunction(this.currentPage);
    }
  }

}
