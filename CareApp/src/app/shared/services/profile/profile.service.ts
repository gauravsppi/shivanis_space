import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private http: HttpClient) { }

  //*********** Fetch Profile Data ************************
  fetch_user_profile_data() {
    return this.http.get<any>(environment.apiBaseUrl + 'getUserProfile')
        .pipe(map((res: any) => {
            return res;
        }));
}
 //*********** fetch care receiver list service ************************
 update_profile(formdata) {
  return this.http.post<any>(environment.apiBaseUrl + 'updateProfile', formdata)
      .pipe(map((res: any) => {
          return res;
  }));
}
}