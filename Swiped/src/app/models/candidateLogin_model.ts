export class candidateLogin_model {
    id:string;
    name:string;
    email:string;
    number:string;
    locations:string;
    password:string;
    company_website:string;
    countrylist:any;
    signuprole:string;
    candidate_checked:string;
     call_request:any;
    // linkedin_profile:string;
    // github_link:string; 
    // sector:any;
    // exper:any;
    // lang:any;
    // specialisms:any;
    // university:any;
    // qualification:any;
    // graduation_year:any;
    // subject:any;
    // result:any;
    // qualification_name:any;
    // qualification_status:any;
    // job_title:any;
    // organisation:any;
    // job_sector:any;
    // job_role:any;
    // start:any;
    // end:any;
    // present:boolean;
    // presents:any;
    // job_type:any;
    // jobsector:any;
    // role:any;
    // location:any;
    // salary:any;
    // intro_video:any;

    //------------------------------------------You tab UI-------------------
    current_job_title: string;
    job_type: string;    // employment type
    for_who: string;
    sector: any;
    skills: any;
    experience: any;
    //specific_project_experience: any;
    //first_language: any;
    current_status: any;   //actively looking
    link_github: string;
    link_linkedin: string;
    your_website: string;
    relevent_project_work_links: any;
    cv_upload: any;
    skill_assessment_upload: any;
    reference_upload: any;

    //------------------------------------------Qualification tab-------------------

    qualification: any;
    grade: any;
    university_school: any;
    date: any;

    //--------------------------------------Idel job tab------------------------------

    job_title: any;
    currencylist: any;
    target_salary: any;
    salary_type: string;
    type_role: string;
    preffered_location: any;
    //current_status: any;
    ideal_start_date: any;
    preffered_industry: any;
    size1: any;
    size2: any;
    size3: any;
    type_of_industry: string;    //culture
    main_driver: string;
    availability:any;
  //------------------------------------interests tab-------------------------------

    describe_friend: string;
    do_well: string;
    passion: string;
    goals: string;
    anything_else: string;
    ideal_job:string;
    video_upload: any;
}