import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObservationsCategoryComponent } from './observations-category.component';

describe('ObservationsCategoryComponent', () => {
  let component: ObservationsCategoryComponent;
  let fixture: ComponentFixture<ObservationsCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObservationsCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObservationsCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
