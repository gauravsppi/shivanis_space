import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import  * as $ from 'jquery';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';

import { AccountService } from '../../services/account.service';
import { Headers, Http } from '@angular/http';
import { Login_model } from '../../models/login_model';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { environment } from 'src/environments/environment';
import { ToolbarService, LinkService, ImageService, HtmlEditorService,TableService, QuickToolbarService } from '@syncfusion/ej2-angular-richtexteditor';



@Component({
  selector: 'app-edit-lesson',
  templateUrl: './edit-lesson.component.html',
  styleUrls: ['./edit-lesson.component.scss'],
  providers: [ToolbarService, LinkService, ImageService, HtmlEditorService,TableService, QuickToolbarService]
})
export class EditLessonComponent implements OnInit {

  public loginModel: Login_model;
  public addValidationError: Login_model;
  public tools: object = {
    items: ['Undo', 'Redo', '|',
        'Bold', 'Italic', 'Underline', 'StrikeThrough', '|',
        'FontName', 'FontSize', 'FontColor', 'BackgroundColor', '|',
        'SubScript', 'SuperScript', '|',
        'LowerCase', 'UpperCase', '|',
        'Formats', 'Alignments', '|', 'OrderedList', 'UnorderedList', '|',
        'Indent', 'Outdent', '|', 'CreateLink','CreateTable',
        'Image', '|', 'ClearFormat', 'Print', 'SourceCode', '|', 'FullScreen']
  };

  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http) { }
  fileSelect:boolean = false;
  fileSelect_error:boolean = false;
  fileSelectThumbnail:boolean = false;
  fileSelectThumbnail_error:boolean = false;

  sucMsg: boolean = false;
  errMsg: boolean = false;
  successMessage: string;   
  errorMessage: string; 
  videoPath : string = '';
  noRecords: boolean = false;
  videoShow: boolean = true;
  buttonDisable: boolean= false;
  thumbnail: string='';
  video: File= null;
  thumbnailName: File= null;
  category_name: string='';
  videolinkshow:boolean=false;
  validatelink:boolean;
  youtubeLink:boolean=false;
  vimeoLink:boolean=false;

  ngOnInit(): void {
    this.loginModel = <Login_model>{
      admin_id: '',
      school_id: '',
      school_sourcedId: '',
      lesson_name:'',
      lesson:'',
      video: File = null,
      video_link:'',
    }

    this.addValidationError = <Login_model>{
      lesson_name:'',
      lesson:'',
    }  
    this.getSELCategoryLessonsByID();  
    this.getSELCategoryLessonsName();
  }

  getSELCategoryLessonsName() {
    //this.loaderShow = true;
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    //this.loginModel.sel_cat_id = this.route.snapshot.params.id;
    this.accountService.getSELCategoryLessonsName(this.loginModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
        this.category_name = response.result[0].category_name; 
      }    
      //this.loaderShow = false;
    })
  }

  getSELCategoryLessonsByID() {
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    this.loginModel.sel_lesson_id = this.route.snapshot.params.id;
    this.accountService.viewSELCategoryLessons(this.loginModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
       console.log('incoimingnjbjbj>>>>>',response.result[0]);
        this.loginModel.sel_lesson_id = response.result[0].id; 
        this.loginModel.lesson_name = response.result[0].lesson_name; 
        this.loginModel.lesson = response.result[0].lessons; 
        this.loginModel.old_video = response.result[0].video; 
        this.loginModel.sel_cat_id = response.result[0].sel_cat_id; 
        this.loginModel.type = response.result[0].type;
        this.getSELCategoryLessonsName();
       // this.loginModel.video = response.result[0].video; 
        this.loginModel.old_thumbnail = response.result[0].thumbnail; 
        //this.loginModel.thumbnail = response.result[0].thumbnail; 
        this.thumbnail = environment.lessonVideoPath+response.result[0].thumbnail; 
        this.video = response.result[0].video; 
        this.thumbnailName = response.result[0].thumbnail; 
        if(response.result[0].video == '' || response.result[0].video == undefined){
          this.videoShow = false;
        }else{ 
          if(response.result[0].type==1){
            this.videoPath = '<iframe width="100%" height="320"  src="'+'https://www.youtube.com/embed/'+response.result[0].video+'?rel=0&modestbranding=1'+'" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
          }else if(response.result[0].type==2){
            this.videoPath = '<iframe width="100%" height="320"  src="'+'https://player.vimeo.com/video/'+response.result[0].video+'" allow="autoplay; encrypted-media" allowfullscreen></iframe>'; 
          }else if(response.result[0].type==0){
            var type = response.result[0].video.split('.').pop();
            console.log('video type>>>',type);
            if(type == 'mp4'){
              this.videoPath = '<iframe width="100%" height="320" src="'+environment.lessonVideoPath+response.result[0].video+'" type="video/mp4"></iframe>';
            }
            if(type == 'ogg'){
              this.videoPath = '<iframe width="100%" height="320" src="'+environment.lessonVideoPath+response.result[0].video+'" type="video/ogg"></iframe>';
            }      
            if(type == 'webm'){
              this.videoPath = '<iframe width="100%" height="320" src="'+environment.lessonVideoPath+response.result[0].video+'" type="video/webm"></iframe>';
            }
          }else{

          }
        }
      }
    })
  }

  onFileSelect(event) {
    this.buttonDisable = false;
    if (event.target.files.length > 0) {
      const video_file = <File>event.target.files[0];
      if(video_file.type == 'video/mp4' || video_file.type == 'video/ogg' || video_file.type == 'video/webm'){
        this.loginModel.video = <File>video_file;
        this.fileSelect = false;
      }else{
        this.fileSelect = true;
        this.loginModel.video = null;
        this.buttonDisable = true;
      } 
    }  
  }

  onFileSelectThumbnail(event) {
    this.buttonDisable = false;
    if (event.target.files.length > 0) {
      const video_file = <File>event.target.files[0];
      if(video_file.type == 'image/jpeg' || video_file.type == 'image/jpg'  || video_file.type == 'image/png'){
        this.loginModel.thumbnail = <File>video_file;
        this.fileSelectThumbnail = false;
      }else{
        this.fileSelectThumbnail = true;
        this.loginModel.thumbnail = null;
        this.buttonDisable = true;
      }  
    }
  }
  onSubmitUpdate() {
    var valid = true;    
    this.buttonDisable = false;
    this.validatelink=false;
    this.addValidationError.lesson_name = '';
    this.addValidationError.lesson = '';

    if (this.loginModel.lesson_name.trim() == "") {
        this.addValidationError.lesson_name = 'Please enter lesson name';
        valid = false;
    }

    if (this.loginModel.lesson.trim() == "") {
      this.addValidationError.lesson = 'Please enter lesson description';
      valid = false;
    }
    if (this.loginModel.video_link!== "") {
      var link = /^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/;
      var youtubeTest=link.test(this.loginModel.video_link);
      if(youtubeTest==true){
        this.youtubeLink=true;
        this.validatelink=true;
      }
      var regExp = /http:\/\/(www\.)?vimeo.com\/(\d+)($|\/)/;
      var vimeoTest = regExp.test(this.loginModel.video_link);
      if(vimeoTest==true){
        this.vimeoLink=true;
        this.validatelink=true;
      }
      if(this.validatelink==false){
      this.addValidationError.video_link = 'Please enter valid link here';
      valid = false;
      }
    }
    
    console.log('video_link hwrefewfdgh',this.loginModel.video_link);
    // if(this.loginModel.thumbnail != null ){
    //   if(this.loginModel.video != null ){

    //   }else{
    //     this.fileSelect_error = true;
    //     valid = false;
    //   }
    // }
    // if(this.loginModel.video != null ){
    //   if(this.loginModel.thumbnail != null ){

    //   }else{
    //     this.fileSelectThumbnail_error = true;
    //     valid = false;
    //   }
    // }

    if(valid) {   
      this.noRecords = true; 
      this.loginModel.sel_lesson_id = this.route.snapshot.params.id;
      if(this.youtubeLink==true){
        this.loginModel.video_link=this.getYoutubeId();
      }else if(this.vimeoLink==true){
        this.loginModel.video_link=this.getVimeoId();
      } else{
        this.loginModel.video_link='null';
      }
      
      // this.loginModel.video = this.video;
      // this.loginModel.thumbnail = this.thumbnailName;
      var formData: FormData  = new FormData();
      formData.append('lesson_name', this.loginModel.lesson_name.trim());
      formData.append('lesson', this.loginModel.lesson.trim());
      formData.append('sel_lesson_id', this.loginModel.sel_lesson_id);
      formData.append('video', this.loginModel.video);
      formData.append('old_video', this.video);
      formData.append('thumbnail', this.loginModel.thumbnail);
      formData.append('old_thumbnail', this.thumbnailName);
      formData.append('videolink', this.loginModel.video_link);
      console.log('form data: ', formData);
      this.accountService.updateSELCategoryLessons(formData).then((response) => { 
        console.log('updated lesson: ', response);
        if (response.code == 200 && response.status == 'success') { 
          // if(response.result.video != ''){ 
          //   this.videoPath = '<video width="100%" height="320" controls> <source src="'+environment.lessonVideoPath+response.result.video+'" type="video/mp4"></video>';
          // }  
          this.sucMsg = true;
          this.loginModel.video_link='';
          this.addValidationError.video_link='';
          this.successMessage = response.message;
          setTimeout (() => { this.sucMsg = false; }, 2000); 
          this.buttonDisable = false;      
          this.noRecords = false;  
          this.getSELCategoryLessonsByID();  
        } else {    
          this.buttonDisable = false;      
          this.noRecords = false;           
          this.errMsg = true;
          this.errorMessage = response.message;
          setTimeout (() => { this.errMsg = false;  }, 2000); 
        }          
      })
    }
  }

  ngAfterViewInit(){
    $(function() {
      $("#view-video button.close2").on("click", function(e) {
        $('#view-video video').trigger('pause');
      });
      $(document).on("click", function(e) {
        $('#view-video video').trigger('pause');
      });
    });
  }
  getYoutubeId(){
    var video_id = this.loginModel.video_link.split('v=')[1].split('&')[0];
    return video_id;
  }
  getVimeoId() {
    var match = /vimeo.*\/(\d+)/i.exec( this.loginModel.video_link );
    // If the match isn't null (i.e. it matched)
    if ( match ) {
      // The grouped/matched digits from the regex
      return match[1];
    }
  }

}
