import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Headers, Http } from '@angular/http';

import { environment } from '../../../environments/environment';
import { AccountService } from '../../services/account.service';
import { teacher_model } from '../../models/teacher_model';
import { RoutingService } from '../../services/routing.service';

@Component({
  selector: 'app-teacher-view',
  templateUrl: './teacher-view.component.html',
  styleUrls: ['./teacher-view.component.scss']
})
export class TeacherViewComponent implements OnInit {

  public teacherModel: teacher_model;
  public classes = [];
  classes_count = 0;
  noRecords: boolean = true;
  teacher_id = this.route.snapshot.params.id;

  previousUrl: string = '';
  currentUrl: string = '';
  arr = [];
  breadcrumb_teacher : boolean = false;
  breadcrumb_class : boolean = false;
  class_id : string = '';

  constructor(private router: Router, private accountService: AccountService, private route: ActivatedRoute, private http: Http, private routingService : RoutingService) {  }

  ngOnInit(): void {
    this.teacherModel = <teacher_model>{      
      teacher_id : '',
      school_id : '',
      name:'',
      email:'',
      phone:'',
      image:'',
      teacher_sourcedId:''
    }
    this.previousUrl = this.routingService.getPreviousUrl(); //get previous url
    this.currentUrl = this.routingService.getCurrentUrl(); //get current url
    
    console.log('get previuo URL :: ', this.previousUrl);
    console.log('get current URL :: ', this.currentUrl);
    if(this.previousUrl != undefined){ 
      this.arr = this.previousUrl.split('/');
      this.previousUrl = this.arr[1];
      this.class_id = this.arr[2];
      if(this.previousUrl == 'class-view'){
        this.breadcrumb_teacher = false;
        this.breadcrumb_class = true;        
      }else{          
        this.breadcrumb_teacher = true;
      }   
    }else{
      this.breadcrumb_teacher = true;
    }  

    this.getTeacherById(this.teacher_id);
  }

  getTeacherById(teacher_id){
    this.teacherModel.teacher_id = teacher_id;
    this.accountService.getTeacherInfo(this.teacherModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
         this.teacherModel.name = response.result[0].name;     
         if(response.result[0].image != null){
          this.teacherModel.image = environment.teacherImagePath + response.result[0].image;                    
         }else{
          this.teacherModel.image = environment.teacherImagePath + 'teacher.png';
         }
         this.teacherModel.email = response.result[0].email;                    
         this.teacherModel.phone = (response.result[0].phone == '' || response.result[0].phone == undefined || response.result[0].phone == 'undefined')? '--' : response.result[0].phone ;                    
         this.teacherModel.sex = response.result[0].sex;                    
         this.teacherModel.identifier = response.result[0].identifier;                    
         this.teacherModel.birthDate = response.result[0].birthDate;                    
         this.teacherModel.cityOfBirth = response.result[0].cityOfBirth;                    
         this.teacherModel.countryOfBirthCode = response.result[0].countryOfBirthCode;                    
         this.teacherModel.stateOfBirthAbbreviation = response.result[0].stateOfBirthAbbreviation;                    
         this.teacherModel.username = response.result[0].username;                    
         this.teacherModel.teacher_sourcedId = response.result[0].sourcedId;
        if(this.teacherModel.teacher_sourcedId != ''){
          this.accountService.getTeacherClasses(this.teacherModel).then((response) => {
              if(response.code == 200 && response.status == 'success'){ 
                this.classes = response.classes.classes;
                this.classes_count = response.classes.classes.length;
                this.noRecords = false;
              }
              if(response.code == 200 && response.status == 'notFound'){ 
                this.noRecords = true;
              }
          })
        }                    
      }
    })
  }
}
