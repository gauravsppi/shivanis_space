import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as $ from 'jquery';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';

import { AccountService } from '../../services/account.service';
import { Headers, Http } from '@angular/http';
import { Login_model } from '../../models/login_model';
import Swal from 'sweetalert2';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-view-lesson',
  templateUrl: './view-lesson.component.html',
  styleUrls: ['./view-lesson.component.scss']
})
export class ViewLessonComponent implements OnInit {

  public loginModel: Login_model;
  public addValidationError: Login_model;

  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http) { }
  sucMsg: boolean = false;
  errMsg: boolean = false;
  successMessage: string;
  errorMessage: string;
  lesson_name: string;
  lesson: string;
  video: string;
  thumbnail: string;
  videoPath: string = '';
  IframeUrl: SafeUrl;
  videoShow: boolean = true;
  noRecords: boolean = false;

  ngOnInit(): void {
    this.loginModel = <Login_model>{
      admin_id: '',
      school_id: '',
      school_sourcedId: '',
    }

    this.getSELCategoryLessons();
  }

  getSELCategoryLessons() {
    this.noRecords = true;
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    this.loginModel.sel_lesson_id = this.route.snapshot.params.id;
    this.accountService.viewSELCategoryLessons(this.loginModel).then((response) => {
    console.log('Lessons resp: ', response);
      if (response.code == 200 && response.status == 'success') {
        this.lesson_name = response.result[0].lesson_name;
        this.lesson = response.result[0].lessons;
        this.video = response.result[0].video;
        this.thumbnail = environment.lessonVideoPath + response.result[0].thumbnail;
        if (this.video == '' || this.video == undefined) {
          this.videoShow = false;
        }
        this.loginModel.video = response.result[0].video;
        this.loginModel.sel_cat_id = response.result[0].sel_cat_id;
        var type = response.result[0].video.split('.').pop();
        if(response.result[0].type==1){
          this.videoPath = '<iframe width="100%" height="320"  src="'+'https://www.youtube.com/embed/'+response.result[0].video+'?rel=0&modestbranding=1'+'" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
        }else if(response.result[0].type==2){
          this.videoPath = '<iframe width="100%" height="320"  src="'+'https://player.vimeo.com/video/'+response.result[0].video+'" allow="autoplay; encrypted-media" allowfullscreen></iframe>'; 
        }else if(response.result[0].type==0){
          if (type == 'mp4') {
            this.videoPath = '<video width="100%" controls playsinline> <source src="' + environment.lessonVideoPath + response.result[0].video + '" type="video/mp4"></video>';
          }
          if (type == 'ogg') {
            this.videoPath = '<video width="100%" controls playsinline> <source src="' + environment.lessonVideoPath + response.result[0].video + '" type="video/ogg"></video>';
          }
          if (type == 'webm') {
            this.videoPath = '<video width="100%" controls playsinline> <source src="' + environment.lessonVideoPath + response.result[0].video + '" type="video/webm"></video>';
          }
        }else{

        }
        this.noRecords = false;
      } else {
        this.noRecords = true;
      }
    })
  }
  ngAfterViewInit() {
    // $("#view-video button.close").click(function(){
    //   $('#view-video video').trigger('pause'); 
    // });

    $(function () {
      // $("video").on("click", function (e) {
      //   $('#view-video video').trigger('pause');
      //   e.stopPropagation();
      // });
      $(document).on("click", function (e) {
        $('video').trigger('pause');
        // if ($(e.target).is("#view-video video") === false) {
        //   $('#view-video video').trigger('pause');
        // }
      });
    });
  }
}  