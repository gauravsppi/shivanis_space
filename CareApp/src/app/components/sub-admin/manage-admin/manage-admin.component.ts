import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import { Admin } from '../../../models/admin.model';
import { AccountService } from '../../../services/account.service';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from "../../../shared/services/spinner.service";

@Component({
  selector: 'app-manage-admin',
  templateUrl: './manage-admin.component.html',
  styleUrls: ['./manage-admin.component.scss']
})
export class ManageAdminComponent implements OnInit {
  public adminModel: Admin;
  public addValidationError: Admin;
  private headers = new Headers();
  page = 1;
  count : number;
  pageSize = 2;
  subadmin:any=[];
  imgUrl:string;
  imageName:string;
  imageSelect:boolean=false;
  staticImg:boolean=false;
  urls:string;
  constructor(
    private accountService: AccountService, 
    private route: ActivatedRoute, 
    private router: Router,
    private http: Http, 
    private cookieService: CookieService,
    private spinner: NgxSpinnerService

    ) { }
  ngOnInit(): void {
    this.adminModel = <Admin>{
      id:'',
      email: '',
      password: '',
      image:'',
      name:'',
      phoneno:''
    }
    this.addValidationError=<Admin>{
      id:'',
      email: '',
      password: '',
      image:'',
      name:'',
      phoneno:''
    }
    this.subadminDetail();
  }
  ngAfterViewInit() {
    $(function () {
      $('.showonhover').click(function (event) {
        $("#selectfile").trigger('click');
      });

    });
  }
  subadminDetail(){ // Get all subadmin details
    this.spinner.show();
    this.accountService.subAdminGet(this.page).then((response) => {
    this.spinner.hide();
      if(response.code==200){
        this.subadmin=response.result;
        this.count = 4;
        this.imgUrl=response.imageUrl;
      }else{

      }
    })
  }
  getData(id,name,email,phone,image){ 
    // for show view subadmin
    this.adminModel.id=id;
    this.adminModel.name=name;
    this.adminModel.email=email;
    this.adminModel.phoneno=phone;
    this.imageName=image;
    console.log("image name>>>",image);
  }
  selectImage(event){ // for upload edit image
    if (event.target.files.length > 0) {
      console.log(event.target.files[0].name);
      const formData = new FormData();
      formData.append('file', <File>event.target.files[0]);
      this.http.post('https://www.thecareapps.com/nodeapis/Admin/imageUpload', formData, { headers: this.headers }).subscribe((res: any) => {
        var obj = res.json();
        this.urls = obj.imgurl + obj.result;
        this.imageSelect=true;
        this.staticImg=true;  
        this.adminModel.image = obj.result;
      });
    }
  }
  
  onTableDataChange(event){
    this.page = event;
    this.subadminDetail();
  } 
  modelClose(){ // edit model close button
    this.imageSelect=false;
    this.staticImg=false; 
  }

  Update(){ // Edit Subadmin
    this.accountService.SubadminUpdate(this.adminModel).then((response) => {
      if(response.code=='200'){
        this.subadminDetail();
        var click = document.getElementById("editCloseBtn");
          click.click();
        this.router.navigate(['/manage-admin']);
      }else{
        console.log("data not updated");
        this.subadminDetail();
        this.router.navigate(['/manage-admin']);
      }
    })
  }
  deleteSubadmin(id){ // Delete One Subadmin 
    this.adminModel.id=id;
    Swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover this Subadmin again!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete this subadmin!',
      cancelButtonText: 'No, keep it',
    }).then((result) => {
      if (result.isConfirmed) {
        this.accountService.SubadminDelete(this.adminModel).then((response) => {
          if (response.code == 200) {
            //this.sucMsg = true;
            Swal.fire({ showCancelButton: false, icon: 'success', title: 'Subadmin deleted successfully' })
            this.subadminDetail();
          } else {
            //this.errMsg = true;
            //this.errmessage = 'Something went wrong';
          }
        })
      } else if (result.isDismissed) {
        Swal.fire({ showCancelButton: false, icon: 'success', title: 'Subadmin data is safe!' })
      }
    })
  }
}
