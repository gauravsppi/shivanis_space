import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Headers, Http } from '@angular/http';
import Swal  from 'sweetalert2';
import  * as $ from 'jquery';
declare const modalOpen_add: any;
declare const modalOpen_edit: any;
declare const modalClose_interventions: any;
import { AccountService } from '../../services/account.service';
import { school_model } from '../../models/school_model';

@Component({
  selector: 'app-list-observation-sub-category',
  templateUrl: './list-observation-sub-category.component.html',
  styleUrls: ['./list-observation-sub-category.component.scss']
})
export class ListObservationSubCategoryComponent implements OnInit {

 
  public schoolModel: school_model;
  public addValidationError: school_model;
  sucMsg: boolean =  false; 
  successMessage: string='';
  errMsg: boolean = false; 
  errorMessage: string=''; 
  noRecords: boolean = false;
  noRecordsSubCategory: boolean = false;

  total : number= 0;
  totalPages : number= 0;
  currentPage:number=1;
  page : number =1;
  key: string = 'subCategory'; //set default
  reverse: boolean = false; 
  loaderShow: boolean = false; 
  observation_name: string=''; 
 
  checkStatus1: boolean = false; 
  checkStatus0: boolean = false; 
  interventionsList=[];
  ObCategories = [];
  checkedInterventionList=[];
  checkedList=[];
  antedentName:string;
  antedentUniqueId:string;
  antedentId:string;
  idantecedent:string;
  constructor(private router: Router, private accountService: AccountService, private route: ActivatedRoute, private http: Http) { }

  ngOnInit(): void {
    this.schoolModel = <school_model>{
      user_id: '',
      antecedentId:'',
      subCat_name: '',
      obCat_id: '',      
      obSubCat_id: '',      
      status: '1' ,
      searchKey: '',
      sortKey: 'id',
      sortBy: 'desc', 
      unique_id:'',
      link_interventions:[],
    }
    this.addValidationError = <school_model>{
      user_id: '',
      subCat_name: '',
      obCat_id: '',      
      obSubCat_id: '', 
      status: '' ,  
    }
    this.schoolModel.obCat_id = this.route.snapshot.params.id;
    this.getObSubCategories();
    modalOpen_add();
    modalOpen_edit();
    modalClose_interventions();
    this.getObservationCategoryName();
  }
  getObservationCategoryName() {
    this.loaderShow = true;
    this.schoolModel.user_id = localStorage.getItem('admin_id');
    this.schoolModel.user_type = 'admin';
    this.accountService.getObservationCategoryName(this.schoolModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
        this.observation_name = response.observation_catName;
        this.noRecords = false;
      }else{
        this.noRecords = true;
      }
      this.loaderShow = false;
    })
  }
  add_btn(){
    this.schoolModel.subCat_name = '';
    this.schoolModel.status = '1';
  }
  update_btn(category_name, ob_id, status, id){
    console.log('enter here with category name', category_name);
    console.log('enter here with unique id', ob_id);
    console.log('enter here with status', status);
    this.schoolModel.subCat_name = category_name;
    this.schoolModel.unique_id = ob_id;
    this.schoolModel.status =status;
    this.schoolModel.obSubCat_id = id;
    if(status == 1){ this.checkStatus1 = true }else{ this.checkStatus0 = true }   
  }

  updateFunction(){
    var valid = true;
    this.addValidationError.subCat_name = '';
    if (this.schoolModel.subCat_name.trim() == "") {
        this.addValidationError.subCat_name = 'Please enter category name';
        valid = false;
    }
    if (valid) {  
      this.schoolModel.user_id = localStorage.getItem('admin_id');
      this.schoolModel.user_type = 'admin';    
      this.schoolModel.type = 'edit';   
      this.schoolModel.subCat_name =this.schoolModel.subCat_name.trim(); 
      this.accountService.saveOBSubCategory(this.schoolModel).then((response) => {  
        if (response.code == 200 && response.status == 'success') {        
          this.getObSubCategories();  
          this.sucMsg = true;
          this.successMessage = response.message;
          setTimeout (() => { this.sucMsg = false;  }, 3000); 
        } else {           
          this.errMsg = true;
          this.errorMessage = response.message;
          setTimeout (() => { this.errMsg = false;  }, 3000); 
        }         
        modalOpen_edit();
      })
    }
  }

  saveFunction(){
    var valid = true;
    this.addValidationError.subCat_name = '';
    if (this.schoolModel.subCat_name.trim() == "") {
        this.addValidationError.subCat_name = 'Please Sub category name';
        valid = false;
    }
    if (valid) {  
      this.schoolModel.user_id = localStorage.getItem('admin_id');
      this.schoolModel.user_type = 'admin';    
      this.schoolModel.type = 'add'; 
      this.schoolModel.subCat_name = this.schoolModel.subCat_name.trim();   
      this.accountService.saveOBSubCategory(this.schoolModel).then((response) => {
        if (response.code == 200 && response.status == 'success') {        
          this.getObSubCategories();  
          this.sucMsg = true;
          this.successMessage = response.message;
          setTimeout (() => { this.sucMsg = false;  }, 3000); 
        } else {           
          this.errMsg = true;
          this.errorMessage = response.message;
          setTimeout (() => { this.errMsg = false;  }, 3000); 
        }         
        modalOpen_add();
      })
    }
  }
  getObSubCategories() {
    this.loaderShow = true;
    this.schoolModel.page= this.currentPage;
    this.schoolModel.user_id = localStorage.getItem('admin_id');
    this.schoolModel.user_type = 'admin';
    this.accountService.getOBSubCategory(this.schoolModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
        this.ObCategories = response.result; 
        console.log('ressult shown here>>>',this.ObCategories);
        this.total = response.count;
        this.totalPages=Math.ceil( response.count/10);
        this.noRecordsSubCategory = false;
      }else{
        this.noRecordsSubCategory = true;
        this.ObCategories = []; 
        this.total = 0;
        this.totalPages=0;
      }
      this.loaderShow = false;
    })
  }

  pageChange(currentPage){
    this.currentPage=currentPage;
    if(this.schoolModel.searchKey==""){
      this.getObSubCategories();
    }else{
      this.searchFunction(this.currentPage);
    } 
  }

  counter(i: number) {
    return new Array(i);
  }

  searchFunction(currentPage){
    if(this.schoolModel.searchKey== ""){
      this.currentPage=1;
      this.getObSubCategories();
    }else{
      this.loaderShow = true;
      this.currentPage=currentPage;
      this.schoolModel.page = this.currentPage;
      this.accountService.getOBSubCategory(this.schoolModel).then((response) => {
        if (response.code == 200 && response.status == 'success') {
          this.total = response.count;
          this.totalPages=Math.ceil(response.count/10);
          this.ObCategories = response.result;  
          this.noRecords = false;                  
        }else{
          this.noRecords = true;
          this.ObCategories = []; 
          this.total = 0;
          this.totalPages=0;
        }
        this.loaderShow = false;
      })
    }
  }

  sortFunction(sortKey){
    this.key = sortKey;
    this.schoolModel.sortKey = sortKey;
    if(this.reverse === true){
      this.schoolModel.sortBy = 'asc';
    }else{
      this.schoolModel.sortBy = 'desc';
    }
    this.reverse = !this.reverse;   
    if(this.schoolModel.searchKey== ""){
      this.currentPage=1;
      this.getObSubCategories();
    }else{
      this.searchFunction(this.currentPage);
    }
  }

  changeStatusOBSubCategory(id, status){ 
    this.schoolModel.user_id = localStorage.getItem('admin_id');
    this.schoolModel.user_type = "admin";
    this.schoolModel.obSubCat_id = id;
    this.schoolModel.status = status;
    this.accountService.changeStatusOBSubCategory(this.schoolModel).then((response) => {
      this.loaderShow = false;
      if (response.code == 200 && response.status == 'success') {
        this.getObSubCategories();  
        this.sucMsg = true;
        this.successMessage = response.message;                    
        setTimeout (() => { this.sucMsg = false;  }, 3000);        
      }else{
        this.errMsg = true;
        this.errorMessage = response.message;
        setTimeout (() => { this.errMsg = false;  }, 3000); 
      }
    })  
  }

  deleteOBSubCategory(id){
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to delete!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
    }).then((result) => {
      this.loaderShow = true;
      if (result.isConfirmed) {        
        this.schoolModel.user_id = localStorage.getItem('admin_id');
        this.schoolModel.user_type = "admin";
        this.schoolModel.obSubCat_id = id;
        this.accountService.deleteOBSubCategory(this.schoolModel).then((response) => {
          this.loaderShow = false;
          if (response.code == 200 && response.status == 'success') {
            this.getObSubCategories();  
            this.sucMsg = true;
            this.successMessage = response.message;                    
            setTimeout (() => { this.sucMsg = false;  }, 3000);        
          }else{
            this.errMsg = true;
            this.errorMessage = response.message;
            setTimeout (() => { this.errMsg = false;  }, 3000); 
          }
        })
      } else if (result.isDismissed) {
        this.loaderShow = false;
        Swal.fire({  showCancelButton: false,  icon: 'success',  title: 'Your file is safe!'})
      }
    })      
  }
  link(antecedentname,uniqueid,id){
    //id=this.schoolModel.antecedentId;
    this.antedentName=antecedentname;
    this.antedentUniqueId=uniqueid;
    this.schoolModel.antecedentId=id;
    this.accountService.getInterventionCheckedList(this.schoolModel).then((response) => {
      console.log('final',response.result);
      if (response.code == 200) {  
        this.interventionsList=response.result;
      }else{
      }
    })
  }
  checkedIteams(event){
    var index = this.checkedInterventionList.map(x => {
      return x;
    }).indexOf(event.target.value);
    console.log('value of index>>',index);
    if(index==-1){
      this.checkedInterventionList.push(event.target.value);
    }else{
      this.checkedInterventionList.splice(index, 1);
    }
  }
saveInterventionsList(){
  this.schoolModel.user_id = localStorage.getItem('admin_id');
  this.schoolModel.link_interventions=this.checkedInterventionList;
  console.log('outgoing data list>>>',this.schoolModel);
  console.log('outgoing interventions list>>>',this.schoolModel.link_interventions);
  this.loaderShow = true;
  this.accountService.saveInterventionsLinks(this.schoolModel).then((response) => {
    console.log('final',response);
    if (response.code == 200) {
      modalClose_interventions();
      this.loaderShow = false;
      this.getObSubCategories();  
      this.sucMsg = true;
      this.successMessage = response.message;                    
      setTimeout (() => { this.sucMsg = false;  }, 3000);        
    }else{
      this.errMsg = true;
      this.errorMessage = response.message;
      setTimeout (() => { this.errMsg = false;  }, 3000); 
    }
  })
}
}
