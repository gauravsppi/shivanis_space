export class student_model {    
   name: string;
   phone: string;   
   email: string;
   school_id: string;
   class_id: string;
   admin_id: string;
   teacher_id: string;   
   student_sourcedId: string;   
   student_id: string;   
   image: string;   
   username: string;   
   sex: string;   
   identifier: string;   
   birthDate: string;   
   cityOfBirth: string;   
   countryOfBirthCode: string;   
   stateOfBirthAbbreviation: string;   
   parentName: string;   
   month: string;   
   observation_id: string;   
   searchKey: string;
   sortKey: string;
   sortBy: string;
   page: number;
   user_id: string;
   user_type: string;
   primaryGrade: string;
   americanIndianOrAlaskaNative: string;
   asian: string;
   blackOrAfricanAmerican: string;
   white: string;
   hispanicOrLatinoEthnicity: string;
   nativeHawaiianOrOtherPacificIslander: string;
   preferred_language: string;
   home_phone: string;
   work_phone: string;
   sms: string;
   preferred_contact_method: string;
   // For intervention category
   
}