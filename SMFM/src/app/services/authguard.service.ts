import { Injectable, Input, OnInit } from '@angular/core';
import { CanActivate } from '@angular/router';
import { ActivatedRouteSnapshot, Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AccountService } from "./account.service";

@Injectable({
  providedIn: 'root'
})

export class AuthGuardService {

  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
    private accountService: AccountService
  ) { }

  canActivate(route: ActivatedRouteSnapshot) {
    console.log("guard!");
    var admin_id = localStorage.getItem('admin_id');
    console.log('admin _id', admin_id);
    if (admin_id == null) {
      this.router.navigate([""]);
      return false
    }
    return true
  }

}