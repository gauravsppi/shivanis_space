import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { CareRecevierRoutingModule } from './care-receiver-routing.module';
import { CareRecieverManagementComponent } from './care-receiver/care-reciever-management.component';
import { PostListComponent } from './post-list/post-list.component';
import { PostDetailsComponent } from './post-details/post-details.component';
import { PostDetailsEditComponent } from './post-details-edit/post-details-edit.component';
import { AddPostComponent } from './add-post/add-post.component';
import { AddRecieverComponent } from './add-reciever/add-reciever.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { UiSwitchModule } from 'ngx-ui-switch';
import { LazyLoadImageModule } from 'ng-lazyload-image';

@NgModule({
  imports: [
    CommonModule,
    CareRecevierRoutingModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    UiSwitchModule,
    LazyLoadImageModule
  ],
  declarations: [CareRecieverManagementComponent, PostListComponent, PostDetailsComponent, PostDetailsEditComponent, AddPostComponent, AddRecieverComponent]
})
export class CareRecevierModule { }