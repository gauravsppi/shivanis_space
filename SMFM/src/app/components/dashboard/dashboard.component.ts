import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Headers, Http } from '@angular/http';

import { environment } from '../../../environments/environment';
import { AccountService } from '../../services/account.service';
import { Login_model } from '../../models/login_model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public loginModel: Login_model;
  admin_id = localStorage.getItem('admin_id');
  teachers =0;
  schools =0;
  students =0;
  classes =0;
  courses =0;
  staff =0;
  menuShow: boolean = false; 
  admin_Id = localStorage.getItem('admin_id');

  constructor(private router: Router, private accountService: AccountService, private route: ActivatedRoute, private http: Http) {  }

  ngOnInit(): void {
    this.loginModel = <Login_model>{      
      admin_id: '',
    }
    this.getGG4lAuthToken();
    this.totalTeachers(this.admin_id);
    this.totalStudents(this.admin_id);
    this.totalClasses(this.admin_id);
    this.totalSchools(this.admin_id);
    this.totalCourses(this.admin_id);
    this.totalStaff(this.admin_id);
    if(this.admin_Id == '1'){
      this.menuShow =  true;
    }
  }
  getGG4lAuthToken(){
    this.accountService.getGG4LToken().then((response) => { 
      localStorage.setItem('authToken', response.result.token);        
    })
  }
  totalTeachers(admin_id){
    this.loginModel.admin_id = admin_id;
    this.accountService.totalTeachers(this.loginModel).then((response) => {
      if(response.code == 200 && response.status == 'success'){ 
        this.teachers = response.totalTeachers;        
      }
    })
  }
  totalStudents(admin_id){
    this.loginModel.admin_id = admin_id;
    this.accountService.totalStudents (this.loginModel).then((response) => {
      if(response.code == 200 && response.status == 'success'){ 
        this.students = response.totalStudents;        
      }
    })
  }
  totalClasses(admin_id){
    this.loginModel.admin_id = admin_id;
    this.accountService.totalClasses(this.loginModel).then((response) => {
      if(response.code == 200 && response.status == 'success'){ 
        this.classes = response.totalClasses;        
      }
    })
  }
  totalSchools(admin_id){
    this.loginModel.admin_id = admin_id;
    this.accountService.totalSchools(this.loginModel).then((response) => {
      if(response.code == 200 && response.status == 'success'){ 
        this.schools = response.totalSchools;        
      }
    })
  }
  totalCourses(admin_id){
    this.loginModel.admin_id = admin_id;
    this.accountService.totalCourses(this.loginModel).then((response) => {
      if(response.code == 200 && response.status == 'success'){ 
        this.courses = response.totalCourses;        
      }
    })
  }
  totalStaff(admin_id){
    this.loginModel.admin_id = admin_id;
    this.accountService.totalStaff(this.loginModel).then((response) => {
      if(response.code == 200 && response.status == 'success'){ 
        this.staff = response.totalStaff;        
      }
    })
  }

}

