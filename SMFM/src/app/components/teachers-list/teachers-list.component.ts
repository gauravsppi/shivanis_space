import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router , RoutesRecognized, NavigationEnd } from '@angular/router';
import { Headers, Http } from '@angular/http';
import { Location } from '@angular/common';
import { filter, pairwise } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { AccountService } from '../../services/account.service';
import { teacher_model } from '../../models/teacher_model';

@Component({
  selector: 'app-teachers-list',
  templateUrl: './teachers-list.component.html',
  styleUrls: ['./teachers-list.component.scss']
})
export class TeachersListComponent implements OnInit {
  public teacherModel: teacher_model;
  teachers: [];
  school_id: '';
  school_name: '';
  school_number: '';
  school_phone: '';
  total : number= 0;
  totalPages : number= 0;
  currentPage:number=1;
  page : number =1;
  key: string = 'name'; //set default
  reverse: boolean = false;
  noRecords: boolean = false;
  loaderShow: boolean = false;
  j=0;

  sucMsg: boolean = false;
  errMsg: boolean = false;
  successMessage: string;   
  errorMessage: string; 

  

  constructor(private router: Router, private accountService: AccountService, private route: ActivatedRoute, private http: Http, private location: Location) {  }

  ngOnInit(): void {
    this.teacherModel = <teacher_model>{      
      teacher_id : '',
      school_id : '',
      name:'',
      email:'',
      phone:'',
      image:'',
      searchKey: '',
      sortKey: 'name',
      sortBy: 'asc'
    }

   
    this.getTeachers();
  }
  getTeachers() {
    this.loaderShow = true;
    this.teacherModel.page = this.currentPage;
    this.teacherModel.admin_id = localStorage.getItem('admin_id');    
    this.teacherModel.school_id = localStorage.getItem('school_id');    
    this.accountService.getTeachersList(this.teacherModel).then((response) => {
        if (response.code == 200 && response.status == 'success') {        
          this.total = response.count;
          this.totalPages=Math.ceil( response.count/10);
          this.teachers = response.result;                 
          this.school_id = response.school_id;                         
          this.school_name = response.school_name;                    
          this.school_number = response.school_number;                    
          this.school_phone = (response.school_phone == '' || response.school_phone == undefined)? '--' :  response.school_phone ; 
          this.noRecords = false;
        }else{ 
          this.noRecords = true;        
          this.total = 0
          this.totalPages=0
          this.teachers = []; 
        }
        this.loaderShow = false;
    })
  }

  pageChange(currentPage){
    this.currentPage=currentPage;
    if(this.teacherModel.searchKey==""){
      this.getTeachers();
    }else{
      this.searchFunction(this.currentPage);
    } 
  }

  counter(i: number) {
    return new Array(i);
  }

  searchFunction(currentPage){
    if(this.teacherModel.searchKey== ""){
      this.currentPage=1;
      this.getTeachers();
    }else{
      this.loaderShow = true;
      this.currentPage=currentPage;
      this.teacherModel.page = this.currentPage;
      this.teacherModel.admin_id = localStorage.getItem('admin_id');    
      this.teacherModel.school_id = localStorage.getItem('school_id');   
      this.accountService.getTeachersList(this.teacherModel).then((response) => {
        if (response.code == 200 && response.status == 'success') {
          this.total = response.count;
          this.totalPages=Math.ceil(response.count/10);
          this.teachers = response.result;
          this.noRecords = false;
        }else{
          this.noRecords = true;
          this.total = 0;
          this.totalPages=0;
          this.teachers = [];
        } 
        this.loaderShow = false;     
      })
    }
  }

  sortFunction(sortKey){
    this.key = sortKey;
    this.teacherModel.sortKey = sortKey;
    if(this.reverse === true){
      this.teacherModel.sortBy = 'asc';
    }else{
      this.teacherModel.sortBy = 'desc';
    }
    this.reverse = !this.reverse;   
    if(this.teacherModel.searchKey== ""){
      this.currentPage=1;
      this.getTeachers();
    }else{
      this.searchFunction(this.currentPage);
    }
  }


  getTeacherId(teacher_id){
    this.teacherModel.teacher_id = teacher_id;
    this.accountService.getTeacherInfo(this.teacherModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
         this.teacherModel.name = response.result[0].name;     
         if(response.result[0].image != null){
          this.teacherModel.image = environment.teacherImagePath + response.result[0].image;                    
         }else{
          this.teacherModel.image = environment.teacherImagePath + 'teacher.png';
         }  
         this.teacherModel.email = response.result[0].email;                    
         this.teacherModel.phone = (response.result[0].phone == '' || response.result[0].phone == undefined || response.result[0].phone == 'undefined')? '--' : response.result[0].phone ;                    
         this.teacherModel.sex = response.result[0].sex;                    
         this.teacherModel.identifier = response.result[0].identifier;                    
         this.teacherModel.birthDate = response.result[0].birthDate;                    
         this.teacherModel.cityOfBirth = response.result[0].cityOfBirth;                    
         this.teacherModel.countryOfBirthCode = response.result[0].countryOfBirthCode;                    
         this.teacherModel.stateOfBirthAbbreviation = response.result[0].stateOfBirthAbbreviation;                    
         this.teacherModel.username = response.result[0].username;                    
      }
    })
  }

  blockTeacher(teacher_id){
    this.teacherModel.admin_id = localStorage.getItem('admin_id');
    this.teacherModel.teacher_id = teacher_id;
    this.accountService.blockTeacherByID(this.teacherModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
          this.sucMsg= true;  
          this.successMessage = response.message;
          setTimeout (() => { this.sucMsg = false; }, 2000); 
          this.getTeachers();       
      }else{
          this.errMsg= false;  
          this.errorMessage = response.message; 
          setTimeout (() => { this.errMsg = false; }, 2000);       
      }
    })
  }
}
