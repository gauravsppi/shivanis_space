var dateFormat = require('dateformat'); //for using date format
const current_datetime = dateFormat(new Date().toLocaleString('en-US'),"yyyy-mm-dd hh:MM:ss"); //get current date and time
const md5 = require('md5'); //for encrypt passwords

const mongoose = require('mongoose');
var userSchema = require('../schema/usersSchema');
var usersDB = mongoose.model('users', userSchema);
// User cars Table
var userCarsSchema = require('../schema/userCarsSchema');
var userCarsTable = mongoose.model('userCars', userCarsSchema);

// Contact Table
var contactSchema = require('../schema/contactSchema');
var contactTable = mongoose.model('contact', contactSchema);

var multiCarCompanyModelSchema = require('../schema/multiCarCompanyModelSchema');
var multiCarCompanyModelDB = mongoose.model('multiCarCompanyModel', multiCarCompanyModelSchema);

// Parts Table
var partsSchema = require('../schema/partsSchema');
var partsDB = mongoose.model('parts', partsSchema);

var userCartItemsSchema = require('../schema/userCartItemsSchema');
var userCart = mongoose.model('userCartItems', userCartItemsSchema);

module.exports = {
    checkEmailAlradyExit:function(con, data, callback) { // for check email exists or not
        usersDB.find({ 'email': data.email }, callback)
    },
    checkIDExits:function(con, data, callback) { // for check email exists or not
        usersDB.find({ '_id': data.user_id }, callback)
    },
    userAdd: function(con, data, callback) { //for user signup
        var User = new usersDB();
        User.name = data.name;
        User.email = data.email;
        User.password = md5(data.password);
        User.postal = '';
        User.state = '';
        User.country = '';
        User.address = '';
        User.status =  '1';         
        User.image =  ''; 
        User.otp =  ''; 
        User.device_token = data.device_token;    
        User.device_id = data.device_id; 
        User.created_at = current_datetime;    
        User.save(callback);
    },
    userLogin: function(con, data, callback) { //for user login 
        var password = md5(data.password);
        usersDB.find({ 'email': data.email, 'password': password }, callback)
    },
    updateDeviceToken:function(con, data, result, callback) { //Update token after login
        var myquery = {  _id: result[0]._id  };
        var newvalues = { $set: 
            { 
                device_token: data.device_token, 
                device_id: data.device_id
            } 
        };
        usersDB.updateOne(myquery, newvalues, callback)
    },
    updateUserInfo:function(con, data, callback) { //Update User info
        var myquery = {  _id: data.user_id  };
        var newvalues = { $set: 
            { 
                name: data.name, 
                image: data.image, 
                address: data.address, 
                state: data.state, 
                country: data.country, 
                postal: data.postal, 
            } 
        };
        usersDB.updateOne(myquery, newvalues, callback)
    }, 
    forgotPassword:function(con, otp, user_id, callback){
        var myquery = {  _id: user_id  };
        var newvalues = { $set: 
            { 
                otp: otp
            } 
        };
        usersDB.updateOne(myquery, newvalues, callback)
    },
    otpVerify: function(con, data, callback) { 
        usersDB.find({ 'otp': data.otp, 'email': data.email }, callback)
    },
    resetPassword:function(con, data, user_id, callback){
        var password = md5(data.password);
        var myquery = {  _id: user_id  };
        var newvalues = { $set: 
            { 
                password: password
            } 
        };
        usersDB.updateOne(myquery, newvalues, callback)
    },   
    changePassword:function(con, data, user_id, callback){
        var password = md5(data.password);
        var myquery = {  _id: user_id  };
        var newvalues = { $set: 
            { 
                password: password
            } 
        };
        usersDB.updateOne(myquery, newvalues, callback)
    },
    addUserCar: function(con, data, callback) { //Add user car details
        console.log('DB data: ', data);
        var UserCarsTable = new userCarsTable();
        UserCarsTable.user_id = data.user_id;
        UserCarsTable.company_id = data.company_id;
        UserCarsTable.model_id = data.model_id;
        UserCarsTable.color = data.color;
        UserCarsTable.year = data.year;
        UserCarsTable.images = data.images;
        UserCarsTable.description = data.description;
        UserCarsTable.created_at = current_datetime;    
        UserCarsTable.save(callback);
    },
    getUserCars:function(con, data, callback) { // Get user added car
        userCarsTable.find({ 'user_id': data.user_id }).populate([ {path:'company_details', select:'_id name'},{path:'models_details', select:'_id model_name'} ]).exec(callback)   
    },
    updateUserCar:function(con, data, callback){
        var myquery = {  _id: data.car_id  };
        var newvalues = { $set: 
            { 
                company_id: data.company_id,
                model_id: data.model_id,
                color: data.color,
                year: data.year,
                description: data.description,
                images: data.images
            } 
        };
        userCarsTable.updateOne(myquery, newvalues, callback)
    },
    deleteUserCar : function(con, data, callback) { //delete user car
        userCarsTable.findByIdAndRemove({ '_id': data.car_id, 'user_id': data.user_id }, callback)
    }, 
    saveContactForm: function(con, data, callback) { //save Contact Form
        var ContactTable = new contactTable();
        ContactTable.user_id = data.user_id;
        ContactTable.subject = data.subject;
        ContactTable.query = data.query;
        ContactTable.created_at = current_datetime;    
        ContactTable.save(callback);
    },
    getCompanyAndModelDetails: function(con, data, callback) { // Get user added car
        userCarsTable.find({ 'user_id': data.user_id,'_id':data.car_id }).exec(callback)   
    },
    getRecordsCompanyModelBased: function(con,result,callback){
        multiCarCompanyModelDB.find({ company_id:result[0].company_id, model_id:result[0].model_id, status:1 }, callback)
    },
    getFilteredRecords: function(con,results,callback){
       var arr=[]; k=1;
       for (var i = 0; i < results.length; i++) {
           var  arr1=results[i];
           partsDB.find({ _id:arr1.add_part_id, }).exec(function(err, result){
             Array.prototype.push.apply(arr,result)
             if (k==results.length) {
                 callback('',arr);
             }
             k++;
         });
       }
    },
    getAllPartsRecords: function(con, data, callback) { // Get user added car
    partsDB.find({ 'status': 1}).exec(callback)   
    },
    addtoCart: function(con, data, callback) { //for user signup
    console.log('current param',data)
    var userCartvalue = new userCart();
    userCartvalue.user_id = data.user_id;
    userCartvalue.part_id = data.part_id; 
    userCartvalue.car_id = data.car_id; 
    userCartvalue.status = '1'; 
    userCartvalue.created_at = current_datetime;    
    userCartvalue.save(callback);
    },
    // getUserCartDetails: function(con, data, callback) { // Get user added car
    //     userCart.find({ 'user_id': data.user_id,'status':1 }).exec(callback)   
    // },
    deleteCartAddedParts: function(con, data, callback) { // Get user added car
     userCart.findByIdAndRemove({ '_id': data.id, 'user_id': data.user_id }, callback) 
    },
    // getUserCartDetails: function(con, data, callback) {
    // userCart.find({}).populate({path: 'parts', select: '_id car_name part_image link'}).
    // exec(callback)
    // }, 
     getUserCartDetails:function(con, data, callback) { //view part
        userCart.find({ user_id : data.user_id })
        .populate({path: 'parts_details', select: '_id car_name part_image link'}).exec(callback)
     },
    // getUserCartDetails:function(con, data, callback) { // Get user added car
    //     userCart.find({ 'user_id': data.user_id }).
    //     populate([ {path:'parts', select:'_id car_name part_image link'},
    //         {path:'models_details', select:'_id model_name'} ]).exec(callback)   
    // },

}
