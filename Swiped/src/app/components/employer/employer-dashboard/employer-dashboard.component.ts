import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from '../../../services/account.service';
import { DataService } from '../../../services/data.service';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2'
import { employer_model } from '../../../models/employer_model';

import Chart from 'chart.js/auto';
@Component({
  selector: 'app-employer-dashboard',
  templateUrl: './employer-dashboard.component.html',
  styleUrls: ['./employer-dashboard.component.scss']
})
export class EmployerDashboardComponent implements OnInit {

  public loginModel: employer_model;
  public addValidationError: employer_model;
  public headers = new Headers();
  errMsg:boolean=false;
  message:string; 
  employerlist:any =[] ;  
  getId:any;
  employerId:any;
  // letsgoTabdata:any [] ;
  // degreeTabdata:any [] ;
  // currentjobTabdata:any [] ;
  // interestsTabdata:any [] ;
  
  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService, private spinner:NgxSpinnerService, private data:DataService) { }


  ngOnInit(): void {
    var ctx = document.getElementById('myChart');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['1', '2', '3', '4', '5', '6', '7'],
            datasets: [{
                label: 'Progress',
                data: [20, 10, 18, 9, 28, 7, 15],
                backgroundColor: [
                    '#d0021b',
                ],
            
                borderColor: [
                  '#d0021b'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true,
                    label: 'Views',
                }
            }
        }
    });

    this.route.paramMap.subscribe(params => {
      this.getId = params.get('id');
      });

    this.loginModel = <employer_model>{
      id: '',
      name:''
    }
    this.addValidationError = <employer_model>{
      id: '',
      name:''
      
    }
    
    this.getEmployerbyId();
  }

  //-----------------------get employer details API------------------------
  getEmployerbyId(){
    this.data.changeStatus(true);
    this.loginModel.id = this.getId;
    this.spinner.show();
    this.accountService.getEmployerWithID(this.loginModel).then((response) => {
      console.log(response);
      if (response.code == 200) {
        console.log(response);
        this.employerId=response.result._id;
        this.spinner.hide();
        this.employerlist=response.result;
        console.log(this.employerlist);
      } else {
        this.errMsg = true;
        this.message = "No Data Found!!!";
      }
    })
  }

}
