import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReligionManagementComponent } from './religion-management.component';

describe('ReligionManagementComponent', () => {
  let component: ReligionManagementComponent;
  let fixture: ComponentFixture<ReligionManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReligionManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReligionManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
