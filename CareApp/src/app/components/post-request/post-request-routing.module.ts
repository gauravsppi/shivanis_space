import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompleteComponent } from './complete/complete.component';
import { InProgressComponent } from './in-progress/in-progress.component';
import { NewRequestComponent } from './new-request/new-request.component';
import { PendingComponent } from './pending/pending.component';
import { NewPostDetailsComponent } from './new-post-details/new-post-details.component';
import { PendingPostDetailsComponent } from './pending-post-details/pending-post-details.component';
import { CompletedPostDetailsComponent } from './completed-post-details/completed-post-details.component';




const routes: Routes = [
  {
    path: 'new-request',
    component: NewRequestComponent
  },
  {
    path: 'pending',
    component: PendingComponent
  },
  {
    path: 'complete',
    component: CompleteComponent
  },
  {
    path: 'in-progress',
    component: InProgressComponent
  },
  {
    path: 'new-request/new-post-detail/:postId/:receiverId',
    component: NewPostDetailsComponent
  },

  {
    path: 'pending/pending-post-detail/:postId/:receiverId',
    component: PendingPostDetailsComponent
  },

  {
    path: 'complete/complete-post-detail/:postId/:receiverId',
    component: CompletedPostDetailsComponent
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostRequestRoutingModule { }