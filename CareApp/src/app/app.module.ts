import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { HttpModule  } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { FooterComponent } from './components/layout/footer/footer.component';
import { SidebarComponent } from './components/layout/sidebar/sidebar.component';
// import { LoginComponent } from './components/login/login.component';
import { LightboxModule } from 'ngx-lightbox';
import { LoaderComponent } from './components/loader/loader.component';
import { AdminComponent } from './components/admin/admin.component';
// import { ManageAdminComponent } from './components/manage-admin/manage-admin.component';
// import { CareWorkerManagementComponent } from './components/care-worker-management/care-worker-management.component';
// import { CareRecieverManagementComponent } from './components/care-reciever-management/care-reciever-management.component';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { ReligionManagementComponent } from './components/religion-management/religion-management.component';
import { AboutIslamComponent } from './components/about-islam/about-islam.component';
import { QuestionAnswerComponent } from './components/question-answer/question-answer.component';
import { AddQuesAnsComponent } from './components/add-ques-ans/add-ques-ans.component';
// import { CreateSubAdminComponent } from './components/create-sub-admin/create-sub-admin.component';
// import { PostListComponent } from './components/post-list/post-list.component';
import { AssignWorkersComponent } from './components/assign-workers/assign-workers.component';
// import { AddRecieverComponent } from './components/add-reciever/add-reciever.component';
// import { AddWorkerComponent } from './components/add-worker/add-worker.component';
// import { AddPostComponent } from './components/add-post/add-post.component';
// import { InProgressComponent } from './components/in-progress/in-progress.component';
// import { NewRequestComponent } from './components/new-request/new-request.component';
// import { CompleteComponent } from './components/complete/complete.component';
// import { PostDetailsComponent } from './components/post-details/post-details.component';
// import { PostDetailsEditComponent } from './components/post-details-edit/post-details-edit.component';
// import { PendingComponent } from './components/pending/pending.component';
// import { PostDetailComponent } from './components/post-detail/post-detail.component';
// import { AdminProfileComponent } from './components/admin-profile/admin-profile.component';
import { MainLayoutComponent } from './components/layout/main-layout/main-layout.component';
import { NotfoundComponent } from './components/notfound/notfound.component';
import { SharedModule } from './shared/shared.module';
import { JwtInterceptor } from '../app/shared/http-interceptor/interceptor';
import { ErrorInterceptor } from '../app/shared/http-interceptor/error.interceptor';
import { NgxSpinnerService } from "../app/shared/services/spinner.service";



@NgModule({
  declarations: [
    AppComponent,
    // DashboardComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    // LoginComponent,
    LoaderComponent,
    AdminComponent,
    // ManageAdminComponent,
    // CareWorkerManagementComponent,
    // CareRecieverManagementComponent,
    ReligionManagementComponent,
    AboutIslamComponent,
    QuestionAnswerComponent,
    AddQuesAnsComponent,
    // CreateSubAdminComponent,
    // PostListComponent,
    AssignWorkersComponent,
    // AddRecieverComponent,
    // AddWorkerComponent,
    // AddPostComponent,
    // InProgressComponent,
    // NewRequestComponent,
    // CompleteComponent,
    // PostDetailsComponent,
    // PostDetailsEditComponent,
    // PendingComponent,
    // PostDetailComponent,
    // AdminProfileComponent,

    MainLayoutComponent,
    NotfoundComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LightboxModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpModule,
    SharedModule
  ],
  providers: [
    NgxSpinnerService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
		{ provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
