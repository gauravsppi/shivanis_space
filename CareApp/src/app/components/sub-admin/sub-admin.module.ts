import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';

import { SubAdminRoutingModule } from './sub-admin-routing.module';
import { CreateSubAdminComponent } from './create-sub-admin/create-sub-admin.component';
import { ManageAdminComponent } from './manage-admin/manage-admin.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    SubAdminRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule
  ],
  declarations: [CreateSubAdminComponent, ManageAdminComponent]
})
export class SubAdminModule { }