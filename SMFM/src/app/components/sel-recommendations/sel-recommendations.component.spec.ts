import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelRecommendationsComponent } from './sel-recommendations.component';

describe('SelRecommendationsComponent', () => {
  let component: SelRecommendationsComponent;
  let fixture: ComponentFixture<SelRecommendationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelRecommendationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelRecommendationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
