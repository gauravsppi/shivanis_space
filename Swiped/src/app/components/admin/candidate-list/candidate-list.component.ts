import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router,NavigationEnd  } from '@angular/router';
import { AccountService } from '../../../services/account.service';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import { admin_model } from '../../../models/admin_model';
import Swal from 'sweetalert2'
import { NgxSpinnerService } from "ngx-spinner";



@Component({
  selector: 'app-candidate-list',
  templateUrl: './candidate-list.component.html',
  styleUrls: ['./candidate-list.component.scss']
})
export class CandidateListComponent implements OnInit {
  public adminModel: admin_model;
  public addValidationError: admin_model;
  public headers = new Headers();
  errMsg:boolean=false;
  message:string;
  candidatelist:any = [];
  getType:any;
  mySubscription: any;
  mainheading : string;

  page = 1;
  count : number;
  pageSize:number = 10;

  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService, private spinner:NgxSpinnerService) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    this.mySubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
      }
    });
   }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.getType = params.get('type');
    });

    if(this.getType=="all"){
      this.mainheading="All Candidates";
    }else if(this.getType=="new"){
      this.mainheading="New Candidates";
    }else if(this.getType=="approved"){
      this.mainheading="Approved Candidates";
    }else if(this.getType=="rejected"){
      this.mainheading="Rejected Candidates";
    }
    this.adminModel = <admin_model>{
      id: ''
    }
    this.addValidationError = <admin_model>{
      id: '',
      admin_password: ''
    }
    this.getCandidatedetails();
  }
  getCandidatedetails(){
    this.adminModel.typeStatus =  this.getType;
    debugger;
    this.spinner.show();
    this.accountService.Candidatesget(this.adminModel).then((response) => {
      debugger;
      if (response.code == 200) {
        this.spinner.hide();
        this.candidatelist=response.result;
        this.count= response.result.length;
        console.log(this.count);
      } else {
        this.errMsg = true;
        this.message = "No Data Found!!!";
      }
    })
  }

  //-------------pagination------------
  onTableDataChange(event){
    this.page = event;
    this.getCandidatedetails();
  } 

  //----------sorting-------------
    key: string = '';
    reverse: boolean = false;
    sort(key){
      this.key = key;
      this.reverse = !this.reverse;
    }

  
  // ----------- Change Status API -----
  changeStatus(id,status){
    this.adminModel.id=id;
    this.adminModel.status=status;
     Swal.fire({
    title: 'Are you sure you want to change status?',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, change the status!',
    cancelButtonText: 'No, keep it',   
    confirmButtonColor:'#D0021B',
    cancelButtonColor: '#D0021B',
  
  }).then((result) => {
    if (result.isConfirmed) {
  this.accountService.CandidateStatus(this.adminModel).then((response) => {
    if (response.code == 200) {
      Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B' , icon: 'success', title: 'Status changed successfully' })
        this.getCandidatedetails();
    } else {
      //this.errMsg = true;
      //this.message = response.message;
    }
  })
} else if (result.isDismissed) {
  Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B', icon: 'success', title: 'Your data is same as before!' })
}
})
  }
  deleteCandidate(id){
    this.adminModel.id=id;
    Swal.fire({
      title: 'Are you sure you want to delete it?',
      text: 'You will not be able to recover this data!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, move it to trash!',
      cancelButtonText: 'No, keep it',
      confirmButtonColor:'#D0021B',
      cancelButtonColor: '#D0021B',
    }).then((result) => {
      if (result.isConfirmed) {
        this.accountService.Statustrash(this.adminModel).then((response) => {
          if (response.code == 200) {
            //this.sucMsg = true;
            Swal.fire({ showCancelButton: false, confirmButtonColor: '#D0021B', icon: 'success', title: 'Saved successfully in trash' })
            this.getCandidatedetails();
          } else {
            //this.errMsg = true;
            //this.errmessage = 'Something went wrong';
          }
        })
      } else if (result.isDismissed) {
        Swal.fire({ showCancelButton: false ,confirmButtonColor: '#D0021B', icon: 'success', title: 'Your data is safe!' })
      }
    })
  }

  //------------- view candidate details  --------------------
  viewCandidate(id){
    this.router.navigate(['/admin/candidate_detail/'+id]);
  }
}
