import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from '../../../services/account.service';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import { candidateLogin_model } from '../../../models/candidateLogin_model';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  public loginModel: candidateLogin_model;
  public addValidationError: candidateLogin_model;
  sucMsg: boolean = false;
  errMsg: boolean = false;
  message: string;
  isTextFieldType1 : boolean = false ;
  isTextFieldType: boolean = false;
  ischecked: boolean = false;
  marked: boolean = false;
  validationname: boolean = false;
  validationemail: boolean = false;
  validationpassword: boolean = false;
  validationcountrylist: boolean = false;
  validationnumber: boolean = false;
  validationlocation: boolean = false;
  validationwebsite:boolean= false;
  buttonDisable:boolean=false;
  selectedOption;
  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService) { 
    this.selectedOption = this.countryCode[1];
    console.log(this.selectedOption);
  }

  ngOnInit(): void {
    this.loginModel = <candidateLogin_model>{
    name:'',
    email:'',
    number:'',
    locations:'',
    password:'',
    signuprole:'',
    company_website:'',
    countrylist:[],
    }
    this.addValidationError = <candidateLogin_model>{
      name: '',
      email: '',
      number: '',
      locations: '',
      password: '',
      candidate_checked:'',
      company_website:''
    }
    //localStorage.clear();
  }
  countryCode = [
    '+44',
    '+11',
  ];
  selected ='+44';
  togglePasswordFieldType() {
    console.log(this.isTextFieldType);
    this.isTextFieldType = !this.isTextFieldType;
  }
  togglePasswordFieldType1(){
    this.isTextFieldType1 = !this.isTextFieldType1;
  }s
  toggleVisibility(e){
    this.marked= e.target.checked;
    console.log(e.target.checked);
  }
  
  emailvalidation(){
    var EMAIL_REGEXP = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
   

    if (this.loginModel.email=="") {
          this.validationemail=true;
        }else{
            this.validationemail=false;
    }

    if (EMAIL_REGEXP.test(this.loginModel.email)===false) {
          this.validationemail=true;
        }else{
          this.validationemail=false;
        }      
    }

    passwordvalidation(){
         if (this.loginModel.password == "") {
             this.validationpassword=true;
            }else{
             this.validationpassword=false;
          }
    }

    locationvalidation(){
         if (this.loginModel.locations == "") {
                this.validationlocation=true;
            }else{
                this.validationlocation=false;
              }
    }

    companyvalidation(){
       if (this.loginModel.company_website == "") {
      this.validationwebsite=true;
    }else{
      this.validationwebsite=false;
    }
 }



  keyPresscontact(event: any) {
    debugger;
    // if (this.loginModel.countrylist != "") {
    //   this.validationcountrylist=false;
    // }else{
    //   this.validationcountrylist=true;
    // } 
    if (this.loginModel.number != "") {
      this.validationnumber=false;
    }else{
      this.validationnumber=true;
    } 
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
    event.preventDefault();
    
    }
    }

    keyPressAlpha(event: any) {
      debugger;
      if (this.loginModel.name != "") {
        this.validationname=false;
      }else{
        this.validationname=true;
      }
      const pattern = /[a-zA-Z ]/;
      let inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
      
      }
      }
  login() {
    var valid = true;
    var EMAIL_REGEXP = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
    var NAME_REGEXP = /^[A-Za-z ]+$/;
    this.addValidationError.name = '';
    this.addValidationError.email = '';
    this.addValidationError.password = '';
    this.addValidationError.number = '';
    this.addValidationError.locations = '';

    if (this.loginModel.name == "") {
      this.validationname=true;
      valid = false;
    }
    if (this.loginModel.email == "") {
      this.validationemail=true;
      valid = false;
    }
    if (EMAIL_REGEXP.test(this.loginModel.email)===false) {
      this.validationemail=true;
      valid = false;
    }
    if (this.loginModel.password == "") {
      this.validationpassword=true;
      valid = false;
    }
    // if (this.loginModel.countrylist == "") {
    //   this.validationcountrylist=true;
    //   valid = false;
    // }
    if (this.loginModel.number == "") {
      this.validationnumber=true;
      valid = false;
    }
    if (this.loginModel.locations == "") {
      this.validationlocation=true;
      valid = false;
    }
    if(this.ischecked==false){
      this.addValidationError.candidate_checked = '* Please Agree With our Term & Policy';
      valid = false;
    }else{
      this.addValidationError.candidate_checked = '';
    }
    if (valid) {
      this.buttonDisable=true;
      this.loginModel.signuprole="candidate";
      this.accountService.SignupCandidate(this.loginModel).then((response) => {
        if (response.code == 200) {
          localStorage.setItem('token',response.result);
          this.router.navigate(['/verify_mail']);
        } else {
          this.buttonDisable=false;
          this.errMsg = true;
          this.message = response.message;
        }
      })
    }
  }
  // -------- Employer Sign Up ----------
  loginemployer(){
    console.log("enter employer");
    var valid = true;
    var EMAIL_REGEXP = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
    var NAME_REGEXP = /^[A-Za-z ]+$/;
    this.addValidationError.name = '';
    this.addValidationError.email = '';
    this.addValidationError.password = '';
    this.addValidationError.number = '';
    this.addValidationError.locations = '';
    if (this.loginModel.name == "") {
      this.validationname=true;
      valid = false;
    }
    if (this.loginModel.email == "") {
      this.validationemail=true;
      valid = false;
    }
    if (EMAIL_REGEXP.test(this.loginModel.email)===false) {
      this.validationemail=true;
      valid = false;
    }
    if (this.loginModel.password == "") {
      this.validationpassword=true;
      valid = false;
    }
    if (this.loginModel.number == "") {
      this.validationnumber=true;
      valid = false;
    }
    if (this.loginModel.locations == "") {
      this.validationlocation=true;
      valid = false;
    }
    if(this.ischecked==false){
      this.addValidationError.candidate_checked = '* Please Agree With our Term & Policy';
      valid = false;
    }else{
      this.addValidationError.candidate_checked = '';
    }
    if (valid) {
      this.buttonDisable=true;
      this.loginModel.signuprole="employer";
      console.log(this.loginModel);
      this.accountService.SignupCandidate(this.loginModel).then((response) => {
        if (response.code == 200) {
          localStorage.setItem('token',response.result);
          this.router.navigate(['/choose_plan']);
        }else{
          this.buttonDisable=false;
          this.errMsg = true;
          this.message = response.message;
        }
      })
    }
  }
}
