import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployerSignupLoginService } from '../../../services/employer-signup-login/employer-signup-login.service';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import { employer_model } from '../../../models/employer_model';

@Component({
  selector: 'app-employer-welcome',
  templateUrl: './employer-welcome.component.html',
  styleUrls: ['./employer-welcome.component.scss']
})
export class EmployerWelcomeComponent implements OnInit {
  public employerModel: employer_model;
  public addValidationError: employer_model;
  sucMsg: boolean = false;
  errMsg: boolean = false;
  message: string;
  idGet: any;
  constructor(private accountService: EmployerSignupLoginService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService) { }

  ngOnInit(): void {
    this.employerModel = <employer_model>{
      id:'',
    }
    this.route.paramMap.subscribe(params => {
      this.idGet = params.get('id');
      if(this.idGet=='' || this.idGet==null){

      }else{
        this.employerModel.id = this.idGet;
        this.accountService.updateStatusOfScreen(this.employerModel).then((response) => {
          if (response.code == 200) {
            localStorage.setItem('token', response.token);
          }else{
            this.router.navigate(['/employer-login']);
          }
        })
      }
    });
  }
  // ngOnDestroy() {
  //   if (this.id) {
  //     clearInterval(this.id);
  //   }
  // }
 //------------------------- after email verify sign in button------------------
 signin(){
  this.router.navigate(['/employer-profile/'+this.idGet]);
}
}
