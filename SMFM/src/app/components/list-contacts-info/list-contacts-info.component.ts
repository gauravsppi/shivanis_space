import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Headers, Http } from '@angular/http';
import Swal  from 'sweetalert2';

import { AccountService } from '../../services/account.service';

import { Login_model } from '../../models/login_model';

@Component({
  selector: 'app-list-contacts-info',
  templateUrl: './list-contacts-info.component.html',
  styleUrls: ['./list-contacts-info.component.scss']
})
export class ListContactsInfoComponent implements OnInit {

  public loginModel: Login_model;

  sucMsg: boolean =  false; 
  successMessage: string='';
  errMsg: boolean = false; 
  errorMessage: string=''; 
  noRecords: boolean = false;

  total : number= 0;
  totalPages : number= 0;
  currentPage:number=1;
  page : number =1;
  key: string = 'title'; //set default
  reverse: boolean = false; 
  loaderShow: boolean = false; 

  contacts = [];
  view_contacts = [];
  view_contacts_info = [];

  constructor(private router: Router, private accountService: AccountService, private route: ActivatedRoute, private http: Http) { }

  ngOnInit(): void {
    this.loginModel = <Login_model>{
      admin_id: '',
      searchKey: '',
      sortKey: 'id',
      sortBy: 'desc',     
    }
    this.getContacts();
    
  }
  getContacts() {
    this.loaderShow = true;
    this.loginModel.page= this.currentPage;
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    this.accountService.getContactInfo(this.loginModel).then((response) => {
      console.log('List contacts:: ', response);
      if (response.code == 200 && response.status == 'success') {
        this.contacts = response.result; 
        this.total = response.count;
        this.totalPages=Math.ceil( response.count/10);
        this.noRecords = false;
      }else{
        this.noRecords = true;
        this.contacts = []; 
        this.total = 0;
        this.totalPages=0;
      }
      this.loaderShow = false;
    })
  }

  pageChange(currentPage){
    this.currentPage=currentPage;
    if(this.loginModel.searchKey==""){
      this.getContacts();
    }else{
      this.searchFunction(this.currentPage);
    } 
  }

  counter(i: number) {
    return new Array(i);
  }

  searchFunction(currentPage){
    if(this.loginModel.searchKey== ""){
      this.currentPage=1;
      this.getContacts();
    }else{
      this.loaderShow = true;
      this.currentPage=currentPage;
      this.loginModel.page = this.currentPage;
      this.accountService.getContactInfo(this.loginModel).then((response) => {
        if (response.code == 200 && response.status == 'success') {
          this.total = response.count;
          this.totalPages=Math.ceil(response.count/10);
          this.contacts = response.result;  
          this.noRecords = false;                  
        }else{
          this.noRecords = true;
          this.contacts = []; 
          this.total = 0;
          this.totalPages=0;
        }
        this.loaderShow = false;
      })
    }
  }

  sortFunction(sortKey){
    this.key = sortKey;
    this.loginModel.sortKey = sortKey;
    if(this.reverse === true){
      this.loginModel.sortBy = 'asc';
    }else{
      this.loginModel.sortBy = 'desc';
    }
    this.reverse = !this.reverse;   
    if(this.loginModel.searchKey== ""){
      this.currentPage=1;
      this.getContacts();
    }else{
      this.searchFunction(this.currentPage);
    }
  }

  ViewContactInfo(id) {
    this.loaderShow = true;
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    this.loginModel.contact_id = id;
    this.accountService.viewContactInfo(this.loginModel).then((response) => {
      this.loaderShow = false;
      console.log('view contact:: ', response);
      if (response.code == 200 && response.status == 'success') {
        this.view_contacts = response.result; 
        this.view_contacts_info = response.result[1]; 
        this.noRecords = false;
      }else{
        this.noRecords = true;
        this.view_contacts = []; 
      }      
    })
  }

  deleteContactInfo(id){
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to delete!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
    }).then((result) => {
      this.loaderShow = true;
      if (result.isConfirmed) {        
        this.loginModel.admin_id = localStorage.getItem('admin_id'); 
        this.loginModel.contact_id = id;
        this.accountService.deleteContactInfo(this.loginModel).then((response) => {
          this.loaderShow = false;
          if (response.code == 200 && response.status == 'success') {
            this.getContacts();  
            this.sucMsg = true;
            this.successMessage = response.message;                    
            setTimeout (() => { this.sucMsg = false;  }, 3000);        
          }else{
            this.errMsg = true;
            this.errorMessage = response.message;
            setTimeout (() => { this.errMsg = false;  }, 3000); 
          }
        })
      } else if (result.isDismissed) {
        this.loaderShow = false;
        Swal.fire({  showCancelButton: false,  icon: 'success',  title: 'Your file is safe!'})
      }
    })      
  }
}
