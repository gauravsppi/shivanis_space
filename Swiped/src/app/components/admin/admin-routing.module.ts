import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import { CandidateListComponent } from './candidate-list/candidate-list.component';
import { CandidateDetailComponent } from './candidate-detail/candidate-detail.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ShowPackageComponent } from './show-package/show-package.component';
import { EditPackageComponent } from './edit-package/edit-package.component';
import { EmployerListComponent } from './employer-list/employer-list.component';
import { EmployerDetailComponent } from './employer-detail/employer-detail.component';
import { ViewCandidateComponent } from './view-candidate/view-candidate.component';
import { ViewPostComponent } from './view-post/view-post.component';


const routes: Routes = [
  {
    path: '',
    component: AdminLoginComponent
  },
  {
    path: 'candidates/:type',
    component: CandidateListComponent
  },
  {
    path: 'candidate_detail/:id',
    component: CandidateDetailComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'show_package',
    component: ShowPackageComponent
  },
   {
    path: 'edit_package',
    component: EditPackageComponent
  },
  {
    path: 'employers/:type',
    component: EmployerListComponent
  },
  {
    path: 'employer_detail/:id',
    component: EmployerDetailComponent
  },
  {
    path: 'edit_package/:id',
    component: EditPackageComponent
  },
  {
    path: 'view_candidate/:id',
    component: ViewCandidateComponent
  },
  {
    path: 'view_post/:id',
    component: ViewPostComponent
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminsRoutingModule { }