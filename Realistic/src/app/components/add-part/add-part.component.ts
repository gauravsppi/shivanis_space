import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from '../../services/account.service';
import { Headers, Http } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { Login_model } from '../../models/login_model';
import { Validation_model } from '../../models/Validation_model';

@Component({
  selector: 'app-add-part',
  templateUrl: './add-part.component.html',
  styleUrls: ['./add-part.component.scss']
})
export class AddPartComponent implements OnInit {

  public loginModel: Login_model;
  public addValidationError: Login_model;
  private headers = new Headers();
  
  record: any = [];
  companyNames: any = [];
  toDoList: any = [];
  models: any = [];
  models_name: any = [];
  total: number = 0;
  loaderShow: boolean = false;
  sucMsg: boolean = false;
  errMsg: boolean = false;
  successMessage : string = '';
  errorMessage : string = '';
  buttonDisable: boolean=false;
  fileSelect:boolean = false;
  fileSelect_error:boolean = false;

  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: HttpClientModule) { }

  getId: string;
  ngOnInit(): void {
    this.loginModel = <Login_model>{
      company_name: '',
      model_name: '',
      car_name: '',
      link: '',
      part_image: File= null, 
      models_name:'',
      toDoList:'',
      id:'',
      company_details:''
    }
    this.addValidationError = <Login_model> {
      company_name: '', 
      model_name: '',
      car_name: '',
      link: '',    
      part_image: File= null,    
    }
    this.isLoggedIn();
    this.deleteAllmultiCarModel();
    this.getCompanyNames();
  }
  //Delete All where status O.
  deleteAllmultiCarModel(){
    this.accountService.deleteAllmultiCarModels().then((response)=>{
     console.log('success');
    }) 
  }
 getMultiDetails(){
  this.accountService.getmultiCarModels().then((response)=>{
    if(response.code==200){
      this.loaderShow = false;
      this.toDoList = response.result;        
    }else{
      this.toDoList = [];
    }
  }) 
}

  getValues(event: any){
    this.loginModel.company_details=event.target.value.split("?");
    this.loginModel.companyNames=this.loginModel.company_details[1];
    this.loginModel.company_id = this.loginModel.company_details[0];
    this.loaderShow= true;
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    this.accountService.getModels(this.loginModel).then((response)=>{
      if(response.code==200){
        this.loaderShow = false;
        this.models = response.result;        
      }else{
        this.models = [];
      }
    })
  }
  
  getValuesModel(event: any){ 
    this.loginModel.companyNames
    this.loginModel.model_details=event.target.value.split("?");
    this.loginModel.models_name = this.loginModel.model_details[1];
    this.loginModel.model_id = this.loginModel.model_details[0];
    // Insert Record on click
    this.accountService.multiCarModels(this.loginModel).then((response)=>{
      this.accountService.getmultiCarModels().then((response)=>{
        console.log('this is awesome',response);
        if(response.code==200){
          this.loaderShow = false;
          this.toDoList = response.result;        
        }else{
          this.toDoList = [];
        }
      }) 
    })
    // Insert record on click close
  
  }

  getCompanyNames(){
    this.loaderShow= true;
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    this.accountService.getCarCompanyNames(this.loginModel).then((response)=>{
      console.log('names: ', response);
      if(response.code==200){
        this.loaderShow = false;
        this.companyNames = response.result;        
      }else{
        this.companyNames = [];
      }
    })
  }

  //Add Company Name
  isLoggedIn() {
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    if (this.loginModel.admin_id == null) {
      this.router.navigate(['/']);
    }
  }

  onFileSelect(event) {
    this.buttonDisable = false;
    if (event.target.files.length > 0) {
      const video_file = <File>event.target.files[0];
      if(video_file.type == 'image/jpeg' || video_file.type == 'image/jpg'  || video_file.type == 'image/png'){
        this.loginModel.part_image = <File>video_file;
        this.fileSelect = false;
      }else{
        this.fileSelect = true;
        this.loginModel.part_image = null;
        this.buttonDisable = true;
      }  
    }
  }
  add_part() {
    var valid = true;
    this.addValidationError.company_name = '';
    this.addValidationError.model_name = '';
    this.addValidationError.car_name = '';
    this.addValidationError.link = '';
    this.addValidationError.part_image_error = '';
    this.addValidationError.description = '';
    if (this.loginModel.company_name == "") {
      this.addValidationError.company_name = 'Please Select Company Name';
      valid = false;
    }
    if (this.loginModel.model_name == "") {
      this.addValidationError.model_name = 'Please Select Model Name';
      valid = false;
    }
    if (this.loginModel.car_name == "") {
      this.addValidationError.car_name = 'Please Enter Car Name';
      valid = false;
    }
    if (this.loginModel.description == "") {
      this.addValidationError.description = 'Please Enter Description';
      valid = false;
    }
    if (this.loginModel.link == "") {
      this.addValidationError.link = 'Please Enter Link';      
      valid = false;
    }else{
      var regExp = new RegExp ("^https?:\/\/()?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?");
      if(!regExp.test(this.loginModel.link)){
        valid = false;
        this.addValidationError.link = 'Please type Link Like http://google.com';     
      }
    }

    if (valid) {
      var formData: FormData  = new FormData();
      formData.append('company_name', this.loginModel.company_id);
      formData.append('model_name', this.loginModel.model_id);
      formData.append('car_name', this.loginModel.car_name);
      formData.append('link', this.loginModel.link);
      formData.append('part_image', this.loginModel.part_image);
      formData.append('admin_id', this.loginModel.admin_id);
      formData.append('description', this.loginModel.description);
      
      this.accountService.addParts(formData).then((response) => {
        if (response.code == 200) {
          this.sucMsg = true;
          this.successMessage = response.message;
          setTimeout(() => { this.router.navigate(['/part']); }, 1500);
        } else {
          this.errMsg = true;
          this.errorMessage = response.message;   
          setTimeout(() => { this.errMsg = false; }, 1500);      
        }
      })
    }
  }

  delete(id){
    if(confirm("Are you sure to delete!")) {
      this.loaderShow = true;
      this.loginModel.id = id;
      this.loginModel.admin_id = localStorage.getItem('admin_id');;
      this.accountService.deleteMultiParts(this.loginModel).then((response)=>{
      this.loaderShow = false;
        if(response.code==200){
          this.sucMsg = true;
          this.successMessage = response.message;
          this.getMultiDetails();
          setTimeout (() => {  this.sucMsg = false }, 1000);
        }else{
          this.errMsg = true;
          this.errorMessage = response.message;
          setTimeout (() => {  this.errMsg = false }, 1000);
        }
      })
    }
  }

  

}
