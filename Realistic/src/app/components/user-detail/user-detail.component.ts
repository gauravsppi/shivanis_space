import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { AccountService } from '../../services/account.service';
import { Headers, Http } from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import { Login_model } from '../../models/login_model';
import { Validation_model } from '../../models/Validation_model';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {
  public loginModel:Login_model;
  public Validation_model:Validation_model;
  record:any=[];
  records:any=[];
  total : number= 0;
  loaderShow: boolean = false;
  constructor(private accountService:AccountService,private route: ActivatedRoute,private router: Router, private http: HttpClientModule) { }
  getId:any=[];

  ngOnInit(): void {
      this.loginModel=<Login_model>{
       user_id:''
      }
      
    this.route.params.subscribe(params => {
      this.getId = params['_id'];
      //  this.getUser()
    });
    this.isLoggedIn();
    this.view_user_details(this.getId);
     this.view_user_car(this.getId);
  }
  isLoggedIn() {
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    if(this.loginModel.admin_id == null){
    this.router.navigate(['/']);
    }
    } 

  view_user_details(user_id){
    let admin_id=localStorage.getItem('admin_id');
    this.accountService.get_user_detail(user_id,admin_id).then((response)=>{
      if(response.code==200){
        this.loaderShow = false;
       this.records = response.result;
      }else{
       alert('No Record FOund');
      }
    })
  }

  view_user_car(user_id){
   // let admin_id=localStorage.getItem('admin_id');
    this.accountService.get_user_car(user_id).then((response)=>{
      console.log(response);
      if(response.code==200){
        this.loaderShow = false;
       this.record = response.result;
       //this.record = response.result;
       //var imagess =[];
      //  this.record.forEach(function(element,id){
      //   console.log(element.images);
      //   this.images=element.images;
      // });
      }else{
       alert('No Record FOund');
      }
    })
  }
  
}
