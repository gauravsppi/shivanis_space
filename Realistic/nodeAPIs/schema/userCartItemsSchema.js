const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userCartItemsSchema = new mongoose.Schema({
    user_id: {
        type: String     
    }, 
    part_id: {
        type: String     
    }, 
    car_id: {
        type: String     
    }, 
    status: {
        type: String     
    },  
    created_at: {
        type: Date,
        default: Date.now
    },
});

userCartItemsSchema.set('toJSON', { virtuals: true });

userCartItemsSchema.virtual('parts_details',{
    ref: 'parts',
    localField: 'part_id',
    foreignField: '_id',
    justOne: true
});

module.exports = userCartItemsSchema;
