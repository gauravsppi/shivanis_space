import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CareRecieverManagementComponent } from './care-reciever-management.component';

describe('CareRecieverManagementComponent', () => {
  let component: CareRecieverManagementComponent;
  let fixture: ComponentFixture<CareRecieverManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CareRecieverManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CareRecieverManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
