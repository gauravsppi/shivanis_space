import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListContactsInfoComponent } from './list-contacts-info.component';

describe('ListContactsInfoComponent', () => {
  let component: ListContactsInfoComponent;
  let fixture: ComponentFixture<ListContactsInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListContactsInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListContactsInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
