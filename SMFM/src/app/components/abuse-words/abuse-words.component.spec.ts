import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbuseWordsComponent } from './abuse-words.component';

describe('AbuseWordsComponent', () => {
  let component: AbuseWordsComponent;
  let fixture: ComponentFixture<AbuseWordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbuseWordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbuseWordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
