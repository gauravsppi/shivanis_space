export class admin_model {
    admin_email:string;
    admin_password:string;
    id:string;
    status:number;
    plan_name:any;
    plan_amount:string;
    plan_time:any;
    success_fee:string;
    fee:string;
    plan_description:string;
    typeStatus:string;
    filterString:string;
    availabilitytime:any;
    candidate_email:string;
}