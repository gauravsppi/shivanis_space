import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { AccountService } from '../../services/account.service';
import { Headers, Http } from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import { Login_model } from '../../models/login_model';
import { Validation_model } from '../../models/Validation_model';

@Component({
  selector: 'app-add-car-models',
  templateUrl: './add-car-models.component.html',
  styleUrls: ['./add-car-models.component.scss']
})

export class AddCarModelsComponent implements OnInit {
  public loginModel:Login_model;
  public addValidationError:Validation_model;
  private headers = new Headers();
  record:any=[];
  total : number= 0;
  loaderShow: boolean = false;
  sucMsg:boolean=false;
  errMsg:boolean=false;
  successMessage='Record Added successfully'; 
  errorMessage='Something went wrong'; 
  constructor(private accountService:AccountService,private route: ActivatedRoute,private router: Router, private http: HttpClientModule) { }
 
  getId: string;
  ngOnInit(): void {
    this.loginModel=<Login_model>{
      model_name:'',
      company_id:'',
    }
    this.addValidationError = <Validation_model>{
      model_name:'',
    }
    this.route.params.subscribe(params => {
      this.getId = params['_id'];
      //  this.getUser()
    });
    this.isLoggedIn();
    this.loginModel.company_id=this.getId;
  }
    
  //Add Company Name
  isLoggedIn() {
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    if(this.loginModel.admin_id == null){
    this.router.navigate(['/']);
    }
    } 
    

    add_car_model(){
          var valid=true;
          this.addValidationError.model_name='';
          if(this.loginModel.model_name==""){
            this.addValidationError.model_name='Please Enter Car Model Name';
            valid=false; 
          }
       //  alert(this.loginModel.company_id);
          if(valid){
            this.sucMsg = true;
            this.accountService.add_model_name(this.loginModel).then((response)=>{
              if (response.code == 200) {
                this.sucMsg = false;
                setTimeout (() => { this.router.navigate(['view-model-list/',this.loginModel.company_id]);  }, 1500);
              } else {
                this.errMsg = true;
                this.router.navigate(['/login']);
              }
            })
           }
        }
  }



