import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from '../../../services/account.service';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import { admin_model } from '../../../models/admin_model';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-show-package',
  templateUrl: './show-package.component.html',
  styleUrls: ['./show-package.component.scss']
})
export class ShowPackageComponent implements OnInit {
  public adminModel: admin_model;
  public addValidationError: admin_model;
  public headers = new Headers();
  plansList:any=[];
  addbutton:number;
  hideshow:boolean=false;
  packesdetails:any=[];
  
  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService,private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.getPlansdetail();   

  }
  getPlansdetail(){
    //this.spinner.show();
    debugger;
    this.accountService.getPlans().then((response) => {
      debugger;
      if (response.code == 200) {
        this.spinner.hide();
        this.addbutton=response.result.length;
        if(this.addbutton<3){
          this.hideshow=true;
        }else{
          this.hideshow=false;
        }
        this.packesdetails=response.result;
      } else {
        //this.errMsg = true;
        //this.message = response.message;
      }
    })
  }
}
