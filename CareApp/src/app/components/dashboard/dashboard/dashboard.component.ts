import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "../../../shared/services/spinner.service";
import { AdminDashboardService } from '../../../shared/services/admin-dashboard/admin-dashboard.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  dashboardCount : any;
  constructor(
    private AdminDashboardService: AdminDashboardService,
    private spinner: NgxSpinnerService,
  ) { }


  ngOnInit(): void {
    this.spinner.show();
    this.fetchRecords().toPromise().then((res) => {
      console.log(res)
      this.spinner.hide();
      if(res.status == 1){
        this.dashboardCount = res
      }else{
         Swal.fire({ icon: 'error', title: 'Oops...', text: res.message })
      }
    })
    .catch(err=> { 
      this.spinner.hide();
      console.log(err) 
    });
  }


  fetchRecords (){
    return this.AdminDashboardService.fetch_total_count();
  }


}
