import { BrowserModule } from '@angular/platform-browser';
import { NgModule,LOCALE_ID } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from  '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { AccountService } from './services/account.service';
import { BaseHttpService } from './services/base-http.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { FooterComponent } from './components/layout/footer/footer.component';
import { SidebarComponent } from './components/layout/sidebar/sidebar.component';
import { LoginComponent } from './components/login/login.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { LightboxModule } from 'ngx-lightbox';
import { ProductsComponent } from './components/products/products.component';
import { ProductViewComponent } from './components/product-view/product-view.component';
import { AddProductComponent } from './components/add-product/add-product.component';
import { EditProductComponent } from './components/edit-product/edit-product.component';
import { LoaderComponent } from './components/loader/loader.component';
import { AdminProfileComponent } from './components/admin-profile/admin-profile.component';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import { CarCompanyModelComponent } from './components/car-company-model/car-company-model.component';
import { AddCarCompanyComponent } from './components/add-car-company/add-car-company.component';
import { ViewCompanyListComponent } from './components/view-company-list/view-company-list.component';
import { AddCarModelsComponent } from './components/add-car-models/add-car-models.component';
import { ViewModelListComponent } from './components/view-model-list/view-model-list.component';
import { EditCompanyNameComponent } from './components/edit-company-name/edit-company-name.component';
import { EditModelNameComponent } from './components/edit-model-name/edit-model-name.component';
import { ManageContentComponent } from './components/manage-content/manage-content.component';
import { ContentComponent } from './components/content/content.component';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import { TermConditionComponent } from './components/term-condition/term-condition.component';
import { EnquiryListComponent } from './components/enquiry-list/enquiry-list.component';
import { PartComponent } from './components/part/part.component';
import { AddPartComponent } from './components/add-part/add-part.component';
import { FormatDatePipe } from './formatDate.pipe';
import { EditPartComponent } from './components/edit-part/edit-part.component'

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    LoginComponent,  
    UserListComponent,
    UserDetailComponent,
    ProductsComponent,
    ProductViewComponent, 
    AddProductComponent,
    EditProductComponent,
    LoaderComponent,
    AdminProfileComponent,
    EditProfileComponent,
    CarCompanyModelComponent,
    AddCarCompanyComponent,
    ViewCompanyListComponent,
    AddCarModelsComponent,
    ViewModelListComponent,
    EditCompanyNameComponent,
    EditModelNameComponent,
    ManageContentComponent,
    ContentComponent,
    TermConditionComponent,
    EnquiryListComponent,
    PartComponent,
    AddPartComponent,
    FormatDatePipe,
    EditPartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LightboxModule,
    HttpModule,
    HttpClientModule,
    FormsModule ,
    AngularEditorModule,
    
  ],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy },AccountService,BaseHttpService, { provide: LOCALE_ID, useValue: 'nl-NL' }],
  bootstrap: [AppComponent]
})   
export class AppModule { }
