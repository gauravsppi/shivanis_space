import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployerAdminVerifyComponent } from './employer-admin-verify.component';

describe('EmployerAdminVerifyComponent', () => {
  let component: EmployerAdminVerifyComponent;
  let fixture: ComponentFixture<EmployerAdminVerifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployerAdminVerifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployerAdminVerifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
