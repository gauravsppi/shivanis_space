import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ReceiverService } from '../../../shared/services/receiver-service/receiver.service';
import { NgxSpinnerService } from "../../../shared/services/spinner.service";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-complete-post-detail',
  templateUrl: './completed-post-details.component.html',
  styleUrls: ['./completed-post-details.component.scss']
})
export class CompletedPostDetailsComponent implements OnInit {
  postDetails: any;
  defaultImage = 'assets/images/loader.gif';
  receiverLanguage: any = [];
  langArray: any = []
  matchingWorkers: any =[];
  workerId: any;
  receiverId: any;
  postId : any;
  selectedIndex: any;
  hairArray: any = [];
  receiverHairs: any = [];



  constructor(
    private activatedRoute: ActivatedRoute,
    private ReceiverService: ReceiverService,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    console.log(this.activatedRoute.snapshot.params)
    let postId = this.activatedRoute.snapshot.params.postId;
    let receiverId = this.activatedRoute.snapshot.params.receiverId;

    this.spinner.show();
    // **************** Fetch receiver details **************
    this.fetchRecords(postId, receiverId).toPromise().then((res) => {
      console.log('res++++++++', res)
      this.spinner.hide();
      if(res.code == 200){
        this.postDetails = res.postDeatils;

        if(this.postDetails.languageandculturalcuisines){
          this.receiverLanguage = this.postDetails.languageandculturalcuisines.language
          this.postDetails.languageandculturalcuisines.language.forEach(obj => {
            console.log('obj++++++',obj)
            this.langArray.push(obj.language)
            console.log('langArray+++++++',this.langArray);
        });
        }

         // Hairs Array
         if(this.postDetails.hairandgroomings){
          this.receiverHairs = this.postDetails.hairandgroomings.hair
            this.hairArray.push(this.receiverHairs)
            console.log('hairArray+++++++',this.hairArray);
        }
        
        
        this.fetchMatchingWorkers(receiverId, postId, this.postDetails)
      }else{
        Swal.fire({ icon: 'error', title: 'Oops...', text: res.message })
      }
    })
    .catch(err=> { 
      this.spinner.hide();
      console.log(err) 
    });
  }


  //*********** Fetch Receiver Details API ****************
  fetchRecords (postId, receiverId){
    var Params = {
      postId:  postId, 
      receiverId: receiverId
    }
    return this.ReceiverService.fetch_single_post_details(Params);
  }


  //********* Fetch matched workers *********************
  fetchMatchingWorkers(receiverId, postId,  postDetails)  : void {
    // this.spinner.show();
    this.apiMatchingWorkers(receiverId, postId, postDetails).toPromise().then((res) => {
      console.log('Matching Workers----------',res)
      this.spinner.hide();
      if(res.status == 1){
        this.matchingWorkers = res.results

      }else{
        Swal.fire({ icon: 'error', title: 'Oops...', text: res.message })
      }
    })
    .catch(err=> { 
      this.spinner.hide();
      console.log(err) 
    });
  }

  assignWorker(workers, index) : void {
    this.workerId = workers._id
    this.selectedIndex = index;
  }


    //********* Fetch Matching Worker API ******************
    apiMatchingWorkers(receiverId, postId, postDetails, ) {
      var Params = {
        receiverId: receiverId,
        postId: postId,
        hair: postDetails.hairandgroomings ? postDetails.hairandgroomings.hair : '',
        language: this.langArray,
        hairs:this.hairArray,
        religion: postDetails.religions ? postDetails.religions.religion : '',
        rateandavailabilities: postDetails.rateandavailabilities,
  
  
      }
      return this.ReceiverService.fetch_matching_complete_workers(Params);
    }


    sendRequestForAssign (status): void {
   
      var reqParams = {
        workerId : this.workerId,
        postId: this.activatedRoute.snapshot.params.postId,
        receiverId: this.activatedRoute.snapshot.params.receiverId,
        status : status
      }
  
      this.spinner.show();
      this.assignMatchingWorkers(reqParams).toPromise().then((res) => {
        this.spinner.hide();
  
        console.log('assign Workers----------',res)
        this.spinner.hide();
        if(res.status == 1){

            this.matchingWorkers[this.selectedIndex]['alreadyAssign'] = {
              status : 0
            }
            Swal.fire('Assign!', 'A request has been sent on selected receiver.', 'success');
  
        }else{
          Swal.fire({ icon: 'error', title: 'Oops...', text: res.message })
        }
      })
      .catch(err=> { 
        this.spinner.hide();
        console.log(err) 
      });
    }
  
  
    //********* assign Matching Worker API ******************
    assignMatchingWorkers(reqParams) {
      return this.ReceiverService.assign_matching_workers(reqParams);
    }


    // Direct assign 
    direactAssignWorker (status): void {
      let record = [];
     
      var reqParams = {
        workerId : this.workerId,
        postId: this.activatedRoute.snapshot.params.postId,
        receiverId: this.activatedRoute.snapshot.params.receiverId,
        status : status
      }
  
      this.spinner.show();
      this.DirectAssignMatchingWorker(reqParams).toPromise().then((res) => {
        this.spinner.hide();
        if(res.status == 1){
  
            this.matchingWorkers[this.selectedIndex]['status'] = 3;
            record.push(this.matchingWorkers[this.selectedIndex]);
            this.matchingWorkers = [];
            this.matchingWorkers = record;
            Swal.fire('Assinged!', 'Worker has been assigned.', 'success');
  
        }else{
          Swal.fire({ icon: 'error', title: 'Oops...', text: res.message })
        }
      })
      .catch(err=> { 
        this.spinner.hide();
        console.log(err) 
      });
    }
  
  
  
    //********* Direct assign Matching Worker API ******************
    DirectAssignMatchingWorker(reqParams) {
      return this.ReceiverService.direct_assign_matching_workers(reqParams);
    }
  

}
