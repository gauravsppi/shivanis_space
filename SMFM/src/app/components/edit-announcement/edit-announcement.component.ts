import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Headers, Http } from '@angular/http';

import { AccountService } from '../../services/account.service';

import { school_model } from '../../models/school_model';
@Component({
  selector: 'app-edit-announcement',
  templateUrl: './edit-announcement.component.html',
  styleUrls: ['./edit-announcement.component.scss']
})
export class EditAnnouncementComponent implements OnInit {
  public schoolModel: school_model;
  public addValidationError: school_model;
  sucMsg: boolean =  false; 
  successMessage: string='';
  errMsg: boolean = false; 
  errorMessage: string=''; 
  announcements = [];
  announcement_id = this.route.snapshot.params.id;

  constructor(private router: Router, private accountService: AccountService, private route: ActivatedRoute, private http: Http) { }

  ngOnInit(): void {
    this.schoolModel = <school_model>{
      user_id: '',
      title: '',
      details: '',
      urgent: '',
      status: ''   
    }
    this.addValidationError = <school_model>{
      title: '',
      details: '',
      urgent: '',
      status: ''   
    }
    this.getAnnouncements();
  }

  getAnnouncements() {   

    var a_id = this.announcement_id;
    this.schoolModel.title = ''
    this.schoolModel.user_id = localStorage.getItem('admin_id');
    this.schoolModel.school_id = localStorage.getItem('school_sourcedId');
    this.schoolModel.user_type = 'admin';
    this.accountService.getAllAnnouncements(this.schoolModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
        this.announcements = response.result;
        //this.announcements.forEach(function (value) {
        // forEach(this.announcements, function (value, index) {
        for (var i = 0; i < response.result.length; i++) {
         
          if(response.result[i].id == a_id){
            this.schoolModel.title = response.result[i].title;
            this.schoolModel.details = response.result[i].details;
            this.schoolModel.urgent = response.result[i].urgent;
            this.schoolModel.status = response.result[i].status;
            console.log('model:: ', this.schoolModel);
          }        
        }
      }else{
        
      }
    })
  }
  
  onUpdate() {
    var valid = true;
    this.schoolModel.searchKey== "";
    if (this.schoolModel.title == "") {
      this.addValidationError.title = 'Please enter title';
      valid = false;
    }
    if (this.schoolModel.details == "") {
      this.addValidationError.details = 'Please enter details';
      valid = false;
    }
  
    if(valid){ 
     // this.loaderShow = true;
      this.schoolModel.user_id = localStorage.getItem('admin_id');
      this.schoolModel.school_id = localStorage.getItem('school_sourcedId');
      this.schoolModel.announcement_id = this.announcement_id;
      this.schoolModel.user_type = 'admin';
      this.accountService.updateAnnouncement(this.schoolModel).then((response) => {
        if (response.code == 200 && response.status == 'success') {
          this.sucMsg = true;
          this.successMessage = response.message;
          setTimeout (() => { this.sucMsg = false;  }, 3000); 
          this.addValidationError.title = '';
          this.addValidationError.details = '';
          this.addValidationError.urgent = '';
          this.addValidationError.status = '';

          this.schoolModel.title = response.result.title;
          this.schoolModel.details = response.result.details;
          this.schoolModel.urgent = response.result.urgent;          
        }else{
          this.errMsg = true;
          this.errorMessage = "Something went wrong";
          setTimeout (() => { this.errMsg = false;  }, 3000); 
          
        }
       // this.loaderShow = false;
      })  
    }
  }
}
