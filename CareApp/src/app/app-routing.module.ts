import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared/guards/auth.guard';
import { LoginGuard } from './shared/guards/login.guard';
import { NotfoundComponent } from './components/notfound/notfound.component';
import { MainLayoutComponent } from './components/layout/main-layout/main-layout.component';

const routes: Routes = [

  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  { 
    path: 'login', 
    canActivate : [LoginGuard],
    loadChildren: () => import(`./components/login/login.module`).then(m => m.AdminLoginModule) 
  },
  {   
    path: 'forgotpassword', 
    // canActivate : [LoginGuard],
    loadChildren: () => import(`./components/forgot-password/forgot-password.module`).then(m => m.ForgotPasswordModule) 
  },
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      
      { 
        path: 'dashboard',
        canActivate : [AuthGuard],
        loadChildren: () => import(`./components/dashboard/dashboard.module`).then(m => m.DashboardModule) 
      },

      { 
        path: 'care-reciever-management',
        canActivate : [AuthGuard],
        loadChildren: () => import(`./components/care-reciever-management/care-receiver.module`).then(m => m.CareRecevierModule) 
      },

      { 
        path: 'care-worker-management',
        canActivate : [AuthGuard],
        loadChildren: () => import(`./components/care-worker-management/care-worker.module`).then(m => m.CareWorkerModule) 
      },

      {
        path: 'post-request',
        canActivate : [AuthGuard],
        loadChildren: () => import(`./components/post-request/post-request.module`).then(m => m.PostRequestModule) 
      },
      {
        path: 'sub-admin',
        canActivate : [AuthGuard],
        loadChildren: () => import(`./components/sub-admin/sub-admin.module`).then(m => m.SubAdminModule) 
      },
      {
        path: 'admin-profile',
        canActivate : [AuthGuard],
        loadChildren: () => import(`./components/admin-profile/admin-profile.module`).then(m => m.AdminProfileModule) 
      },
      {
        path: 'agency',
        canActivate : [AuthGuard],
        loadChildren: () => import(`./components/agency/agency.module`).then(m => m.AgencyModule) 
      },
    ],
  },

  {path: '404', component: NotfoundComponent},
  {path: '**', redirectTo: '/404'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
