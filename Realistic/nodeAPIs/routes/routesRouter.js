const express = require("express") //node framework
const router = express.Router() //Using this define routes
const loginController = require("../controller/admin/loginController") //set admin controller for routes
const usersController = require("../controller/users/usersController") //set users controller for routes
const pagesController = require("../controller/pages/pagesController") //set pages controller for routes
const categoryController = require("../controller/category/categoryController") //set category controller for routes
const carsController = require("../controller/cars/carsController") //set Car controller for routes


/******* For app api's *********/
router.post("/addUser", usersController.addUser) //user signup api route
router.post("/userLogin", usersController.userLogin) //user login api route
router.post("/updateUserInfo", usersController.updateUserInfo) //user info update api route
router.post("/forgotPassword", usersController.forgotPassword) // User forgot Password
router.post("/otpVerify", usersController.otpVerify) // User OTP verify
router.post("/resetPassword", usersController.resetPassword) // User reset password
router.post("/changePassword", usersController.changePassword) // User change password
router.post("/addUserCar", usersController.addUserCar) // User Add car
router.post("/getUserCar", usersController.getUserCar) // User Get car
router.post("/updateUserCar", usersController.updateUserCar) // User update car
router.post("/deleteUserCar", usersController.deleteUserCar) // User delete car

router.post("/getAllParts_UserCarBased", usersController.getAllParts_UserCarBased) // User delete car

router.post("/AddPart_to_cart", usersController.AddPart_to_cart) // User Part add to cart car
router.post("/getAddedCartParts", usersController.getAddedCartParts) // Get all added part
router.post("/deleteCartPart", usersController.deleteCartPart) // Delete added part




router.post("/addTryParts", carsController.addTryParts) // Add try parts
router.post("/getTryParts", carsController.getTryParts) // Get try parts
router.post("/saveUsedTryParts", carsController.saveUsedTryParts) // save try parts
router.post("/saveTryPartsOfImage", carsController.saveTryPartsOfImage) // Save try parts image

router.post("/getUserTryPartsImage", usersController.getUserTryPartsImage) // get user try parts image
router.post("/deleteUserTryPartsImage", usersController.deleteUserTryPartsImage) // delete user try parts image

router.post("/saveContactForm", usersController.saveContactForm) // save contact info

router.post("/getCarCompanyNames", carsController.getCarCompanyNames) // Get car company names
router.post("/getCarModels", carsController.getCarModels) // Get car models by company id

router.post("/getContent", pagesController.getContent) // Pages content get 

router.post("/save_multiCarModel", carsController.save_multiCarModel) // Save multiple CarCompany Model
router.post("/getmultiCarModel", carsController.getmultiCarModel) // Get Multi CarCompany Model
router.post("/deleteMultiParts", carsController.deleteMultiParts) //delete parts data
router.post("/deleteAllmultiCarModels", carsController.deleteAllmultiCarModels) //delete parts data

router.post("/get_edit_multiCarModel", carsController.get_edit_multiCarModel) // Get Update Multi CarCompany Model
router.post("/save_multiCarModelwithID", carsController.save_multiCarModelwithID) // Save multiple CarCompany Model with AddPart ID

  

/***** Admin Dashboard API's*****/ 
router.post("/adminlogin", loginController.login) 
router.post("/adminProfile", loginController.getProfile) 
router.post("/adminProfileImageUpdate", loginController.updateProfileImage) 
router.post("/adminProfileUpdate", loginController.updateProfile)
router.post("/adminChangePassword", loginController.changePassword) //admin change password

router.post("/getAllUsers", loginController.getAllUsers)//get all users 
router.post("/getUserInfo", loginController.getUserInfo) //get single user info
router.post("/countAllUsers", loginController.countAllUsers) //count all users
router.post("/getUserCars", loginController.getUserCars) // User Get car

router.post("/addCarCompanyName", carsController.addCarCompanyName) //add car company names
router.post("/updateCarCompanyName", carsController.updateCarCompanyName) // update company names
router.post("/deleteCarCompanyName", carsController.deleteCarCompanyName) // delete company names
router.post("/viewCarCompanyName", carsController.viewCarCompanyName) //view car company names
router.post("/getCarCompanyName", carsController.getCarCompanyName) // Get car company names

router.post("/getCarModel", carsController.getCarModel) // Get car models by company id
router.post("/addCarModel", carsController.addCarModel) //add car model
router.post("/updateCarModel", carsController.updateCarModel) //update car model
router.post("/deleteCarModels", carsController.deleteCarModels) //delete car model
router.post("/viewCarModels", carsController.viewCarModels) //view car model

router.post("/tryPartsStatics", loginController.tryPartsStatics) //try parts graph data
router.post("/tryPartsWithCarStatics", loginController.tryPartsWithCarStatics) //try parts with cars graph data

router.post("/getContactData", loginController.getContactData) //get contact data

router.post("/addParts", loginController.addParts) //add parts data
router.post("/getParts", loginController.getParts) //get parts data
router.post("/deleteParts", loginController.deleteParts) //delete parts data
router.post("/updateParts", loginController.updateParts) //update parts data
router.post("/viewParts", loginController.viewParts) //view parts data

router.get("/test", carsController.test) //test api

/*** For Mongoose****/
router.post("/addAdmin", loginController.addAdmin) 
router.post("/addContent", pagesController.addContent) 
router.post("/updateContent", pagesController.updateContent) 


module.exports = router
                           