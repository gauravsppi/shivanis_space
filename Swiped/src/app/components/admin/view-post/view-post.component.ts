import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from '../../../services/account.service';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import { admin_model } from '../../../models/admin_model';
import { employer_model } from '../../../models/employer_model';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2'

@Component({
  selector: 'app-view-post',
  templateUrl: './view-post.component.html',
  styleUrls: ['./view-post.component.scss']
})
export class ViewPostComponent implements OnInit {

  public adminModel: admin_model;
  public employerModel: employer_model;
  public addValidationError: admin_model;
  public headers = new Headers();
  errMsg:boolean=false;
  message:string;
  employerlist:any ;
  getId:any;
  
  packesdetails:any=[];
  showbtn:boolean=false;

  campaigndetailstabdata:any  ;
  skillsneedtabdata:any  ;
  companyroledesctabdata:any  ;
  campaignquestabdata:any  ;
  matchedCandidate: any ;

  candidatelist:any ;
  letsgoTabdata:any [] ;
  degreeTabdata:any [] ;
  currentjobTabdata:any [] ;
  interestsTabdata:any [] ;

  searchText: string;
  page = 1;
  count : number;
  pageSize:number = 10;

  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService, private spinner:NgxSpinnerService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.getId = params.get('id');
      
    });

    this.adminModel = <admin_model>{
      id: ''
    }
    this.addValidationError = <admin_model>{
      id: '',
      admin_password: ''
    }
    this.employerModel = <employer_model>{
      post:'',
      job_type:'',
      e_specialism:[],
      
    }
    //this.getEmployerdetailsWithID();
    this.getEmployerPostWithID();
    
  }
  //-------------pagination------------
  onTableDataChange(event){
    this.page = event;
    this.getEmployerPostWithID();
  } 

//----------sorting-------------
  key: string = '';
  reverse: boolean = false;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }

  //----------------------------get post details------------------

  getEmployerPostWithID(){
    console.log(this.adminModel);
    this.adminModel.id = this.getId;
    this.spinner.show();
    debugger;
    this.accountService.getEmployerJobPostWithID(this.adminModel).then((response) => {
      debugger;
      console.log(response);
    if (response.code == 200) {
      this.spinner.hide();
      this.campaigndetailstabdata=response.result;
      //this.campaigndetailstabdata=response.campaigndetailstabdata;
      this.skillsneedtabdata=response.skillsneedtabdata;
      this.companyroledesctabdata=response.companyroledesctabdata;
      this.campaignquestabdata=response.campaignquestabdata;
    
    } else {
      //this.errMsg = true;
      //this.message = response.message; 
      }
    })
  }

  //-------------------------- on post details click---------------------------
  getPostDetails()
  {
    this.showbtn=false;
   
  }

  //----------------------------get matched candidates---------------------------
  getMatchedCandidates(job_type,e_specialism){
    this.showbtn=true;
    //this.employerModel.post = post;
    this.employerModel.job_type = job_type;
    this.employerModel.e_specialism = e_specialism;    
     this.spinner.show();
     debugger;
    this.accountService.getCandidateMatched(this.employerModel).then((response) => {
      debugger;
      console.log(response);
    if (response.code == 200) {
      this.spinner.hide();
      this.matchedCandidate=response.matchedCandidate;
      this.count= response.result.length;
      } else {
      // this.errMsg = true;
      // this.message = "No Candidate Found"; 
      }
    })
  }

//---------------------get employer detail by id-------------------
  getEmployerdetailsWithID(){
    this.adminModel.id = this.getId;
    this.spinner.show();
    debugger;
    this.accountService.getEmployerdetailsWithID(this.adminModel).then((response) => {
      debugger;
    if (response.code == 200) {
      this.spinner.hide();
      this.employerlist=response.result;
      this.campaigndetailstabdata=response.campaigndetailstabdata;
      this.skillsneedtabdata=response.skillsneedtabdata;
      this.companyroledesctabdata=response.companyroledesctabdata;
      this.campaignquestabdata=response.campaignquestabdata;
    } else {
      //this.errMsg = true;
      //this.message = response.message; 
      }
    })
  }

//----------------show candidate details with id-------------
showCandidatelist(id){
  this.adminModel.id = id;
  this.spinner.show();
  debugger;
  this.accountService.candidateDetailsWithID(this.adminModel).then((response) => {
    debugger;
    if (response.code == 200) {
      console.log(response);
      this.spinner.hide();
      this.candidatelist=response.result;
      this.letsgoTabdata=response.letsgoTabdata;
      this.degreeTabdata=response.degreeTabdata;
      this.currentjobTabdata=response.currentjobTabdata;
      this.interestsTabdata=response.interestsTabdata;
      
      
    } else {
      this.errMsg = true;
      this.message = "No Data Found!!!";
    }
  })
}
   
 
}
