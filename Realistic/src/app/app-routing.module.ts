import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule} from '@angular/common/http';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { ProductsComponent } from './components/products/products.component';
import { ProductViewComponent } from './components/product-view/product-view.component';
import { AddProductComponent } from './components/add-product/add-product.component';
import { EditProductComponent } from './components/edit-product/edit-product.component';
import { AdminProfileComponent } from './components/admin-profile/admin-profile.component';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import { CarCompanyModelComponent } from './components/car-company-model/car-company-model.component';
import { AddCarCompanyComponent } from './components/add-car-company/add-car-company.component';
import { ViewCompanyListComponent } from './components/view-company-list/view-company-list.component';
import { AddCarModelsComponent } from './components/add-car-models/add-car-models.component';
import { ViewModelListComponent } from './components/view-model-list/view-model-list.component';
import { EditCompanyNameComponent } from './components/edit-company-name/edit-company-name.component';
import { EditModelNameComponent } from './components/edit-model-name/edit-model-name.component';
import { ManageContentComponent } from './components/manage-content/manage-content.component';
import { ContentComponent } from './components/content/content.component';
import { TermConditionComponent } from './components/term-condition/term-condition.component';
import { EnquiryListComponent } from './components/enquiry-list/enquiry-list.component';
import { PartComponent } from './components/part/part.component'; 
import { AddPartComponent } from './components/add-part/add-part.component';
import { EditPartComponent } from './components/edit-part/edit-part.component';


const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'user-list', component: UserListComponent },
  { path: 'user-detail/:_id', component: UserDetailComponent },
  { path: 'products', component: ProductsComponent },
  { path: 'product_view', component: ProductViewComponent },
  { path: 'add_product', component: AddProductComponent },
  { path: 'edit_product', component: EditProductComponent },
  { path: 'admin-profile', component: AdminProfileComponent },
  { path: 'edit-profile', component: EditProfileComponent },
  { path: 'car-company-model', component: CarCompanyModelComponent },
  { path: 'add-company-name', component: AddCarCompanyComponent },
  { path: 'view-company-list', component: ViewCompanyListComponent },
  { path: 'add-car-model/:_id', component: AddCarModelsComponent },
  // { path: 'view-model-list', component: ViewModelListComponent },
  { path: 'edit-company/:_id', component: EditCompanyNameComponent },
  { path: 'company_delete/:_id', component: ViewCompanyListComponent },
  { path: 'view-model-list/:_id', component: ViewModelListComponent },
  { path: 'edit-model/:_id', component: EditModelNameComponent },
  { path: 'manage-content', component: ManageContentComponent },
  { path: 'content', component: ContentComponent },
  { path: 'term-conditions', component: TermConditionComponent  },
  { path: 'enquiry', component: EnquiryListComponent  },
  { path: 'part', component: PartComponent  },
  { path: 'add-part', component: AddPartComponent  },
  { path: 'edit-part/:id', component: EditPartComponent  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes), BrowserModule,HttpClientModule,],
  exports: [RouterModule]
})
export class AppRoutingModule { }
