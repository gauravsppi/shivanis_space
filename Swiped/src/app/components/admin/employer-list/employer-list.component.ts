import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router,NavigationEnd } from '@angular/router';
import { AccountService } from '../../../services/account.service';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import { admin_model } from '../../../models/admin_model';
import Swal from 'sweetalert2'
import { NgxSpinnerService } from "ngx-spinner";
import * as $ from 'jquery';

@Component({
  selector: 'app-employer-list',
  templateUrl: './employer-list.component.html',
  styleUrls: ['./employer-list.component.scss']
})
export class EmployerListComponent implements OnInit {
  public adminModel: admin_model;
  public addValidationError: admin_model;
  public headers = new Headers();
  employerlist:any=[];
  employerlistPackage:any=[] ;
  packesdetails:any=[];
  getType:any;
  getId:any;
  mySubscription: any;
  mainheading : string;
  sucMsg:boolean=false;
  errMsg: boolean = false;
  message: string;
  filterString: string ='';

  searchText: string;
  page = 1;
  count : number;
  pageSize:number = 10;

  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService,private spinner:NgxSpinnerService) { 
        this.router.routeReuseStrategy.shouldReuseRoute = function () {
          return false;
        };
        this.mySubscription = this.router.events.subscribe((event) => {
          if (event instanceof NavigationEnd) {
            // Trick the Router into believing it's last link wasn't previously loaded
            this.router.navigated = false;
          }
         });
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
    this.getType = params.get('type');
    this.getId = params.get('id');
    });
    if(this.getType=="all"){
      this.mainheading="All Employers";
    }else if(this.getType=="new"){
      this.mainheading="New Employers";
    }else if(this.getType=="approved"){
      this.mainheading="Approved Employers";
    }else if(this.getType=="rejected"){
      this.mainheading="Rejected Employers";
    }

    this.adminModel = <admin_model>{
      id: '',
      filterString: ''
    }
    this.addValidationError = <admin_model>{
      id: '',
      admin_password: ''
    }
    this.getallemployers();
    this.getPackagedetails();
 
  }
//-------------pagination------------
  onTableDataChange(event){
    this.page = event;
    this.getallemployers();
    this.getPackagedetails();
  } 

//----------sorting-------------
key: string = '';
  reverse: boolean = false;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }
  // onFilterChange() {
  //   //alert("111");
  //   this.employerlist = this.employerlist.filter((invoice) => this.isMatch(invoice));
  // }

  // isMatch(item) {
  //   if (item instanceof Object) {
  //     return Object.keys(item).some((k) => this.isMatch(item[k]));
  //   } else {
  //     return item.toString().indexOf(this.filterString) > -1
  //   }
  // }
    //--------------- Get All Packages Details API------------
  getPackagedetails(){
    this.accountService.getPlans().then((response) => {
      if (response.code == 200) {
        this.packesdetails=response.result;
      } else {
        //this.errMsg = true;
        //this.message = response.message;
      }
    })
  }
 

  //--------------- Get All employer Details API by its type------------
getallemployers(){
  this.employerlist=[];
  this.adminModel.typeStatus =  this.getType;
  this.spinner.show();
  this.accountService.Employerget(this.adminModel).then((response) => {
    console.log('enter here with',response);
    if (response.code == 200) {
      this.spinner.hide();
      this.employerlist=response.result;
      this.count= response.result.length;
    } else {
      this.employerlist=[];
      this.count= 0;
      this.errMsg = true;
      this.message = response.message;
    }
  })
}

 // ----------- Change Status API -----
 changeStatus(id,status){
  this.adminModel.id=id;
  this.adminModel.status=status;
  Swal.fire({
    title: 'Are you sure you want to change status?',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, change the status!',
    cancelButtonText: 'No, keep it',   
    confirmButtonColor:'#D0021B',
    cancelButtonColor: '#D0021B',
  
  }).then((result) => {
    if (result.isConfirmed) {
  this.accountService.CandidateStatus(this.adminModel).then((response) => {
    if (response.code == 200) {
      Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B' , icon: 'success', title: 'Status changed successfully' })
        this.getallemployers();
    } else {
      //this.errMsg = true;
      //this.message = response.message;
    }
  })
} else if (result.isDismissed) {
  Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B', icon: 'success', title: 'Your data is same as before!' })
}
})
}

// ----------- delete employer -----
deleteEmployer(id){
  this.adminModel.id=id;
  Swal.fire({
    title: 'Are you sure you want to delete it?',
    text: 'You will not be able to recover this data!',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, move it to trash!',
    cancelButtonText: 'No, keep it',
    confirmButtonColor:'#D0021B',
    cancelButtonColor: '#D0021B',
  }).then((result) => {
    if (result.isConfirmed) {
      this.accountService.Statustrash(this.adminModel).then((response) => {
        if (response.code == 200) {
          //this.sucMsg = true;
          Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B' , icon: 'success', title: 'Saved successfully in trash' })
          this.getallemployers();
        } else {
          //this.errMsg = true;
          //this.errmessage = 'Something went wrong';
        }
      })
    } else if (result.isDismissed) {
      Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B' , icon: 'success', title: 'Your data is safe!' })
    }
  })
}

 //------------- view employer details  --------------------
 viewEmployer(id){
  this.router.navigate(['/admin/employer_detail/'+id]);
}

//------------- show package model according to particular employer  --------------------
showPackage(id){  
  this.adminModel.id=id;
   //this.spinner.show();
  this.accountService.getEmployerPackagedetailsWithID(this.adminModel).then((response) => {
    if (response.code == 200) {
      this.spinner.hide();
      this.employerlistPackage=response.result[0];
    } else {
      this.employerlistPackage =[];
      //this.errMsg = true;
      //this.message = response.message;
      }
  })
  
}
 //---------------reject candidate ----------
 rejectEmployer(id,status){
  this.adminModel.id=id;
  this.adminModel.status=status;
  Swal.fire({
    title: 'Are you sure you want to reject employer?',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, move it to rejected!',
    cancelButtonText: 'No, keep it',   
    confirmButtonColor:'#D0021B',
    cancelButtonColor: '#D0021B',
  
  }).then((result) => {
    if (result.isConfirmed) {
        //this.accountService.CandidateStatus(this.adminModel).then((response) => {
            //if (response.code == 200) {
                this.accountService.Statustrash(this.adminModel).then((response) => {
                    if (response.code == 200) {
                      //this.sucMsg = true;
                      Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B' , icon: 'success', title: 'Rejected! Successfully saved in rejected' })
                      //this.getEmployerdetailsWithID();
                    } else {
                      //this.errMsg = true;
                      //this.errmessage = 'Something went wrong';
                    }
                })
      } else if (result.isDismissed) {
              Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B', icon: 'success', title: 'Your data is same as before!' })
        }
 })
}
// send mail ---------------
sendMail(id){
this.spinner.show();
this.adminModel.id=id;
this.accountService.planSendMail(this.adminModel).then((response) => {
  //debugger;
  if (response.code == 200) {
    this.spinner.hide();
    this.message = response.mailmessage;   
    this.sucMsg = true;                 
    setTimeout (() => { this.sucMsg = false;  }, 2000); 
  } else {
    this.message = response.mailmessage;   
    this.errMsg = true;                 
    setTimeout (() => { this.errMsg = false;  }, 2000); 
  }
})
}
//------------- activate/update new plan by emp id API  --------------------
activatePackage(id,plan_name,plan_amount,plan_description,access_fee,success_fee,plan_time){  
  //this.spinner.show();
  this.adminModel.id=id;
  this.adminModel.plan_name=plan_name;
  this.adminModel.plan_amount=plan_amount;
  this.adminModel.plan_description=plan_description;
  this.adminModel.fee=access_fee;
  this.adminModel.success_fee=success_fee;
  this.adminModel.plan_time=plan_time;
  Swal.fire({
    title: 'Are you sure you want to change package?',
    //text: 'You are not able to edit data here',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor:'#D0021B',
    confirmButtonText: 'Yes,change package',
    cancelButtonColor: '#D0021B',
    cancelButtonText: 'No, keep it',
    
  }).then((result) => {
    if (result.isConfirmed) {
  this.accountService.activatePlan(this.adminModel).then((response) => {
    if (response.code == 200) {
      Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B' , icon: 'success', title: 'Package changed successfully' })
        //this.spinner.hide();
        //Swal.fire('Package updated successfully!');
        this.router.navigate(['/admin/employers/all']);
    } else {
      //this.errmsg=true;
      //this.message="Something Went Wrong";
    }
  })
} else if (result.isDismissed) {
  Swal.fire({ showCancelButton: false,confirmButtonColor: '#D0021B', icon: 'success', title: 'Package not changed!' })
}
})
}
ngAfterViewInit() {
  $('.swal2-confirm').click(function (){
    $('.modal-backdrop').addClass('d-none');
  });
}
}
