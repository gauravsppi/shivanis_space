import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from '../../../services/account.service';
import { CookieService } from 'ngx-cookie-service';
import { Headers, Http } from '@angular/http';
import { employer_model } from '../../../models/employer_model';
@Component({
  selector: 'app-choose-plan',
  templateUrl: './choose-plan.component.html',
  styleUrls: ['./choose-plan.component.scss']
})
export class ChoosePlanComponent implements OnInit {
  public employerModel: employer_model;
  public addValidationError: employer_model;
  packesdetails:any=[];
  constructor(private accountService: AccountService, private route: ActivatedRoute, private router: Router, private http: Http, private cookieService: CookieService) { }

  ngOnInit(): void {
    this.employerModel = <employer_model>{
      packagename: ''
    }
    this.addValidationError = <employer_model>{
      packagename: ''
    }

    this.getPackagedetails();
  }

//--------------- Get All Packages Details API------------
  getPackagedetails(){
    //this.spinner.show();
    //debugger;
    this.accountService.getPlans().then((response) => {
      //debugger;
      console.log('package details>>>',response);
      if (response.code == 200) {
        this.packesdetails=response.result;
      } else {
        //this.errMsg = true;
        //this.message = response.message;
      }
    })
  }

  

  getPlanname($event){
    this.employerModel.packagename=$event.target.id;
    console.log(this.employerModel);
    debugger;
    this.accountService.PlanAdd(this.employerModel).then((response) => {
      debugger;
      if (response.code == 200) {
        this.router.navigate(['/verify_email']);
      }else{
        //this.errMsg = true;
        //this.message = response.message;
      }
    })
  }

}
