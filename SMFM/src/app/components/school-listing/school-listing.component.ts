import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Headers, Http } from '@angular/http';


import { environment } from '../../../environments/environment';
import { AccountService } from '../../services/account.service';

import { school_model } from '../../models/school_model';
import { Login_model } from '../../models/login_model';

@Component({
  selector: 'app-school-listing',
  templateUrl: './school-listing.component.html',
  styleUrls: ['./school-listing.component.scss']
})
export class SchoolListingComponent implements OnInit {
  public schoolModel: school_model;
  public loginModel: Login_model;
  schools: [];  
  total : number= 0;
  totalPages : number= 0;
  currentPage:number=1;
  page : number =1;
  key: string = 'name'; //set default
  reverse: boolean = false;
  noRecords: boolean = false;
  loaderShow: boolean = false;


  constructor(private router: Router, private accountService: AccountService, private route: ActivatedRoute, private http: Http) { }

  ngOnInit(): void {
    this.schoolModel = <school_model>{
      name: '',
      identifier: '',
      phoneNumber: '',
      schoolNumber: '',
      admin_id: '',
      school_id: '',
      searchKey: '',
      sortKey: 'name',
      sortBy: 'asc'  
    }
    this.getSchools();
  }
  
  getSchools() {
    this.loaderShow = true;
    this.schoolModel.admin_id = localStorage.getItem('admin_id');
    this.schoolModel.school_id = localStorage.getItem('school_id');
    this.schoolModel.page = this.currentPage;
    this.schoolModel.searchKey = '';
    this.accountService.getSchools(this.schoolModel).then((response) => {
        if (response.code == 200 && response.status == 'success') {
          this.total = response.count;
          this.totalPages=Math.ceil( response.count/10);
          this.schools = response.result;  
          this.noRecords = false;                  
        }else{
          this.noRecords = true;
          this.total = 0
          this.totalPages=0
          this.schools = []; 
        }
        this.loaderShow = false;
    })
  }
  getSchoolId(school_id){
    this.schoolModel.school_id = school_id;
    this.accountService.getSchoolsInfo(this.schoolModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
         this.schoolModel.name = response.result[0].name;                    
         this.schoolModel.schoolNumber = response.result[0].metadataSchoolNumber;                    
         this.schoolModel.identifier = response.result[0].identifier;                    
         this.schoolModel.school_sourcedId = response.result[0].sourcedId;                    
      }
    })
  }
  pageChange(currentPage){
    this.currentPage=currentPage;
    if(this.schoolModel.searchKey==""){
      this.getSchools();
    }else{
      this.searchFunction(this.currentPage);
    } 
  }
  counter(i: number) {
    return new Array(i);
  }
  searchFunction(currentPage){
    if(this.schoolModel.searchKey==""){
      this.currentPage=1;
      this.getSchools();
    }else{
      this.loaderShow = true;
      this.currentPage=currentPage;
      this.accountService.getSchools(this.schoolModel).then((response) => {
        if (response.code == 200 && response.status == 'success') {
          this.total = response.count;
          this.totalPages=Math.ceil( response.count/10);
          this.schools = response.result;  
          this.noRecords = false;                  
        }else{
          this.noRecords = true;
          this.total = 0
          this.totalPages=0
          this.schools = []; 
        }
        this.loaderShow = false;
      })
    }
  }
  sortFunction(sortKey){
    this.key = sortKey;
    this.schoolModel.sortKey = sortKey;
    if(this.reverse === true){
      this.schoolModel.sortBy = 'asc';
    }else{
      this.schoolModel.sortBy = 'desc';
    }
    this.reverse = !this.reverse;   
    if(this.schoolModel.searchKey== ""){
      this.currentPage=1;
      this.getSchools();
    }else{
      this.searchFunction(this.currentPage);
    }
  }

}
