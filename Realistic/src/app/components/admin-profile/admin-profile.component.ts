import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { AccountService } from '../../services/account.service';
import { Headers, Http } from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import { Login_model } from '../../models/login_model';
import { Validation_model } from '../../models/Validation_model';
@Component({
  selector: 'app-admin-profile',
  templateUrl: './admin-profile.component.html',
  styleUrls: ['./admin-profile.component.scss']
})
export class AdminProfileComponent implements OnInit {
  public loginModel:Login_model;
  public addValidationError:Validation_model;
  private headers = new Headers();
  record:any=[];
  total : number= 0;
  loaderShow: boolean = false;
  constructor(private accountService:AccountService,private route: ActivatedRoute,private router: Router, private http: HttpClientModule) { }
  sucMsg:boolean=false;
  errMsg:string='';
  getId: string;
  ngOnInit(): void {
    this.loginModel=<Login_model>{
      c_password: '',
      password:'',
    }
    // this.activatedRoute.params.subscribe(params => {
    //   this.getId = params['token'];
    //  //  this.getUser()
    // });
    this.addValidationError = <Login_model>{
      c_password: '',
      password:'',
    }
   this.isLoggedIn();
    this.get_admin_detail();
  }
  
  isLoggedIn() {
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    if(this.loginModel.admin_id == null){
    this.router.navigate(['/']);
    }
    } 
    

    get_admin_detail(){
      this.loaderShow= true;
      this.accountService.get_admin_details().then((response)=>{
        debugger;
        this.loaderShow = false;
        if(response.code==200){
         this.loginModel.name = response.result.name;
         this.loginModel.email = response.result.email;
         this.loginModel.phone = response.result.phone; 
         this.loginModel.image = response.result.image;
        }else{
         alert('No Record FOund');
        }
      })
    }

  update_password(){
    var valid=true;
    this.addValidationError.password='';
    this.addValidationError.c_password='';
    if(this.loginModel.password==""){
      this.addValidationError.password='Please Enter Password';
      valid=false; 
    }
    if(this.loginModel.c_password==""){
      this.addValidationError.c_password='Please Enter Conform Password';
      valid=false; 
    }
    if(this.loginModel.password==this.loginModel.c_password){
      
    }else{
      this.addValidationError.c_password='Passwords do not match';
      valid=false; 
    }
    if(valid){
    this.loginModel.admin_id=localStorage.getItem('admin_id');
      this.accountService.update_admin_password(this.loginModel).then((response)=>{
        // debugger;
        // console.log(response);
        if(response.code==200){
         // this.router.navigate(['/admin-profile']);
          this.addValidationError.success='Congrats, Your Password has been Updated Successfully';
          this.sucMsg = true;
          setTimeout (() => { this.router.navigate(['/admin-profile']);  }, 3000);
          window.location.reload();
        }else{
          this.errMsg = 'Something Went Wrong !';
          this.router.navigate(['/admin-profile']);
        }
      })
    }

    }

}

