import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingPostDetailsComponent } from './pending-post-details.component';

describe('PendingPostDetailsComponent', () => {
  let component: PendingPostDetailsComponent;
  let fixture: ComponentFixture<PendingPostDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingPostDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingPostDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
