import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SpinnerComponent } from './components/spinner/spinner.component';
@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		RouterModule,
	],
	declarations: [
		SpinnerComponent,
	],
	exports: [
		CommonModule,
		FormsModule,
		RouterModule,
		SpinnerComponent,
	]
})
export class SharedModule { }