import { Injectable, Input } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { environment } from '../../environments/environment';
import { Login_model } from '../models/login_model';
import { school_model } from '../models/school_model';
import { teacher_model } from '../models/teacher_model'; 
import { student_model } from '../models/student_model';
import { class_model } from '../models/class_model';
import { IEP_model } from '../models/IEP_model';
@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private adminLoginUrl =environment.apiBaseUrl + environment.apiBasePath + "adminLoginWeb";
  private adminProfileUrl =environment.apiBaseUrl + environment.apiBasePath + "adminProfile";
  private updateProfileImageUrl =environment.apiBaseUrl + environment.apiBasePath + "updateProfileImage";
  private updateProfileUrl =environment.apiBaseUrl + environment.apiBasePath + "updateProfile";
  private getSchoolUrl =environment.apiBaseUrl + environment.apiBasePath + "getSchools";
  private getTeachersBySchoolIDUrl =environment.apiBaseUrl + environment.apiBasePath + "getTeachersBySchoolID";
  private getTeachersUrl =environment.apiBaseUrl + environment.apiBasePath + "getTeachers";
  private ggetSchoolsInfoUrl =environment.apiBaseUrl + environment.apiBasePath + "getSchoolsInfo";
  private getTeacherInfoUrl =environment.apiBaseUrl + environment.apiBasePath + "getTeacherInfo";
  private getStudentInfoUrl =environment.apiBaseUrl + environment.apiBasePath + "getStudentInfo";
  private getStudentsUrl =environment.apiBaseUrl + environment.apiBasePath + "getStudents";
  private getStudentsByTeacherIDUrl =environment.apiBaseUrl + environment.apiBasePath + "getStudentsByTeacherID";
  private getWordsUrl =environment.apiBaseUrl + environment.apiBasePath + "getAllChatAbuseWords";
  private addAbuseWordUrl =environment.apiBaseUrl + environment.apiBasePath + "addAbuseWord";
  private deleteAbuseWordUrl =environment.apiBaseUrl + environment.apiBasePath + "deleteAbuseWord";
  private getSELCategoryUrl =environment.apiBaseUrl + environment.apiBasePath + "getSELCategory";
  private updateSELCategoryUrl =environment.apiBaseUrl + environment.apiBasePath + "updateSELCategory";
  private addSELCategoryUrl =environment.apiBaseUrl + environment.apiBasePath + "addSELCategory";
  private deleteSELCategoryUrl =environment.apiBaseUrl + environment.apiBasePath + "deleteSELCategory";

  private getSELCategoryLessonsUrl =environment.apiBaseUrl + environment.apiBasePath + "getSELCategoryLessons";
  private updateSELCategoryLessonsUrl =environment.apiBaseUrl + environment.apiBasePath + "updateSELCategoryLessons";
  private addSELCategoryLessonsUrl =environment.apiBaseUrl + environment.apiBasePath + "addSELCategoryLessons";
  private deleteSELCategoryLessonsUrl =environment.apiBaseUrl + environment.apiBasePath + "deleteSELCategoryLessons";
  private viewSELCategoryLessonsUrl =environment.apiBaseUrl + environment.apiBasePath + "viewSELCategoryLessons";
  private getSELCategoryLessonsNameUrl =environment.apiBaseUrl + environment.apiBasePath + "getSELCategoryLessonsName";


  private getSELrecommendationsUrl =environment.apiBaseUrl + environment.apiBasePath + "getSELrecommendations";
  private deleteSELrecommendationsUrl =environment.apiBaseUrl + environment.apiBasePath + "deleteSELrecommendations";
  private viewSELrecommendationsUrl =environment.apiBaseUrl + environment.apiBasePath + "viewSELrecommendations";
  private getTeacherClassesByAPIUrl =environment.apiBaseUrl + environment.apiBasePath + "getTeacherClassesByTeacherID";
  private getClassInfoUrl =environment.apiBaseUrl + environment.apiBasePath + "getClassInfo";
  private getStudentInfoByIDUrl =environment.apiBaseUrl + environment.apiBasePath + "getStudentInfoByID";
  private getStudentsByClassIDUrl =environment.apiBaseUrl + environment.apiBasePath + "getStudentsByClassID";
  private getTeachersByClassIDUrl =environment.apiBaseUrl + environment.apiBasePath + "getTeachersByClassID";
  private getClassByStudentIDUrl =environment.apiBaseUrl + environment.apiBasePath + "getClassesByStudentID";

  private totalTeachersUrl =environment.apiBaseUrl + environment.apiBasePath + "totalTeachers";
  private totalStudentsUrl =environment.apiBaseUrl + environment.apiBasePath + "totalStudents";
  private totalSchoolsUrl =environment.apiBaseUrl + environment.apiBasePath + "totalSchools";
  private totalClassesUrl =environment.apiBaseUrl + environment.apiBasePath + "totalClasses";
  private totalCoursesUrl =environment.apiBaseUrl + environment.apiBasePath + "totalCourses";
  private totalStaffUrl =environment.apiBaseUrl + environment.apiBasePath + "totalStaff";

  private getIEPStudentsUrl =environment.apiBaseUrl + environment.apiBasePath + "getStudentsIEP";
  private saveIEPStudentsUrl =environment.apiBaseUrl + environment.apiBasePath + "saveStudentIEP";
  private deleteIEPUrl =environment.apiBaseUrl + environment.apiBasePath + "deleteStudentIEP";
  private deleteAllIEPUrl =environment.apiBaseUrl + environment.apiBasePath + "deleteAllStudentIEP";

  private getStudentParentsUrl =environment.apiBaseUrl + environment.apiBasePath + "getStudentParents";
  private getIEPByStudentIDUrl =environment.apiBaseUrl + environment.apiBasePath + "getIEPByStudentID";
  private getObservationsByStudentIDUrl =environment.apiBaseUrl + environment.apiBasePath + "getObservationsByStudentID";
  private getObservationNotesByIDUrl =environment.apiBaseUrl + environment.apiBasePath + "getObservationNotesByID";
  private getStudentFeelingUrl =environment.apiBaseUrl + environment.apiBasePath + "getStudentFeeling";
  private getAcademicBehaviourByStudentIDUrl =environment.apiBaseUrl + environment.apiBasePath + "getAcademicBehaviourByStudentID";
  private getStudentAdditionalInfoUrl =environment.apiBaseUrl + environment.apiBasePath + "getStudentAdditionalInfo";
  private getStudentGoalsByStudentIDUrl =environment.apiBaseUrl + environment.apiBasePath + "getStudentGoalsByStudentID";
  private getStudentAttendenceByMonthUrl =environment.apiBaseUrl + environment.apiBasePath + "getStudentAttendenceByMonth";
  private getGG4lAuthTokenUrl =environment.apiBaseUrl + environment.apiBasePath + "getGG4LAuthToken";

  private getCoursesUrl =environment.apiBaseUrl + environment.apiBasePath + "getCourses";
  private getClassesUrl =environment.apiBaseUrl + environment.apiBasePath + "getClasses";
  private getStaffUrl =environment.apiBaseUrl + environment.apiBasePath + "getStaff";

  private assignStaffToStudentUrl =environment.apiBaseUrl + environment.apiBasePath + "assignStaffToStudent";
  private getAssignStaffToStudentUrl =environment.apiBaseUrl + environment.apiBasePath + "getAssignStaffToStudent";

  private saveAnnouncementUrl =environment.apiBaseUrl + environment.apiBasePath + "saveAnnouncement";
  private updateAnnouncementUrl =environment.apiBaseUrl + environment.apiBasePath + "updateAnnouncement";
  private deleteAnnouncementUrl =environment.apiBaseUrl + environment.apiBasePath + "deleteAnnouncement";
  private getSchoolAnnouncementsUrl =environment.apiBaseUrl + environment.apiBasePath + "getSchoolAnnouncementsForAdmin";
  private getAnnouncementsUrl =environment.apiBaseUrl + environment.apiBasePath + "getAnnouncements";

  private blockTeacherByIDUrl =environment.apiBaseUrl + environment.apiBasePath + "blockTeacherByID";
  private blockStudentByIDUrl =environment.apiBaseUrl + environment.apiBasePath + "blockStudentByID";
  private blockStaffByIDUrl =environment.apiBaseUrl + environment.apiBasePath + "blockStaffByID";

  private getAllContentUrl =environment.apiBaseUrl + environment.apiBasePath + "getAllcontent";
  private updateContentUrl =environment.apiBaseUrl + environment.apiBasePath + "updateContent";

  private getOBCategoryUrl =environment.apiBaseUrl + environment.apiBasePath + "getOBCategory";
  private saveOBCategoryUrl =environment.apiBaseUrl + environment.apiBasePath + "addOBCategory";
  private deleteOBCategoryUrl =environment.apiBaseUrl + environment.apiBasePath + "deleteOBCategory";
  private changeStatusOBCategoryUrl =environment.apiBaseUrl + environment.apiBasePath + "changeStatusOBCategory";

  private getOBSubCategoryUrl =environment.apiBaseUrl + environment.apiBasePath + "getOBSubCategory";
  private saveOBSubCategoryUrl =environment.apiBaseUrl + environment.apiBasePath + "addOBSubCategory";
  private deleteOBSubCategoryUrl =environment.apiBaseUrl + environment.apiBasePath + "deleteOBSubCategory";
  private changeStatusOBSubCategoryUrl =environment.apiBaseUrl + environment.apiBasePath + "changeStatusOBSubCategory";

  private getOBSubSubCategoryUrl =environment.apiBaseUrl + environment.apiBasePath + "getOBSubSubCategory";
  private saveOBSubSubCategoryUrl =environment.apiBaseUrl + environment.apiBasePath + "addOBSubSubCategory";
  private deleteOBSubSubCategoryUrl =environment.apiBaseUrl + environment.apiBasePath + "deleteOBSubSubCategory";
  private changeStatusOBSubSubCategoryUrl =environment.apiBaseUrl + environment.apiBasePath + "changeStatusOBSubSubCategory";
  
  private getObservationCategoryNameUrl =environment.apiBaseUrl + environment.apiBasePath + "getObservationCategoryName";
  
  private getInterventionUrl =environment.apiBaseUrl + environment.apiBasePath + "getIntervention";
  private addInterventionUrl =environment.apiBaseUrl + environment.apiBasePath + "addIntervention";
  private deleteInterventionUrl =environment.apiBaseUrl + environment.apiBasePath + "deleteIntervention";
  private changeStatusInterventionUrl =environment.apiBaseUrl + environment.apiBasePath + "changeStatusIntervention";

  private addObservations =environment.apiBaseUrl + environment.apiBasePath + "addnewOberservation";
  private getObservations =environment.apiBaseUrl + environment.apiBasePath + "getObservationData";
  private deleteObservations =environment.apiBaseUrl + environment.apiBasePath + "deleteOneObservation";
  private changeStatusObservations =environment.apiBaseUrl + environment.apiBasePath + "changeObservationStatus";

  private getAllContactInfoUrl =environment.apiBaseUrl + environment.apiBasePath + "getAllContactInfo";
  private viewContactInfoUrl =environment.apiBaseUrl + environment.apiBasePath + "viewContactInfo";
  private deleteContactInfoUrl =environment.apiBaseUrl + environment.apiBasePath + "deleteContactInfo";
  
  private getLinkInterventionsList =environment.apiBaseUrl + environment.apiBasePath + "getInterventionsListWithChecked";
  private saveLinkInterventions =environment.apiBaseUrl + environment.apiBasePath + "saveLinkInterventions";

  private getInterventionList =environment.apiBaseUrl + environment.apiBasePath + "getInterventionsListWithChecked";
  private changePosition =environment.apiBaseUrl + environment.apiBasePath + "positionChange";
  private getInterventionPosition =environment.apiBaseUrl + environment.apiBasePath + "getInterventionsListWithPosition";

  constructor(private baseHttpService: BaseHttpService) { }

  adminLogin(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.adminLoginUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  }
  adminProfile(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.adminProfileUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  }
  adminProfileImageUpdate(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.updateProfileImageUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  }
  adminProfileUpdate(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.updateProfileUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  }
  getSchools(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.getSchoolUrl, schoolModel)
    .then(function(response){
        return response.json();
    });
  } 
  getSchoolsInfo(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.ggetSchoolsInfoUrl, schoolModel)
    .then(function(response){
        return response.json();
    });
  }
  getTeachersBySchoolID(teacherModel:teacher_model): Promise<any>{
    return this.baseHttpService.Post(this.getTeachersBySchoolIDUrl, teacherModel)
    .then(function(response){
        return response.json();
    });
  }
  getTeachersList(teacherModel:teacher_model): Promise<any>{
    return this.baseHttpService.Post(this.getTeachersUrl, teacherModel)
    .then(function(response){
        return response.json();
    });
  }
  getTeacherInfo(teacherModel:teacher_model): Promise<any>{
    return this.baseHttpService.Post(this.getTeacherInfoUrl, teacherModel)
    .then(function(response){
        return response.json();
    });
  }
  getStudents(studentModel:student_model): Promise<any>{
    return this.baseHttpService.Post(this.getStudentsUrl, studentModel)
    .then(function(response){
        return response.json();
    });
  }
  getStudentInfo(studentModel:student_model): Promise<any>{
    return this.baseHttpService.Post(this.getStudentInfoUrl, studentModel)
    .then(function(response){
        return response.json();
    });
  }
  getWords(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.getWordsUrl, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  getStaffMembers(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.getStaffUrl, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  assignStaffToStudent(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.assignStaffToStudentUrl, schoolModel  )
    .then(function(response){
        return response.json();
    }); 
  }
  getAssignStaffToStudent(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.getAssignStaffToStudentUrl, schoolModel  )
    .then(function(response){
        return response.json();
    }); 
  }
  blockStaff(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.blockStaffByIDUrl, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  addWord(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.addAbuseWordUrl, schoolModel)
    .then(function(response){
        return response.json();
    });
  } 
  deleteWord(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.deleteAbuseWordUrl, schoolModel)
    .then(function(response){
        return response.json();
    });
  }
  getSELCategory(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.getSELCategoryUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  }

  addSELCategory(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.addSELCategoryUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  }  

  updateSELCategory(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.updateSELCategoryUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  }   

  deleteSELCategory(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.deleteSELCategoryUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  } 
  getSELCategoryLessonsName(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.getSELCategoryLessonsNameUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  } 
  getSELCategoryLessons(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.getSELCategoryLessonsUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  }
  addSELCategoryLessons(formdata): Promise<any>{
    return this.baseHttpService.FormPost(this.addSELCategoryLessonsUrl, formdata)
    .then(function(response){
        return response.json();
    });
  } 
  updateSELCategoryLessons(formdata): Promise<any>{
    return this.baseHttpService.FormPost(this.updateSELCategoryLessonsUrl, formdata)
    .then(function(response){
        return response.json();
    });
  } 
  deleteSELCategoryLessons(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.deleteSELCategoryLessonsUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  }  

  viewSELCategoryLessons(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.viewSELCategoryLessonsUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  }  

  getSELrecommendations(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.getSELrecommendationsUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  }
  deleteSELrecommendations(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.deleteSELrecommendationsUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  }  
  viewSELrecommendations(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.viewSELrecommendationsUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  } 
  getTeacherClasses(teacherModel:teacher_model): Promise<any>{
    return this.baseHttpService.Post(this.getTeacherClassesByAPIUrl, teacherModel)
    .then(function(response){
        return response.json();
    });
  } 
  blockTeacherByID(teacherModel:teacher_model): Promise<any>{
    return this.baseHttpService.Post(this.blockTeacherByIDUrl, teacherModel)
    .then(function(response){
        return response.json();
    });
  } 
  getClassInfo(classModel:class_model): Promise<any>{
    return this.baseHttpService.Post(this.getClassInfoUrl, classModel)
    .then(function(response){
        return response.json();
    });
  } 
  getStudentsByClassID(classModel:class_model): Promise<any>{
    return this.baseHttpService.Post(this.getStudentsByClassIDUrl, classModel)
    .then(function(response){
        return response.json();
    });
  } 
  getTeachersByClassID(classModel:class_model): Promise<any>{
    return this.baseHttpService.Post(this.getTeachersByClassIDUrl, classModel)
    .then(function(response){
        return response.json();
    });
  } 
  getStudentInfoByID(studentModel:student_model): Promise<any>{
    return this.baseHttpService.Post(this.getStudentInfoByIDUrl, studentModel)
    .then(function(response){
        return response.json();
    });
  } 
  blockStudentByID(studentModel:student_model): Promise<any>{
    return this.baseHttpService.Post(this.blockStudentByIDUrl, studentModel)
    .then(function(response){
        return response.json();
    });
  } s
  getClassesByStudentID(studentModel:student_model): Promise<any>{
    return this.baseHttpService.Post(this.getClassByStudentIDUrl, studentModel)
    .then(function(response){
        return response.json();
    });
  } 
  totalTeachers(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.totalTeachersUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  } 
  totalStudents(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.totalStudentsUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  } 
  totalSchools(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.totalSchoolsUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  }  
  totalClasses(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.totalClassesUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  } 
  totalCourses(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.totalCoursesUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  } 
  totalStaff(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.totalStaffUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  } 

  getAllContent(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.getAllContentUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  } 

  updateContent(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.updateContentUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  } 

  getIEPStudents(IEPModel:IEP_model): Promise<any>{
    return this.baseHttpService.Post(this.getIEPStudentsUrl, IEPModel)
    .then(function(response){
        return response.json();
    });
  } 
  saveIEPStudents(IEPModel:IEP_model): Promise<any>{
    return this.baseHttpService.Post(this.saveIEPStudentsUrl, IEPModel)
    .then(function(response){
        return response.json();
    });
  } 
  deleteIEP(IEPModel:IEP_model): Promise<any>{
    return this.baseHttpService.Post(this.deleteIEPUrl, IEPModel)
    .then(function(response){
        return response.json();
    });
  } 
  deleteAllIEP(IEPModel:IEP_model): Promise<any>{
    return this.baseHttpService.Post(this.deleteAllIEPUrl, IEPModel)
    .then(function(response){
        return response.json();
    });
  } 
  getStudentParent(studentModel:student_model): Promise<any>{
    return this.baseHttpService.Post(this.getStudentParentsUrl, studentModel)
    .then(function(response){
        return response.json();
    });
  }
  getIEPByStudentID(studentModel:student_model): Promise<any>{
    return this.baseHttpService.Post(this.getIEPByStudentIDUrl, studentModel)
    .then(function(response){
        return response.json();
    });
  }  
  getObservationsByStudentID(studentModel:student_model): Promise<any>{
    return this.baseHttpService.Post(this.getObservationsByStudentIDUrl, studentModel)
    .then(function(response){
        return response.json();
    });
  }  
  getObservationNotesByID(studentModel:student_model): Promise<any>{
    return this.baseHttpService.Post(this.getObservationNotesByIDUrl, studentModel)
    .then(function(response){
        return response.json();
    });
  }  
  getAcademicBehaviourByStudentID(studentModel:student_model): Promise<any>{
    return this.baseHttpService.Post(this.getAcademicBehaviourByStudentIDUrl, studentModel)
    .then(function(response){
        return response.json();
    });
  }  

  getStudentAdditionalInfo(studentModel:student_model): Promise<any>{
    return this.baseHttpService.Post(this.getStudentAdditionalInfoUrl, studentModel)
    .then(function(response){
        return response.json();
    });
  } 
  getStudentFeeling(studentModel:student_model): Promise<any>{
    return this.baseHttpService.Post(this.getStudentFeelingUrl, studentModel)
    .then(function(response){
        return response.json();
    });
  } 
  getStudentGoalsByStudentID(studentModel:student_model): Promise<any>{
    return this.baseHttpService.Post(this.getStudentGoalsByStudentIDUrl, studentModel)
    .then(function(response){
        return response.json();
    });
  } 
  getStudentAttendenceByMonth(studentModel:student_model): Promise<any>{
    return this.baseHttpService.Post(this.getStudentAttendenceByMonthUrl, studentModel)
    .then(function(response){
        return response.json();
    });
  } 

  getGG4LToken(): Promise<any>{
    return this.baseHttpService.POST(this.getGG4lAuthTokenUrl)
    .then(function(response){
        return response.json();
    });
  } 

  getCourses(classModel:class_model): Promise<any>{
    return this.baseHttpService.Post(this.getCoursesUrl, classModel)
    .then(function(response){
        return response.json();
    });
  }

  getClasses(classModel:class_model): Promise<any>{
    return this.baseHttpService.Post(this.getClassesUrl, classModel)
    .then(function(response){
        return response.json();
    });
  }

  saveAnnouncement(schoolModel: school_model): Promise<any>{
    return this.baseHttpService.Post(this.saveAnnouncementUrl, schoolModel)
    .then(function(response){
        return response.json();
    });
  }

  updateAnnouncement(schoolModel: school_model): Promise<any>{
    return this.baseHttpService.Post(this.updateAnnouncementUrl, schoolModel)
    .then(function(response){
        return response.json();
    });
  }

  deleteAnnouncement(schoolModel: school_model): Promise<any>{
    return this.baseHttpService.Post(this.deleteAnnouncementUrl, schoolModel)
    .then(function(response){
        return response.json();
    });
  }

  getAnnouncements(schoolModel: school_model): Promise<any>{
    return this.baseHttpService.Post(this.getSchoolAnnouncementsUrl, schoolModel)
    .then(function(response){
        return response.json();
    });
  }
  getAllAnnouncements(schoolModel: school_model): Promise<any>{
    return this.baseHttpService.Post(this.getAnnouncementsUrl, schoolModel)
    .then(function(response){
        return response.json();
    });
  }
  getOBCategory(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.getOBCategoryUrl, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  saveOBCategory(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.saveOBCategoryUrl, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  deleteOBCategory(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.deleteOBCategoryUrl, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  changeStatusOBCategory(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.changeStatusOBCategoryUrl, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  getOBSubCategory(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.getOBSubCategoryUrl, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  saveOBSubCategory(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.saveOBSubCategoryUrl, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  deleteOBSubCategory(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.deleteOBSubCategoryUrl, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  changeStatusOBSubCategory(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.changeStatusOBSubCategoryUrl, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  getOBSubSubCategory(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.getOBSubSubCategoryUrl, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  saveOBSubSubCategory(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.saveOBSubSubCategoryUrl, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  deleteOBSubSubCategory(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.deleteOBSubSubCategoryUrl, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  changeStatusOBSubSubCategory(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.changeStatusOBSubSubCategoryUrl, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  getObservationCategoryName(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.getObservationCategoryNameUrl, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  getInterventionStrategy(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.getInterventionUrl, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  saveInterventionStrategy(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.addInterventionUrl, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  deleteInterventionStrategy(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.deleteInterventionUrl, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  changeStatusInterventionStrategy(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.changeStatusInterventionUrl, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  saveobservations(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.addObservations, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  getObservationLists(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.getObservations, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  deletedObservations(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.deleteObservations, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  changeObservationsStatus(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.changeStatusObservations, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  saveInterventionsLinks(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.saveLinkInterventions, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  getContactInfo(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.getAllContactInfoUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  }
  viewContactInfo(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.viewContactInfoUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  }
  deleteContactInfo(loginModel:Login_model): Promise<any>{
    return this.baseHttpService.Post(this.deleteContactInfoUrl, loginModel)
    .then(function(response){
        return response.json();
    });
  }
  getInterventionsList(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.getLinkInterventionsList, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  getInterventionCheckedList(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.getInterventionList, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  changePositions(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.changePosition, schoolModel  )
    .then(function(response){
        return response.json();
    });
  }
  getInterventionListPosition(schoolModel:school_model): Promise<any>{
    return this.baseHttpService.Post(this.getInterventionPosition, schoolModel)
    .then(function(response){
        return response.json();
    });
  }
}
