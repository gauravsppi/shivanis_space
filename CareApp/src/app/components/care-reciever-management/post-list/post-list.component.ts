import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ReceiverService } from '../../../shared/services/receiver-service/receiver.service';
import { NgxSpinnerService } from "../../../shared/services/spinner.service";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {
  postArray: any = [];
  page = 1;
  count : number;
  pageSize = 10;
  postLanguage: any = [];
  postDetail = {
    any_health_prob_text: '',
    any_allergies_text:'',
    gender:'',
    dob:'',
    customsandtraditions: {},
    hairandgroomings:{},
    healthanddetails: {},
    preferences: {},
    rateandavailabilities:{},
    religions:{},
    specialoccasionholidays: {},
  };

  constructor(
    private activatedRoute: ActivatedRoute,
    private ReceiverService: ReceiverService,
    private spinner: NgxSpinnerService,

  ) { }

  ngOnInit(): void {
    let receiverId = this.activatedRoute.snapshot.params.id;
    console.log(receiverId)

    this.spinner.show();
    this.fetchRecords(receiverId).toPromise().then((res) => {
      console.log(res)
      this.spinner.hide();
      if(res.code == 200){
        this.postArray = res.receiverAllPost;
        console.log('post arrrays>>>>',this.postArray);
        this.count = res.numOfPosts
      }else{
        Swal.fire({ icon: 'error', title: 'Oops...', text: res.message })
      }
    })
    .catch(err=> { 
      this.spinner.hide();
      console.log(err) 
    });

  }


  fetchRecords (receiverId){
    var Params = {
      receiverId:  receiverId, 
      page:  this.page, 
      pageSize: this.pageSize,
    }
    return this.ReceiverService.fetch_post_by_receiver(Params);
  }



  onTableDataChange(event){
    this.page = event;
    this.ngOnInit();
  }  

  ViewPostDetails(posts){
  this.postDetail = posts;
  this.postLanguage = posts.languageandculturalcuisines ? posts.languageandculturalcuisines.language : [];


  }

  //******** Detele post function **************
  deletePost(id, status): void {
    // this.page = 1;
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {

        this.spinner.show();
        this.ReceiverService.delete_receiver_post(id, status).subscribe((data: any) => {
          this.spinner.hide();
          console.log(data);
          if(data.status == 1){
            this.ngOnInit()
            var foundIndex = this.postArray.findIndex(x => x._id == id);
            this.postArray.splice(foundIndex , 1)
            Swal.fire( 'Deleted!', 'Post has been deleted.', 'success')
          }else{
            Swal.fire({  icon: 'error', title: 'Oops...', text: data.message })
          }
        }, error => {
          this.spinner.hide();
          console.log(error)
        })
      }
    })
  }

}
