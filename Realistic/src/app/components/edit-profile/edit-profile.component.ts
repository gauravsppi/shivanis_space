
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { AccountService } from '../../services/account.service';
import { Headers, Http } from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import { Login_model } from '../../models/login_model';
import { Validation_model } from '../../models/Validation_model';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})

export class EditProfileComponent implements OnInit {
  public loginModel:Login_model;
  public Validation_model:Validation_model;
  private headers = new Headers();
  record:any=[];
  total : number= 0;
  loaderShow: boolean = false;
  sucMsg:boolean =true;
  errMsg:boolean =true;
  errorMessage:'';
  constructor(private accountService:AccountService,private route: ActivatedRoute,private router: Router, private http: HttpClientModule) { }

  ngOnInit(): void {
    this.loginModel=<Login_model>{
     name:'',
     imgFile:'',
     imgType:'',

    }
    this.get_admin_detail();
    this.isLoggedIn();
  }
  isLoggedIn() {
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    if(this.loginModel.admin_id == null){
    this.router.navigate(['/']);
    }
    } 
  
  get_admin_detail(){
    this.loaderShow= true;
    this.accountService.get_admin_details().then((response)=>{
      this.loaderShow = false;
      if(response.code==200){
       // this.record=response.result;
       this.loginModel.name = response.result.name;
       console.log(this.loginModel.name);
       this.loginModel.email = response.result.email;
       this.loginModel.phone = response.result.phone;
       this.loginModel.image = response.result.image; 
       console.log(this.loginModel.phone);
      }else{
       alert('No Record FOund');
      }
    })
  }


   onFileChange(event) {
    
    this.loginModel.admin_id = localStorage.getItem('admin_id');
    const reader = new FileReader();
    reader.onload = (e: any) => {
    const image = new Image();
    image.src = e.target.result;
    this.loginModel.imgFile = e.target.result;
    this.loginModel.imgFile = this.loginModel.imgFile.replace('data:' + event.target.files[0].type + ';base64,', '');
    if (event.target.files[0].type == 'image/jpeg') {
    this.loginModel.imgType = 'jpg';
    }
    if (event.target.files[0].type == 'image/png') {
    this.loginModel.imgType = 'png';
    }
    //console.log(this.loginModel.imgType);
    //console.log(this.loginModel.imgFile);
    }
    reader.readAsDataURL(event.target.files[0]);
    }



    UpdateProfile(){
      
      //console.log(this.loginModel);
      var valid=true;
      if(this.loginModel.name==""){
        valid=false; 
      }
      if(this.loginModel.email==""){
        valid=false; 
      }
      if(this.loginModel.phone==""){
        valid=false; 
      }
      if(valid){
      
      this.accountService.adminProfileAllUpdate(this.loginModel).then((response) => {
        if (response.code == 200) {
        // this.ImagePath = environment.imagePath+response.imageName;
        // this.sucMsg = true;
        // this.successMessage = response.message
        setTimeout (() => { this.sucMsg = false; }, 3000);
        this.router.navigate(['/admin-profile']);
        } else {
        this.errMsg = true;
        this.errorMessage = response.message
        setTimeout (() => { this.errMsg = false; }, 3000);
        }
        })
      }
     
    }


  // update_admin_profile(){

  // }

}

