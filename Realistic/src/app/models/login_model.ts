export class Login_model {
    email:string
    password:string
    user_id:string
    admin_id:string
    phone:string
    name:string
    image:string
    imgFile:string
    imgType:string
    ImagePath:string
    c_password:string
    success: string
    company_name:string
    type:string
    id:string
    company_id:string
    searchKey:string
    model_name:string
    model_id:string
    sortKey:string
    sortBy:string
    page:number
    record:string
    title:string
    content:string
    link:string
    car_name:string
    part_image: File
    part_id: string
    part_img: string
    part_image_error: string
    Equip: any; 
    models_name:any;
    toDoList:any;
    companyNames:any;
    company_details:any;
    model_details:any;
    description:any;
}   