const mongoose = require('mongoose');
var adminSchema = new mongoose.Schema({
    name: {
        type: String,       
    },
    username: {
        type: String,       
    },
    password: {
        type: String,       
    },
    email: {
        type: String
    },   
    phone: {
        type: String
    },
    image: {
        type: String
    },
    device_id: {
        type: String
    },
    device_token: {
       type: String
    },
    device_type: {
        type: String
    },
    status: {
        type: String
    },
    created_at: {
        type: Date,
        default: Date.now
    },
});
module.exports = adminSchema;
