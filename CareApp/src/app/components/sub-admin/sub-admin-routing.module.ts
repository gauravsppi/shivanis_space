import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateSubAdminComponent } from './create-sub-admin/create-sub-admin.component';
import { ManageAdminComponent } from './manage-admin/manage-admin.component';


const routes: Routes = [
  {
    path: 'create-sub-admin',
    component: CreateSubAdminComponent
  },

  {
    path: 'manage-admin',
    component: ManageAdminComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubAdminRoutingModule { }