import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCarModelsComponent } from './add-car-models.component';

describe('AddCarModelsComponent', () => {
  let component: AddCarModelsComponent;
  let fixture: ComponentFixture<AddCarModelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCarModelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCarModelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
