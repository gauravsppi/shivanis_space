import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Headers, Http } from '@angular/http';

import { AccountService } from '../../services/account.service';

import { school_model } from '../../models/school_model';
import Swal  from 'sweetalert2';


@Component({
  selector: 'app-abuse-words',
  templateUrl: './abuse-words.component.html',
  styleUrls: ['./abuse-words.component.scss']
})


export class AbuseWordsComponent implements OnInit {
  public schoolModel: school_model;
  public addValidationError: school_model;
  words: [];  
  sucMsg: boolean =  false; 
  successMessage: string='';
  errMsg: boolean = false; 
  errorMessage: string=''; 
  noRecords: boolean = false;

  total : number= 0;
  totalPages : number= 0;
  currentPage:number=1;
  page : number =1;
  key: string = 'words'; //set default
  reverse: boolean = false; 
  loaderShow: boolean = false; 

  constructor(private router: Router, private accountService: AccountService, private route: ActivatedRoute, private http: Http) { }

  ngOnInit(): void {
    this.schoolModel = <school_model>{
      word: '',     
      word_id: '',  
      searchKey: '',
      sortKey: 'id',
      sortBy: 'desc',       
    }
    this.addValidationError = <school_model>{
      word:'', 
    }
    this.getWords();
    
  }
  getWords() {
    this.loaderShow = true;
    this.schoolModel.page= this.currentPage;
    this.schoolModel.admin_id = localStorage.getItem('admin_id');
    this.accountService.getWords(this.schoolModel).then((response) => {
      if (response.code == 200 && response.status == 'success') {
        this.words = response.result; 
        this.total = response.count;
        this.totalPages=Math.ceil( response.count/10);
        this.noRecords = false;
      }else{
        this.noRecords = true;
        this.words = []; 
        this.total = 0;
        this.totalPages=0;
      }
      this.loaderShow = false;
    })
  }

  pageChange(currentPage){
    this.currentPage=currentPage;
    if(this.schoolModel.searchKey==""){
      this.getWords();
    }else{
      this.searchFunction(this.currentPage);
    } 
  }

  counter(i: number) {
    return new Array(i);
  }

  searchFunction(currentPage){
    if(this.schoolModel.searchKey== ""){
      this.currentPage=1;
      this.getWords();
    }else{
      this.loaderShow = true;
      this.currentPage=currentPage;
      this.schoolModel.page = this.currentPage;
      this.schoolModel.school_id = this.route.snapshot.params.id;
      this.accountService.getWords(this.schoolModel).then((response) => {
        if (response.code == 200 && response.status == 'success') {
          this.total = response.count;
          this.totalPages=Math.ceil(response.count/10);
          this.words = response.result;  
          this.noRecords = false;                  
        }else{
          this.noRecords = true;
          this.words = []; 
          this.total = 0;
          this.totalPages=0;
        }
        this.loaderShow = false;
      })
    }
  }

  sortFunction(sortKey){
    this.key = sortKey;
    this.schoolModel.sortKey = sortKey;
    if(this.reverse === true){
      this.schoolModel.sortBy = 'asc';
    }else{
      this.schoolModel.sortBy = 'desc';
    }
    this.reverse = !this.reverse;   
    if(this.schoolModel.searchKey== ""){
      this.currentPage=1;
      this.getWords();
    }else{
      this.searchFunction(this.currentPage);
    }
  }


  onSubmit() {   
    var valid = true;
    this.schoolModel.searchKey== "";
    if (this.schoolModel.word.trim() == "") {
      this.addValidationError.word = 'Please enter word';
      valid = false;
    }else{
      this.loaderShow = true;
      this.schoolModel.word = this.schoolModel.word.trim();
      this.accountService.addWord(this.schoolModel).then((response) => {
        if (response.code == 200) {
          this.sucMsg = true;
          this.successMessage = response.message;
          setTimeout (() => { this.sucMsg = false;  }, 3000); 
          this.addValidationError.word = '';
          this.schoolModel.word = '';
          this.getWords();
        }else{
          this.errMsg = true;
          this.errorMessage = "Something went wrong";
          setTimeout (() => { this.errMsg = false;  }, 3000); 
        }
        this.loaderShow = false;
      })  
    }
  }
  deleteWord(id) {
    this.schoolModel.word_id= id;
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to delete!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
    }).then((result) => {
      this.loaderShow = true;
      if (result.isConfirmed) {
        this.accountService.deleteWord(this.schoolModel).then((response) => {
          this.loaderShow = false;
          if (response.code == 200 && response.status == 'success') {
            this.sucMsg = true;
            this.successMessage = response.message;
            setTimeout (() => { this.sucMsg = false;  }, 3000); 
            this.getWords();                
          }else{
            this.errMsg = true;
            this.errorMessage = "Something went wrong";
            setTimeout (() => { this.errMsg = false;  }, 3000); 
          }
        })
      } else if (result.isDismissed) {
        Swal.fire({  showCancelButton: false,  icon: 'success',  title: 'Your file is safe!'})
        this.loaderShow = false;
      }
    })   
  }

  
}
