import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddIepComponent } from './add-iep.component';

describe('AddIepComponent', () => {
  let component: AddIepComponent;
  let fixture: ComponentFixture<AddIepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddIepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddIepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
